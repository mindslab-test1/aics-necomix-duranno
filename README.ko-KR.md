# aics-necomix-duranno

## 개발 시작하기 (docker-compose로 환경 설정)

1. `docker`와 `docker-compose`를 설치하세요.

- Docker 설치: https://docs.docker.com/get-docker/
- docker-compose 설치: https://docs.docker.com/compose/install/

2. Firebase 프로젝트를 설정합니다.

- [Firebase Console](https://console.firebase.google.com)에 접속하고, 프로젝트를 생성합니다.

- 먼저, 웹 앱을 추가합니다. 웹 앱을 추가하면 다음과 같은 형태의 configuration을 얻을 수 있습니다.

```
{
    apiKey: 'AIzaSyBy5kcwmtXl5HM2u7euQgd7EOQt2ue2Kvk',
    authDomain: 'duranno-audiobook-dev.firebaseapp.com',
    databaseURL: 'https://duranno-audiobook-dev.firebaseio.com',
    projectId: 'duranno-audiobook-dev',
    storageBucket: 'duranno-audiobook-dev.appspot.com',
    messagingSenderId: '402830798110',
    appId: '1:402830798110:web:1c308a2df7c9013e30ad0f',
    measurementId: 'G-13X9ECEW0E',
}
```

- 이 configuration을 **`admin-app/src/utils/const.ts`에 복사하세요.**

```
...

export const getFirebaseConfig = (): { [key: string]: string } => {
  const env = getEnv('stage');
  if (!env) {
    throw Error('invalid environment variable "STAGE"');
  }

  if (env === 'development') {
    /* 여기에 복사하세요. */
    return {
      apiKey: 'AIzaSyBy5kcwmtXl5HM2u7euQgd7EOQt2ue2Kvk',
      authDomain: 'duranno-audiobook-dev.firebaseapp.com',
      databaseURL: 'https://duranno-audiobook-dev.firebaseio.com',
      projectId: 'duranno-audiobook-dev',
      storageBucket: 'duranno-audiobook-dev.appspot.com',
      messagingSenderId: '402830798110',
      appId: '1:402830798110:web:1c308a2df7c9013e30ad0f',
      measurementId: 'G-13X9ECEW0E',
    };
  }

...

```

- 프로젝트에 Authentication을 추가하고, "Sign-in method" 탭에서 "익명", "이메일/비밀번호", "Google"을 추가하세요.

- 다음으로, 해당 프로젝트에 [Admin SDK를 추가](https://firebase.google.com/docs/admin/setup)합니다.

  - 서비스 계정의 json configuration을 **`server/.credentials/firebase-admin-dev.json`에 저장하세요.** (해당 configuration은 공개된 장소에 노출하지 마세요.)

- 마지막으로, `docker-compose.yml` 파일에 방금 만든 Firebase 프로젝트의 이름을 입력하세요.

```
...

 server:
    build: ./server
    entrypoint: bash docker-entrypoint.sh
    environment:
      ...
      LOG_LEVEL: debug
      DB_USERNAME: system
      DB_PASSWORD: oracle
      ORACLEDB_CONNECTION_STRING: db:1521
      FIREBASE_PROJECT_NAME: duranno-audiobook-dev # <- 여기에 프로젝트 이름을 입력하세요.
      GOOGLE_APPLICATION_CREDENTIALS: /usr/src/app/.credentials/firebase-admin-dev.json
      ...
...
```

3. 개발에 사용할 AWS를 설정합니다.

- S3 버킷을 생성하세요.
- 버킷의 "권한" 섹션으로 들어가서, CORS 항목을 다음과 같이 설정하세요.

```
[
    {
        "AllowedHeaders": [
            "*"
        ],
        "AllowedMethods": [
            "PUT"
        ],
        "AllowedOrigins": [
            "http://localhost:7008"
        ],
        "ExposeHeaders": []
    },
    {
        "AllowedHeaders": [
            "*"
        ],
        "AllowedMethods": [
            "GET"
        ],
        "AllowedOrigins": [
            "*"
        ],
        "ExposeHeaders": []
    }
]
```

- 다음으로, AWS IAM User를 만드세요. 해당 User에게는 S3에 관한 권한이 필요합니다. 다음과 같은 policy를 부여하세요. `<bucket-arn>` 에는 방금 생성한 S3 버킷의 ARN을 넣으세요. (예시: `"arn:aws:s3:::dev.resource.neocomix-audiobook.mindslab.ai/*"`)

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:*Object",
                "s3:PutObjectAcl"
            ],
            "Resource": [
                <bucket-arn>
            ],
            "Effect": "Allow"
        }
    ]
}
```

- `docker-compose.yml` 파일과 `.env` 파일에 (`.env` 파일은 직접 만드세요.) 방금 만든 S3 버킷과 IAM user의 정보를 넣으세요.

`docker-compose.yml`

```
...

 server:
    build: ./server
    entrypoint: bash docker-entrypoint.sh
    environment:
        ...
        # mutiple users can act as admin, by separating the uids with comma
        ADMIN_UIDS: <admin-uid>
        # set resouce bucket to own buckets, or request an aws role to use these buckets
        RESOURCE_BUCKET: dev.resource.neocomix-audiobook.mindslab.ai # <- 여기에 버킷 이름을 쓰세요.
        MEDIA_BUCKET: dev.resource.neocomix-audiobook.mindslab.ai # <- 여기에 버킷 이름을 쓰세요.
        # secret environment values: write them at .env
        JWT_SECRET_KEY: ${JWT_SECRET_KEY}
        AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID}
        ...
...
```

`.env`

```
AWS_ACCESS_KEY_ID=***************
AWS_SECRET_ACCESS_KEY=*****************************
```

4. `JWT_SECRET_KEY` 환경 변수를 `.env`에 설정하세요.

- 아무 문자열이나 넣으면 되는데, 보통 `openssl`로 만듭니다.

```
$ openssl rand -base64 24
```

- 결과로 나온 문자열을 `.env`에 적으세요.

```
JWT_SECRET_KEY=<random-string>     # <- 여기에 쓰세요.
AWS_ACCESS_KEY_ID=***************
AWS_SECRET_ACCESS_KEY=*****************************
```

5. 프로젝트 루트 디렉토리에서 데이터베이스, 서버, 관리자 웹 페이지 컨테이너를 띄웁니다.

```
$ docker-compose build
$ docker-compose up -d
```

- 해당 컨테이너들은 1521, 7007, 7008번 포트를 사용합니다. 해당 포트들이 이미 열려있지 않은지 확인하세요.

6. 컨테이너들이 잘 띄워졌는지 확인합니다.

```
$ docker ps
7eea9f897c80        aics-necomix-duranno_admin-app           "/docker-entrypoint.…"   4 minutes ago       Up 4 minutes        0.0.0.0:7008->80/tcp                       aics-necomix-duranno_admin-app_1
e8b247f0999f        aics-necomix-duranno_server              "bash docker-entrypo…"   4 minutes ago       Up 4 minutes        0.0.0.0:7007->7007/tcp                     aics-necomix-duranno_server_1
b7336dc7d51e        oracleinanutshell/oracle-xe-11g:latest   "/bin/sh -c '/usr/sb…"   4 minutes ago       Up 4 minutes        22/tcp, 8080/tcp, 0.0.0.0:1521->1521/tcp   aics-necomix-duranno_db_1
```

```
# 서버 연결 테스트
$ curl localhost:7007
{"msg":"duranno-server@0.1.0-local server is alive! :)"}
```

7. 관리자 웹 페이지에 접속하세요. (http://localhost:7008)

- 구글 로그인을 하고 나면, "관리자로 등록되지 않은 계정입니다"라는 메세지가 표시됩니다. Firebase console로 돌아가세요.
- Authentication의 "User" 탭으로 이동하면, 방금 로그인한 구글 계정이 표시됩니다. 해당 계정의 id를 복사하세요.
- id를 `docker-compose.yml`에 `ADMIN_UID` 항목에 적으세요.

```
...
    server:
    build: ./server
    entrypoint: bash docker-entrypoint.sh
    environment:
        ...
        # set this as uid of firebase user you want to make admin
        # mutiple users can act as admin, by separating the uids with comma
        ADMIN_UIDS: <admin-uid> # <- 여기에 적으세요.
        ...
...
```

- `docker-compose up -d`를 다시 실행하고, 관리자 페이지에 재접속하면 통과됩니다.

- 책을 추가해 보세요.

8. [프론트엔드 프로젝트를 설정하세요.](https://pms.maum.ai/bitbucket/projects/AICS/repos/aics-necomix-duranno/browse/app)
