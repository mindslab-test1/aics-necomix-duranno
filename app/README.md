# duranno_app

AICS Neocomix Duranno Audiobook Application

- To start debug session, execute following command:

```
$ flutter run -t lib/main_dev.dart --flavor dev
```

### Configuration

In development environment, write your local server address at `API_URI` environment variable listed at `.env.local`.

For example, if your local server is running at `http://localhost:7007`, update `API_URI` variable to:

```
API_URI=http://localhost:7007
```

### Firebase Analytics DebugView Configuration

If you are working on android emulator, you should execute following command to enable registering events to DebugView in Firebase Console:

```
#!/bin/bash
adb shell setprop debug.firebase.analytics.app com.mindslab_ai.duranno_audiobook.dev
```

On iOS simulator, no further settings are required. However, you should run the app at Xcode for once (apparently, `FIRDebugEnabled` argument is applied as the scheme specifies for debug command in Visual Studio Code after we run the app at Xcode).
