# duranno_app

AICS 팀 Neocomix Duranno Audiobook 모바일 앱

- 디버그 세션을 시작하려면, 다음 명령을 실행하세요.

```
$ flutter run -t lib/main_dev.dart --flavor dev
```

### 설정

로컬에서 개발하고 있다면, 로컬 API 서버 주소를 `.env.local` 파일의 `API_URI` 환경 변수에 적으세요.

예를 들어, 로컬 서버가 `http://localhost:7007` 에서 돌아가고 있다면, `.env.local` 파일의 `API_URI` 변수를 다음과 같이 수정해야 합니다.

```
API_URI=http://localhost:7007
```

### Firebase Analytics DebugView 설정

안드로이드 에뮬레이터에서 발생하는 Firebase Analytics Event를 Firebase Analytics Console DebugView에서 보기 위해서는, 다음 명령을 실행하세요.

```
#!/bin/bash
adb shell setprop debug.firebase.analytics.app com.mindslab_ai.duranno_audiobook.dev
```

iOS에서는 별다른 설정이 필요하지 않습니다. Xcode에서 앱을 한 번 실행하고 돌아오세요. (`FIRDebugEnabled` argument가 처음에는 적용되지 않다가, Xcode에서 앱을 실행하고 돌아오니 Visual Studio Code에서도 적용됩니다.)
