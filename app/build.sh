#!/bin/bash

KEYSTORE_DIRECTORY=.keystore
FIREBASE_DIRECTORY=.firebase
STAGE=beta
IOS_STAGE=Beta
DART_ENTRY_FILE=lib/main_beta.dart
USAGE="$(basename "$0") [-h] [-s STAGE] -- build apk and ipa files for this project
options:
    -h    shows this help text
    -s    sets app stage (available values: beta, production)
"

while getopts :hs: opt
do
  case ${opt} in
    h) echo "$USAGE"
      exit
      ;;
    s) STAGE="$OPTARG" ;;
    \?) printf "illegal option: -%s\n" "$OPTARG" >&2
      echo "$USAGE" >&2
      exit 1
      ;;
  esac
done

if [ "$STAGE" != "beta" ] && [ "$STAGE" != "production" ]; then
    printf "illegal app stage: %s\n" "$STAGE" >&2
    echo "$USAGE" >&2
    exit 1
fi

if [ "$STAGE" = "beta" ]; then
  IOS_STAGE=Beta
  DART_ENTRY_FILE=lib/main_beta.dart
elif [ "$STAGE" = "production" ]; then
  IOS_STAGE=Production
  DART_ENTRY_FILE=lib/main.dart
fi

if ! flutter doctor; then
    printf "flutter doctor failed!" >&2
    exit 1
fi

echo "Cleaning workspace ..."

flutter clean

echo "Installing dependencies ..."

flutter pub get

echo "Building apk and ipa files for app stage: $STAGE"

echo "Copying keystore files ..."

if ! cp $KEYSTORE_DIRECTORY/$STAGE/key.properties android/; then
    printf "Copying keystore files failed!" >&2
    exit 1
fi

if ! cp $KEYSTORE_DIRECTORY/$STAGE/keystore.jks android/app/; then
    printf "Copying keystore files failed!" >&2
    exit 1
fi

echo "Copying firebase configuration files ..."

if ! (mkdir -p android/app/src/$STAGE && cp $FIREBASE_DIRECTORY/$STAGE/google-services.json android/app/src/$STAGE/); then
    printf "Copying firebase configuration file failed!" >&2
    exit 1
fi

if ! (mkdir -p ios/Runner/Firebase/$IOS_STAGE && cp $FIREBASE_DIRECTORY/$STAGE/GoogleService-Info.plist ios/Runner/Firebase/$IOS_STAGE); then
    printf "Copying firebase configuration file failed!" >&2
    exit 1
fi

echo "Building apk ..."

if [ "$STAGE" = "beta" ]; then
  flutter build apk --split-per-abi --flavor $STAGE --release -t $DART_ENTRY_FILE
elif [ "$STAGE" = "production" ]; then
  flutter build appbundle --flavor $STAGE --release -t $DART_ENTRY_FILE
fi

if [ "$STAGE" = "production" ]; then
  echo "Uploading built app bundle to google play store alpha track ..."

  cd android/

  fastlane alpha

  cd ..
fi

# echo "Building ios app ..."

# flutter build ios --flavor $STAGE --release
