import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/input_field.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class FindPasswordRoute extends AppRoute {
  FindPasswordRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.findPassword;

  @override
  String get routeClassName => 'FindPasswordRoute';

  @override
  String get title => '비밀번호 찾기';

  @override
  _FindPasswordRouteState createState() => _FindPasswordRouteState();
}

class _FindPasswordRouteState extends AppRouteState<FindPasswordRoute> {
  final TextEditingController _emailController = TextEditingController();
  AuthenticationProvider _authentication;
  String _emailErrorText;
  bool _isInputFieldNull;

  @override
  void initState() {
    _isInputFieldNull = true;
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    _authentication =
        Provider.of<AuthenticationProvider>(context, listen: false);

    super.didChangeDependencies();
  }

  void _checkInputFieldNull() {
    if (_emailController.text.isNotEmpty) {
      _isInputFieldNull = false;
    } else {
      _isInputFieldNull = true;
    }
    setState(() {});
  }

  @override
  Widget buildBody(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
          _checkInputFieldNull();
        }
      },
      child: LayoutBuilder(
        builder: (context, constraint) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(height: 44),
                  //Title
                  Padding(
                    padding: EdgeInsets.only(left: 32),
                    child: Text('비밀번호를 분실한\n이메일을 입력해주세요.',
                        style: Theme.of(context).textTheme.headline1),
                  ),
                  SizedBox(height: 39),
                  //InputField
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 32,
                    ),
                    child: InputField(
                      controller: _emailController,
                      errorMessage: _emailErrorText,
                      textLabel: '이메일 주소',
                      isPassword: false,
                      clearable: true,
                      onEditingComplete: _checkInputFieldNull,
                    ),
                  ),
                  Expanded(child: Container()),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: ThemeFlatButton(
                      type: FlatButtonType.Primary,
                      label: '다음으로',
                      onPressed: () async {
                        setState(() {
                          _isInputFieldNull = true; //disable button
                        });
                        if (_checkEmailFormat()) {
                          try {
                            await _authentication.requestPasswordResetEmail(
                                email: _emailController.text);
                          } catch (e) {}
                          Navigator.of(context).pushNamed(
                              RouteName.confirmResetPassword,
                              arguments: {'email': _emailController.text});
                        } else {
                          setState(() {
                            _isInputFieldNull = false;
                            _emailErrorText = '올바른 이메일 형식을 입력해주세요.';
                          });
                        }
                      },
                      enabled: !_isInputFieldNull,
                    ),
                  ),
                  SizedBox(height: 28),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  bool _checkEmailFormat() {
    return EmailValidator.validate(_emailController.text);
  }
}
