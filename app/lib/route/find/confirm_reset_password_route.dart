import 'package:duranno_app/components/common/dialog.dart';
import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ConfirmResetPasswordRoute extends AppRoute {
  ConfirmResetPasswordRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.confirmResetPassword;

  @override
  String get routeClassName => 'ConfirmResetPasswordRoute';

  @override
  String get title => '비밀번호 찾기';

  @override
  _ConfirmResetPasswordRouteState createState() =>
      _ConfirmResetPasswordRouteState();
}

class _ConfirmResetPasswordRouteState
    extends AppRouteState<ConfirmResetPasswordRoute> {
  AuthenticationProvider _authentication;
  String _email;

  @override
  void didChangeDependencies() {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;
    _email = arguments['email'];
    assert(_email != null);
    _authentication =
        Provider.of<AuthenticationProvider>(context, listen: false);

    super.didChangeDependencies();
  }

  Future<void> _resendEmail() async {
    try {
      await _authentication.requestPasswordResetEmail(email: _email);
    } catch (e) {}
    showThemeDialog(
      DialogType.OneAction,
      context: context,
      title: '메일을 다시 전송했습니다.',
      onConfirm: (context) {},
    );
  }

  @override
  Widget buildBody(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SizedBox(height: 44.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 32.0),
          child: Text(
            '메일을 확인해주세요.\n메일에 첨부된 링크로 비밀번호를 재설정하세요.',
            style: Theme.of(context).textTheme.headline1,
          ),
        ),
        Spacer(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextLabel.h6(
              '메일이 오지 않으셨나요?',
              color: NeocomixThemeColors.secondaryLight,
              fontWeight: FontWeight.normal,
            ),
            SizedBox(width: 12),
            GestureDetector(
              onTap: _resendEmail,
              child: TextLabel.p(
                '재전송 받기',
                fontWeight: FontWeight.w500,
                color: NeocomixThemeColors.secondaryDark,
              ),
            )
          ],
        ),
        SizedBox(height: 24.0),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: ThemeFlatButton.primary(
            label: '로그인하러 가기',
            onPressed: () {
              Navigator.of(context)
                  .popUntil(ModalRoute.withName(RouteName.login));
            },
          ),
        ),
        SizedBox(height: 28.0),
      ],
    );
  }
}
