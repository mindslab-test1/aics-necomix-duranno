import 'package:duranno_app/components/common/dialog.dart';
import 'package:duranno_app/graphql/query/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';

enum _SilentSignInState { Signed, Unsigned, ConnectionError }

class InitRoute extends AppRoute {
  InitRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.init;

  @override
  String get routeClassName => 'InitRoute';

  @override
  String get title => '';

  @override
  bool get useCustomAppBar => true;

  @override
  bool get useCustomScaffold => true;

  @override
  _InitRouteState createState() => _InitRouteState();
}

class _InitRouteState extends AppRouteState<InitRoute> {
  Future<_SilentSignInState> _signInSilently() async {
    final authentication =
        Provider.of<AuthenticationProvider>(context, listen: false);

    try {
      await authentication.refresh();

      if (authentication.isSigned) {
        if (authentication.isEmailVerified) {
          final result = await MeQuery.instance.execute(context);

          if (result.hasException) {
            return _SilentSignInState.ConnectionError;
          }

          final bool registered = result.data['me']['registered'];
          return registered
              ? _SilentSignInState.Signed
              : _SilentSignInState.Unsigned;
        }

        await authentication.signOut();
      }

      return _SilentSignInState.Unsigned;
    } catch (err) {
      return _SilentSignInState.Unsigned;
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    Future.wait<_SilentSignInState>([
      _signInSilently(),
      Future.delayed(const Duration(seconds: 2)),
    ]).then((raw) async {
      final signInState = raw.first;

      switch (signInState) {
        case _SilentSignInState.Signed:
          Navigator.of(context).pushReplacementNamed(RouteName.home);
          break;
        case _SilentSignInState.Unsigned:
          Navigator.of(context).pushReplacementNamed(RouteName.login);
          break;
        case _SilentSignInState.ConnectionError:
          await showThemeDialog(
            DialogType.OneAction,
            context: context,
            title: '서버와의 연결에 실패했습니다.',
            content: '로그인 페이지로 이동합니다.',
          );
          Navigator.of(context).pushReplacementNamed(RouteName.login);
          break;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: buildBody(context));
  }

  @override
  Widget buildBody(BuildContext context) {
    return Container(
      color: NeocomixThemeColors.primary,
      child: Center(
        child: Lottie.asset(
          'asset/lottie/launch.json',
          width: 360.0,
          fit: BoxFit.fitWidth,
        ),
      ),
    );
  }
}
