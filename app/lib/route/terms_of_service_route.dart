import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const _termsOfServiceFilePath = '/etc/TERMS_OF_SERVICE.txt';

class TermsOfServiceRoute extends AppRoute {
  TermsOfServiceRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.termsOfService;

  @override
  String get routeClassName => 'TermsOfServiceRoute';

  @override
  String get title => '이용약관';

  @override
  _TermsOfServiceRouteState createState() => _TermsOfServiceRouteState();
}

class _TermsOfServiceRouteState extends AppRouteState<TermsOfServiceRoute> {
  @override
  Widget buildBody(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final environment = Provider.of<EnvironmentProvider>(context);

    final contentWidget = FutureBuilder<String>(
      future: environment.parseResource(_termsOfServiceFilePath),
      builder: (context, snapshot) {
        if (snapshot.error != null) {
          return Center(child: errorWidget);
        }

        if (snapshot.data == null) {
          return Center(child: wrappedProgressIndicatorDark);
        }

        return Text(
          snapshot.data,
          style: textTheme.subtitle1.copyWith(
            color: NeocomixThemeColors.secondaryDark,
          ),
        );
      },
    );

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(32.0, 24.0, 32.0, 52.0),
        child: contentWidget,
      ),
    );
  }
}
