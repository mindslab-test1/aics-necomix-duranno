import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/post/post_item.dart';
import 'package:duranno_app/graphql/query/post.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/pagination/paginated_sliver_list.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

@immutable
class AnnouncementRouteArguments {
  final String postId;
  AnnouncementRouteArguments({
    this.postId,
  });

  factory AnnouncementRouteArguments.fromJson(dynamic json) =>
      AnnouncementRouteArguments(
        postId: json['postId'],
      );
}

const _paginationLimit = 20;

class AnnouncementRoute extends AppRoute {
  AnnouncementRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.announcement;

  @override
  String get routeClassName => 'AnnouncementRoute';

  @override
  String get title => '공지사항';

  @override
  _AnnouncementRouteState createState() => _AnnouncementRouteState();
}

class _AnnouncementRouteState extends AppRouteState<AnnouncementRoute> {
  Map<String, PostItemController> _itemControllers;
  PostItemController _getController(
    String key, {
    bool initialExpanded,
  }) {
    if (_itemControllers[key] == null) {
      _itemControllers[key] =
          PostItemController(initialExpanded: initialExpanded);
    }

    return _itemControllers[key];
  }

  @override
  void initState() {
    super.initState();

    _itemControllers = {};
  }

  @override
  void dispose() {
    _itemControllers.values.forEach((controller) {
      controller.dispose();
    });

    super.dispose();
  }

  @override
  Widget buildBody(BuildContext context) {
    final args =
        ModalRoute.of(context).settings.arguments as AnnouncementRouteArguments;

    return PaginatedSliverList<dynamic, dynamic, String>(
      queryOptions: QueryOptions(
        documentNode: PostQuery.announcements,
      ),
      itemsSelector: (data) => data['announcements']['items'],
      cursorSelector: (data) => data['announcements']['cursor'],
      mergeFetchMoreResult: (prev, cur) => {
        'announcements': {
          'items': [
            ...prev['announcements']['items'],
            ...cur['announcements']['items'],
          ],
          'cursor': cur['announcements']['cursor'],
        }
      },
      itemBuilder: ({itemData, key, refetch}) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 32.0),
        child: PostItem(
          data: itemData,
          controller: _getController(
            (key as ValueKey).value,
            initialExpanded: args?.postId == itemData['id'],
          ),
          key: key,
        ),
      ),
      itemKeySelector: (itemData) => itemData['id'],
      isScrollViewNested: false,
      numPageItems: _paginationLimit,
      marginTop: 0,
      contentMarginTop: 24.0,
      contentMarginBottom: 52.0,
      emptyLabelWidget: Container(
        height: 96.0,
        child: Center(
          child: TextLabel.p(
            '공지사항이 없습니다.',
            color: NeocomixThemeColors.secondaryLight,
          ),
        ),
      ),
    );
  }
}
