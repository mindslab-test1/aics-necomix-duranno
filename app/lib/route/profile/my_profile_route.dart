import 'dart:async';
import 'dart:math';

import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/subscription/active_subscription_card.dart';
import 'package:duranno_app/graphql/query/post.dart';
import 'package:duranno_app/graphql/query/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/customer_service_route.dart';
import 'package:duranno_app/route/login_route.dart';
import 'package:duranno_app/route/profile/announcement_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

const String _membershipInactiveSvgPath = 'asset/image/membership-inactive.svg';

class MyProfileRoute extends AppRoute {
  MyProfileRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.myProfile;

  @override
  String get routeClassName => 'MyProfileRoute';

  @override
  String get title => '내 정보';

  @override
  _MyProfileRouteState createState() => _MyProfileRouteState();
}

class _MyProfileRouteState extends AppRouteState<MyProfileRoute> {
  @override
  Widget buildBody(BuildContext context) {
    final authentication = Provider.of<AuthenticationProvider>(context);

    final signOutLink = GestureDetector(
      child: TextLabel.h6(
        '로그아웃',
        fontWeight: FontWeight.w500,
        color: NeocomixThemeColors.secondaryLight,
      ),
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        Navigator.of(context).popUntil(ModalRoute.withName(RouteName.home));
        Navigator.of(context).pushReplacementNamed(RouteName.login);
        authentication.signOut();
      },
    );

    final unsubscribeLink = GestureDetector(
      child: TextLabel.h6(
        '회원탈퇴',
        fontWeight: FontWeight.w500,
        color: NeocomixThemeColors.secondaryLight,
      ),
      behavior: HitTestBehavior.translucent,
      onTap: () async {
        Navigator.of(context).pushNamed(RouteName.unsubscribe);
      },
    );

    final me = authentication.isAnonymous
        ? Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    _buildUsername(context, username: '게스트'),
                  ],
                ),
              ),
              SizedBox(height: 16.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32.0),
                child: _buildUnsubscribedStateCard(
                  context,
                  content: '로그인 후 이용 가능합니다.',
                  buttonLabel: '로그인',
                  onButtonPressed: () {
                    Navigator.of(context).pushNamed(RouteName.login,
                        arguments:
                            LoginRouteArguments(shouldPopAfterLogin: true));
                  },
                ),
              ),
            ],
          )
        : Query(
            options: QueryOptions(documentNode: UserQuery.myProfile),
            builder: (result, {fetchMore, refetch}) {
              if (result.loading) {
                return Container(
                  height: 155.0,
                  child: Center(child: wrappedProgressIndicatorDark),
                );
              }

              if (result.hasException) {
                return Container(
                  height: 155.0,
                  child: Center(child: errorWidget),
                );
              }

              final String email = result.data['me']['email'];
              final bool subscribed = result.data['me']['subscribed'];
              final activeSubscription =
                  result.data['me']['activeSubscription'];

              return Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        _buildUsername(context, username: email),
                      ],
                    ),
                  ),
                  SizedBox(height: 16.0),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32.0),
                    child: subscribed
                        ? ActiveSubscriptionCard(data: activeSubscription)
                        : _buildUnsubscribedStateCard(
                            context,
                            content: '멤버십을 구독하고\n오디오북을 자유롭게 이용하세요.',
                            buttonIcon: ThemeIconData.headphone,
                            buttonLabel: '오디오북 멤버십 이용하기',
                            onButtonPressed: () {
                              Navigator.of(context)
                                  .pushNamed(RouteName.membershipHome);
                            },
                          ),
                  ),
                ],
              );
            },
          );

    final announcementRouteLink = GestureDetector(
      child: TextLabel.p(
        '모두 보기',
        color: NeocomixThemeColors.secondaryLight,
      ),
      onTap: () {
        Navigator.of(context).pushNamed(RouteName.announcement);
      },
      behavior: HitTestBehavior.translucent,
    );

    final announcementsWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _buildLabel(
          context,
          label: '공지사항',
          actionWidget: announcementRouteLink,
        ),
        SizedBox(height: 16.0),
        Query(
            options: QueryOptions(documentNode: PostQuery.recentAnnouncements),
            builder: (result, {refetch, fetchMore}) {
              if (result.loading) {
                return Container(
                  height: 92.0,
                  child: Center(child: wrappedProgressIndicatorDark),
                );
              }

              if (result.hasException) {
                return Container(
                  height: 92.0,
                  child: Center(child: errorWidget),
                );
              }

              final List<dynamic> announcements =
                  result.data['announcements']['items'];

              if (announcements.isEmpty) {
                return Container(
                  height: 92.0,
                  child: Center(
                    child: TextLabel.p(
                      '공지사항이 없습니다.',
                      color: NeocomixThemeColors.secondaryLight,
                    ),
                  ),
                );
              }

              return Container(
                height: 92.0,
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      ...IterableUtils.join<Widget>(
                        announcements.map(
                          (post) => _buildLink(
                            context,
                            label: post['title'],
                            onPressed: () {
                              Navigator.of(context).pushNamed(
                                RouteName.announcement,
                                arguments: AnnouncementRouteArguments(
                                    postId: post['id']),
                              );
                            },
                          ),
                        ),
                        separator: SizedBox(height: 16.0),
                      ),
                    ],
                  ),
                ),
              );
            }),
      ],
    );

    final customerServiceLinkWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _buildLabel(context, label: '고객센터'),
        SizedBox(height: 16.0),
        ...IterableUtils.join<Widget>(
          [
            _buildLink(
              context,
              label: 'FAQ',
              onPressed: () {
                Navigator.of(context).pushNamed(
                  RouteName.customerService,
                  arguments: CustomerServiceRouteArguments(initialTabIndex: 0),
                );
              },
            ),
            _buildLink(
              context,
              label: '1대1 문의',
              onPressed: () {
                Navigator.of(context).pushNamed(
                  RouteName.customerService,
                  arguments: CustomerServiceRouteArguments(initialTabIndex: 1),
                );
              },
            ),
            _buildLink(
              context,
              label: '제휴 문의',
              onPressed: () {
                Navigator.of(context).pushNamed(
                  RouteName.customerService,
                  arguments: CustomerServiceRouteArguments(initialTabIndex: 2),
                );
              },
            ),
          ],
          separator: SizedBox(height: 16.0),
        ),
      ],
    );

    final termsOfServiceLinkWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        _buildLabel(context, label: '이용약관'),
        SizedBox(height: 16.0),
        ...IterableUtils.join<Widget>(
          [
            _buildLink(
              context,
              label: '이용약관',
              onPressed: () {
                Navigator.of(context).pushNamed(RouteName.termsOfService);
              },
            ),
          ],
          separator: SizedBox(height: 16.0),
        ),
      ],
    );

    final footerOptions = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        signOutLink,
        SizedBox(width: 24),
        Container(height: 10, width: 1, color: NeocomixThemeColors.border),
        SizedBox(width: 24),
        unsubscribeLink
      ],
    );

    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 24.0),
          ...IterableUtils.join<Widget>(
            [
              me,
              announcementsWidget,
              customerServiceLinkWidget,
              termsOfServiceLinkWidget,
              footerOptions
            ],
            separator: SizedBox(height: 36.0),
          ),
          SizedBox(height: 54.0),
        ],
      ),
    );
  }

  Widget _buildUsername(BuildContext context, {String username}) {
    return RichText(
      text: TextSpan(
        children: [
          TextSpan(
            text: username,
            style: Theme.of(context).textTheme.headline4,
          ),
          TextSpan(
            text: '님',
            style: Theme.of(context)
                .textTheme
                .headline4
                .copyWith(fontWeight: FontWeight.normal),
          ),
        ],
        style: Theme.of(context).textTheme.headline4,
      ),
    );
  }

  Widget _buildLabel(
    BuildContext context, {
    String label,
    Widget actionWidget,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: Row(
        children: [
          TextLabel.h4(label),
          if (actionWidget != null) actionWidget,
        ],
        crossAxisAlignment: CrossAxisAlignment.end,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
      ),
    );
  }

  Widget _buildLink(
    BuildContext context, {
    String label,
    FutureOr<void> Function() onPressed,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 32.0),
      child: GestureDetector(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            TextLabel.p(label),
            ThemeIcon(
              ThemeIconData.arrow_right,
              size: 18.0,
              color: NeocomixThemeColors.secondaryLightest,
            ),
          ],
        ),
        behavior: HitTestBehavior.translucent,
        onTap: onPressed,
      ),
    );
  }

  Widget _buildUnsubscribedStateCard(
    BuildContext context, {
    String content,
    ThemeIconData buttonIcon,
    String buttonLabel,
    void Function() onButtonPressed,
  }) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final preferredHeight = constraints.maxWidth * (132.0 / 296.0);

        return Stack(
          alignment: Alignment.center,
          children: [
            Positioned.fill(
              child: Container(
                decoration: BoxDecoration(
                  color: NeocomixThemeColors.background,
                  borderRadius: BorderRadius.circular(4.0),
                ),
              ),
            ),
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: SvgPicture.asset(
                _membershipInactiveSvgPath,
                width: constraints.maxWidth,
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 16.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    content,
                    style: Theme.of(context).textTheme.headline6.copyWith(
                          fontWeight: FontWeight.normal,
                          color: NeocomixThemeColors.secondaryDark,
                        ),
                  ),
                  SizedBox(height: max(12.0, preferredHeight - 120.0)),
                  ThemeFlatButton.primary(
                    label: buttonLabel,
                    icon: buttonIcon,
                    onPressed: onButtonPressed,
                  )
                ],
              ),
            ),
          ],
        );
      },
    );
  }
}
