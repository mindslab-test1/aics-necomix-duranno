import 'package:duranno_app/route/route.dart';
import 'package:flutter/material.dart';

class NotFoundRoute extends AppRoute {
  NotFoundRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.notFound;

  @override
  String get routeClassName => 'NotFoundRoute';

  @override
  String get title => '';

  @override
  _NotFoundRouteState createState() => _NotFoundRouteState();
}

class _NotFoundRouteState extends AppRouteState<NotFoundRoute> {
  @override
  Widget buildBody(BuildContext context) {
    return Center(
      child: Text(
        '페이지를 찾을 수 없습니다.',
        style: Theme.of(context).textTheme.headline5,
      ),
    );
  }
}
