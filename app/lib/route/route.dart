import 'package:duranno_app/components/audio/audio_mini_player.dart';
import 'package:duranno_app/components/navigation/app_header.dart';
import 'package:duranno_app/provider/analytics_provider.dart';
import 'package:duranno_app/provider/route_provider.dart';
import 'package:duranno_app/route/audio/audio_player_route.dart';
import 'package:duranno_app/route/book_detail_route.dart';
import 'package:duranno_app/route/customer_service_route.dart';
import 'package:duranno_app/route/login_route.dart';
import 'package:duranno_app/route/profile/announcement_route.dart';
import 'package:duranno_app/route/register/user_agreement_route.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RouteName {
  static const values = [
    init,
    home,
    login,
    library,
    playlistDetails,
    commentDetails,
    membershipHome,
    membershipUserAgreement,
    membershipTermsOfService,
    registrationEmailConfirmation,
    registrationForm,
    registrationUserAgreement,
    customerService,
    settings,
    search,
    bookDetail,
    audioPlayer,
    audioSequence,
    notFound,
    myProfile,
    announcement,
    termsOfService,
    privacyPolicy,
    findPassword,
    confirmResetPassword,
    unsubscribe
  ];

  static const String init = '/init';
  static const String home = '/home';
  static const String login = '/login';

  static const String library = '/library';
  static const String playlistDetails = library + '/playlist';
  static const String commentDetails = library + '/comment';

  static const String membershipPrefix = '/membership';
  static const String membershipHome = membershipPrefix + '/home';
  static const String membershipUserAgreement =
      membershipPrefix + '/user-agreement';
  static const String membershipTermsOfService = membershipPrefix + '/tos';
  static const String unsubscribe = '/unsubscribe';

  static const String registrationPrefix = '/register';
  static const String registrationEmailConfirmation =
      registrationPrefix + '/email-confirmation';
  static const String registrationForm = registrationPrefix + '/form';
  static const String registrationUserAgreement =
      registrationPrefix + '/user-agreement';

  static const String customerService = '/cs';
  static const String settings = '/settings';
  static const String search = '/search';
  static const String bookDetail = '/book';
  static const String notFound = '/not-found';

  static const String audioPrefix = '/audio';
  static const String audioPlayer = audioPrefix + '/player';
  static const String audioSequence = audioPrefix + '/sequence';

  static const String _profilePrefix = '/profile';
  static const String myProfile = _profilePrefix + '/me';

  static const String _postPrefix = '/post';
  static const String announcement = _postPrefix + '/announcement';

  static const String termsOfService = '/tos';
  static const String privacyPolicy = '/privacy-policy';

  static const String findPassword = '/find/password';
  static const String confirmResetPassword = '/find/confirm-reset-password';
}

dynamic parseRouteArgs(dynamic json, String routeName) {
  switch (routeName) {
    case RouteName.announcement:
      return AnnouncementRouteArguments.fromJson(json);
    case RouteName.registrationUserAgreement:
      return UserAgreementRouteArguments.fromJson(json);
    case RouteName.audioPlayer:
      return AudioPlayerRouteArguments.fromJson(json);
    case RouteName.bookDetail:
      return BookDetailRouteArguments.fromJson(json);
    case RouteName.customerService:
      return CustomerServiceRouteArguments.fromJson(json);
    case RouteName.login:
      return LoginRouteArguments.fromJson(json);
    default:
      return null;
  }
}

abstract class AppRoute extends StatefulWidget {
  AppRoute({Key key}) : super(key: key);

  String get routeName;
  String get routeClassName;
  String get title;

  Brightness get brightness => Brightness.light;
  bool get useCustomScaffold => false;
  bool get useCustomAppBar => false;
}

abstract class AppRouteState<T extends AppRoute> extends State<T>
    with RouteAware, FirebaseAnalyticsRoute {
  double get statusBarHeight => MediaQuery.of(context).padding.top;

  @override
  Widget build(BuildContext context) {
    if (widget.useCustomScaffold) {
      return Theme(
        data: Theme.of(context).copyWith(brightness: widget.brightness),
        child: Builder(builder: (context) => buildScaffold(context)),
      );
    }

    final appBar = widget.useCustomAppBar
        ? buildAppBar(context)
        : AppHeader(
            title: widget.title,
          );

    final drawer = _hasDrawer
        ? Builder(
            builder: (context) => buildDrawer(context),
          )
        : null;

    final hideMiniPlayer =
        ModalRoute.of(context).settings.name.contains(RouteName.audioPrefix);

    final body = SafeArea(
      child: LayoutBuilder(
        builder: (context, constraints) => GestureDetector(
          child: Container(
            height: constraints.maxHeight,
            child: Stack(
              children: [
                Positioned(
                  top: 0.0,
                  left: 0.0,
                  right: 0.0,
                  height: constraints.maxHeight,
                  child: buildBody(context),
                ),
                if (!hideMiniPlayer)
                  Positioned(
                    left: 0.0,
                    right: 0.0,
                    bottom: 0.0,
                    child: Hero(tag: 'audio-player', child: AudioMiniPlayer()),
                  ),
              ],
            ),
          ),
          behavior: HitTestBehavior.translucent,
          onTap: () {
            FocusScope.of(context).unfocus();
          },
        ),
      ),
    );

    return Theme(
      data: Theme.of(context).copyWith(brightness: widget.brightness),
      child: Scaffold(
        appBar: appBar,
        body: body,
        drawer: drawer,
        drawerScrimColor: const Color.fromRGBO(0, 0, 0, 0.2),
        extendBodyBehindAppBar: widget.useCustomAppBar,
      ),
    );
  }

  Widget buildAppBar(BuildContext context) {
    return null;
  }

  Widget buildBody(BuildContext context) {
    return Container();
  }

  Widget buildDrawer(BuildContext context) {
    return Drawer();
  }

  Widget buildScaffold(BuildContext context) {
    return Scaffold();
  }

  bool get _hasDrawer => ModalRoute.of(context).settings.name == RouteName.home;
}

mixin FirebaseAnalyticsRoute<T extends AppRoute> on State<T>, RouteAware {
  AnalyticsProvider _analytics;
  RouteProvider _route;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _analytics = Provider.of<AnalyticsProvider>(context, listen: false);
    if (_route != null) {
      _route.observer.unsubscribe(this);
    }
    _route = Provider.of<RouteProvider>(context, listen: false);
    _route.observer.subscribe(this, ModalRoute.of(context));
  }

  @override
  void dispose() {
    _route.observer.unsubscribe(this);

    super.dispose();
  }

  @override
  void didPopNext() {
    super.didPopNext();

    _setCurrentRouteToAnalytics();
  }

  @override
  void didPush() {
    super.didPush();

    _setCurrentRouteToAnalytics();
  }

  Future<void> _setCurrentRouteToAnalytics() =>
      _analytics.setCurrentScreen(widget.routeName);
}
