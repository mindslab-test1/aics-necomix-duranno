import 'package:audio_service/audio_service.dart';
import 'package:duranno_app/components/audio/audio_actions.dart';
import 'package:duranno_app/components/book/blurred_background_image.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/navigation/app_header.dart';
import 'package:duranno_app/provider/audio_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/audio/audio_controller.dart';
import 'package:duranno_app/utils/book.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tuple/tuple.dart';

const _backgroundBottomContainerDecoration = const BoxDecoration(
  gradient: const LinearGradient(
    colors: [
      const Color.fromRGBO(0, 0, 0, 0.51),
      const Color.fromRGBO(0, 0, 0, 0.75)
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  ),
);

class AudioSequenceRoute extends AppRoute {
  AudioSequenceRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.audioSequence;

  @override
  String get routeClassName => 'AudioSequenceRoute';

  @override
  String get title => '목차';

  @override
  Brightness get brightness => Brightness.dark;

  @override
  bool get useCustomAppBar => true;

  @override
  _AudioSequenceRouteState createState() => _AudioSequenceRouteState();
}

class _AudioSequenceRouteState extends AppRouteState<AudioSequenceRoute> {
  @override
  Widget buildBody(BuildContext context) {
    final audio = Provider.of<AudioProvider>(context);

    final appBar = AppHeader(
      title: widget.title,
      useLeadingButton: false,
      actionButtonWidget: ThemeIconButton(
        materialIcon: Icons.clear,
        iconColor: NeocomixThemeColors.white,
        onPressed: () => Navigator.of(context).pop(),
      ),
    );

    final backgroundWidget = Positioned.fill(
      child: Hero(
        tag: 'gradient-layer',
        child: Container(decoration: _backgroundBottomContainerDecoration),
      ),
    );

    return StreamBuilder<Tuple3<IAudioController, AudioQueue, MediaItem>>(
      stream: Rx.combineLatest3(
          audio.onControllerChanged,
          audio.onAudioQueueChanged,
          audio.onMediaItemChanged,
          (controller, audioQueue, mediaItem) =>
              Tuple3<IAudioController, AudioQueue, MediaItem>(
                  controller, audioQueue, mediaItem)),
      initialData: Tuple3<IAudioController, AudioQueue, MediaItem>(
          audio.controller, audio.audioQueue, audio.mediaItem),
      builder: (context, snapshot) {
        final controller = snapshot.data?.item1;
        final audioQueue = snapshot.data?.item2;
        final mediaItem = snapshot.data?.item3;

        final totalDuration = Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ThemeIcon(
              ThemeIconData.playlist,
              size: 24.0,
              color: NeocomixThemeColors.secondaryLightest,
            ),
            SizedBox(width: 4.0),
            TextLabel.p(
              '전체 재생 시간',
              color: NeocomixThemeColors.secondaryLightest,
            ),
            SizedBox(width: 8.0),
            if (audioQueue?.totalDuration != null)
              TextLabel.p(
                NumUtils.formatTimeInSeconds(
                    audioQueue.totalDuration.inSeconds),
                color: NeocomixThemeColors.secondaryLightest,
              ),
          ],
        );

        final buildChapterWidget = (Chapter chapter) {
          final sequenceOfChapter =
              audioQueue?.getSequenceOfChapter(chapter) ?? -1;
          final sequenceOfMediaItem = mediaItem != null
              ? audioQueue?.getSequenceOfMediaItem(mediaItem) ?? -1
              : -1;

          final isPlayingCurrentChapter = sequenceOfChapter >= 0 &&
              sequenceOfMediaItem >= 0 &&
              sequenceOfChapter == sequenceOfMediaItem;

          Widget widget = Container(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12.0),
            color: isPlayingCurrentChapter
                ? const Color.fromRGBO(255, 255, 255, 0.12)
                : null,
            child: chapter.isSequence
                ? Row(
                    children: [
                      if (isPlayingCurrentChapter)
                        ...([
                          Lottie.asset(
                            'asset/lottie/equalizer.json',
                            width: 20.0,
                            height: 20.0,
                          ),
                          SizedBox(width: 12.0),
                        ]),
                      Expanded(
                        child: TextLabel.p(
                          chapter.title,
                          fontWeight: FontWeight.w500,
                          color: NeocomixThemeColors.white,
                          expanded: true,
                        ),
                      ),
                      SizedBox(width: 12.0),
                      SizedBox(
                        width: 48.0,
                        child: TextLabel.h6(
                          NumUtils.formatTimeInSeconds(
                              chapter.duration.inSeconds),
                          fontWeight: FontWeight.w500,
                          color: NeocomixThemeColors.secondaryLightest,
                        ),
                      ),
                    ],
                  )
                : TextLabel.p(
                    chapter.title,
                    fontWeight: FontWeight.w500,
                    color: NeocomixThemeColors.secondaryLightest,
                    expanded: true,
                  ),
          );

          if (chapter.isSequence && !isPlayingCurrentChapter) {
            widget = GestureDetector(
              child: widget,
              behavior: HitTestBehavior.translucent,
              onTap: () {
                controller?.seekToSequence(sequenceOfChapter);
              },
            );
          }

          return widget;
        };

        final chapterList = SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(vertical: 24.0),
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: totalDuration,
                ),
                SizedBox(height: 36.0),
                ...(audioQueue?.chapters?.map(buildChapterWidget) ?? []),
              ],
            ),
          ),
        );

        final content = Column(
          children: [
            appBar,
            Expanded(child: chapterList),
            SizedBox(height: 44.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Hero(tag: 'audio-actions', child: AudioActions()),
            ),
            SizedBox(height: 36.0),
          ],
        );

        return SafeArea(
          child: Stack(
            children: [
              Hero(
                tag: 'blurred-background-image',
                child: BlurredBackgroundImage(imageUri: mediaItem?.artUri),
              ),
              backgroundWidget,
              Positioned.fill(child: content),
            ],
          ),
        );
      },
    );
  }
}
