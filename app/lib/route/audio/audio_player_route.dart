import 'package:audio_service/audio_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/components/audio/audio_actions.dart';
import 'package:duranno_app/components/audio/audio_rate_picker.dart';
import 'package:duranno_app/provider/audio_provider.dart';
import 'package:duranno_app/components/audio/audio_seeker.dart';
import 'package:duranno_app/components/book/blurred_background_image.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/navigation/app_header.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:marquee/marquee.dart';
import 'package:provider/provider.dart';

const _backgroundBottomContainerDecoration = const BoxDecoration(
  gradient: const LinearGradient(
    colors: [
      const Color.fromRGBO(0, 0, 0, 0.3),
      const Color.fromRGBO(0, 0, 0, 0.5)
    ],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
  ),
);

@immutable
class AudioPlayerRouteArguments {
  final String bookId;
  final int sequence;
  AudioPlayerRouteArguments({
    @required this.bookId,
    this.sequence,
  }) : assert(bookId != null);

  factory AudioPlayerRouteArguments.fromJson(dynamic json) =>
      AudioPlayerRouteArguments(
        bookId: json['bookId'],
        sequence: json['sequence'],
      );
}

class AudioPlayerRoute extends AppRoute {
  AudioPlayerRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.audioPlayer;

  @override
  String get routeClassName => 'AudioPlayerRoute';

  @override
  String get title => '';

  @override
  Brightness get brightness => Brightness.dark;

  @override
  bool get useCustomAppBar => true;

  @override
  _AudioPlayerRouteState createState() => _AudioPlayerRouteState();
}

class _AudioPlayerRouteState extends AppRouteState<AudioPlayerRoute> {
  AudioProvider _audio;

  void _onAudioStopped() {
    Navigator.of(context).popUntil(
      (route) =>
          !route.willHandlePopInternally &&
          route is ModalRoute &&
          !(route.settings.name == null ||
              route.settings.name.contains(RouteName.audioPrefix)),
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    final args =
        ModalRoute.of(context).settings.arguments as AudioPlayerRouteArguments;

    _audio = Provider.of<AudioProvider>(context, listen: false);

    _audio.fetch(bookId: args.bookId, initialSequence: args.sequence);
    _audio.addOnStopListener(_onAudioStopped);
  }

  @override
  void dispose() {
    _audio.removeOnStopListener(_onAudioStopped);

    super.dispose();
  }

  @override
  Widget buildBody(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final textColor = NeocomixThemeColors.white;

    final audio = Provider.of<AudioProvider>(context);

    final appBar = AppHeader(
      title: widget.title,
      leadingButtonWidget: ThemeIconButton(
        icon: ThemeIconData.close,
        iconColor: NeocomixThemeColors.white,
        onPressed: () {
          AudioService.stop();
          Navigator.of(context).pop();
        },
      ),
      actionButtonWidget: ThemeIconButton(
        icon: ThemeIconData.arrow_down,
        iconColor: NeocomixThemeColors.white,
        onPressed: () => Navigator.of(context).pop(),
      ),
    );

    final backgroundWidget = Positioned.fill(
      child: LayoutBuilder(
        builder: (context, constraints) => Column(
          children: [
            Container(
              height: (constraints.maxHeight - 104.0) * 0.5,
            ),
            Hero(
              tag: 'gradient-layer',
              child: Container(
                height: (constraints.maxHeight + 104.0) * 0.5,
                decoration: _backgroundBottomContainerDecoration,
              ),
            ),
          ],
        ),
      ),
    );

    final queryErrorWidget = Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 64.0),
        Icon(
          Icons.error,
          size: 24.0,
          color: NeocomixThemeColors.secondaryDarkest,
        ),
        SizedBox(height: 4.0),
        TextLabel.span('정보를 불러올 수 없습니다.'),
        Spacer(),
      ],
    );

    final thumbnailErrorWidget = Material(
      elevation: 8.0,
      child: Container(
        width: 160.0,
        height: 240.0,
        child: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Icon(
                Icons.error,
                size: 18.0,
                color: textColor,
              ),
              SizedBox(height: 4.0),
              TextLabel.smallSpan('이미지를 찾을 수 없습니다.', color: textColor),
            ],
          ),
        ),
      ),
    );

    final sequenceLinkButton = GestureDetector(
      child: ThemeIcon(
        ThemeIconData.playlist,
        size: 28.0,
        color: NeocomixThemeColors.border,
      ),
      behavior: HitTestBehavior.translucent,
      onTap: () {
        Navigator.of(context).pushNamed(RouteName.audioSequence);
      },
    );

    return StreamBuilder<MediaItem>(
      stream: audio.onMediaItemChanged,
      initialData: audio.mediaItem,
      builder: (context, snapshot) {
        final titleWidget = Text(
          snapshot.data?.album ?? '',
          style: textTheme.headline3.copyWith(color: textColor),
        );

        final authorNameWidget = Text(
          snapshot.data?.artist ?? '',
          style: textTheme.headline5.copyWith(color: textColor),
        );

        final thumbnailWidget = snapshot.data?.artUri != null
            ? CachedNetworkImage(
                imageUrl: snapshot.data.artUri,
                imageBuilder: (context, imageProvider) => Material(
                  elevation: 8.0,
                  child: Container(
                    height: 240.0,
                    child: Image(image: imageProvider, fit: BoxFit.fitHeight),
                  ),
                ),
                errorWidget: (context, url, error) => thumbnailErrorWidget,
              )
            : SizedBox(height: 240.0);

        final chapterTitleWidget = Container(
          height: 20.0,
          child: Marquee(
            text: snapshot.data?.title ?? ' ',
            style: textTheme.subtitle1.copyWith(color: textColor),
            scrollAxis: Axis.horizontal,
            crossAxisAlignment: CrossAxisAlignment.start,
            blankSpace: MediaQuery.of(context).size.width - 140.0,
            velocity: 25.0,
          ),
        );

        final content = StreamBuilder<AudioQueryState>(
          stream: audio.onAudioQueryStateChanged,
          initialData: audio.audioQueryState,
          builder: (context, snapshot) => Column(
            children: [
              Spacer(),

              /* title and author name */
              if (snapshot.data == AudioQueryState.Completed) titleWidget,
              if (snapshot.data == AudioQueryState.Completed)
                SizedBox(height: 6.0),
              if (snapshot.data == AudioQueryState.Completed) authorNameWidget,
              if (snapshot.data == AudioQueryState.Pending)
                SizedBox(
                  height: 60.0,
                  child: Center(child: wrappedProgressIndicatorLight),
                ),
              if (snapshot.data == AudioQueryState.Error)
                SizedBox(
                  height: 60.0,
                  child: Center(child: queryErrorWidget),
                ),
              SizedBox(height: 28.0),

              /* thumbnail */
              if (snapshot.data == AudioQueryState.Completed) thumbnailWidget,
              if (snapshot.data == AudioQueryState.Pending)
                SizedBox(height: 240.0),
              if (snapshot.data == AudioQueryState.Error) thumbnailErrorWidget,

              /* chapter title and audio seeker */
              Spacer(),
              SizedBox(height: 10.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Row(
                  children: [
                    ...IterableUtils.join<Widget>(
                      [
                        Expanded(child: chapterTitleWidget),
                        AudioRatePicker(),
                        sequenceLinkButton,
                      ],
                      separator: SizedBox(width: 16.0),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 12.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: AudioSeeker(),
              ),
              SizedBox(height: 16.0),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Hero(tag: 'audio-actions', child: AudioActions()),
              ),
              SizedBox(height: 36.0),
            ],
          ),
        );

        return SafeArea(
          child: Stack(
            children: [
              Hero(
                tag: 'blurred-background-image',
                child: BlurredBackgroundImage(imageUri: snapshot.data?.artUri),
              ),
              backgroundWidget,
              Align(alignment: Alignment.topLeft, child: appBar),
              Positioned.fill(child: content),
            ],
          ),
        );
      },
    );
  }
}
