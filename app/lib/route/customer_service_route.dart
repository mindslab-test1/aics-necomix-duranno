import 'package:duranno_app/components/common/tab_page/tab_page.dart';
import 'package:duranno_app/components/common/tab_page/tab_page_bar.dart';
import 'package:duranno_app/components/common/tab_page/tab_page_controller.dart';
import 'package:duranno_app/components/customer_service/faq.dart';
import 'package:duranno_app/components/customer_service/feedback_form_controller.dart';
import 'package:duranno_app/components/customer_service/feedback_form.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

@immutable
class CustomerServiceRouteArguments {
  final int initialTabIndex;
  CustomerServiceRouteArguments({
    @required this.initialTabIndex,
  });

  factory CustomerServiceRouteArguments.fromJson(dynamic json) =>
      CustomerServiceRouteArguments(
        initialTabIndex: json['initialTabIndex'],
      );
}

class CustomerServiceRoute extends AppRoute {
  CustomerServiceRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.customerService;

  @override
  String get routeClassName => 'CustomerServiceRoute';

  @override
  String get title => '고객센터';

  @override
  _CustomerServiceRouteState createState() => _CustomerServiceRouteState();
}

class _CustomerServiceRouteState extends AppRouteState<CustomerServiceRoute>
    with TickerProviderStateMixin {
  TabPageController _tabPageController;
  FeedbackFormController _individualFeedbackFormController;
  FeedbackFormController _cooperationFeedbackFormController;

  @override
  void initState() {
    super.initState();

    _tabPageController = TabPageController(numItems: 3);
    _tabPageController.initialize(vsync: this);

    _individualFeedbackFormController = FeedbackFormController(
      type: 'individual',
      categoryOptions: [
        '멤버십',
        '결제/요금',
        '불편사항/오류',
        '로그인/계정',
        '이벤트',
        '기타',
      ],
    );
    _cooperationFeedbackFormController = FeedbackFormController(
      type: 'cooperation',
      categoryOptions: [
        '콘텐츠 제휴',
        '마케팅 제휴',
        'B2B 제휴',
        '법인 관련 제휴',
        '기타 제휴',
      ],
    );
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    final args = ModalRoute.of(context).settings.arguments
        as CustomerServiceRouteArguments;
    if (args != null) {
      _tabPageController.setActive(args.initialTabIndex);
    }
  }

  @override
  void dispose() {
    _tabPageController.dispose();
    _individualFeedbackFormController.dispose();
    _cooperationFeedbackFormController.dispose();

    super.dispose();
  }

  @override
  Widget buildBody(BuildContext context) {
    return Column(
      children: [
        TabPageBar(
          controller: _tabPageController,
          labels: ['FAQ', '1:1 문의', '제휴 문의'],
          verticalPadding: 12.0,
        ),
        Container(height: 1.0, color: NeocomixThemeColors.border),
        Expanded(
          child: TabPage(
            controller: _tabPageController,
            builder: (context, index) {
              switch (index) {
                case 0:
                  return SingleChildScrollView(child: Faq());
                case 1:
                  return SingleChildScrollView(
                    child: FeedbackForm(
                      title: '문의사항이 있으신가요?',
                      typeLabel: '문의 유형',
                      controller: _individualFeedbackFormController,
                    ),
                  );
                case 2:
                  return SingleChildScrollView(
                    child: FeedbackForm(
                      title: '제휴문의가 있으신가요?',
                      typeLabel: '제안 유형',
                      controller: _cooperationFeedbackFormController,
                    ),
                  );
              }
            },
          ),
        ),
      ],
    );
  }
}
