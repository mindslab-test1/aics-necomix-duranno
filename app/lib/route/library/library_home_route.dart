import 'package:duranno_app/components/book/book_list_item.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/history/user_history_list.dart';
import 'package:duranno_app/components/library/user_comment.dart';
import 'package:duranno_app/graphql/query/playlist.dart';
import 'package:duranno_app/graphql/query/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class LibraryHomeRoute extends AppRoute {
  LibraryHomeRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.library;

  @override
  String get routeClassName => 'LibraryHomeRoute';

  @override
  String get title => '나의 오디오북';

  @override
  _LibraryHomeRouteState createState() => _LibraryHomeRouteState();
}

class _LibraryHomeRouteState extends AppRouteState<LibraryHomeRoute> {
  @override
  Widget buildBody(BuildContext context) {
    final authentication = Provider.of<AuthenticationProvider>(context);

    final emptyPlaylistWidget = Container(
      height: 104.0,
      child: Center(
        child: TextLabel.p(
          '담은 오디오북이 없습니다.',
          color: NeocomixThemeColors.secondaryLight,
        ),
      ),
    );

    final playlistSummaryWidget = authentication.isAnonymous
        ? Column(
            children: [
              _buildLabel(context, title: '나의 오디오북'),
              SizedBox(height: 16.0),
              emptyPlaylistWidget,
            ],
          )
        : PlaylistSummaryQuery.instance.build(
            context,
            pendingBuilder: (context) => Column(
              children: [
                _buildLabel(context, title: '나의 오디오북'),
                SizedBox(height: 16.0),
                Container(
                  height: 104.0,
                  child: Center(child: wrappedProgressIndicatorDark),
                ),
              ],
            ),
            errorBuilder: (context, {error}) => Column(
              children: [
                _buildLabel(context, title: '나의 오디오북'),
                SizedBox(height: 16.0),
                Container(
                  height: 104.0,
                  child: Center(child: errorWidget),
                ),
              ],
            ),
            builder: (context, {data, refetch}) {
              final int count = data['count'];
              final List<dynamic> items = data['list']['items'];

              return Column(
                children: [
                  _buildLabel(
                    context,
                    title: '나의 오디오북',
                    subtitle: '$count개',
                    action: '모두 보기',
                    onActionPressed: () {
                      Navigator.of(context)
                          .pushNamed(RouteName.playlistDetails);
                    },
                  ),
                  SizedBox(height: 16.0),
                  if (items.isEmpty) emptyPlaylistWidget,
                  if (items.isNotEmpty)
                    ...IterableUtils.join<Widget>(
                      items.map((playlist) => playlist['book']).map(
                            (book) => Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 32.0),
                              child: BookListItem(
                                data: book,
                                actions: [BookAction.DeleteFromPlaylist],
                                onActionComplete: ({args, type, data}) {
                                  switch (type) {
                                    case BookAction.DeleteFromPlaylist:
                                      {
                                        setState(() {});
                                        break;
                                      }
                                  }
                                },
                              ),
                            ),
                          ),
                      separator: SizedBox(height: 16.0),
                    ),
                ],
              );
            },
          );

    final emptyCommentsWidget = Container(
      height: 104.0,
      child: Center(
        child: TextLabel.p(
          '남긴 댓글이 없습니다.',
          color: NeocomixThemeColors.secondaryLight,
        ),
      ),
    );

    final myCommentsSummaryWidget = authentication.isAnonymous
        ? Column(
            children: [
              _buildLabel(context, title: '남긴 댓글'),
              SizedBox(height: 16.0),
              emptyCommentsWidget,
            ],
          )
        : Query(
            options: QueryOptions(
              documentNode: UserQuery.myCommentsSummary,
            ),
            builder: (result, {refetch, fetchMore}) {
              if (result.loading) {
                return Column(
                  children: [
                    _buildLabel(context, title: '남긴 댓글'),
                    SizedBox(height: 16.0),
                    Container(
                      height: 104.0,
                      child: Center(child: wrappedProgressIndicatorDark),
                    ),
                  ],
                );
              }

              if (result.hasException) {
                final logger = Provider.of<EnvironmentProvider>(context).logger;
                logger.error(result.exception, null);
                return Column(
                  children: [
                    _buildLabel(context, title: '남긴 댓글'),
                    SizedBox(height: 16.0),
                    Container(
                      height: 104.0,
                      child: Center(child: errorWidget),
                    ),
                  ],
                );
              }

              final int count = result.data['MyComments']['count'];
              final List<dynamic> comments =
                  result.data['myComments']['comments'];

              return Column(
                children: [
                  _buildLabel(
                    context,
                    title: '남긴 댓글',
                    subtitle: '$count개',
                    action: '모두 보기',
                    onActionPressed: () {
                      Navigator.of(context).pushNamed(RouteName.commentDetails);
                    },
                  ),
                  SizedBox(height: 16.0),
                  if (comments.isEmpty) emptyCommentsWidget,
                  if (comments.isNotEmpty)
                    ...IterableUtils.join<Widget>(
                      comments.map(
                        (comment) => Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 32.0),
                          child: UserComment(data: comment),
                        ),
                      ),
                      separator: SizedBox(height: 16.0),
                    ),
                ],
              );
            },
          );

    return SingleChildScrollView(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(height: 24.0),
          _buildLabel(context, title: '최근 재생'),
          SizedBox(height: 16.0),
          UserHistoryList(),
          SizedBox(height: 36.0),
          playlistSummaryWidget,
          SizedBox(height: 36.0),
          myCommentsSummaryWidget,
          SizedBox(height: 52.0),
        ],
      ),
    );
  }

  Widget _buildLabel(
    BuildContext context, {
    String title,
    String subtitle,
    String action,
    void Function() onActionPressed,
  }) {
    final titleWidget = TextLabel.h4(title);
    final subtitleWidget = subtitle != null
        ? TextLabel.p(subtitle, color: NeocomixThemeColors.secondaryLight)
        : null;
    final actionWidget = action != null && onActionPressed != null
        ? GestureDetector(
            child:
                TextLabel.p(action, color: NeocomixThemeColors.secondaryLight),
            onTap: onActionPressed,
          )
        : null;

    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 32.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          titleWidget,
          SizedBox(width: 8.0),
          if (subtitleWidget != null) subtitleWidget,
          Spacer(),
          if (actionWidget != null) actionWidget,
        ],
      ),
    );
  }
}
