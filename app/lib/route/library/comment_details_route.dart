import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/library/user_comment.dart';
import 'package:duranno_app/graphql/query/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/pagination/paginated_sliver_list.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

const double _marginTop = 24.0;
const double _contentMarginTop = 16.0;
const double _marginBetweenItems = 16.0;
const double _contentMarginBottom = 52.0;

class CommentDetailsRoute extends AppRoute {
  CommentDetailsRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.commentDetails;

  @override
  String get routeClassName => 'CommentDetailsRoute';

  @override
  String get title => '남긴 댓글';

  @override
  _CommentDetailsRouteState createState() => _CommentDetailsRouteState();
}

class _CommentDetailsRouteState extends AppRouteState<CommentDetailsRoute> {
  Widget buildBody(BuildContext context) {
    final authentication = Provider.of<AuthenticationProvider>(context);

    final emptyListWidget = Container(
      height: 104.0,
      child: Center(
        child: TextLabel.p(
          '남긴 댓글이 없습니다.',
          color: NeocomixThemeColors.secondaryLight,
        ),
      ),
    );

    if (authentication.isAnonymous) {
      return Column(
        children: [
          SizedBox(height: _marginTop),
          _buildLabel(context, title: '남긴 댓글'),
          SizedBox(height: _contentMarginTop),
          emptyListWidget,
          SizedBox(height: _contentMarginBottom),
        ],
      );
    }

    final headerWidget = Query(
      options: QueryOptions(
        documentNode: UserQuery.myCommentsCount,
      ),
      builder: (result, {refetch, fetchMore}) {
        if (result.loading) {
          return _buildLabel(context, title: '남긴 댓글');
        }

        if (result.hasException) {
          final logger = Provider.of<EnvironmentProvider>(context).logger;
          logger.error(result.exception, null);
          return _buildLabel(context, title: '남긴 댓글');
        }

        final int count = result.data['MyComments']['count'];

        return _buildLabel(
          context,
          title: '남긴 댓글',
          subtitle: '$count개',
        );
      },
    );

    return PaginatedSliverList<dynamic, dynamic, String>(
      queryOptions: QueryOptions(
        documentNode: UserQuery.myCommentsPage,
        variables: {},
      ),
      itemsSelector: (data) => data['myComments']['comments'],
      cursorSelector: (data) => data['myComments']['cursor'],
      mergeFetchMoreResult: (prev, cur) => {
        'myComments': {
          'comments': [
            ...prev['myComments']['comments'],
            ...cur['myComments']['comments'],
          ],
          'cursor': cur['myComments']['cursor'],
        },
      },
      itemBuilder: ({itemData, key, refetch}) => Padding(
        key: key,
        padding: const EdgeInsets.symmetric(horizontal: 32.0),
        child: UserComment(data: itemData),
      ),
      itemKeySelector: (itemData) => itemData['id'],
      isScrollViewNested: false,
      headerWidget: headerWidget,
      emptyLabelWidget: emptyListWidget,
      marginTop: _marginTop,
      contentMarginTop: _contentMarginTop,
      marginBetweenItems: _marginBetweenItems,
      contentMarginBottom: _contentMarginBottom,
    );
  }

  Widget _buildLabel(
    BuildContext context, {
    String title,
    String subtitle,
  }) {
    final titleWidget = TextLabel.h4(title);
    final subtitleWidget = subtitle != null
        ? TextLabel.p(subtitle, color: NeocomixThemeColors.secondaryLight)
        : null;

    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 32.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          titleWidget,
          SizedBox(width: 8.0),
          if (subtitleWidget != null) subtitleWidget,
          Spacer(),
        ],
      ),
    );
  }
}
