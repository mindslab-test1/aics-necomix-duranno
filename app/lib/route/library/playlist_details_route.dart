import 'package:duranno_app/components/book/book_list_item.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/query/playlist.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/pagination/paginated_sliver_list.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

const double _marginTop = 24.0;
const double _contentMarginTop = 16.0;
const double _marginBetweenItems = 16.0;
const double _contentMarginBottom = 52.0;

class PlaylistDetailsRoute extends AppRoute {
  PlaylistDetailsRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.playlistDetails;

  @override
  String get routeClassName => 'PlaylistDetailsRoute';

  @override
  String get title => '나의 오디오북';

  @override
  _PlaylistDetailsRouteState createState() => _PlaylistDetailsRouteState();
}

class _PlaylistDetailsRouteState extends AppRouteState<PlaylistDetailsRoute> {
  Widget buildBody(BuildContext context) {
    final authentication = Provider.of<AuthenticationProvider>(context);

    final emptyListWidget = Container(
      height: 104.0,
      child: Center(
        child: TextLabel.p(
          '담은 오디오북이 없습니다.',
          color: NeocomixThemeColors.secondaryLight,
        ),
      ),
    );

    if (authentication.isAnonymous) {
      return Column(
        children: [
          SizedBox(height: _marginTop),
          _buildLabel(context, title: '나의 오디오북'),
          SizedBox(height: _contentMarginTop),
          emptyListWidget,
          SizedBox(height: _contentMarginBottom),
        ],
      );
    }

    final headerWidget = PlaylistCountQuery.instance.build(
      context,
      pendingBuilder: (context) => _buildLabel(context, title: '나의 오디오북'),
      errorBuilder: (context, {error}) =>
          _buildLabel(context, title: '나의 오디오북'),
      builder: (context, {data, refetch}) {
        final int count = data['count'];

        return _buildLabel(
          context,
          title: '나의 오디오북',
          subtitle: '$count개',
        );
      },
    );

    return PaginatedSliverList<dynamic, dynamic, String>(
      queryOptions: QueryOptions(
        documentNode: PlaylistQuery.page,
        variables: {},
      ),
      itemsSelector: PlaylistQuery.playlistItemsSelector,
      cursorSelector: PlaylistQuery.playlistCursorSelector,
      mergeFetchMoreResult: PlaylistQuery.mergePlaylistFetchMore,
      itemBuilder: ({itemData, key, refetch}) => Padding(
        key: key,
        padding: const EdgeInsets.symmetric(horizontal: 32.0),
        child: BookListItem(
          data: itemData['book'],
          actions: [BookAction.DeleteFromPlaylist],
          onActionComplete: ({args, type, data}) {
            switch (type) {
              case BookAction.DeleteFromPlaylist:
                {
                  setState(() {});
                  break;
                }
            }
          },
        ),
      ),
      itemKeySelector: (itemData) => itemData['book']['id'],
      isScrollViewNested: false,
      headerWidget: headerWidget,
      emptyLabelWidget: emptyListWidget,
      marginTop: _marginTop,
      contentMarginTop: _contentMarginTop,
      marginBetweenItems: _marginBetweenItems,
      contentMarginBottom: _contentMarginBottom,
    );
  }

  Widget _buildLabel(
    BuildContext context, {
    String title,
    String subtitle,
  }) {
    final titleWidget = TextLabel.h4(title);
    final subtitleWidget = subtitle != null
        ? TextLabel.p(subtitle, color: NeocomixThemeColors.secondaryLight)
        : null;

    return Padding(
      padding: const EdgeInsets.only(left: 20.0, right: 32.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          titleWidget,
          SizedBox(width: 8.0),
          if (subtitleWidget != null) subtitleWidget,
          Spacer(),
        ],
      ),
    );
  }
}
