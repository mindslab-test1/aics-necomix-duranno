import 'package:duranno_app/components/common/agreement_checkbox.dart';
import 'package:duranno_app/components/common/checkbox_controller.dart';
import 'package:duranno_app/components/common/dialog.dart';
import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/graphql/mutation/user.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class MembershipUserAgreementRoute extends AppRoute {
  MembershipUserAgreementRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.membershipUserAgreement;

  @override
  String get routeClassName => 'MembershipUserAgreementRoute';

  @override
  String get title => '오디오북 이용권';

  @override
  _MembershipUserAgreementRouteState createState() =>
      _MembershipUserAgreementRouteState();
}

class _MembershipUserAgreementRouteState
    extends AppRouteState<MembershipUserAgreementRoute> {
  bool _selectedAllRequiredFields = false;
  bool _isPending = false;

  CheckBoxController _checkBoxController = CheckBoxController(totalNum: 3);
  static const List<int> _optionalFields = [2];

  void initState() {
    super.initState();

    _checkBoxController.optional = _optionalFields;
  }

  void _refresh() {
    setState(() {
      _selectedAllRequiredFields = _checkBoxController.allSelect();
    });
  }

  @override
  Widget buildBody(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    final agreementWidget = Container(
      decoration: BoxDecoration(
        border: Border.all(color: NeocomixThemeColors.border, width: 1.0),
        borderRadius: BorderRadius.circular(4.0),
      ),
      padding: const EdgeInsets.fromLTRB(12.0, 20.0, 12.0, 12.0),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18.0),
            child: AgreementCheckBox(
              controller: _checkBoxController,
              text: '전체동의',
              subTitle: '필수 동의 항목 및 수신 동의(선택)에\n전체 동의 합니다.',
              isTitle: true,
              onChange: _refresh,
            ),
          ),
          SizedBox(height: 20.0),
          Container(
            decoration: BoxDecoration(
              color: NeocomixThemeColors.background,
              borderRadius: BorderRadius.circular(2.0),
            ),
            padding: const EdgeInsets.fromLTRB(16.0, 20.0, 0.0, 20.0),
            child: Column(
              children: [
                ...IterableUtils.join<Widget>(
                  [
                    AgreementCheckBox(
                      controller: _checkBoxController,
                      text: '[필수] 멤버십 정기결제 동의',
                      isTitle: false,
                      index: 0,
                      onChange: _refresh,
                    ),
                    AgreementCheckBox(
                      controller: _checkBoxController,
                      text: '[필수] 이용약관 동의',
                      isTitle: false,
                      index: 1,
                      onChange: _refresh,
                      onTap: () {
                        Navigator.of(context)
                            .pushNamed(RouteName.membershipTermsOfService);
                      },
                    ),
                    AgreementCheckBox(
                      controller: _checkBoxController,
                      text: '[선택] 광고성 정보 수신 동의',
                      isTitle: false,
                      index: 2,
                      onChange: _refresh,
                    ),
                  ],
                  separator: SizedBox(height: 12.0),
                ),
              ],
            ),
          ),
        ],
      ),
    );

    final subscribeButton = ThemeFlatButton.primary(
      label: '결제하기',
      enabled: _selectedAllRequiredFields,
      onPressed: () async {
        setState(() {
          _isPending = true;
        });

        final client = GraphQLProvider.of(context).value;
        final result = await client.mutate(
          MutationOptions(
            documentNode: UserMutation.mockSubscribe,
            variables: {
              'promotionEnabled': _checkBoxController.isSelected[2],
            },
          ),
        );

        setState(() {
          _isPending = false;
        });

        if (result.hasException) {
          final environment =
              Provider.of<EnvironmentProvider>(context, listen: false);
          environment.logger.error(result.exception, null);
          showThemeDialog(
            DialogType.OneAction,
            context: context,
            title: '오류',
            content: '오류가 발생했습니다.',
          );
        } else {
          final bool success = result.data['mockSubscribe'] != null;
          if (success) {
            await showThemeDialog(
              DialogType.OneAction,
              context: context,
              title: '멤버십 가입이 완료되었습니다.',
            );
            Navigator.of(context).popUntil(
              (route) =>
                  !route.willHandlePopInternally &&
                  route is ModalRoute &&
                  !(route.settings.name == null ||
                      route.settings.name.contains(RouteName.membershipPrefix)),
            );
          } else {
            showThemeDialog(
              DialogType.OneAction,
              context: context,
              title: '오류',
              content: '이미 구독된 사용자입니다.',
            );
          }
        }
      },
    );

    return Stack(
      children: [
        LayoutBuilder(
          builder: (context, constraint) => SingleChildScrollView(
            child: ConstrainedBox(
              constraints: BoxConstraints(minHeight: constraint.maxHeight),
              child: IntrinsicHeight(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    SizedBox(height: 44.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 32.0),
                      child: Text(
                        '네오코믹스',
                        style: textTheme.headline2
                            .copyWith(fontWeight: FontWeight.normal),
                      ),
                    ),
                    SizedBox(height: 2.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 32.0),
                      child: Text('멤버십 가입하기', style: textTheme.headline1),
                    ),
                    SizedBox(height: 48.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 32.0),
                      child: RichText(
                        text: TextSpan(
                          children: [
                            TextSpan(
                              text: '월 ',
                              style: textTheme.headline5
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                            TextSpan(
                              text: '4,900',
                              style: textTheme.headline5,
                            ),
                            TextSpan(
                              text: '원',
                              style: textTheme.headline5
                                  .copyWith(fontWeight: FontWeight.normal),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 36.0),
                    Spacer(),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: agreementWidget,
                    ),
                    SizedBox(height: 24.0),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20.0),
                      child: subscribeButton,
                    ),
                    SizedBox(height: 28.0),
                  ],
                ),
              ),
            ),
          ),
        ),
        if (_isPending)
          Positioned.fill(
            child: Container(
              color: const Color.fromRGBO(0, 0, 0, 0.6),
              child: wrappedProgressIndicatorLight,
            ),
          )
      ],
    );
  }
}
