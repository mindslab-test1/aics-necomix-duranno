import 'package:duranno_app/components/common/agreement_checkbox.dart';
import 'package:duranno_app/components/common/checkbox_controller.dart';
import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';
import 'package:duranno_app/components/common/dialog.dart';

// const _privacyPolicyFilePath =
//     '/etc/PRIVACY_POLICY.txt'; // where is this file stored?

class UnsubscribeRoute extends AppRoute {
  @override
  String get routeName => RouteName.unsubscribe;

  @override
  String get routeClassName => 'UnsubscribeRoute';

  @override
  String get title => '회원탈퇴';

  @override
  _UnsubscribeRouteState createState() => _UnsubscribeRouteState();
}

class _UnsubscribeRouteState extends AppRouteState<UnsubscribeRoute> {
  CheckBoxController _checkBoxController = CheckBoxController(totalNum: 1);
  bool _unsubscribeEnabled;

  @override
  void initState() {
    _unsubscribeEnabled = false;
    super.initState();
  }

  void _refresh() {
    setState(() {
      _unsubscribeEnabled = !_unsubscribeEnabled;
    });
  }

  Future<void> _unsubscribeSuccess() async {
    //TODO put in function to unsubscribe account
    showThemeDialog(
      DialogType.OneAction,
      context: context,
      title: '회원탈퇴가 완료되었습니다.',
      content: "그동안 네오오디오를 이용해 주셔서 감사합니다.",
      onConfirm: (context) {
        Navigator.of(context).popUntil((route) => route.isFirst);
        Navigator.of(context).pushReplacementNamed(
            RouteName.login); //TODO unsubscribe and go to login page
      },
    );
  }

  @override
  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 24),
                TextLabel.h4('나의 이용 현황'),
                SizedBox(height: 16),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: NeocomixThemeColors.border,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20, horizontal: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                width: 98,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: TextLabel.p(
                                    '현재 이용 멤버십',
                                    color: NeocomixThemeColors.secondaryLight,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Expanded(child: Container()),
                              TextLabel.p('오디오북 이용권',
                                  color: NeocomixThemeColors.black),
                            ],
                          ),
                          SizedBox(height: 8),
                          Row(
                            children: [
                              Container(
                                width: 98,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: TextLabel.p(
                                    '다음 결제일',
                                    color: NeocomixThemeColors.secondaryLight,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Expanded(child: Container()),
                              TextLabel.p('9999.99.99', //TODO put the date
                                  color: NeocomixThemeColors.black),
                            ],
                          )
                        ],
                      )),
                ),
                SizedBox(height: 16),
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: NeocomixThemeColors.border,
                      width: 1,
                    ),
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20, horizontal: 16),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Container(
                                width: 98,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: TextLabel.p(
                                    '나의 오디오북',
                                    color: NeocomixThemeColors.secondaryLight,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Expanded(child: Container()),
                              TextLabel.p('x개', //TODO get value
                                  color: NeocomixThemeColors.black),
                            ],
                          ),
                          SizedBox(height: 8),
                          Row(
                            children: [
                              Container(
                                width: 98,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: TextLabel.p(
                                    '남긴 댓글',
                                    color: NeocomixThemeColors.secondaryLight,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Expanded(child: Container()),
                              TextLabel.p('x개', //TODO get value
                                  color: NeocomixThemeColors.black),
                            ],
                          ),
                          SizedBox(height: 8),
                          Row(
                            children: [
                              Container(
                                width: 98,
                                child: Align(
                                  alignment: Alignment.centerRight,
                                  child: TextLabel.p(
                                    '남긴 별점',
                                    color: NeocomixThemeColors.secondaryLight,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                              Expanded(child: Container()),
                              TextLabel.p('x개', //TODO get value
                                  color: NeocomixThemeColors.black),
                            ],
                          )
                        ],
                      )),
                ),
                SizedBox(height: 36),
                TextLabel.h4('탈퇴 회원 유의사항'),
                //TODO bring the text through textfile
                Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal:
                          12), //already includes 20px horizontal padding
                  child: Column(
                    children: [
                      SizedBox(height: 16),
                      Container(
                        child: Text(
                            '•  탈퇴를 해도 직접 작성하신 댓글과 별점은 자동 삭제되지 않습니다. 노출을 원치 않으실 경우 탈퇴 전 삭제하시기 바랍니다.',
                            style: Theme.of(context).textTheme.subtitle1),
                      ),
                      SizedBox(height: 16),
                      Container(
                        child: Text(
                            '•  탈퇴를 하실 경우, 어떠한 경우에도 기존 이용에 대한 복구는 불가능하며 로그인이 필요한 모든 서비스를 이용하실 수 없습니다.',
                            style: Theme.of(context).textTheme.subtitle1),
                      ),
                      SizedBox(height: 16),
                      Container(
                        child: Text(
                            '•  탈퇴를 하실 경우 기존 결제하신 멤버십 이용권 및 서비스 이용 권한을 포기하는 것으로 간주됩니다. 원치 않으실 경우, 탈퇴를 보류해주세요.',
                            style: Theme.of(context).textTheme.subtitle1),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          SizedBox(height: 36),
          Container(
            decoration: BoxDecoration(
              color: NeocomixThemeColors.background,
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: Container(
                height: 80,
                child: Center(
                  child: AgreementCheckBox(
                    textOverflow: true,
                    text: '회원 탈퇴에 관한 모든 내용을 숙지하였고, 회원 탈퇴를 신청합니다.',
                    controller: _checkBoxController,
                    onChange: _refresh,
                    index: 0,
                  ),
                ),
              ),
            ),
          ),
          SizedBox(height: 36),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Row(
              children: [
                Expanded(
                  child: ThemeFlatButton.secondary(
                      enabled: true,
                      label: '뒤로 가기',
                      onPressed: Navigator.of(context).pop,
                      color: NeocomixThemeColors.white),
                ),
                SizedBox(width: 8),
                Expanded(
                    child: ThemeFlatButton.primary(
                        label: '회원탈퇴하기',
                        color: NeocomixThemeColors.primary,
                        onPressed:
                            _unsubscribeSuccess, //TODO authentication singnout
                        enabled:
                            _unsubscribeEnabled) //pass in value when checckbox is checked
                    ),
              ],
            ),
          ),
          SizedBox(height: 26),
        ],
      ),
    );
  }
}
