import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/responsive_image.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/query/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/route/login_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class MembershipHomeRoute extends AppRoute {
  MembershipHomeRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.membershipHome;

  @override
  String get routeClassName => 'MembershipHomeRoute';

  @override
  String get title => '멤버십 소개';

  @override
  _MembershipHomeRouteState createState() => _MembershipHomeRouteState();
}

class _MembershipHomeRouteState extends AppRouteState<MembershipHomeRoute> {
  @override
  Widget buildBody(BuildContext context) {
    final authentication = Provider.of<AuthenticationProvider>(context);
    final environment = Provider.of<EnvironmentProvider>(context);
    final textTheme = Theme.of(context).textTheme;

    final loginLinkButton = ThemeFlatButton.primary(
      label: '로그인 후 정기 결제하기',
      onPressed: () async {
        await Navigator.of(context).pushNamed(
          RouteName.login,
          arguments: LoginRouteArguments(shouldPopAfterLogin: true),
        );
      },
    );

    final subscribeButton = Query(
      options: QueryOptions(documentNode: UserQuery.me),
      builder: (result, {refetch, fetchMore}) {
        return ThemeFlatButton.primary(
          label: '정기 결제하기',
          enabled: !result.loading &&
              !result.hasException &&
              !result.data['me']['subscribed'],
          onPressed: () {
            Navigator.of(context).pushNamed(RouteName.membershipUserAgreement);
          },
        );
      },
    );

    final actionButton =
        authentication.isAnonymous ? loginLinkButton : subscribeButton;

    return LayoutBuilder(
      builder: (context, constraints) => SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 44.0),
            Padding(
              padding: const EdgeInsets.only(left: 32.0),
              child: Text('네오 오디오 멤버십', style: textTheme.headline1),
            ),
            SizedBox(height: 6.0),
            Padding(
              padding: const EdgeInsets.only(left: 32.0),
              child: Text(
                """베스트 셀러를
오디오북으로 더욱 생생하게""",
                style: textTheme.bodyText1,
              ),
            ),
            SizedBox(height: 8.0),
            ResponsiveImage(
              originalWidth: 360,
              originalHeight: 330,
              imageUri1x:
                  environment.getResourceUri('/images/membership-home@1x.png'),
              imageUri2x:
                  environment.getResourceUri('/images/membership-home@2x.png'),
              imageUri3x:
                  environment.getResourceUri('/images/membership-home@3x.png'),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ...IterableUtils.join<Widget>(
                  [
                    itemWithIcon(
                      icon: ThemeIconData.headphone,
                      iconColor: const Color(0xFF9EA1E8),
                      title: '오디오북',
                      subtitle: '언제든 간편하게',
                    ),
                    itemWithIcon(
                      icon: ThemeIconData.openbook,
                      iconColor: const Color(0xFFC5A7E8),
                      title: '베스트셀러',
                      subtitle: '더욱 생생히 즐기는',
                    ),
                    itemWithIcon(
                      icon: ThemeIconData.membership,
                      iconColor: const Color(0xFFD49AD6),
                      title: '정기 결제',
                      subtitle: '매월 편리하게',
                    ),
                  ],
                  separator: SizedBox(width: 4.0),
                ),
              ],
            ),
            SizedBox(height: 48.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: actionButton,
            ),
            SizedBox(height: 24.0),
            footer(),
          ],
        ),
      ),
    );
  }

  Widget itemWithIcon({
    ThemeIconData icon,
    Color iconColor,
    String title,
    String subtitle,
  }) =>
      Container(
        width: 104.0,
        child: Column(
          children: [
            SizedBox(height: 12.0),
            ThemeIcon(icon, size: 32.0, color: iconColor),
            SizedBox(height: 10.0),
            TextLabel.h6(subtitle, fontWeight: FontWeight.normal),
            TextLabel.p(title, fontWeight: FontWeight.bold),
            SizedBox(height: 12.0),
          ],
        ),
      );

  Widget footer() => Container(
        color: NeocomixThemeColors.background,
        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 40.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              '이용권 유의사항',
              style: Theme.of(context)
                  .textTheme
                  .headline6
                  .copyWith(color: NeocomixThemeColors.secondaryLight),
            ),
            SizedBox(height: 6.0),
            Text(
              """• 네오 이용권은 스마트폰, 태블릿 등 모든 기기에서 사용하실 수 있습니다.
• 오프라인 재생 서비스는 인증된 모바일 디바이스 1대에서 이용 가능합니다.""",
              style: Theme.of(context).textTheme.headline6.copyWith(
                    color: NeocomixThemeColors.secondaryLight,
                    fontWeight: FontWeight.normal,
                  ),
            ),
          ],
        ),
      );
}
