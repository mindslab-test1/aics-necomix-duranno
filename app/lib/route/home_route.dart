import 'package:duranno_app/components/book/book_list_item.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/home/carousel_dot.dart';
import 'package:duranno_app/components/common/tab_page/tab_page_bar.dart';
import 'package:duranno_app/components/common/tab_page/tab_page_controller.dart';
import 'package:duranno_app/components/history/user_history_list.dart';
import 'package:duranno_app/components/home/featured_books_shelf.dart';
import 'package:duranno_app/components/home/footer.dart';
import 'package:duranno_app/components/home/featured_books_carousel.dart';
import 'package:duranno_app/components/home/most_viewed_books_shelf.dart';
import 'package:duranno_app/components/home/tag_filter_selector_bar.dart';
import 'package:duranno_app/components/navigation/drawer.dart';
import 'package:duranno_app/graphql/query/misc.dart';
import 'package:duranno_app/graphql/query/search.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/pagination/paginated_sliver_list.dart';
import 'package:duranno_app/utils/sliver/sliver_footer.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class _SliverFixedHeightHeaderDelegate extends SliverPersistentHeaderDelegate {
  final Widget child;
  final double height;
  _SliverFixedHeightHeaderDelegate({
    @required this.child,
    @required this.height,
  });

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return SizedBox.expand(child: child);
  }

  @override
  double get minExtent => height;

  @override
  double get maxExtent => height;

  @override
  bool shouldRebuild(_SliverFixedHeightHeaderDelegate old) {
    return old.child != child;
  }
}

class HomeRoute extends AppRoute {
  HomeRoute({Key key}) : super(key: key);

  String get routeName => RouteName.home;
  String get routeClassName => 'HomeRoute';
  String get title => null;

  @override
  _HomeRouteState createState() => _HomeRouteState();
}

class _HomeRouteState extends AppRouteState<HomeRoute>
    with TickerProviderStateMixin {
  PageController _pageController;
  TabPageController _tabPageController;
  ValueNotifier<int> _tagFilterIndexNotifier;

  static const _numTabs = 4;

  static const _tabLabels = [
    '홈',
    '베스트셀러',
    '새로나온 책',
    '분야별 도서',
  ];

  static const _tagFilterLabels = [
    '전체',
    '인문',
    '소설',
    '시 \u00b7 에세이',
    '경제 \u00b7 경영',
    '자기계발',
    '예술',
    '과학',
  ];

  static const _tagFilters = [
    null,
    '인문',
    '소설',
    '에세이',
    '경제',
    '자기계발',
    '예술',
    '과학',
  ];

  @override
  void initState() {
    super.initState();

    _pageController = PageController();
    _tabPageController = TabPageController(numItems: 4)
      ..initialize(vsync: this);
    _tagFilterIndexNotifier = ValueNotifier<int>(0);

    _tabPageController.activeListenable.addListener(() {
      setState(() {});
      _pageController.jumpToPage(_tabPageController.active);
    });
  }

  @override
  void dispose() {
    _tagFilterIndexNotifier.dispose();
    _pageController.dispose();
    _tabPageController.dispose();

    super.dispose();
  }

  @override
  Widget buildBody(BuildContext context) {
    final showTagFilterSelector = _tabPageController.active == 3;

    return NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScrolled) => [
        SliverOverlapAbsorber(
          handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
          sliver: SliverPersistentHeader(
            delegate: _SliverFixedHeightHeaderDelegate(
              height: showTagFilterSelector ? 88.0 : 50.0,
              child: Column(
                children: [
                  TabPageBar(
                    controller: _tabPageController,
                    labels: _tabLabels,
                    shrink: true,
                  ),
                  if (showTagFilterSelector)
                    TagFilterSelectorBar(
                      valueNotifier: _tagFilterIndexNotifier,
                      labels: Map<int, String>.fromIterable(
                        List<int>.generate(
                            _tagFilters.length, (index) => index),
                        key: (index) => index,
                        value: (index) => _tagFilterLabels[index],
                      ),
                      options: List<int>.generate(
                          _tagFilters.length, (index) => index),
                    ),
                ],
              ),
            ),
          ),
        ),
      ],
      body: PageView(
        controller: _pageController,
        physics:
            NeverScrollableScrollPhysics(), // do not change tabs with swipe
        children: List<int>.generate(_numTabs, (index) => index)
            .map((index) => _buildTabPage(context, index))
            .toList(),
      ),
    );
  }

  Widget _buildTabPage(BuildContext context, int index) {
    if (index == 0) {
      final homeBannerCarousel = HomeBannersQuery.instance.build(
        context,
        builder: (context, {data, refetch}) {
          return CarouselDot(
              data: data as List<dynamic>, sliderTime: Duration(seconds: 4));
        },
      );

      return CustomScrollView(
        physics: ClampingScrollPhysics(),
        key: PageStorageKey<int>(index),
        slivers: [
          SliverToBoxAdapter(child: homeBannerCarousel),
          SliverToBoxAdapter(child: SizedBox(height: 40.0)),
          SliverToBoxAdapter(child: _buildLabel(context, label: '최근 재생')),
          SliverToBoxAdapter(child: SizedBox(height: 16.0)),
          SliverToBoxAdapter(child: UserHistoryList()),
          SliverToBoxAdapter(child: SizedBox(height: 36.0)),
          SliverToBoxAdapter(child: _buildLabel(context, label: '추천도서')),
          SliverToBoxAdapter(child: SizedBox(height: 16.0)),
          SliverToBoxAdapter(child: FeaturedBooksCarousel()),
          SliverToBoxAdapter(child: SizedBox(height: 36.0)),
          SliverToBoxAdapter(child: _buildLabel(context, label: '추천도서')),
          SliverToBoxAdapter(child: SizedBox(height: 16.0)),
          SliverToBoxAdapter(child: FeaturedBooksShelf()),
          SliverToBoxAdapter(child: SizedBox(height: 36.0)),
          SliverToBoxAdapter(child: _buildLabel(context, label: '베스트셀러')),
          SliverToBoxAdapter(child: SizedBox(height: 16.0)),
          SliverToBoxAdapter(child: MostViewedBooksShelf()),
          SliverToBoxAdapter(child: SizedBox(height: 52.0)),
          SliverFooter(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Footer(),
            ),
          ),
        ],
      );
    }

    return ValueListenableBuilder(
      valueListenable: _tagFilterIndexNotifier,
      builder: (context, tagFilter, child) {
        final label = () {
          switch (index) {
            case 1:
              return '주간 베스트셀러';
            case 2:
              return '새로나온 책';
            case 3:
              return '${_tagFilterLabels[tagFilter]} 베스트';
          }
        }();

        final queryVariables = () {
          switch (index) {
            case 1:
              return {
                'orderBy': 'weeklyViews',
              };
            case 2:
              return {
                'orderBy': 'updatedAt',
              };
            case 3:
              return {
                'tag': _tagFilters[tagFilter],
                'orderBy': 'weeklyViews',
              };
          }
        }();

        final pageIndex = index + _tagFilterIndexNotifier.value;

        return PaginatedSliverList<dynamic, dynamic, String>(
          queryOptions: QueryOptions(
            documentNode: SearchQuery.books,
            variables: queryVariables,
          ),
          itemsSelector: (data) => data['searchBooks']['items'],
          cursorSelector: (data) => data['searchBooks']['cursor'],
          mergeFetchMoreResult: (prev, cur) => {
            'searchBooks': {
              'items': [
                ...prev['searchBooks']['items'],
                ...cur['searchBooks']['items'],
              ],
              'cursor': cur['searchBooks']['cursor']
            },
          },
          itemBuilder: ({itemData, key, refetch}) => Padding(
            key: key,
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: BookListItem(data: itemData),
          ),
          itemKeySelector: (itemData) => itemData['id'],
          pageStorageKey: PageStorageKey<int>(pageIndex),
          headerWidget: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: TextLabel.h4(label, expanded: true),
          ),
          emptyLabelWidget: Center(
            child: TextLabel.p(
              '검색 결과가 없습니다.',
              color: NeocomixThemeColors.secondaryLight,
            ),
          ),
          marginTop: 36.0,
          contentMarginTop: 16.0,
          contentMarginBottom: 52.0,
          footerWidget:
              Align(alignment: Alignment.bottomCenter, child: Footer()),
        );
      },
    );
  }

  Widget _buildLabel(BuildContext context, {String label}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: TextLabel.h4(label, expanded: true),
    );
  }

  @override
  Widget buildDrawer(BuildContext context) {
    return NavigationDrawer();
  }
}
