import 'package:duranno_app/components/book/book_list_item.dart';
import 'package:duranno_app/components/common/input_field.dart';
import 'package:duranno_app/components/common/tab_page/tab_page.dart';
import 'package:duranno_app/components/common/tab_page/tab_page_bar.dart';
import 'package:duranno_app/components/common/tab_page/tab_page_controller.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/graphql/query/search.dart';
import 'package:duranno_app/provider/local_storage_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/pagination/paginated_sliver_list.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class SearchRoute extends AppRoute {
  SearchRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.search;

  @override
  String get routeClassName => 'SearchRoute';

  @override
  String get title => '검색';

  @override
  _SearchRouteState createState() => _SearchRouteState();
}

class _SearchRouteState extends AppRouteState<SearchRoute>
    with TickerProviderStateMixin {
  final ValueNotifier<String> _keywordValueNotifier =
      ValueNotifier<String>(null);

  TextEditingController _searchInputController;
  TabPageController _tabPageController;
  bool _showSearchResult = false;
  PageStorageBucket _pageStorageBucket;
  List<dynamic> _recentList; //Contains List of _SearchAndTimeInfo
  List<dynamic>
      _searchAndTimeInfo; //Index 0: (Search Word) Index 1: (Date Time)
  bool _resetList;
  AuthenticationProvider _authentication;

  @override
  void initState() {
    super.initState();
    _searchAndTimeInfo = List.empty(growable: true);
    _resetList = false;
    _searchInputController = TextEditingController();
    _tabPageController = TabPageController(numItems: 3);
    _tabPageController.initialize(vsync: this);
    _pageStorageBucket = PageStorageBucket();
  }

  @override
  void dispose() {
    _searchInputController.dispose();
    _tabPageController.dispose();
    _keywordValueNotifier.dispose();

    super.dispose();
  }

  Future<List<dynamic>> _getFutureList() async {
    if (await Provider.of<LocalStorageProvider>(context, listen: false).getList(
            LocalStorageKey.recentSearchId + _authentication.user.uid) ==
        null) {
      return null;
    } else {
      return await Provider.of<LocalStorageProvider>(context, listen: false)
          .getList(LocalStorageKey.recentSearchId + _authentication.user.uid);
    }
  }

  @override
  Widget buildBody(BuildContext context) {
    _authentication = Provider.of<AuthenticationProvider>(context);
    final searchInputField = InputField(
      controller: _searchInputController,
      onEditingComplete: _onEditingSearchInputFieldComplete,
      icon: ThemeIconData.search,
      clearable: true,
      hintText: '콘텐츠 제목, 저자',
      errorMessage: null,
    );

    return NestedScrollView(
      headerSliverBuilder: (context, innerboxIsScrolled) => [
        SliverOverlapAbsorber(
          handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
          sliver: SliverList(
            delegate: SliverChildListDelegate([
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: searchInputField,
              ),
              if (_showSearchResult) SizedBox(height: 8.0),
              if (_showSearchResult)
                ValueListenableBuilder(
                  valueListenable: _keywordValueNotifier,
                  builder: (context, value, child) => _buildSearchTabBar(
                    context,
                    keyword: value,
                  ),
                ),
              if (_showSearchResult) SizedBox(height: 18.0),
            ]),
          ),
        )
      ],
      body: _showSearchResult
          ? PageStorage(
              bucket: _pageStorageBucket,
              child: ValueListenableBuilder<String>(
                valueListenable: _keywordValueNotifier,
                builder: (context, keyword, child) => TabPage(
                  controller: _tabPageController,
                  builder: _showSearchResult
                      ? _buildSearchedBookList
                      : (context, index) => CustomScrollView(),
                ),
              ),
            )
          : _buildRecentList(),
    );
  }

  Widget _showListExistLabel(int index) {
    if (index == 0) {
      return Column(children: [
        SizedBox(
          height: 16,
        ),
        if (_recentList != null)
          _recentList.isEmpty
              ? Container()
              : Row(children: [
                  TextLabel.h6(
                    '최근 검색어',
                    color: NeocomixThemeColors.secondaryLight,
                    fontWeight: FontWeight.normal,
                  ),
                  Expanded(child: Container()),
                  GestureDetector(
                    onTap: () {
                      _clearLocalStorage();
                    },
                    child: TextLabel.h6(
                      '전체 삭제',
                      color: NeocomixThemeColors.secondaryLight,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ]),
        SizedBox(height: 14),
      ]);
    }
    return Container();
  }

  Widget _recentListEmptyText() {
    _recentList = List.empty(growable: true);
    return Padding(
      padding: EdgeInsets.only(top: 52),
      child: Align(
        alignment: Alignment.topCenter,
        child: TextLabel.p(
          '최근 검색어가 없습니다.',
          color: NeocomixThemeColors.secondaryLight,
        ),
      ),
    );
  }

  Widget _buildRecentList() {
    return FutureBuilder(
      future: _getFutureList(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        Widget children;
        if (snapshot.hasData && !_resetList) {
          _recentList = snapshot.data;
          children = ListView.builder(
            itemCount: _recentList.length + 1,
            itemBuilder: (context, index) => index != _recentList.length
                ? Padding(
                    padding: EdgeInsets.only(bottom: 16),
                    child: Column(
                      children: [
                        _showListExistLabel(index),
                        Row(
                          children: [
                            Expanded(
                              child: GestureDetector(
                                  child: TextLabel.p(
                                    _recentList[_recentList.length - index - 1]
                                        [0],
                                    expanded: true,
                                  ),
                                  onTap: () {
                                    _searchInputController.text = _recentList[
                                            _recentList.length - index - 1][0]
                                        .toString();
                                    _onEditingSearchInputFieldComplete();
                                  }),
                            ),
                            SizedBox(width: 36),
                            GestureDetector(
                                child: Icon(
                                  Icons.clear,
                                  color: NeocomixThemeColors.secondaryLight,
                                  size: 24,
                                ),
                                onTap: () {
                                  setState(() {
                                    _recentList.removeAt(
                                        _recentList.length - index - 1);
                                  });
                                  _recentList.length == 0
                                      ? _clearLocalStorage()
                                      : _updateLocalStorate();
                                }),
                          ],
                        ),
                      ],
                    ),
                  )
                : Center(
                    child: TextLabel.p(
                      '최근 검색어는 10개까지 제공됩니다.',
                      color: NeocomixThemeColors.secondaryLight,
                    ),
                  ),
          );
        } else {
          children = _recentListEmptyText();
        }

        return Padding(
          padding: EdgeInsets.only(
            left: 20,
            right: 20,
          ),
          child: children,
        );
      },
    );
  }

  Widget _buildSearchTabBar(
    BuildContext context, {
    String keyword,
  }) {
    return Query(
      options: QueryOptions(
        documentNode: SearchQuery.keywordBookCount,
        variables: {
          'keyword': keyword,
        },
      ),
      builder: (result, {fetchMore, refetch}) {
        final hasData = result.data != null;
        final labels = hasData
            ? [
                '전체(${result.data['countAll'] ?? 0})',
                '제목(${result.data['countTitle'] ?? 0})',
                '저자(${result.data['countAuthor'] ?? 0})',
              ]
            : [
                '전체',
                '제목',
                '저자',
              ];

        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TabPageBar(
              controller: _tabPageController,
              labels: labels,
              shrink: true,
              labelTextStyle: Theme.of(context).textTheme.subtitle1,
            ),
          ],
        );
      },
    );
  }

  Widget _buildSearchedBookList(BuildContext context, int index) {
    return PaginatedSliverList<dynamic, dynamic, String>(
      queryOptions: QueryOptions(
        documentNode: SearchQuery.books,
        variables: {
          'keyword': _keywordValueNotifier.value,
          'keywordScope': [
            if (index == 0 || index == 1) 'title',
            if (index == 0 || index == 2) 'authorName',
          ],
          'limit': 20,
        },
      ),
      itemsSelector: (data) => data['searchBooks']['items'],
      cursorSelector: (data) => data['searchBooks']['cursor'],
      mergeFetchMoreResult: (prev, cur) => {
        'searchBooks': {
          'items': [
            ...prev['searchBooks']['items'],
            ...cur['searchBooks']['items']
          ],
          'cursor': cur['searchBooks']['cursor'],
        }
      },
      itemBuilder: ({itemData, key, refetch}) => Padding(
        key: key,
        padding: const EdgeInsets.symmetric(horizontal: 32.0),
        child: BookListItem(data: itemData),
      ),
      itemKeySelector: (itemData) => itemData['id'],
      emptyLabelWidget: Padding(
        padding: EdgeInsets.only(top: 52),
        child: Center(
          child: TextLabel.p(
            '검색 결과가 없습니다.',
            color: NeocomixThemeColors.secondaryLight,
          ),
        ),
      ),
      isScrollViewNested: true,
      pageStorageKey: PageStorageKey<int>(index),
    );
  }

  void _addToLocalStorage() {
    if (_recentList.any((e) => e.contains(_searchInputController.text))) {
      _recentList.removeAt(_recentList
          .indexWhere((e) => e.contains(_searchInputController.text)));
    } else {
      if (_recentList.length == 10) {
        _recentList.removeAt(0);
      }
    }

    _searchAndTimeInfo.add(_searchInputController.text);
    _searchAndTimeInfo.add(DateTime.now().toString());

    _recentList.add(_searchAndTimeInfo);

    Provider.of<LocalStorageProvider>(context, listen: false).setList(
        LocalStorageKey.recentSearchId + _authentication.user.uid, _recentList);
    setState(() {
      _resetList = false;
    });
    _searchAndTimeInfo = List.empty(growable: true);
  }

  void _clearLocalStorage() {
    Provider.of<LocalStorageProvider>(context, listen: false).setList(
        LocalStorageKey.recentSearchId + _authentication.user.uid, null);
    setState(() {
      _resetList = true;
    });
  }

  void _updateLocalStorate() {
    Provider.of<LocalStorageProvider>(context, listen: false).setList(
        LocalStorageKey.recentSearchId + _authentication.user.uid, _recentList);
  }

  void _onEditingSearchInputFieldComplete() {
    if (_searchInputController.text.isNotEmpty) {
      setState(() {
        _showSearchResult = true;
      });
      _keywordValueNotifier.value = _searchInputController.text;
      _addToLocalStorage();
    } else {
      setState(() {
        _showSearchResult = false;
      });
    }
  }
}
