import 'dart:async';

import 'package:duranno_app/components/book/book_detail_banner.dart';
import 'package:duranno_app/components/book/book_info.dart';
import 'package:duranno_app/components/book/book_table_of_contents.dart';
import 'package:duranno_app/components/book/comment/book_comment_list.dart';
import 'package:duranno_app/components/common/dialog.dart';
import 'package:duranno_app/components/navigation/app_header.dart';
import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/button/raised_button.dart';
import 'package:duranno_app/components/common/snack_bar.dart';
import 'package:duranno_app/components/common/tab_page/tab_page_bar.dart';
import 'package:duranno_app/components/common/tab_page/tab_page_controller.dart';
import 'package:duranno_app/graphql/mutation/playlist.dart';
import 'package:duranno_app/graphql/query/book.dart';
import 'package:duranno_app/graphql/query/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/route/audio/audio_player_route.dart';
import 'package:duranno_app/route/login_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/constants.dart';
import 'package:duranno_app/utils/lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widget.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

final _addToPlaylistSuccessSnackBar = ThemeSnackBar(
  content: Text('나의 오디오북에 담겼습니다.'),
);

final _addToPlaylistFailureSnackBar = ThemeSnackBar(
  content: Text('오류: 나의 오디오북에 담지 못했습니다.'),
);

@immutable
class BookDetailRouteArguments {
  final String bookId;
  BookDetailRouteArguments({@required this.bookId}) : assert(bookId != null);

  factory BookDetailRouteArguments.fromJson(dynamic json) =>
      BookDetailRouteArguments(
        bookId: json['bookId'],
      );
}

class BookDetailRoute extends AppRoute {
  BookDetailRoute({
    Key key,
  }) : super(key: key);

  String get routeName => RouteName.bookDetail;
  String get routeClassName => 'BookDetailRoute';
  String get title => '';

  Brightness get brightness => Brightness.dark;
  bool get useCustomAppBar => true;

  @override
  _BookDetailRouteState createState() => _BookDetailRouteState();
}

class _BookDetailRouteState extends AppRouteState<BookDetailRoute>
    with TickerProviderStateMixin {
  TabPageController _tabPageController;
  PageStorageBucket _pageStorageBucket;

  AuthenticationProvider _authentication;

  ValueNotifier<double> _scrollExtentNotifier;

  @override
  void initState() {
    super.initState();

    _tabPageController = TabPageController(
      numItems: 2,
      initialActive: 0,
    );
    _tabPageController.initialize(vsync: this);

    _pageStorageBucket = PageStorageBucket();

    _scrollExtentNotifier = ValueNotifier<double>(0.0);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _authentication = Provider.of<AuthenticationProvider>(context);
  }

  @override
  void dispose() {
    _tabPageController.dispose();
    _scrollExtentNotifier.dispose();

    super.dispose();
  }

  Future<void> redirectToLogin(BuildContext context) {
    return showThemeDialog(
      DialogType.TwoActions,
      context: context,
      title: '로그인 및 멤버십 가입 후 이용 가능합니다.',
      content: "로그인 페이지로 이동하시겠습니까?",
      cancelLabel: '닫기',
      confirmLabel: '확인',
      overrideDefaultConfirmCallback: true,
      onConfirm: (context) async {
        await Navigator.of(context).pushNamed(RouteName.login,
            arguments: LoginRouteArguments(shouldPopAfterLogin: true));

        if (!_authentication.isAnonymous) {
          Navigator.of(context).pop();
        }
      },
      onCancel: (context) {},
    );
  }

  Future<void> redirectToMembership(BuildContext context) async {
    return showThemeDialog(
      DialogType.TwoActions,
      context: context,
      title: '멤버십 가입 후 이용 가능합니다.',
      content: "멤버십 가입 페이지로 이동하시겠습니까?",
      cancelLabel: '닫기',
      confirmLabel: '확인',
      overrideDefaultConfirmCallback: true,
      onConfirm: (context) async {
        await Navigator.of(context).pushNamed(RouteName.membershipHome);

        final me = await MeQuery.instance
            .execute(context)
            .then((result) => result.data);
        final bool subscribed = (me ?? const {})['subscribed'] ?? false;

        if (subscribed) {
          Navigator.of(context).pop();
        }
      },
      onCancel: (context) {},
    );
  }

  Future<void> maybeLinkToAudioPlayerRoute(
    BuildContext context, {
    @required String bookId,
    int sequence,
  }) async {
    assert(bookId != null);

    if (_authentication.isAnonymous) {
      redirectToLogin(context);
      return;
    }

    final me = await MeQuery.instance
        .execute(context)
        .then((result) => (result.data ?? const {})['me']);
    final bool subscribed = (me ?? const {})['subscribed'] ?? false;
    if (!subscribed) {
      redirectToMembership(context);
      return;
    }

    final audioPlayerRouteArgs = AudioPlayerRouteArguments(
      bookId: bookId,
      sequence: sequence,
    );

    return Navigator.of(context).pushNamed(
      RouteName.audioPlayer,
      arguments: audioPlayerRouteArgs,
    );
  }

  @override
  Widget buildBody(BuildContext context) {
    final BookDetailRouteArguments args =
        ModalRoute.of(context).settings.arguments;

    final linkToAudioPlayerRaisedButton = ThemeRaisedButton(
      label: '오디오북 재생하기',
      icon: ThemeIconData.headphone,
      onPressed: () =>
          maybeLinkToAudioPlayerRoute(context, bookId: args.bookId),
    );

    final linkToAudioPlayerFlatButton = ThemeFlatButton.primary(
      label: '지금 재생하기',
      icon: ThemeIconData.headphone,
      onPressed: () =>
          maybeLinkToAudioPlayerRoute(context, bookId: args.bookId),
      color: NeocomixThemeColors.primary,
      contentPadding: const EdgeInsets.symmetric(vertical: 18.0),
    );

    final addToPlaylistButton = AddToPlaylistMutation.instance.build(
      context,
      builder: ({runMutation, result}) => ThemeRaisedButton(
        label: '내 오디오북에 담기',
        icon: ThemeIconData.headphone,
        onPressed: runMutation,
      ),
      args: {
        'bookId': args.bookId,
      },
      onCompleted: (data) {
        Scaffold.of(context).showSnackBar(_addToPlaylistSuccessSnackBar);
      },
      onError: (error) {
        Scaffold.of(context).showSnackBar(_addToPlaylistFailureSnackBar);
      },
    );

    final navigateToPlaylistButton = ThemeFlatButton.secondary(
      label: '나의 오디오북 열기',
      onPressed: () {
        Navigator.of(context).pushNamed(RouteName.playlistDetails);
      },
      contentPadding: const EdgeInsets.symmetric(vertical: 18.0),
    );

    final signedPrimaryAction = Query(
      options: QueryOptions(
        documentNode: BookQuery.primaryAction,
        variables: {
          'bookId': args.bookId,
        },
      ),
      builder: (result, {fetchMore, refetch}) {
        if (result.loading) {
          return SizedBox(
            height: 56.0,
            child: Center(child: wrappedProgressIndicatorDark),
          );
        }

        if (result.hasException) {
          final environment = Provider.of<EnvironmentProvider>(context);
          environment.logger.error(result.exception, null);
          return SizedBox(
            height: 56.0,
            child: Center(child: errorWidget),
          );
        }

        final bool subscribed = result.data['me']['subscribed'];
        final bool isInMyPlaylist = result.data['book']['isInMyPlaylist'];

        return Row(
          children: [
            if (subscribed && isInMyPlaylist) ...[
              Expanded(child: navigateToPlaylistButton),
              SizedBox(width: 8.0),
              Expanded(child: linkToAudioPlayerFlatButton),
            ] else if (subscribed && !isInMyPlaylist)
              Expanded(child: addToPlaylistButton)
            else
              Expanded(child: linkToAudioPlayerRaisedButton),
          ],
        );
      },
    );

    final primaryAction = _authentication.isEmailVerified
        ? signedPrimaryAction
        : linkToAudioPlayerRaisedButton;

    final tabPageSelector = TabPageBar(
      controller: _tabPageController,
      labels: ['도서정보', '목차'],
      verticalPadding: 12.0,
    );

    final scrollView = NestedScrollView(
      headerSliverBuilder: (context, innerBoxIsScrolled) => [
        SliverOverlapAbsorber(
          handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
          sliver: SliverList(
            delegate: SliverChildListDelegate([
              BookDetailBanner(bookId: args.bookId),
              Container(
                color: NeocomixThemeColors.background,
                padding: contentPadding,
                height: 96.0,
                child: Center(child: primaryAction),
              ),
              tabPageSelector,
              Divider(color: NeocomixThemeColors.border, height: 1.0)
            ]),
          ),
        ),
      ],
      body: PageStorage(
        bucket: _pageStorageBucket,
        child: ValueListenableBuilder(
          valueListenable: _tabPageController.activeListenable,
          builder: (context, index, child) => LazyLoadScrollView(
            child: CustomScrollView(
              key: PageStorageKey<int>(index),
              physics: ClampingScrollPhysics(),
              slivers: [
                SliverOverlapInjector(
                  handle:
                      NestedScrollView.sliverOverlapAbsorberHandleFor(context),
                ),

                /* book info tab */
                if (index == 0)
                  SliverToBoxAdapter(child: BookInfo(bookId: args.bookId)),
                if (index == 0) BookCommentList(bookId: args.bookId),
                if (index == 0)
                  SliverToBoxAdapter(child: SizedBox(height: 52.0)),

                /* table of contents tab */
                if (index == 1)
                  BookTableOfContents(
                    bookId: args.bookId,
                    maybeLinkToAudioPlayerRoute: maybeLinkToAudioPlayerRoute,
                  ),
              ],
            ),
          ),
        ),
      ),
    );

    final appHeader = Query(
      options: QueryOptions(
        documentNode: BookQuery.header,
        variables: {
          'bookId': args.bookId,
        },
      ),
      builder: (result, {refetch, fetchMore}) {
        if (result.data == null) {
          return AppHeader(
            title: '',
            opaquedWhenScrolled: true,
            scrollExtentListener: _scrollExtentNotifier,
          );
        }

        final String bookTitle =
            getSafeField(result.data, fields: ['book', 'title']) ?? '';

        return AppHeader(
          title: bookTitle,
          opaquedWhenScrolled: true,
          scrollExtentListener: _scrollExtentNotifier,
        );
      },
    );

    return NotificationListener<ScrollNotification>(
      onNotification: (notification) {
        if (notification is ScrollUpdateNotification) {
          _scrollExtentNotifier.value = notification.metrics.extentBefore;
        }

        return false;
      },
      child: Stack(
        children: [
          scrollView,
          Column(
            children: [
              appHeader,
              Expanded(child: IgnorePointer(child: Container())),
            ],
          ),
        ],
      ),
    );
  }
}
