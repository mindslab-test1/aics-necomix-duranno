import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:package_info/package_info.dart';

class SettingsRoute extends AppRoute {
  SettingsRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.settings;

  @override
  String get routeClassName => 'SettingsRoute';

  @override
  String get title => '환경설정';

  @override
  _SettingsRouteState createState() => _SettingsRouteState();
}

class _SettingsRouteState extends AppRouteState<SettingsRoute> {
  @override
  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          SizedBox(height: 36.0),
          ...IterableUtils.join<Widget>(
            [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 32.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    TextLabel.p(
                      '버전정보',
                      color: NeocomixThemeColors.secondaryDark,
                    ),
                    FutureBuilder(
                        future: PackageInfo.fromPlatform()
                            .then((info) => info.version),
                        builder: (context, snapshot) {
                          if (!snapshot.hasData) {
                            return Container();
                          }

                          return TextLabel.p(
                            snapshot.data,
                            color: NeocomixThemeColors.secondaryLight,
                          );
                        }),
                  ],
                ),
              ),
            ],
            separator: SizedBox(height: 24.0),
          ),
        ],
      ),
    );
  }
}
