import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

const _privacyPolicyFilePath = '/etc/PRIVACY_POLICY.txt';

class PrivacyPolicyRoute extends AppRoute {
  PrivacyPolicyRoute({Key key}) : super(key: key);

  @override
  String get routeName => RouteName.privacyPolicy;

  @override
  String get routeClassName => 'PrivacyPolicyRoute';

  @override
  String get title => '개인정보 처리방침';

  @override
  _PrivacyPolicyRouteState createState() => _PrivacyPolicyRouteState();
}

class _PrivacyPolicyRouteState extends AppRouteState<PrivacyPolicyRoute> {
  @override
  Widget buildBody(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final environment = Provider.of<EnvironmentProvider>(context);

    final contentWidget = FutureBuilder<String>(
      future: environment.parseResource(_privacyPolicyFilePath),
      builder: (context, snapshot) {
        if (snapshot.error != null) {
          return Center(child: errorWidget);
        }

        if (snapshot.data == null) {
          return Center(child: wrappedProgressIndicatorDark);
        }

        return Text(
          snapshot.data,
          style: textTheme.subtitle1.copyWith(
            color: NeocomixThemeColors.secondaryDark,
          ),
        );
      },
    );

    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(32.0, 24.0, 32.0, 52.0),
        child: contentWidget,
      ),
    );
  }
}
