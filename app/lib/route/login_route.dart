//Daniel Kang 09/17/20
//Login Page
import 'package:duranno_app/components/common/dialog.dart';
import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/input_field.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/navigation/app_header.dart';
import 'package:duranno_app/graphql/query/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/register/user_agreement_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/duranno/social_button.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/theme/neocomix/logo.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

final _loginErrorDialog = ThemeDialog.oneAction(
  title: '로그인 중 오류가 발생했습니다.',
  content: '다시 시도해 주세요.',
  confirmLabel: '확인',
  overrideDefaultConfirmCallback: false,
);

@immutable
class LoginRouteArguments {
  final bool shouldPopAfterLogin;
  LoginRouteArguments({
    @required this.shouldPopAfterLogin,
  }) : assert(shouldPopAfterLogin != null);

  factory LoginRouteArguments.fromJson(dynamic json) => LoginRouteArguments(
        shouldPopAfterLogin: json['shouldPopAfterLogin'],
      );

  @override
  String toString() => '{ shouldPopAfterLogin: $shouldPopAfterLogin }';
}

class LoginRoute extends AppRoute {
  String get routeName => RouteName.login;
  String get routeClassName => 'Login';

  @override
  _LoginRouteState createState() => _LoginRouteState();

  @override
  bool get useCustomAppBar => true;

  @override
  String get title => null;
}

class _LoginRouteState extends AppRouteState<LoginRoute> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  AuthenticationProvider _authentication;
  bool _isInputFieldNull;

  String _nameErrorText;
  String _passwordErrorText;

  bool _isPending = false;

  void _checkInputFieldNull() {
    if (_nameController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty) {
      _isInputFieldNull = false;
    } else {
      _isInputFieldNull = true;
    }
    setState(() {});
  }

  @override
  void initState() {
    _isInputFieldNull = true;
    _passwordController.addListener(_checkInputFieldNull);
    _nameController.addListener(_checkInputFieldNull);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    _authentication =
        Provider.of<AuthenticationProvider>(context, listen: false);

    super.didChangeDependencies();
  }

  @override
  void dispose() {
    _nameController.removeListener(_checkInputFieldNull);
    _passwordController.removeListener(_checkInputFieldNull);
    _nameController.dispose();
    _passwordController.dispose();

    super.dispose();
  }

  Future<void> _performSignInTask(Future<void> Function() task) async {
    setState(() {
      _isPending = true;
    });
    await task().then((_) async {
      if (!_authentication.isSigned) {
        return;
      }

      final args =
          ModalRoute.of(context).settings.arguments as LoginRouteArguments;

      /* anonymous sign-in */
      if (_authentication.isAnonymous) {
        if (args != null && args.shouldPopAfterLogin) {
          Navigator.of(context).pop();
        } else {
          Navigator.of(context).pushReplacementNamed(RouteName.home);
        }
        return;
      }

      /* corner case: email not verified yet */
      if (!_authentication.isEmailVerified) {
        Navigator.of(context).pushNamed(
          RouteName.registrationEmailConfirmation,
          arguments: {'allowedPromotionEmail': false},
        );
        return;
      }

      /* registration for social account users */
      if (_authentication.user.providerData.length == 1 &&
          _authentication.isSocialAccount) {
        final result = await MeQuery.instance.execute(
          context,
          onError: (err) {
            showDialog(
              context: context,
              builder: (context) => _loginErrorDialog,
            );
          },
        );

        if (result.hasException) {
          return;
        }

        final bool registered = result.data['me']['registered'];

        if (!registered) {
          Navigator.of(context).pushReplacementNamed(
            RouteName.registrationUserAgreement,
            arguments: args != null
                ? UserAgreementRouteArguments(
                    shouldPopAfterRegistration: args.shouldPopAfterLogin,
                  )
                : null,
          );
          return;
        }
      }

      /* general case: redirection */
      if (args != null && args.shouldPopAfterLogin) {
        Navigator.of(context).pop();
      } else {
        Navigator.of(context).pushReplacementNamed(RouteName.home);
      }
    }).catchError((err) {
      showDialog(
        context: context,
        builder: (context) => _loginErrorDialog,
      );
    }).whenComplete(() {
      setState(() {
        _isPending = false;
      });
    });
  }

  @override
  AppHeader buildAppBar(BuildContext context) {
    final args =
        ModalRoute.of(context).settings.arguments as LoginRouteArguments;
    if (args != null) {
      if (args.shouldPopAfterLogin != null ||
          args.shouldPopAfterLogin != false) {
        return AppHeader(
          title: "",
          useLeadingButton: false,
          actionButtonWidget: ThemeIconButton(
            icon: ThemeIconData.close,
            iconColor: Colors.black,
            iconSize: 20.0,
            buttonSize: 48.0,
            onPressed: () => Navigator.of(context).pop(),
          ),
        );
      }
    }

    return null;
  }

  @override
  Widget buildBody(BuildContext context) {
    final logoWidget = NeocomixLogo(width: 142, height: 40);

    final emailInputField = InputField(
      controller: _nameController,
      errorMessage: _nameErrorText,
      textLabel: '이메일 주소',
      isPassword: false,
      clearable: true,
      onEditingComplete: _checkInputFieldNull,
    );

    final passwordInputField = InputField(
      controller: _passwordController,
      errorMessage: _passwordErrorText,
      textLabel: '비밀번호',
      isPassword: true,
      clearable: true,
      onEditingComplete: _checkInputFieldNull,
    );

    final loginButton = Container(
      height: 50,
      width: double.infinity,
      child: ThemeFlatButton(
        label: '로그인',
        onPressed: () async {
          setState(() {
            _nameErrorText = _loginInputError(
                authenticationProvider: _authentication,
                name: _nameController,
                password: _passwordController,
                isName: true);
            _passwordErrorText = _loginInputError(
                authenticationProvider: _authentication,
                name: _nameController,
                password: _passwordController,
                isName: false);
          });
          if (_nameErrorText == null && _passwordErrorText == null) {
            await _performSignInTask(() async {
              try {
                await _authentication.signInWithLocalUser(
                    email: _nameController.text,
                    password: _passwordController.text);
              } on DurannoException catch (e) {
                if (e.code == ExceptionCode.AuthInvalidEmail) {
                  setState(() {
                    _nameErrorText = '올바른 이메일 형식을 입력해주세요.';
                    _passwordErrorText = '';
                  });
                } else if (e.code == ExceptionCode.AuthWrongPassword) {
                  setState(() {
                    _nameErrorText = '';
                    _passwordErrorText = '이메일 및 비밀번호를 다시 확인해주세요.';
                  });
                } else if (e.code == ExceptionCode.AuthUserNotFound) {
                  setState(() {
                    _nameErrorText = '';
                    _passwordErrorText = '계정이 존재하지 않습니다.';
                  });
                } else if (e.code == ExceptionCode.AuthWrongPassword) {
                  setState(() {
                    _nameErrorText = '';
                    _passwordErrorText = '비활성화된 계정입니다.';
                  });
                } else {
                  throw e;
                }
              }
            });
          }
        },
        type: FlatButtonType.Primary,
        enabled: !_isInputFieldNull,
      ),
    );

    final findPasswordButton = GestureDetector(
      child: TextLabel.h6(
        '비밀번호 찾기',
        fontWeight: FontWeight.normal,
        color: NeocomixThemeColors.secondaryLight,
      ),
      onTap: () {
        Navigator.of(context).pushNamed(RouteName.findPassword);
      },
    );

    final anonymouslySignInButton = GestureDetector(
      child: TextLabel.p('게스트 입장', fontWeight: FontWeight.w500),
      onTap: () => _performSignInTask(_authentication.signInAnonymously),
    );

    final appleSignInButton = DurannoSocialLoginButton(
      type: DurannoSocialLoginButtonType.apple,
      onPressed: () => _performSignInTask(_authentication.signInWithApple),
    );

    final googleSignInButton = DurannoSocialLoginButton(
      type: DurannoSocialLoginButtonType.google,
      onPressed: () => _performSignInTask(_authentication.signInWithGoogle),
    );

    final registerLabel = TextLabel.h6(
      '서비스가 처음이신가요?',
      fontWeight: FontWeight.normal,
      color: NeocomixThemeColors.secondaryLight,
    );

    final registerLink = GestureDetector(
      child: TextLabel.p('회원가입', fontWeight: FontWeight.w500),
      onTap: () {
        Navigator.of(context).pushNamed(RouteName.registrationUserAgreement);
      },
    );

    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
          _checkInputFieldNull();
        }
      },
      child: LayoutBuilder(
        builder: (context, constraint) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child: Stack(
                children: [
                  Column(
                    children: [
                      SizedBox(height: 100),
                      //TITLE
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.only(left: 32),
                          child: logoWidget,
                        ),
                      ),

                      SizedBox(height: 80),

                      //INPUTFIELDS
                      Padding(
                        padding: EdgeInsets.only(left: 32, right: 32),
                        child: Column(
                          children: [
                            emailInputField,
                            SizedBox(height: 10.0),
                            passwordInputField,
                            SizedBox(height: 36.0),
                            loginButton,
                            SizedBox(height: 16.0),
                            Align(
                              alignment: Alignment.centerRight,
                              child: findPasswordButton,
                            ),
                          ],
                        ),
                      ),

                      SizedBox(height: 48),

                      //SOCIAL LOGIN BUTTONS
                      Padding(
                        padding: EdgeInsets.only(left: 32, right: 32),
                        child: Row(
                          children: [
                            Expanded(child: appleSignInButton),
                            SizedBox(width: 8),
                            Expanded(child: googleSignInButton),
                          ],
                        ),
                      ),

                      SizedBox(height: 16),

                      anonymouslySignInButton,

                      SizedBox(height: 16.0),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          registerLabel,
                          SizedBox(width: 6),
                          registerLink,
                        ],
                      ),
                      SizedBox(height: 32),

                      Spacer(),
                    ],
                  ),
                  if (_isPending)
                    Positioned.fill(
                      child: Container(
                        color: const Color.fromRGBO(0, 0, 0, 0.6),
                        child: Center(child: wrappedProgressIndicatorLight),
                      ),
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

String _loginInputError(
    {AuthenticationProvider authenticationProvider,
    TextEditingController name,
    TextEditingController password,
    bool isName}) {
  //Username checker
  if (name.text.isEmpty && isName) {
    return '모든 칸을 입력해주세요.';
  } else if (!EmailValidator.validate(name.text) && isName) {
    return '올바른 이메일 형식을 입력해주세요.';
  } else if (password.text.isEmpty && isName) {
    return '';
  }
  //password checker
  if (password.text.isEmpty) {
    return '모든 칸을 입력해주세요.';
  } else if (name.text.isEmpty) {
    return null;
  } else if (!EmailValidator.validate(name.text)) {
    return null;
  }

  return null;
}
