import 'dart:async';

import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/input_field.dart';
import 'package:duranno_app/components/registration/registration_check.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class RegistrationRoute extends AppRoute {
  String get routeName => RouteName.registrationForm;
  String get routeClassName => 'Registration';

  @override
  _RegistrationRoute createState() => _RegistrationRoute();

  @override
  String get title => '회원가입';
}

class _RegistrationRoute extends AppRouteState<RegistrationRoute> {
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordConfirmationController =
      TextEditingController();
  AuthenticationProvider _authenticationProvider;
  RegistrationCheck _registrationCheck;
  StreamSubscription _emailListener;
  String _nameError;
  String _emailError;
  String _passwordError;
  String _passwordConfirmError;
  bool _registerSent;

  final double titleSidePadding = 32;

  bool _isInputFieldNull;

  bool _allowedPromotionEmail;

  void _checkInputFieldNull() {
    if (_nameController.text.isNotEmpty &&
        _emailController.text.isNotEmpty &&
        _passwordController.text.isNotEmpty &&
        _passwordConfirmationController.text.isNotEmpty) {
      _isInputFieldNull = false;
    } else {
      _isInputFieldNull = true;
    }
  }

  @override
  void initState() {
    _registerSent = false;
    _nameError = null;
    _emailError = null;
    _passwordError = null;
    _passwordConfirmError = null;
    _isInputFieldNull = true;
    _passwordController.addListener(_checkInputFieldNull);
    _nameController.addListener(_checkInputFieldNull);
    _emailController.addListener(_checkInputFieldNull);
    _passwordConfirmationController.addListener(_checkInputFieldNull);
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _emailController.dispose();
    _passwordController.dispose();
    _passwordConfirmationController.dispose();
    _emailListener.cancel();
    _registrationCheck.dispose();
    _passwordController.removeListener(_checkInputFieldNull);
    _nameController.removeListener(_checkInputFieldNull);
    _emailController.removeListener(_checkInputFieldNull);
    _passwordConfirmationController.removeListener(_checkInputFieldNull);
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    final Map arguments = ModalRoute.of(context).settings.arguments as Map;
    _allowedPromotionEmail = arguments['allowedPromotionEmail'];
    assert(_allowedPromotionEmail != null);

    _authenticationProvider = Provider.of<AuthenticationProvider>(context);
    _registrationCheck = RegistrationCheck(
        authenticationProvider: _authenticationProvider,
        name: _nameController,
        emailName: _emailController,
        password: _passwordController,
        passwordConfirm: _passwordConfirmationController);
    _emailListener = _registrationCheck.emailTextChanged.listen((value) {
      setState(() {
        _emailError = value;
      });
    });
    super.didChangeDependencies();
  }

  @override
  Widget buildBody(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScopeNode currentFocus = FocusScope.of(context);

        if (!currentFocus.hasPrimaryFocus) {
          currentFocus.unfocus();
          _checkInputFieldNull();
        }
      },
      child: LayoutBuilder(
        builder: (context, constraint) => SingleChildScrollView(
          child: ConstrainedBox(
            constraints: BoxConstraints(minHeight: constraint.maxHeight),
            child: IntrinsicHeight(
              child: Stack(
                children: [
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            left: titleSidePadding, right: titleSidePadding),
                        child: Column(
                          children: [
                            SizedBox(
                              height: 44,
                            ),
                            Container(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                '정보를 입력해주세요.',
                                style: Theme.of(context).textTheme.headline1,
                                textAlign: TextAlign.left,
                              ),
                            ),
                            SizedBox(height: 80),
                            InputField(
                              onEditingComplete: _checkInputFieldNull,
                              controller: _nameController,
                              errorMessage: _nameError,
                              textLabel: '이름',
                            ),
                            SizedBox(height: 10),
                            InputField(
                              onEditingComplete: _checkInputFieldNull,
                              controller: _emailController,
                              errorMessage: _emailError,
                              textLabel: '이메일 주소',
                            ),
                            SizedBox(height: 10),
                            InputField(
                              isPassword: true,
                              onEditingComplete: _checkInputFieldNull,
                              controller: _passwordController,
                              errorMessage: _passwordError,
                              textLabel: '비밀번호',
                            ),
                            SizedBox(height: 10),
                            InputField(
                              isPassword: true,
                              onEditingComplete: _checkInputFieldNull,
                              controller: _passwordConfirmationController,
                              errorMessage: _passwordConfirmError,
                              textLabel: '비밀번호 확인',
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 48.0),
                      Spacer(),
                      Padding(
                        padding:
                            EdgeInsets.only(left: 20, right: 20, bottom: 28),
                        child: ThemeFlatButton.primary(
                          enabled: !_isInputFieldNull && !_registerSent,
                          label: '다음으로',
                          onPressed: () async {
                            setState(() {
                              _registerSent = true;
                              _nameError = _registrationCheck.checkNameError();
                              _emailError =
                                  _registrationCheck.checkEmailError();

                              _passwordError =
                                  _registrationCheck.checkPasswordError();
                              _passwordConfirmError = _registrationCheck
                                  .checkConfirmPasswordError();
                            });

                            final success = await _registrationCheck.register();

                            if (success) {
                              await _authenticationProvider
                                  .requestEmailVerification();
                              Navigator.of(context).pushReplacementNamed(
                                RouteName.registrationEmailConfirmation,
                                arguments: {
                                  'allowedPromotionEmail':
                                      _allowedPromotionEmail,
                                },
                              );
                            }

                            setState(() {
                              _registerSent = false;
                            });
                          },
                        ),
                      ),
                    ],
                  ),
                  if (_registerSent)
                    Positioned.fill(
                      child: Container(
                        color: const Color.fromRGBO(0, 0, 0, 0.6),
                        child: Center(
                          child: wrappedProgressIndicatorLight,
                        ),
                      ),
                    )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
