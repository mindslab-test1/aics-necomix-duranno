import 'package:duranno_app/components/common/agreement_checkbox.dart';
import 'package:duranno_app/components/common/checkbox_controller.dart';
import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/snack_bar.dart';
import 'package:duranno_app/graphql/mutation/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

@immutable
class UserAgreementRouteArguments {
  final bool shouldPopAfterRegistration;
  UserAgreementRouteArguments({@required this.shouldPopAfterRegistration});

  factory UserAgreementRouteArguments.fromJson(dynamic json) =>
      UserAgreementRouteArguments(
        shouldPopAfterRegistration: json['shouldPopAfterRegistration'],
      );
}

class UserAgreementRoute extends AppRoute {
  String get routeName => RouteName.registrationUserAgreement;
  String get routeClassName => 'UserAgreement';

  @override
  _UserAgreementRoute createState() => _UserAgreementRoute();

  @override
  String get title => '회원가입';
}

class _UserAgreementRoute extends AppRouteState<UserAgreementRoute> {
  final double _bigTitleTopPadding = 44;
  final double _titleSidePadding = 32;
  final double _userAgreementBorderHeight = 254;
  final double _inUserAgreementBorderHeight = 136;
  final double _outBorderRadiusValue = 4;
  final double _inBorderRadiusValue = 2;
  final CheckBoxController _boxController = CheckBoxController(totalNum: 3);
  final int _marketingIndex = 2; //3rd checkbox

  bool _isButtonEnabled;
  bool _isPending = false;

  void _onChangeAgreement() {
    setState(() {
      _boxController.allSelect()
          ? _isButtonEnabled = true
          : _isButtonEnabled = false;
    });
  }

  @override
  void initState() {
    _isButtonEnabled = false;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget buildBody(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            SizedBox(
              height: _bigTitleTopPadding,
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(
                left: _titleSidePadding,
              ),
              child: Text(
                '환영합니다.',
                style: Theme.of(context).textTheme.headline1,
                textAlign: TextAlign.left,
              ),
            ),
            SizedBox(
              height: 8,
            ),
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.only(
                left: _titleSidePadding,
              ),
              child: Text(
                '회원가입을 위해\n약관에 동의해주세요.',
                style: Theme.of(context).textTheme.subtitle1,
                textAlign: TextAlign.left,
              ),
            ),
            Expanded(
              child: Align(
                alignment: FractionalOffset.bottomCenter,
                child: Container(),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Container(
                height: _userAgreementBorderHeight,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(_outBorderRadiusValue),
                  border: Border.all(color: NeocomixThemeColors.border),
                ),
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 28),
                      child: AgreementCheckBox(
                        controller: _boxController,
                        text: '전체동의',
                        isTitle: true,
                        onChange: _onChangeAgreement,
                        subTitle: '필수 동의 항목 및 수신 동의(선택)에\n전체 동의 합니다.',
                      ),
                    ),
                    Expanded(
                      child: Container(),
                    ),
                    Padding(
                      padding: EdgeInsets.only(left: 12, right: 12),
                      child: Container(
                        height: _inUserAgreementBorderHeight,
                        decoration: BoxDecoration(
                          color: NeocomixThemeColors.background,
                          borderRadius:
                              BorderRadius.circular(_inBorderRadiusValue),
                        ),
                        child: Padding(
                          padding: EdgeInsets.only(left: 16, top: 20),
                          child: Column(
                            children: [
                              AgreementCheckBox(
                                controller: _boxController,
                                text: '[필수] 이용약관 동의',
                                isTitle: false,
                                index: 0,
                                onChange: _onChangeAgreement,
                                onTap: () {
                                  Navigator.of(context)
                                      .pushNamed(RouteName.termsOfService);
                                },
                              ),
                              SizedBox(height: 12),
                              AgreementCheckBox(
                                controller: _boxController,
                                text: '[필수] 개인정보 수집 및 이용동의',
                                isTitle: false,
                                index: 1,
                                onChange: _onChangeAgreement,
                                onTap: () {
                                  Navigator.of(context)
                                      .pushNamed(RouteName.privacyPolicy);
                                },
                              ),
                              SizedBox(height: 12),
                              AgreementCheckBox(
                                controller: _boxController,
                                text: '[선택] 혜택 이메일 수신 동의',
                                isTitle: false,
                                index: _marketingIndex,
                                isOptional: true,
                                onChange: _onChangeAgreement,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 12,
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 21,
            ),
            Padding(
              padding: EdgeInsets.only(left: 20, right: 20),
              child: Container(
                height: 50,
                width: double.infinity,
                child: ThemeFlatButton.primary(
                  label: '다음으로',
                  enabled: _isButtonEnabled,
                  onPressed: _onComplete(context),
                ),
              ),
            ),
            SizedBox(height: 28),
          ],
        ),
        if (_isPending)
          Positioned.fill(
            child: Container(
              color: const Color.fromRGBO(0, 0, 0, 0.6),
              child: Center(
                child: wrappedProgressIndicatorLight,
              ),
            ),
          ),
      ],
    );
  }

  Future<void> Function() _onComplete(BuildContext context) {
    return () async {
      final authentication =
          Provider.of<AuthenticationProvider>(context, listen: false);

      setState(() {
        _isPending = true;
      });

      if (authentication.isSocialAccount) {
        /* registration case, for social accounts */
        final client = GraphQLProvider.of(context).value;

        final result = await client.mutate(
          MutationOptions(
            documentNode: UserMutation.register,
            variables: {
              'isAllowedPromotionEmail':
                  _boxController.isSelected[_marketingIndex],
            },
          ),
        );

        final bool success = result.data['register'];
        final UserAgreementRouteArguments args =
            ModalRoute.of(context).settings.arguments;

        if (success) {
          if (args != null && args.shouldPopAfterRegistration) {
            Navigator.of(context).pop();
          } else {
            Navigator.of(context).pushReplacementNamed(RouteName.home);
          }
        } else {
          final errorSnackBar = ThemeSnackBar(content: Text('오류: 처리에 실패했습니다.'));
          Scaffold.of(context).showSnackBar(errorSnackBar);
        }
      } else {
        Navigator.of(context).pushNamed(RouteName.registrationForm, arguments: {
          'allowedPromotionEmail': _boxController.isSelected[_marketingIndex]
        });
      }

      setState(() {
        _isPending = false;
      });
    };
  }
}
