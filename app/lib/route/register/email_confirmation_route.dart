import 'package:duranno_app/components/common/dialog.dart';
import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/snack_bar.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/mutation/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class EmailConfirmationRoute extends AppRoute {
  String get routeName => RouteName.registrationEmailConfirmation;
  String get routeClassName => 'EmailConfirmation';

  @override
  _EmailConfirmationRouteState createState() => _EmailConfirmationRouteState();

  @override
  String get title => '회원가입';
}

class _EmailConfirmationRouteState
    extends AppRouteState<EmailConfirmationRoute> {
  AuthenticationProvider _authentication;
  bool _allowedPromotionEmail;
  bool _isDialogActive = false;
  bool _isPending = false;

  Future<void> _emailVerifyFail() async {
    _isDialogActive = true;
    await showThemeDialog(
      DialogType.OneAction,
      context: context,
      title: '인증을 해주세요.',
      content: "아직 인증이 되지 않았습니다.",
      onConfirm: (context) {},
    );
    _isDialogActive = false;
  }

  Future<void> _emailVerifySuccess() async {
    _isDialogActive = true;
    final errorSnackBar = ThemeSnackBar(
      content: Text('업데이트에 실패했습니다. 다시 시도해 주세요.'),
    );

    await showDialog(
      context: context,
      builder: (context) => Mutation(
        options: MutationOptions(
          documentNode: UserMutation.register,
          onCompleted: (data) {
            if (data != null && data['register']) {
              _authentication.signOut().then((_) {
                Navigator.of(context).popUntil(
                  (route) =>
                      !route.willHandlePopInternally &&
                      route is ModalRoute &&
                      !(route.settings.name == null ||
                          route.settings.name
                              .contains(RouteName.registrationPrefix)),
                );
              });
            } else {
              Scaffold.of(context).showSnackBar(errorSnackBar);
            }
          },
          onError: (error) {
            Scaffold.of(context).showSnackBar(errorSnackBar);
          },
        ),
        builder: (runMutation, result) => ThemeDialog.oneAction(
          title: "인증이 완료되었습니다.",
          content: "회원가입을 축하드립니다!\n인증된 계정으로 다시 로그인하세요.",
          confirmLabel: '확인',
          overrideDefaultConfirmCallback: true,
          onConfirm: (context) {
            runMutation({'isAllowedPromotionEmail': _allowedPromotionEmail});
          },
        ),
      ),
    );

    _isDialogActive = false;
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    final Map arguments = ModalRoute.of(context).settings.arguments as Map;
    _allowedPromotionEmail = arguments['allowedPromotionEmail'];
    assert(_allowedPromotionEmail != null);

    if (_authentication != null) {
      _authentication.removeListener(_checkEmailConfirm);
    }
    _authentication = Provider.of<AuthenticationProvider>(context);
    _authentication.addListener(_checkEmailConfirm);
  }

  @override
  void dispose() {
    _authentication.removeListener(_checkEmailConfirm);
    _authentication.signOut();

    super.dispose();
  }

  void _checkEmailConfirm() {
    if (_isDialogActive) {
      return;
    }

    try {
      if (_authentication.user.emailVerified) {
        _emailVerifySuccess();
      }
    } on DurannoException catch (e) {
      if (e.code == ExceptionCode.AuthEmailNotVerified) {
        _emailVerifyFail();
      } else if (e.code == ExceptionCode.AuthUnauthorized) {
        return;
      } else {
        throw e;
      }
    }
  }

  @override
  Widget buildBody(BuildContext context) {
    final resendVerificationLink = GestureDetector(
      child: TextLabel.p(
        '재전송 받기',
        color: NeocomixThemeColors.secondaryDark,
        fontWeight: FontWeight.w500,
      ),
      onTap: () async {
        setState(() {
          _isPending = true;
        });
        await _authentication.requestEmailVerification().then((_) {
          final successSnackbar =
              ThemeSnackBar(content: Text('인증 이메일을 전송했습니다.'));
          Scaffold.of(context).showSnackBar(successSnackbar);
        }).catchError((err) {
          final errorSnackbar =
              ThemeSnackBar(content: Text('오류: 인증 이메일을 전송하지 못했습니다.'));
          Scaffold.of(context).showSnackBar(errorSnackbar);
        });
        setState(() {
          _isPending = false;
        });
      },
    );

    final confirmVerificationButton = ThemeFlatButton.primary(
      label: '인증완료',
      enabled: true,
      onPressed: () async {
        setState(() {
          _isPending = true;
        });
        await _authentication.refresh();
        setState(() {
          _isPending = false;
        });
      },
    );

    return Container(
      color: Colors.white,
      alignment: Alignment.centerLeft,
      child: Stack(
        children: [
          Column(
            children: [
              //PADDING TOP TITLE
              SizedBox(
                height: 44,
              ),
              //TITLE
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(
                  left: 32,
                ),
                child: Text(
                  '인증을 해주세요',
                  style: Theme.of(context).textTheme.headline1,
                  textAlign: TextAlign.left,
                ),
              ),
              SizedBox(
                height: 8,
              ),
              //SUBTITLE
              Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.only(
                  left: 32,
                ),
                child: Text(
                  '이메일 인증을 진행해주세요.\n이메일 인증 후 아래 인증 완료 버튼을\n눌러주세요.',
                  style: Theme.of(context).textTheme.subtitle1,
                  textAlign: TextAlign.left,
                ),
              ),
              //ALL ITEMS FOOTER
              SizedBox(height: 48.0),
              Spacer(),
              Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  TextLabel.h6(
                    '메일이 오지 않으셨나요?',
                    fontWeight: FontWeight.normal,
                    color: NeocomixThemeColors.secondaryLight,
                  ),
                  SizedBox(width: 12.0),
                  resendVerificationLink,
                ],
              ),
              SizedBox(height: 24.0),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: confirmVerificationButton,
              ),
              SizedBox(
                height: 28,
              ),
            ],
          ),
          if (_isPending)
            Positioned.fill(
              child: Container(
                color: const Color.fromRGBO(0, 0, 0, 0.6),
                child: Center(
                  child: wrappedProgressIndicatorLight,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
