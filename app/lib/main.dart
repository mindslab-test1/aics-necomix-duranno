import 'package:duranno_app/app.dart';
import 'package:duranno_app/utils/reporter/app_runner.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

Future<void> main() async {
  await DotEnv().load('.env.production');
  AppRunner(App(), env: DotEnv().env);
}
