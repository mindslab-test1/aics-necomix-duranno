import 'package:uuid/uuid.dart' as UuidLib;

class Uuid {
  static UuidLib.Uuid _instance;
  static UuidLib.Uuid get instance {
    if (_instance == null) {
      _instance = UuidLib.Uuid();
    }
    return _instance;
  }

  static String get v1 => instance.v1();
  static String get v4 => instance.v4();
}
