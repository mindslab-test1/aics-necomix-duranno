import 'dart:async';
import 'dart:convert';

import 'package:audio_service/audio_service.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/audio/audio_background_tasks.dart';
import 'package:duranno_app/utils/audio/audio_controller.dart';
import 'package:duranno_app/utils/book.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:rxdart/subjects.dart';

class _AsyncLock {
  Completer _completer;

  Future<void> acquire() async {
    await _completer?.future;
    _completer = Completer();
  }

  void release() {
    _completer.complete();
  }

  bool get isBusy => _completer != null && !_completer.isCompleted;
}

class AudioServiceController extends IAudioController {
  AudioServiceController() {
    _positionSubscription =
        AudioService.positionStream.listen(_onPositionChanged);
    _eventSubscription = AudioService.customEventStream.listen(_onCustomEvent);
  }

  BehaviorSubject<AudioQueue> _audioQueueSubject =
      BehaviorSubject<AudioQueue>();

  StreamSubscription<Duration> _positionSubscription;
  StreamSubscription<dynamic> _eventSubscription;

  bool _isMediaPrepared = false;
  bool _isDisposed = false;

  BuildContext _context;
  EnvironmentProvider _environment;
  AuthenticationProvider _authentication;
  Logger _logger;

  void _checkValidMethodCall() {
    if (_isDisposed) {
      throw DurannoError(
        code: ErrorCode.Logic,
        message: 'AudioController is already disposed.',
      );
    }
  }

  /* static variables to ensure sync between multiple AudioServiceControllers */
  static Timer _disposalTimer;
  static final _AsyncLock _lock = _AsyncLock();

  AudioControllerState _mapPlaybackStateToControllerState(
      PlaybackState playbackState) {
    switch (playbackState?.processingState) {
      case AudioProcessingState.none:
      case AudioProcessingState.connecting:
      case AudioProcessingState.buffering:
      case AudioProcessingState.fastForwarding:
      case AudioProcessingState.rewinding:
      case AudioProcessingState.skippingToNext:
      case AudioProcessingState.skippingToPrevious:
      case AudioProcessingState.skippingToQueueItem:
        return AudioControllerState.Pending;
      case AudioProcessingState.completed:
        return AudioControllerState.Completed;
      case AudioProcessingState.stopped:
        if (!_isMediaPrepared) {
          return AudioControllerState.Pending;
        }
        return AudioControllerState.Stopped;
      case AudioProcessingState.error:
        return AudioControllerState.Stopped;
      case AudioProcessingState.ready:
        _isMediaPrepared = true;
        return playbackState.playing
            ? AudioControllerState.Playing
            : AudioControllerState.Paused;
    }

    return AudioControllerState.Pending;
  }

  @override
  AudioControllerState get state =>
      _mapPlaybackStateToControllerState(AudioService.playbackState);

  @override
  Stream<AudioControllerState> get onStateChanged =>
      AudioService.playbackStateStream
          .map(_mapPlaybackStateToControllerState)
          .distinct();

  @override
  void initialize(BuildContext context) {
    _checkValidMethodCall();

    assert(context != null);

    _context = context;

    _environment = Provider.of<EnvironmentProvider>(context, listen: false);
    _authentication =
        Provider.of<AuthenticationProvider>(context, listen: false);
    _logger = _environment.logger;
  }

  @override
  void onContextChanged(BuildContext newContext) {
    _checkValidMethodCall();

    assert(newContext != null);

    if (newContext == _context) {
      return;
    }

    _context = newContext;

    _environment = Provider.of<EnvironmentProvider>(newContext, listen: false);
    _authentication =
        Provider.of<AuthenticationProvider>(newContext, listen: false);
    _logger = _environment.logger;
  }

  @override
  Future<void> disconnect() async {
    if (!AudioService.connected) {
      return;
    }

    await AudioService.stop();

    _logger.verbose('Scheduling disposal.');
    _disposalTimer = Timer(
      const Duration(seconds: 3),
      () async {
        _logger.verbose('Executing scheduled disposal.');

        await _lock.acquire();

        if (AudioService.connected) {
          _logger.verbose('Disconnecting from AudioService ...');
          await AudioService.disconnect();
          _logger.verbose('Disconnected from AudioService.');
        } else {
          _logger.verbose('Already disconnected from AudioService.');
        }

        _lock.release();

        dispose();
      },
    );
  }

  @override
  void dispose() {
    if (_isDisposed) {
      return;
    }

    _logger.verbose('Disposing the disconnected controller.');

    _isDisposed = true;

    _positionSubscription?.cancel();
    _eventSubscription?.cancel();
    _audioQueueSubject.close();
  }

  @override
  Future<void> prepare({
    @required AudioQueue audioQueue,
    int initialSequence,
  }) async {
    _checkValidMethodCall();

    assert(audioQueue != null);

    if (audioQueue.bookId == _audioQueueSubject.value?.bookId) {
      return;
    }

    _audioQueueSubject.add(audioQueue);

    final initialAudioItem = audioQueue.queue[initialSequence ?? 0];

    _logger.verbose('Initializing AudioServiceController.');

    if (_disposalTimer != null) {
      if (_disposalTimer.isActive) {
        // requested disposal is not executed yet, cancel the disposal
        _disposalTimer.cancel();
        _logger.verbose('Cancelled scheduled AudioService disposal.');
      }

      _disposalTimer = null;
    }

    await _lock.acquire();

    if (!AudioService.connected) {
      _logger.verbose('Connecting to AudioService ...');
      await AudioService.connect();
      _logger.verbose('Connected to AudioService.');
    } else {
      _logger.verbose('Already connected to AudioService. Proceeding ...');
    }

    _lock.release();

    if (AudioService.running ?? false) {
      await AudioService.stop();
    }

    _isMediaPrepared = await AudioService.start(
      backgroundTaskEntrypoint: audioPlayerTaskEntrypoint,
      params: {
        'mediaItems': jsonEncode(audioQueue
            .toMediaItems()
            .map((mediaItem) => mediaItem.toJson())
            .toList()),
        'bookId': initialAudioItem.bookId,
        'initialSequence': initialSequence,
        'initialPosition': initialAudioItem.seekToPosition?.inMilliseconds,
        'useSequence': audioQueue.useSequence,
        'environment.mediaAuthApiName': _environment.mediaAuthApiName,
        'environment.apiStage': _environment.apiStage.name,
        'environment.apiUri': _environment.apiUri,
      },
      androidNotificationChannelName: '네오 오디오',
      androidNotificationIcon: 'mipmap/ic_noti',
      androidEnableQueue: false,
    );
  }

  Future<bool> restoreFromService() async {
    await _lock.acquire();

    if (!AudioService.connected) {
      _logger.verbose('Connecting to AudioService for restoration ...');
      await AudioService.connect();
      _logger.verbose('Connected to AudioService.');
    } else {
      _logger.verbose('Already connected to AudioService. Proceeding ...');
    }

    _lock.release();

    if (AudioService.currentMediaItem != null) {
      /* background media is active, restore the controller */
      final mediaItems = AudioService.queue;
      final queue = mediaItems.restoreQueue();
      _audioQueueSubject.add(queue);
      _isMediaPrepared = true;
      return true;
    } else {
      /* clear the resources */
      await disconnect();
      return false;
    }
  }

  @override
  Future<void> play() async {
    _checkValidMethodCall();

    return AudioService.play();
  }

  @override
  Future<void> pause() async {
    _checkValidMethodCall();

    return AudioService.pause();
  }

  @override
  Future<Duration> get duration async {
    _checkValidMethodCall();

    return AudioService.currentMediaItem?.duration;
  }

  @override
  Stream<Duration> get onDurationChanged {
    return AudioService.currentMediaItemStream
        .map((mediaItem) => mediaItem?.duration);
  }

  @override
  Future<Duration> get position async {
    _checkValidMethodCall();

    return _position;
  }

  @override
  Stream<Duration> get onPositionChanged {
    _checkValidMethodCall();

    return AudioService.positionStream;
  }

  @override
  Future<MediaItem> get mediaItem async {
    _checkValidMethodCall();

    return AudioService.currentMediaItem;
  }

  @override
  Stream<MediaItem> get onMediaItemChanged {
    _checkValidMethodCall();

    return AudioService.currentMediaItemStream;
  }

  @override
  Stream<int> get onSequenceChanged {
    _checkValidMethodCall();

    return Rx.combineLatest2<MediaItem, AudioQueue, int>(
      onMediaItemChanged,
      _audioQueueSubject.stream,
      (mediaItem, audioQueue) => audioQueue.getSequenceOfMediaItem(mediaItem),
    );
  }

  @override
  AudioQueue get audioQueue => _audioQueueSubject.value;

  @override
  Stream<AudioQueue> get onAudioQueueChanged => _audioQueueSubject.stream;

  @override
  Future<void> seek(Duration seekTo) async {
    _checkValidMethodCall();

    return AudioService.seekTo(seekTo);
  }

  @override
  Future<void> setPlaybackRate(double value) async {
    _checkValidMethodCall();

    return AudioService.setSpeed(value);
  }

  @override
  Future<void> seekToPrevious() async {
    _checkValidMethodCall();

    return AudioService.skipToPrevious();
  }

  @override
  Future<void> seekToNext() async {
    _checkValidMethodCall();

    return AudioService.skipToNext();
  }

  @override
  Future<void> seekToSequence(int sequence) {
    _checkValidMethodCall();

    return AudioService.skipToQueueItem(
        audioQueue.generateMediaId(audioQueue.queue[sequence]));
  }

  Duration _position;
  Future<void> _onPositionChanged(Duration position) async {
    _position = position;
  }

  Future<void> _onCustomEvent(dynamic json) async {
    final event = jsonDecode(json);
    _logger.debug('AudioServiceController received event: $event');

    switch (event['name']) {
      case 'request_id_token':
        try {
          await AudioService.customAction(
            'id_token',
            {'result': 'success', 'value': await _authentication.idToken},
          );
        } catch (e) {
          if (e is DurannoException &&
              e.code == ExceptionCode.AuthUnauthorized) {
            await AudioService.customAction('id_token', {'result': 'failed'});
          } else
            rethrow;
        }
        break;
      default:
        _logger.warning(
            'Unknown event from AudioServiceBackground: ${event['name']}');
    }
  }
}
