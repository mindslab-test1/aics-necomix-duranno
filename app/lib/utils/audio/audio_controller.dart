import 'dart:async';

import 'package:audio_service/audio_service.dart';
import 'package:duranno_app/utils/book.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

enum AudioControllerState {
  Stopped,
  Playing,
  Pending,
  Paused,
  Completed,
}

/// [IAudioController] is in charge of handling media actions,
/// such as play and pause.
///
/// It is also reponsible of authenticating the current user for media requests,
/// and updating play history of the user.
abstract class IAudioController {
  IAudioController();

  /// Indicates the state of the controller.
  ///
  /// [Stream] of this variable is available at [onStateChanged].
  AudioControllerState get state;

  bool get isPending => state == AudioControllerState.Pending;
  bool get isPlaying => state == AudioControllerState.Playing;

  /// A [Stream] that exposes the state of
  Stream<AudioControllerState> get onStateChanged;

  /// Initialize the controller.
  ///
  /// The implementation of this method should reference the context-dependent
  /// objects from this method, such as logger.
  void initialize(BuildContext context);

  /// The implementation of this method should update context-dependent objects
  /// and refresh the authentication.
  ///
  /// This method should be called on [didChangeDependencies] of the provider
  /// of this controller.
  void onContextChanged(BuildContext newContext);

  /// Disconnects the controller.
  ///
  /// This frees the resources that controller was claiming.
  Future<void> disconnect();

  /// Disposes the controller.
  ///
  /// Other methods cannot be called after the controller is disposed.
  void dispose();

  // Checks if current user can access the media, and retrieve subscription token.
  ///
  /// [prepare] should be called as the initial step for media request.
  /// The view should notify the user that play request is pending.
  Future<void> prepare({@required AudioQueue audioQueue, int initialSequence});

  /// Plays or resumes the current media.
  ///
  /// The view should notify the user that play request is pending before it is ready.
  Future<void> play();

  /// Pauses the current media.
  Future<void> pause();

  /// Retrieves duration of the requesting media file.
  ///
  /// [Stream] of this variable is available at [onDurationChanged].
  Future<Duration> get duration;

  /// A [Stream] that exposes the duration of current media item.
  Stream<Duration> get onDurationChanged;

  /// Retrieves current position of the media file.
  ///
  /// [Stream] of this variable is available at [onPositionChanged].
  Future<Duration> get position;

  /// A [Stream] that exposes the current position of the media.
  Stream<Duration> get onPositionChanged;

  /// Retrieves current media item.
  ///
  /// [Stream] of this variable is available at [onMediaItemChanged].
  Future<MediaItem> get mediaItem;

  /// A [Stream] that exposes current media item.
  Stream<MediaItem> get onMediaItemChanged;

  /// A [Stream] that exposes the index of current media item.
  Stream<int> get onSequenceChanged;

  /// Retrieves current audio queue.
  AudioQueue get audioQueue;

  /// [Stream] that exposes the current audio queue.
  Stream<AudioQueue> get onAudioQueueChanged;

  /// Seeks to the given position.
  Future<void> seek(Duration seekTo);

  /// Sets the playback rate.
  Future<void> setPlaybackRate(double value);

  /// Seeks to previous chapter.
  Future<void> seekToPrevious();

  /// Seeks to next chapter.
  Future<void> seekToNext();

  /// Seeks to given sequence;
  Future<void> seekToSequence(int sequence);
}
