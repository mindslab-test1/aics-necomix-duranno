import 'dart:async';

import 'package:audio_service/audio_service.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:duranno_app/graphql/mutation/user.dart';
import 'package:duranno_app/graphql/query/subscription.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/audio/audio_controller.dart';
import 'package:duranno_app/utils/book.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/logger.dart';
import 'package:duranno_app/utils/task_queue/interface.dart';
import 'package:duranno_app/utils/task_queue/simple_task_queue.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:meta/meta.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class AudioplayersController extends IAudioController {
  final String mediaUri;
  final String bookId;
  int sequence;
  String chapterTitle;
  void Function(DurannoException) onException;
  AudioplayersController({
    @required this.mediaUri,
    @required this.bookId,
    this.sequence,
    this.chapterTitle,
    this.onException,
  }) {
    _player = AudioPlayer();
    _taskQueue = SimpleTaskQueue();
  }

  AudioPlayer _player;
  StreamSubscription<String> _playerErrorSubscription;
  ITaskQueue _taskQueue;

  String _subscriptionToken;
  DateTime _subscriptionTokenExpiration;

  bool get _isTokenExpired => _subscriptionTokenExpiration != null
      ? DateTime.now().isAfter(_subscriptionTokenExpiration)
      : false;

  bool get _isTokenExpiringSoon => _subscriptionTokenExpiration != null
      ? DateTime.now().isAfter(
          _subscriptionTokenExpiration.add(const Duration(minutes: 10)))
      : false;

  bool get _hasToken => _subscriptionToken != null && !_isTokenExpired;

  bool get _shouldRefreshToken => !_hasToken || _isTokenExpiringSoon;

  void _invalidateSubscriptionToken() {
    _subscriptionToken = null;
    _subscriptionTokenExpiration = null;
  }

  Completer _preparationCompleter = Completer();
  Completer _fetchDurationCompleter = Completer();

  bool _isPrepareInvoked = false;
  bool _isFetchDurationInvoked = false;
  bool _isDisposed = false;

  BuildContext _context;
  GraphQLClient _gqlClient;
  EnvironmentProvider _environment;
  AuthenticationProvider _authentication;
  Logger _logger;

  void _checkValidMethodCall() {
    if (_isDisposed) {
      throw DurannoError(
        code: ErrorCode.Logic,
        message: 'AudioController is already disposed.',
      );
    }
  }

  @override
  AudioControllerState get state {
    _checkValidMethodCall();

    if (_taskQueue.isPending) {
      return AudioControllerState.Pending;
    }

    switch (_player.state) {
      case AudioPlayerState.COMPLETED:
        return AudioControllerState.Completed;
      case AudioPlayerState.PLAYING:
        return AudioControllerState.Playing;
      case AudioPlayerState.PAUSED:
        return AudioControllerState.Paused;
      case AudioPlayerState.STOPPED:
        return AudioControllerState.Stopped;
    }

    // default state (before initialization)
    return AudioControllerState.Pending;
  }

  Stream<AudioControllerState> _onStateChanged;

  @override
  Stream<AudioControllerState> get onStateChanged => _onStateChanged;

  @override
  void initialize(BuildContext context) {
    _checkValidMethodCall();

    assert(context != null);

    _context = context;
    _gqlClient = GraphQLProvider.of(context).value;
    _environment = Provider.of<EnvironmentProvider>(context, listen: false);
    _authentication =
        Provider.of<AuthenticationProvider>(context, listen: false);
    _logger = _environment.logger;

    _playerErrorSubscription = _player.onPlayerError.listen((err) {
      _logger.debug(err);
    });

    _onStateChanged =
        Rx.combineLatest2<AudioPlayerState, bool, AudioControllerState>(
            _player.onPlayerStateChanged, _taskQueue.onPendingChanged,
            (playerState, pending) {
      if (pending) {
        return AudioControllerState.Pending;
      }

      switch (playerState) {
        case AudioPlayerState.COMPLETED:
          return AudioControllerState.Completed;
        case AudioPlayerState.PLAYING:
          return AudioControllerState.Playing;
        case AudioPlayerState.PAUSED:
          return AudioControllerState.Paused;
        case AudioPlayerState.STOPPED:
          return AudioControllerState.Stopped;
      }

      // default state (before initialization)
      return AudioControllerState.Pending;
    }).distinct();
  }

  @override
  void onContextChanged(BuildContext newContext) {
    _checkValidMethodCall();

    assert(newContext != null);

    if (newContext == _context) {
      return;
    }

    _context = newContext;

    final newGraphQLClient = GraphQLProvider.of(newContext).value;
    if (_gqlClient != newGraphQLClient) {
      _gqlClient = newGraphQLClient;
      _invalidateSubscriptionToken();
    }

    _environment = Provider.of<EnvironmentProvider>(newContext, listen: false);
    _authentication =
        Provider.of<AuthenticationProvider>(newContext, listen: false);
    Logger logger = _environment.logger;

    if (logger != _logger) {
      _logger = logger;

      _playerErrorSubscription.cancel();
      _playerErrorSubscription = _player.onPlayerError.listen((err) {
        _logger.debug(err);
      });
    }
  }

  @override
  Future<void> disconnect() async {
    throw new DurannoError(code: ErrorCode.NotImplemented);
  }

  @override
  void dispose() {
    if (_isDisposed) {
      return;
    }

    _isDisposed = true;

    _player.dispose();
    _taskQueue.dispose();
  }

  @override
  Future<void> prepare({
    @required AudioQueue audioQueue,
    int initialSequence,
  }) async {
    _checkValidMethodCall();

    if (_isPrepareInvoked) {
      return;
    }

    _isPrepareInvoked = true;
    final taskId = _taskQueue.register();

    if (_shouldRefreshToken) {
      await _retrieveSubscriptionToken().catchError((err) {
        _taskQueue.resolve(taskId);
        throw err;
      });
    }

    final result =
        await _player.setUrl(_signMediaUriWithToken(_subscriptionToken));

    if (result != 1) {
      _taskQueue.resolve(taskId);
      _preparationCompleter.complete();
      _handleException(
        exception: DurannoException(
          code: ExceptionCode.MediaUnknown,
          message: 'error on AudioPlayer.setUrl()',
        ),
      );
      return;
    }

    _player.onAudioPositionChanged.listen((position) {
      if (!_taskQueue.isPending) {
        _updatePlayHistory(position: position.inMilliseconds);
      }
    });

    _preparationCompleter.complete();
    _taskQueue.resolve(taskId);

    await _updatePlayHistory();
  }

  @override
  Future<void> play() async {
    _checkValidMethodCall();
    await _preparationCompleter.future;

    if (state == AudioControllerState.Playing) {
      return;
    }

    final taskId = _taskQueue.register();

    if (_shouldRefreshToken) {
      await _retrieveSubscriptionToken().catchError(
        (err) {
          _taskQueue.resolve(taskId);
          throw err;
        },
      );
    }

    final result = await _player.resume();

    if (result != 1) {
      _taskQueue.resolve(taskId);
      _handleException(
        exception: DurannoException(
          code: ExceptionCode.MediaUnknown,
          message: 'error on AudioPlayer.resume()',
        ),
      );
      return;
    }

    _taskQueue.resolve(taskId);
  }

  @override
  Future<void> pause() async {
    _checkValidMethodCall();
    await _preparationCompleter.future;

    if (state == AudioControllerState.Paused ||
        state == AudioControllerState.Completed ||
        state == AudioControllerState.Stopped) {
      return;
    }

    await _player.pause();
    await _updatePlayHistory();
  }

  Duration _duration;

  @override
  Future<Duration> get duration async {
    _checkValidMethodCall();
    await _preparationCompleter.future;

    final taskId = _taskQueue.register();

    if (_isFetchDurationInvoked) {
      await _fetchDurationCompleter.future;
    }

    _isFetchDurationInvoked = true;

    if (_duration != null) {
      _taskQueue.resolve(taskId);
      return _duration;
    }

    _duration = await _retryMaybeFailingTask<Duration>(
      () async {
        final result = await _player.getDuration();
        if (result > 0) {
          return Duration(milliseconds: result);
        }

        throw Exception('invalid duration: _player.getDuration() failed');
      },
      chancesLeft: 5,
      sleepDurationBeforeRetry: const Duration(seconds: 3),
      onError: (error) {
        _handleException(
            exception: DurannoException(
          code: ExceptionCode.MediaUnknown,
          message: 'error at get AudioplayersController.duration',
        ));
      },
    );

    if (_duration != null) {
      _fetchDurationCompleter.complete();
    }

    _taskQueue.resolve(taskId);
    return _duration;
  }

  @override
  Stream<Duration> get onDurationChanged {
    throw new DurannoError(code: ErrorCode.NotImplemented);
  }

  @override
  Future<Duration> get position async {
    _checkValidMethodCall();
    await _preparationCompleter.future;

    return Duration(milliseconds: await _player.getCurrentPosition());
  }

  @override
  Stream<Duration> get onPositionChanged {
    _checkValidMethodCall();

    return _player.onAudioPositionChanged;
  }

  @override
  Future<MediaItem> get mediaItem async {
    throw DurannoError(code: ErrorCode.NotImplemented);
  }

  @override
  Stream<MediaItem> get onMediaItemChanged {
    throw DurannoError(code: ErrorCode.NotImplemented);
  }

  @override
  Stream<int> get onSequenceChanged {
    throw DurannoError(code: ErrorCode.NotImplemented);
  }

  @override
  AudioQueue get audioQueue {
    throw DurannoError(code: ErrorCode.NotImplemented);
  }

  @override
  Stream<AudioQueue> get onAudioQueueChanged {
    throw DurannoError(code: ErrorCode.NotImplemented);
  }

  @override
  Future<void> seek(Duration seekTo) async {
    _checkValidMethodCall();
    await _preparationCompleter.future;

    final taskId = _taskQueue.register();

    final result = await _player.seek(seekTo);

    if (result != 1) {
      _taskQueue.resolve(taskId);
      _handleException(
        exception: DurannoException(
          code: ExceptionCode.MediaUnknown,
          message: 'error on AudioPlayer.seek()',
        ),
      );
      return;
    }

    _taskQueue.resolve(taskId);

    await _updatePlayHistory();
  }

  @override
  Future<void> setPlaybackRate(double value) async {
    _checkValidMethodCall();
    await _preparationCompleter.future;

    final taskId = _taskQueue.register();

    final previousState = _player.state;
    final result = await _player.setPlaybackRate(playbackRate: value);

    if (result != 1) {
      _taskQueue.resolve(taskId);
      _handleException(
        exception: DurannoException(
          code: ExceptionCode.MediaUnknown,
          message: 'error on AudioPlayer.setPlaybackRate()',
        ),
      );
      return;
    }

    if (previousState == AudioPlayerState.PAUSED) {
      /* fix that audio resumes after setting playback rate on paused */
      await _player.pause();
    }

    _taskQueue.resolve(taskId);
  }

  @override
  Future<void> seekToPrevious() {
    throw DurannoError(code: ErrorCode.NotImplemented);
  }

  @override
  Future<void> seekToNext() {
    throw DurannoError(code: ErrorCode.NotImplemented);
  }

  @override
  Future<void> seekToSequence(int sequence) {
    throw DurannoError(code: ErrorCode.NotImplemented);
  }

  int _prevUpdatedPosition;
  static final int _updatePositionDifferenceThreshold =
      5 * 1000; /* 5 seconds */

  Future<void> _updatePlayHistory({
    int position,
  }) async {
    position ??= await _player.getCurrentPosition();

    if (_prevUpdatedPosition != null &&
        (_prevUpdatedPosition - position).abs() <
            _updatePositionDifferenceThreshold) {
      return;
    }

    _prevUpdatedPosition = position;

    _logger.debug(
        'Update play history of bookId: $bookId, sequence: $sequence, chapterTitle: $chapterTitle to position $position');

    await UpdateHistoryMutation.instance.execute(_context, args: {
      'bookId': bookId,
      'mediaUri': mediaUri,
      'position': position,
      'sequence': sequence,
      'chapterTitle': chapterTitle,
      'userId': _authentication.user.uid,
    });
  }

  String _signMediaUriWithToken(String token) {
    assert(token != null);

    if (!_hasToken) {
      throw DurannoError(
        code: ErrorCode.Logic,
        message: 'Tried to sign media uri with invalid token',
      );
    }

    return '$mediaUri?token=$token&app_name=${_environment.mediaAuthApiName}&app_stage=${_environment.apiStage.name}';
  }

  Future<void> _retrieveSubscriptionToken() async {
    _logger.info(
        'Retrieving subscription token from $mediaUri with bookId: $bookId' +
            (sequence != null ? ', sequence: $sequence' : ''));

    final result = await _gqlClient.query(
      QueryOptions(
        documentNode: SubscriptionQuery.token,
        variables: {
          'bookId': bookId,
          'sequence': sequence,
        },
      ),
    );

    if (result.hasException) {
      throw new DurannoError(
        code: ErrorCode.Server,
        message: result.exception.toString(),
      );
    }

    final String token = result.data['subscriptionToken']['token'];
    final String error = result.data['subscriptionToken']['error'];

    if (error != null) {
      if (error.contains('user is not subscribed')) {
        throw DurannoException(code: ExceptionCode.MediaUserNotSubscribed);
      }

      if (error.contains('invalid bookId') ||
          error.contains('book is deleted')) {
        throw DurannoException(code: ExceptionCode.MediaNotFound);
      }

      if (error.contains('exceeded monthly limit')) {
        throw DurannoException(code: ExceptionCode.MediaUserExceedLimit);
      }

      throw DurannoException(code: ExceptionCode.MediaUnknown, message: error);
    }

    _subscriptionToken = token;
    _subscriptionTokenExpiration = DateTime.now().add(Duration(hours: 3));
  }

  bool _handleException({Exception exception}) {
    if (onException != null) {
      onException(exception);
      return true;
    } else {
      throw exception;
    }
  }

  Future<T> _retryMaybeFailingTask<T>(
    Future<T> Function() invoke, {
    String invokeFuncInfo,
    int chancesLeft,
    Duration sleepDurationBeforeRetry = const Duration(milliseconds: 200),
    void Function(dynamic) onError,
  }) async {
    try {
      return await invoke();
    } catch (error) {
      if (chancesLeft == 0) {
        if (onError != null) {
          onError(error);
          return null;
        } else {
          throw error;
        }
      }

      _logger.warning(
          'Failed executing ${invokeFuncInfo ?? 'native method'}, retry chances left: ${chancesLeft - 1}');

      await Future.delayed(sleepDurationBeforeRetry);

      return await _retryMaybeFailingTask(
        invoke,
        invokeFuncInfo: invokeFuncInfo,
        chancesLeft: chancesLeft - 1,
        sleepDurationBeforeRetry: sleepDurationBeforeRetry,
        onError: onError,
      );
    }
  }
}
