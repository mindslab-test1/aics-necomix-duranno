import 'dart:async';
import 'dart:convert';

import 'package:audio_service/audio_service.dart';
import 'package:audio_session/audio_session.dart';
import 'package:duranno_app/utils/constants.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:http/http.dart' as http;
import 'package:just_audio/just_audio.dart';
import 'package:meta/meta.dart';

class _IdTokenManager {
  _IdTokenManager();

  Completer<String> _idTokenRequestCompleter;

  Future<String> get token async {
    _idTokenRequestCompleter = Completer();
    _requestIdToken();

    return _idTokenRequestCompleter.future;
  }

  void _requestIdToken() {
    final event = jsonEncode({'name': 'request_id_token'});
    AudioServiceBackground.sendCustomEvent(event);
  }

  void receiveIdToken(String token) {
    if (_idTokenRequestCompleter != null &&
        !_idTokenRequestCompleter.isCompleted) {
      _idTokenRequestCompleter.complete(token);
    }
  }

  void dispose() {}
}

@immutable
class _SubscriptionTokenObject {
  final String token;
  final DateTime expiresAt;
  _SubscriptionTokenObject({
    @required this.token,
  })  : assert(token != null),
        expiresAt = DateTime.now().add(const Duration(hours: 3));

  bool get expired => DateTime.now().isAfter(expiresAt);

  dynamic toJson() => {
        'token': token,
        'expiresAt': expiresAt.toIso8601String(),
      };

  @override
  String toString() => jsonDecode(toJson());
}

class _SubscriptionTokenManager {
  final Map<String, _SubscriptionTokenObject> tokenObjects;
  _SubscriptionTokenManager() : tokenObjects = {};

  String bookId;
  bool useSequence;
  List<MediaItem> queue;
  Map<String, String> environment;

  _IdTokenManager idTokenManager;

  Future<String> signedMediaUri(int sequence) async {
    assert(bookId != null);
    assert(useSequence != null);
    assert(queue != null);
    assert(environment != null);

    final mediaItem = queue[useSequence ? sequence : 0];

    if (tokenObjects[mediaItem.id] == null ||
        tokenObjects[mediaItem.id].expired) {
      final newToken = await _resolveTokenFromNetwork(sequence);
      tokenObjects[mediaItem.id] = _SubscriptionTokenObject(token: newToken);
    }

    return _signMediaUri(
        mediaItem.extras['mediaUri'], tokenObjects[mediaItem.id].token);
  }

  void dispose() {}

  String _signMediaUri(String mediaUri, String token) =>
      '$mediaUri?token=$token&app_name=${environment['mediaAuthApiName']}&app_stage=${environment['apiStage']}';

  String _getApiUri(int sequence) =>
      '${environment['apiUri']}/media/token?book_id=$bookId${useSequence ? '&sequence=$sequence' : ''}';

  Future<String> _resolveTokenFromNetwork(int sequence) async {
    final httpResponse = await http.get(
      _getApiUri(sequence),
      headers: {
        'Authorization': 'Bearer ${await idTokenManager.token}',
      },
    );

    if (httpResponse.statusCode != 200) {
      throw DurannoError(
        code: ErrorCode.Server,
        message: httpResponse.body,
      );
    }

    final response = jsonDecode(httpResponse.body);
    final String mediaToken = response['token'];

    return mediaToken;
  }
}

class _UserHistorySubscriber {
  _UserHistorySubscriber();

  Stream<Duration> _positionStream;

  set positionStream(Stream<Duration> stream) {
    _positionStream = stream;

    _positionSubscription?.cancel();
    _positionSubscription = _positionStream.listen(_update);
  }

  String bookId;
  bool useSequence;
  List<MediaItem> queue;
  Map<String, String> environment;

  _IdTokenManager idTokenManager;

  StreamSubscription<Duration> _positionSubscription;
  Duration _prevUpdated;

  static const Duration _waitInterval = const Duration(seconds: 5);

  void dispose() {
    _positionSubscription?.cancel();
  }

  Future<void> _update(Duration position) async {
    final currentMediaItem = AudioServiceBackground.mediaItem;
    final sequence = currentMediaItem != null
        ? queue.indexWhere((item) => item.id == currentMediaItem.id)
        : -1;

    if (position == null ||
        (_prevUpdated != null &&
            (_prevUpdated - position).abs() < _waitInterval) ||
        sequence < 0) {
      return;
    }

    _prevUpdated = position;

    final apiUri = '${environment['apiUri']}/user/history';
    final requestBody = jsonEncode({
      'bookId': bookId,
      'mediaUri': currentMediaItem.extras['mediaUri'],
      'position': position.inMilliseconds,
      'sequence': useSequence ? sequence : null,
      'chapterTitle': currentMediaItem.title,
    });

    await http.put(
      apiUri,
      headers: {
        'Authorization': 'Bearer ${await idTokenManager.token}',
        'Content-Type': 'application/json; charset=utf-8',
      },
      body: requestBody,
    );
  }
}

class _AudioPlayerTask extends BackgroundAudioTask {
  final AudioPlayer _player;
  final _IdTokenManager _idTokenManager;
  final _SubscriptionTokenManager _subscriptionTokenManager;
  final _UserHistorySubscriber _userHistorySubscriber;
  _AudioPlayerTask()
      : _player = AudioPlayer(),
        _idTokenManager = _IdTokenManager(),
        _subscriptionTokenManager = _SubscriptionTokenManager(),
        _userHistorySubscriber = _UserHistorySubscriber() {
    _subscriptionTokenManager.idTokenManager = _idTokenManager;
    _userHistorySubscriber.idTokenManager = _idTokenManager;
  }

  AudioProcessingState _skipState;

  StreamSubscription<PlaybackEvent> _eventSubscription;
  StreamSubscription<ProcessingState> _processingStateSubscription;

  bool _useSequence;
  List<MediaItem> _mediaItems;
  int get _currentIndex => _player.currentIndex;
  MediaItem get _currentMediaItem =>
      _mediaItems != null && _currentIndex != null
          ? _mediaItems[_currentIndex]
          : null;

  @override
  Future<void> onStart(Map<String, dynamic> params) async {
    final List<MediaItem> mediaItems = jsonDecode(params['mediaItems'])
        .map<MediaItem>((raw) => MediaItem.fromJson(raw))
        .toList();
    final int initialSequence = params['initialSequence'];
    final Duration initialPosition = params['initialPosition'] != null
        ? Duration(milliseconds: params['initialPosition'])
        : null;

    _subscriptionTokenManager
      ..bookId = params['bookId']
      ..useSequence = params['useSequence']
      ..queue = mediaItems
      ..environment = {
        'mediaAuthApiName': params['environment.mediaAuthApiName'],
        'apiStage': params['environment.apiStage'],
        'apiUri': params['environment.apiUri'],
      };

    _userHistorySubscriber
      ..bookId = params['bookId']
      ..useSequence = params['useSequence']
      ..queue = mediaItems
      ..environment = {
        'mediaAuthApiName': params['environment.mediaAuthApiName'],
        'apiStage': params['environment.apiStage'],
        'apiUri': params['environment.apiUri'],
      }
      ..positionStream = _player.positionStream;

    _mediaItems = mediaItems;
    _useSequence = params['useSequence'];

    final session = await AudioSession.instance;
    await session.configure(AudioSessionConfiguration.speech());

    _eventSubscription = _player.playbackEventStream.listen((event) {
      _broadcastState();
    });

    _processingStateSubscription =
        _player.processingStateStream.listen((state) {
      switch (state) {
        case ProcessingState.completed:
          onSkipToNext();
          break;
        case ProcessingState.ready:
          _skipState = null;
          break;
        default:
          break;
      }
    });

    try {
      final signedUri =
          await _subscriptionTokenManager.signedMediaUri(initialSequence);

      await AudioServiceBackground.setQueue(_mediaItems);
      await AudioServiceBackground.setMediaItem(
          _mediaItems[initialSequence ?? 0]);

      await _player.load(ProgressiveAudioSource(Uri.parse(signedUri)));
      if (initialPosition != null) {
        await _player.seek(initialPosition);
      }
      onPlay();
    } catch (e) {
      _handleError(e);
    }
  }

  @override
  Future<void> onPlay() => _player.play();

  @override
  Future<void> onPause() => _player.pause();

  @override
  Future<void> onSeekTo(Duration position) => _player.seek(position);

  @override
  Duration get fastForwardInterval => audioFastForwardDuration;

  @override
  Duration get rewindInterval => audioRewindDuration;

  @override
  Future<void> onFastForward() => _seekRelative(fastForwardInterval);

  @override
  Future<void> onRewind() => _seekRelative(-rewindInterval);

  @override
  Future<void> onSetSpeed(double value) => _player.setSpeed(value);

  @override
  Future<void> onSkipToQueueItem(String mediaId) async {
    if (!_useSequence || _mediaItems == null || _currentIndex == null) {
      return;
    }

    final newIndex = _mediaItems.indexWhere((item) => item.id == mediaId);
    if (newIndex < 0) {
      return;
    }

    _skipState = newIndex > _currentIndex
        ? AudioProcessingState.skippingToNext
        : AudioProcessingState.skippingToPrevious;

    await AudioServiceBackground.setMediaItem(_mediaItems[newIndex]);

    try {
      final signedUri =
          await _subscriptionTokenManager.signedMediaUri(newIndex);
      await _player.load(ProgressiveAudioSource(Uri.parse(signedUri)));
      await _player.seek(Duration.zero);
      await _player.play();
    } catch (e) {
      _handleError(e);
    }
  }

  @override
  Future<void> onCustomAction(String name, dynamic arguments) async {
    try {
      switch (name) {
        case 'id_token':
          final String result = arguments['result'];
          final String token = arguments['value'];

          if (result == 'success') {
            _idTokenManager?.receiveIdToken(token);
          } else {
            throw DurannoException(
              code: ExceptionCode.MediaToken,
              message: 'firebase id token retrieval failed',
            );
          }
          break;
        default:
        /* Unknown custom action. ignore */
      }
    } catch (e) {
      _handleError(e);
    }
  }

  @override
  Future<void> onStop() async {
    await _player.pause();
    await _player.dispose();
    await _eventSubscription.cancel();
    await _processingStateSubscription.cancel();
    await _broadcastState();

    _idTokenManager.dispose();
    _subscriptionTokenManager.dispose();
    _userHistorySubscriber.dispose();

    await super.onStop();
  }

  Future<void> _seekRelative(Duration offset) async {
    if (_currentMediaItem == null) {
      return;
    }

    var newPosition = _player.position + offset;

    if (newPosition < Duration.zero) newPosition = Duration.zero;
    if (newPosition > _currentMediaItem.duration)
      newPosition = _currentMediaItem.duration;

    await _player.seek(newPosition);
  }

  Future<void> _broadcastState() async {
    await AudioServiceBackground.setState(
      controls: [
        MediaControl(
          androidIcon: 'mipmap/ic_audio_service_skip_previous',
          label: 'Previous',
          action: MediaAction.skipToPrevious,
        ),
        if (_player.playing)
          MediaControl(
            androidIcon: 'mipmap/ic_audio_service_pause',
            label: 'Pause',
            action: MediaAction.pause,
          )
        else
          MediaControl(
            androidIcon: 'mipmap/ic_audio_service_play',
            label: 'Play',
            action: MediaAction.play,
          ),
        MediaControl(
          androidIcon: 'mipmap/ic_audio_service_skip_next',
          label: 'Next',
          action: MediaAction.skipToNext,
        ),
        MediaControl(
          androidIcon: 'mipmap/ic_audio_service_stop',
          label: 'Stop',
          action: MediaAction.stop,
        ),
      ],
      systemActions: [
        MediaAction.seekTo,
        MediaAction.seekForward,
        MediaAction.seekBackward,
      ],
      androidCompactActions: [0, 1, 2],
      processingState: _getProcessingState(),
      playing: _player.playing,
      position: _player.position,
      bufferedPosition: _player.bufferedPosition,
      speed: _player.speed,
    );
  }

  AudioProcessingState _getProcessingState() {
    if (_skipState != null) {
      return _skipState;
    }

    switch (_player.processingState) {
      case ProcessingState.none:
        return AudioProcessingState.stopped;
      case ProcessingState.loading:
        return AudioProcessingState.connecting;
      case ProcessingState.buffering:
        return AudioProcessingState.buffering;
      case ProcessingState.ready:
        return AudioProcessingState.ready;
      case ProcessingState.completed:
        return AudioProcessingState.completed;
      default:
        throw DurannoError(
          code: ErrorCode.Logic,
          message: "Invalid state: ${_player.processingState}",
        );
    }
  }

  void _handleError(Error e) {
    onStop();
  }
}

void audioPlayerTaskEntrypoint() {
  AudioServiceBackground.run(() => _AudioPlayerTask());
}
