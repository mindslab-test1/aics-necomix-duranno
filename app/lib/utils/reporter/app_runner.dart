import 'dart:async';
import 'dart:isolate';

import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/reporter/mode/report_mode.dart';
import 'package:duranno_app/utils/reporter/model/app_environment_options.dart';
import 'package:duranno_app/utils/reporter/model/error_report.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AppRunner implements ReportModeListener {
  static GlobalKey<NavigatorState> _navigatorKey;
  static GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;

  final Widget rootWidget;
  final Map<String, String> env;
  final AppEnvironmentOptions developmentOptions;
  final AppEnvironmentOptions betaOptions;
  final AppEnvironmentOptions productionOptions;

  AppEnvironmentOptions _options;

  AppRunner(
    this.rootWidget, {
    @required this.env,
    this.developmentOptions,
    this.betaOptions,
    this.productionOptions,
    GlobalKey<NavigatorState> navigatorKey,
  })  : assert(rootWidget != null),
        assert(env != null) {
    _configureNavigatorKey(navigatorKey);
    _configureApplicationEnvironment();
    _configureErrorHandlers();
  }

  void _configureNavigatorKey(GlobalKey<NavigatorState> navigatorKey) {
    if (navigatorKey != null) {
      _navigatorKey = navigatorKey;
    } else {
      _navigatorKey = GlobalKey<NavigatorState>();
    }
  }

  void _configureApplicationEnvironment() {
    final appStage = AppStage.values.firstWhere(
        (appStage) =>
            appStage.toString().split('.').last.toLowerCase() ==
            env['APP_STAGE'],
        orElse: () => AppStage.Development);

    switch (appStage) {
      case AppStage.Development:
        {
          _options = developmentOptions ??
              AppEnvironmentOptions.getDefaultDevelopmentOptions();
          break;
        }
      case AppStage.Beta:
        {
          _options =
              betaOptions ?? AppEnvironmentOptions.getDefaultBetaOptions();
          break;
        }
      case AppStage.Production:
        {
          _options = productionOptions ??
              AppEnvironmentOptions.getDefaultProductionOptions();
          break;
        }
    }

    _options.mode.reportModeListener = this;
  }

  Future<void> _configureErrorHandlers() async {
    FlutterError.onError = (FlutterErrorDetails details) {
      _reportError(details.exception, details.stack, details: details);
    };

    Isolate.current.addErrorListener(RawReceivePort((pair) async {
      final isolateError = pair as List<dynamic>;
      _reportError(
        isolateError.first.toString(),
        isolateError.last.toString(),
      );
    }).sendPort);

    runZonedGuarded<Future<void>>(() async {
      runApp(rootWidget);
    }, (dynamic error, StackTrace stackTrace) {
      _reportError(error, stackTrace);
    });
  }

  void _reportError(
    dynamic error,
    dynamic stackTrace, {
    FlutterErrorDetails details,
  }) {
    final report = ErrorReport(
        error: error,
        stackTrace: stackTrace,
        dateTime: DateTime.now(),
        details: details);

    _options.mode.requestHandle(report, context);
  }

  @override
  void onUserAccept(ErrorReport report) {
    _options.handlers.forEach((handler) {
      handler.handle(report, context).then((success) {
        if (success) {
          Provider.of<EnvironmentProvider>(context, listen: false)
              .logger
              .debug('${handler.toString()} reported error');
        } else {
          Provider.of<EnvironmentProvider>(context, listen: false)
              .logger
              .warning('${handler.toString()} failed to report error');
        }
      }).timeout(_options.handlerTimeout, onTimeout: () {
        Provider.of<EnvironmentProvider>(context, listen: false)
            .logger
            .warning('timeout while ${handler.toString()} is reporting error');
      });
    });
  }

  @override
  void onUserReject(ErrorReport report) {
    Provider.of<EnvironmentProvider>(context, listen: false)
        .logger
        .debug('user rejected to report error');
  }

  BuildContext get context => _navigatorKey.currentState.overlay.context;
}
