import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

@immutable
class ErrorReport {
  final dynamic error;
  final dynamic stackTrace;
  final DateTime dateTime;
  final FlutterErrorDetails details;
  ErrorReport(
      {@required this.error,
      @required this.stackTrace,
      @required this.dateTime,
      this.details})
      : assert(error != null),
        assert(dateTime != null);

  Map<String, String> toJson() => {
        "error": error.toString(),
        "stackTrace": stackTrace?.toString(),
        "dateTime": dateTime.toIso8601String(),
      };

  @override
  String toString() => JsonEncoder.withIndent("  ").convert(toJson());
}
