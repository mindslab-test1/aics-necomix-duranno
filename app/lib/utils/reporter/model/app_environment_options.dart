import 'package:duranno_app/utils/reporter/handler/logger_report_handler.dart';
import 'package:duranno_app/utils/reporter/handler/report_handler.dart';
import 'package:duranno_app/utils/reporter/mode/silent_report_mode.dart';
import 'package:duranno_app/utils/reporter/mode/report_mode.dart';
import 'package:meta/meta.dart';

@immutable
class AppEnvironmentOptions {
  final ReportMode mode;
  final List<ReportHandler> handlers;
  final Duration handlerTimeout;
  AppEnvironmentOptions({
    @required this.mode,
    @required this.handlers,
    this.handlerTimeout = const Duration(milliseconds: 10000),
  })  : assert(mode != null),
        assert(handlers != null);

  factory AppEnvironmentOptions.getDefaultDevelopmentOptions() =>
      AppEnvironmentOptions(
        mode: SilentReportMode(),
        handlers: [LoggerReportHandler()],
      );

  factory AppEnvironmentOptions.getDefaultBetaOptions() =>
      AppEnvironmentOptions(
        mode: SilentReportMode(),
        handlers: [LoggerReportHandler()],
      );

  factory AppEnvironmentOptions.getDefaultProductionOptions() =>
      AppEnvironmentOptions(
        mode: SilentReportMode(),
        handlers: [LoggerReportHandler()],
      );
}
