import 'dart:async';

import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/reporter/handler/report_handler.dart';
import 'package:duranno_app/utils/reporter/model/error_report.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LoggerReportHandler implements ReportHandler {
  @override
  Future<bool> handle(ErrorReport report, BuildContext context) {
    if (report.error is DurannoError) {
      Provider.of<EnvironmentProvider>(context, listen: false)
          .logger
          .durannoError(report.error);
    } else {
      Provider.of<EnvironmentProvider>(context, listen: false)
          .logger
          .error(report.error, report.stackTrace);
    }
    return Future.value(true);
  }
}
