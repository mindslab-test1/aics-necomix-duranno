import 'dart:async';

import 'package:duranno_app/utils/reporter/model/error_report.dart';
import 'package:flutter/material.dart';

abstract class ReportHandler {
  Future<bool> handle(ErrorReport report, BuildContext context);
}
