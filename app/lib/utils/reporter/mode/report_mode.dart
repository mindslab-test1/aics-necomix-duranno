import 'package:duranno_app/utils/reporter/model/error_report.dart';
import 'package:flutter/material.dart';

abstract class ReportModeListener {
  void onUserAccept(ErrorReport report);
  void onUserReject(ErrorReport report);
}

abstract class ReportMode {
  ReportModeListener _listener;
  set reportModeListener(ReportModeListener listener) {
    _listener = listener;
  }

  void requestHandle(ErrorReport report, BuildContext context);

  void onUserAccept(ErrorReport report) {
    _listener.onUserAccept(report);
  }

  void onUserReject(ErrorReport report) {
    _listener.onUserReject(report);
  }
}
