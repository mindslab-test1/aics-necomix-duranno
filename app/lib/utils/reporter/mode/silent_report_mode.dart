import 'package:duranno_app/utils/reporter/mode/report_mode.dart';
import 'package:duranno_app/utils/reporter/model/error_report.dart';
import 'package:flutter/material.dart';

class SilentReportMode extends ReportMode {
  @override
  void requestHandle(ErrorReport report, BuildContext context) {
    super.onUserAccept(report);
  }
}
