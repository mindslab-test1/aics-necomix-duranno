/* Reference: https://github.com/QuirijnGB/lazy-load-scrollview/blob/master/lib/src/lazy_load_scrollview.dart */

/*
 * - Modified so that updating onEndOfPage callbacks can be registered on its descendant widgets.
 */

import 'package:flutter/widgets.dart';

enum LoadingStatus { LOADING, STABLE }

/// Signature for EndOfPageListeners
typedef void EndOfPageListenerCallback();

/// A widget that wraps a [Widget] and will trigger [onEndOfPage] when it
/// reaches the bottom of the list
class LazyLoadScrollView extends StatefulWidget {
  /// The [Widget] that this widget watches for changes on
  final Widget child;

  /// The offset to take into account when triggering [onEndOfPage] in pixels
  final int scrollOffset;

  @override
  State<StatefulWidget> createState() => LazyLoadScrollViewState();

  LazyLoadScrollView({
    Key key,
    @required this.child,
    this.scrollOffset = 100,
  })  : assert(child != null),
        super(key: key);
}

class LazyLoadScrollViewState extends State<LazyLoadScrollView> {
  LoadingStatus loadMoreStatus = LoadingStatus.STABLE;
  void Function() onScrollEndCallback;

  @override
  Widget build(BuildContext context) {
    return InheritedLazyScrollViewWidget(
      state: this,
      child: NotificationListener(
        child: widget.child,
        onNotification: (notification) =>
            _onNotification(notification, context),
      ),
    );
  }

  bool _onNotification(Notification notification, BuildContext context) {
    if (notification is ScrollUpdateNotification) {
      if (notification.metrics.maxScrollExtent > notification.metrics.pixels &&
          notification.metrics.maxScrollExtent - notification.metrics.pixels <=
              widget.scrollOffset) {
        if (loadMoreStatus != null && loadMoreStatus == LoadingStatus.STABLE) {
          if (onScrollEndCallback != null) {
            loadMoreStatus = LoadingStatus.LOADING;
            onScrollEndCallback();
          }
        }
      }
      return true;
    }
    if (notification is OverscrollNotification) {
      if (notification.overscroll > 0) {
        if (loadMoreStatus != null && loadMoreStatus == LoadingStatus.STABLE) {
          if (onScrollEndCallback != null) {
            loadMoreStatus = LoadingStatus.LOADING;
            onScrollEndCallback();
          }
        }
      }
      return true;
    }
    return false;
  }
}

class InheritedLazyScrollViewWidget extends InheritedWidget {
  final LazyLoadScrollViewState state;
  InheritedLazyScrollViewWidget({
    Key key,
    @required this.state,
    @required Widget child,
  })  : assert(state != null),
        assert(child != null),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) => true;

  static void setOnScrollEndCallback(BuildContext context,
          {void Function() callback}) =>
      context
          .dependOnInheritedWidgetOfExactType<InheritedLazyScrollViewWidget>()
          .state
          .onScrollEndCallback = callback;

  static void onDidUpdate(BuildContext context) {
    context
        .dependOnInheritedWidgetOfExactType<InheritedLazyScrollViewWidget>()
        .state
        .loadMoreStatus = LoadingStatus.STABLE;
  }
}
