import 'package:duranno_app/provider/route_provider.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:provider/provider.dart';

class IterableUtils {
  static Iterable<T> join<T>(
    Iterable<T> iterable, {
    @required T separator,
  }) {
    assert(iterable != null);
    assert(separator != null);

    return iterable.fold(
        <T>[],
        (prev, cur) => <T>[
              ...prev,
              ...(prev.isEmpty ? [] : [separator]),
              cur
            ]);
  }
}

class ColorUtils {
  static hexToColor(String hex) =>
      Color(int.parse(hex.replaceFirst('#', '0xFF')));
}

class NumUtils {
  static zeroPad(int number, int digits) {
    final numberStr = number.toString();
    return numberStr.padLeft(digits, '0');
  }

  static String formatTimeInSeconds(
    final num timeInSeconds, {
    final bool showHours = false,
    final bool round = true,
  }) {
    final int rounded = timeInSeconds is double
        ? round
            ? timeInSeconds.round()
            : timeInSeconds.floor()
        : timeInSeconds;

    final int hours = (rounded / 3600.0).floor();

    int minutes = (rounded / 60.0).floor();
    if (showHours) {
      minutes %= 60;
    }

    final int seconds = rounded % 60;

    if (showHours) {
      return '$hours:${zeroPad(minutes, 2)}:${zeroPad(seconds, 2)}';
    } else {
      return '$minutes:${zeroPad(seconds, 2)}';
    }
  }
}

dynamic getSafeField(
  dynamic raw, {
  Iterable<dynamic> fields,
}) {
  if (raw == null || fields == null || fields.length == 0) {
    return raw;
  }

  final field = fields.first;
  if (!(field is int || field is String)) {
    throw DurannoError(
      code: ErrorCode.Logic,
      message: 'invalid field argument: got $field',
    );
  }

  if (raw is List) {
    return field is int
        ? getSafeField(raw[field], fields: fields.skip(1))
        : null;
  } else {
    return getSafeField(raw[field], fields: fields.skip(1));
  }
}

class NeverGlowScrollBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}

mixin RebuildWidgetOnPopNext<T extends StatefulWidget> on State<T>, RouteAware {
  RouteProvider _route;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    final route = Provider.of<RouteProvider>(context, listen: false);
    if (route != _route) {
      _route = route;
      _route.observer.subscribe(this, ModalRoute.of(context));
    }
  }

  @override
  void dispose() {
    _route.observer.unsubscribe(this);

    super.dispose();
  }

  @override
  void didPopNext() {
    super.didPopNext();

    setState(() {});
  }
}
