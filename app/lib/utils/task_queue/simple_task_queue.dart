import 'dart:async';

import 'package:duranno_app/utils/task_queue/interface.dart';

class SimpleTaskQueue implements ITaskQueue {
  bool _isPending;
  int _count;

  final StreamController<bool> _isPendingController =
      StreamController<bool>.broadcast();
  final Set<String> _tasks = Set<String>();
  SimpleTaskQueue() {
    _isPending = false;
    _isPendingController.add(false);

    _count = 0;
  }

  set _pending(bool value) {
    _isPending = value;
    _isPendingController.add(value);
  }

  @override
  bool get isPending => _isPending;

  @override
  Stream<bool> get onPendingChanged => _isPendingController.stream;

  @override
  String register() {
    final taskId = _generateTaskId();
    _tasks.add(taskId);

    if (!_isPending) {
      _pending = true;
    }

    return taskId;
  }

  @override
  void resolve(String taskId) {
    _tasks.remove(taskId);

    if (_tasks.isEmpty) {
      _pending = false;
    }
  }

  @override
  void dispose() {
    _isPendingController.close();
  }

  String _generateTaskId() {
    return (_count++).toString();
  }
}
