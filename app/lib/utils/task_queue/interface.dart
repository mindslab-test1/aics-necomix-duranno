abstract class ITaskQueue {
  bool get isPending;
  Stream<bool> get onPendingChanged;

  /// registers a task to the queue.
  ///
  /// returns id of a task.
  String register();

  /// report that the registered task has finished.
  void resolve(String taskId);

  /// disposes this queue.
  void dispose();
}
