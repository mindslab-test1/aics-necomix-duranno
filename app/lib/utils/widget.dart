import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

final wrappedProgressIndicatorLight = Container(
  height: 48.0,
  child: Center(
    child: SizedBox(
      width: 24.0,
      height: 24.0,
      child: CircularProgressIndicator(
        strokeWidth: 2.0,
        valueColor: AlwaysStoppedAnimation<Color>(
          const Color.fromRGBO(255, 255, 255, 0.5),
        ),
      ),
    ),
  ),
);

final wrappedProgressIndicatorDark = Container(
  height: 48.0,
  child: Center(
    child: SizedBox(
      width: 24.0,
      height: 24.0,
      child: CircularProgressIndicator(
        strokeWidth: 2.0,
        valueColor: AlwaysStoppedAnimation<Color>(
          const Color.fromRGBO(0, 0, 0, 0.5),
        ),
      ),
    ),
  ),
);

final errorWidget = Container(
  height: 48.0,
  child: Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Icon(Icons.error, size: 24.0, color: NeocomixThemeColors.secondaryDark),
      SizedBox(width: 8.0),
      Text('정보를 불러올 수 없습니다.'),
    ],
  ),
);
