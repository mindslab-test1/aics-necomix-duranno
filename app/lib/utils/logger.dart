import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:meta/meta.dart';
import 'package:path_provider/path_provider.dart';

enum LogLevel { Verbose, Debug, Info, Warning, Error }

class Logger {
  static final _jsonEncoder = JsonEncoder.withIndent('  ');
  static const String _logFileName = 'log.txt';

  Directory _appDocDir;
  File _logFile;
  bool _initialized = false;
  List<dynamic> _preInitializeLoggedObjects = [];

  final AppStage appStage;

  int _severity;
  Logger({@required LogLevel level, @required this.appStage})
      : assert(level != null),
        assert(appStage != null),
        _severity = _getSeverity(level) {
    _initializeLogFile();
  }

  Future<void> _initializeLogFile() async {
    _appDocDir = await getApplicationDocumentsDirectory();
    _logFile = File('${_appDocDir.path}/$_logFileName');
    await _logFile.writeAsString('');
    _initialized = true;
    _preInitializeLoggedObjects.forEach(_writeToLogFile);
  }

  void _writeToLogFile(dynamic object) {
    if (!_initialized) {
      _preInitializeLoggedObjects.add(object);
      return;
    }

    _logFile.writeAsStringSync(
        '${object is String ? object : _jsonEncoder.convert(object)}\n',
        mode: FileMode.append);
  }

  void setLogLevel(LogLevel level) {
    _severity = _getSeverity(level);
  }

  String getMessageString(LogLevel level, String message) =>
      '[${DateTime.now().toIso8601String()}] [${_formatLogLevel(level)}] ${message ?? ''}';

  String getErrorString(dynamic error, dynamic stack) => """
${getMessageString(LogLevel.Error, error.toString())}
${getMessageString(LogLevel.Error, stack.toString())}
""";

  String getDurannoErrorString(DurannoError error) => """
[${DateTime.now().toIso8601String()}] [${_formatLogLevel(LogLevel.Error)}] [${error.code.toString()}] ${error.message ?? ''}
${error.stackTrace.toString()}
""";

  void logMessage(LogLevel level, String message) {
    if (_severity <= _getSeverity(level)) {
      final msgString = getMessageString(level, message);
      if (appStage == AppStage.Development) {
        log(msgString);
      }
      _writeToLogFile(msgString);
    }
  }

  void logError(LogLevel level, DurannoError error) {
    if (_severity <= _getSeverity(level)) {
      final errString = getDurannoErrorString(error);
      if (appStage == AppStage.Development) {
        log(errString);
      }
      _writeToLogFile(errString);
    }
  }

  void error(dynamic error, dynamic stack) {
    logMessage(LogLevel.Error, getErrorString(error, stack));
  }

  void durannoError(DurannoError error) => logError(LogLevel.Error, error);
  void warning(Object message) => logMessage(LogLevel.Warning,
      message is String ? message : _jsonEncoder.convert(message));
  void info(Object message) => logMessage(LogLevel.Info,
      message is String ? message : _jsonEncoder.convert(message));
  void debug(Object message) => logMessage(LogLevel.Debug,
      message is String ? message : _jsonEncoder.convert(message));
  void verbose(Object message) => logMessage(LogLevel.Verbose,
      message is String ? message : _jsonEncoder.convert(message));

  static int _getSeverity(LogLevel level) {
    switch (level) {
      case LogLevel.Verbose:
        return 100;
      case LogLevel.Debug:
        return 200;
      case LogLevel.Info:
        return 300;
      case LogLevel.Warning:
        return 400;
      case LogLevel.Error:
        return 500;
      default:
        return 0;
    }
  }

  static String _formatLogLevel(LogLevel level) =>
      level.toString().split('.').last.toUpperCase();
}
