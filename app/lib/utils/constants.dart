import 'package:flutter/material.dart';

const contentPadding = const EdgeInsets.symmetric(horizontal: 20.0);

const Duration audioFastForwardDuration = const Duration(seconds: 30);
const Duration audioRewindDuration = const Duration(seconds: 30);
