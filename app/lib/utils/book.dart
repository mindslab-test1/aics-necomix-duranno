import 'dart:convert';

import 'package:audio_service/audio_service.dart';
import 'package:equatable/equatable.dart';
import 'package:http/http.dart' as http;
import 'package:meta/meta.dart';

@immutable
class Chapter extends Equatable {
  final String title;
  final String mediaUri;
  final Duration duration;
  Chapter({
    @required this.title,
    this.mediaUri,
    this.duration,
  }) : assert(title != null);

  factory Chapter.fromJson(dynamic raw) => Chapter(
        title: raw['label'],
        duration: raw['duration'] != null
            ? Duration(milliseconds: (raw['duration'] * 1000.0).floor())
            : null,
        mediaUri: raw['mediaUri'],
      );

  dynamic toJson() => {
        'label': title,
        'mediaUri': mediaUri,
        'duration':
            duration != null ? (duration.inMilliseconds / 1000.0) : null,
      };

  @override
  List<Object> get props => [title, mediaUri, duration];

  bool get isSequence => mediaUri != null && duration != null;

  @override
  String toString() =>
      '{ title: $title, duration: $duration, mediaUri: $mediaUri }';
}

@immutable
class AudioItem {
  final String mediaUri;
  final String bookId;
  final int sequence;
  final String chapterTitle;
  final String bookTitle;
  final String authorName;
  final Duration duration;
  final String bookThumbnailUri;
  final Duration seekToPosition;
  AudioItem({
    @required this.mediaUri,
    @required this.bookId,
    this.sequence,
    this.chapterTitle,
    @required this.bookTitle,
    this.authorName,
    @required this.duration,
    this.bookThumbnailUri,
    this.seekToPosition,
  })  : assert(mediaUri != null),
        assert(bookId != null),
        assert(bookTitle != null),
        assert(duration != null);

  factory AudioItem.fromJson(dynamic raw) => AudioItem(
        mediaUri: raw['mediaUri'],
        bookId: raw['bookId'],
        sequence: raw['sequence'],
        chapterTitle: raw['chapterTitle'],
        bookTitle: raw['bookTitle'],
        authorName: raw['authorName'],
        duration: Duration(milliseconds: raw['duration']),
        bookThumbnailUri: raw['bookThumbnailUri'],
        seekToPosition: raw['seekToPosition'] != null
            ? Duration(milliseconds: raw['seekToPosition'])
            : null,
      );

  dynamic toJson() => {
        'mediaUri': mediaUri,
        'bookId': bookId,
        'sequence': sequence,
        'chapterTitle': chapterTitle,
        'bookTitle': bookTitle,
        'authorName': authorName,
        'duration': duration.inMilliseconds,
        'bookThumbnailUri': bookThumbnailUri,
        'seekToPosition': seekToPosition?.inMilliseconds,
      };

  @override
  String toString() => '''
  {
    mediaUri: $mediaUri,
    bookId: $bookId,
    sequence: $sequence,
    chapterTitle: $chapterTitle,
    bookTitle: $bookTitle,
    authorName: $authorName,
    duration: $duration,
    bookThumbnailUri: $bookThumbnailUri,
    seekToPosition: $seekToPosition
  }
  ''';
}

@immutable
class AudioQueue {
  final List<Chapter> chapters;
  final List<AudioItem> queue;
  final Duration totalDuration;
  final int initialSequence;
  final bool useSequence;
  AudioQueue({
    this.chapters,
    @required this.queue,
    @required this.totalDuration,
    @required this.initialSequence,
    @required this.useSequence,
  })  : assert(useSequence != null),
        assert(totalDuration != null),
        assert(queue != null);

  factory AudioQueue.fromJson(dynamic raw) => AudioQueue(
        chapters: (raw['chapters'] as List<dynamic>)
            .map<Chapter>((item) => Chapter.fromJson(item))
            .toList(),
        queue: (raw['queue'] as List<dynamic>)
            .map<AudioItem>((item) => AudioItem.fromJson(item))
            .toList(),
        totalDuration: Duration(milliseconds: raw['totalDuration']),
        initialSequence: raw['initialSequence'],
        useSequence: raw['useSequence'],
      );

  dynamic toJson() => {
        'chapters': chapters.map((chapter) => chapter.toJson()).toList(),
        'queue': queue.map((audioItem) => audioItem.toJson()).toList(),
        'totalDuration': totalDuration.inMilliseconds,
        'initialSequence': initialSequence,
        'useSequence': useSequence,
      };

  String generateMediaId(AudioItem item) {
    if (useSequence) {
      return '${item.bookId}.${item.sequence}';
    } else {
      return item.bookId;
    }
  }

  int getSequenceOfMediaItem(MediaItem mediaItem) {
    final mediaIds = queue.map(generateMediaId).toList();
    return mediaIds.indexWhere((id) => id == mediaItem.id);
  }

  int getSequenceOfChapter(Chapter chapter) {
    final chaptersWithMedia =
        chapters?.where((chapter) => chapter.isSequence)?.toList();
    return chaptersWithMedia?.indexOf(chapter) ?? 0;
  }

  String get bookId => queue[0].bookId;

  List<MediaItem> toMediaItems() {
    return queue
        .map(
          (audioItem) => MediaItem(
            id: generateMediaId(audioItem),
            album: audioItem.bookTitle,
            title: audioItem.chapterTitle ?? audioItem.bookTitle,
            artist: audioItem.authorName,
            duration: audioItem.duration,
            artUri: audioItem.bookThumbnailUri,
            extras: {
              'mediaUri': audioItem.mediaUri,
              'audioQueue': jsonEncode(toJson()),
            },
          ),
        )
        .toList();
  }

  @override
  String toString() => '''
  {
    chapters: $chapters,
    queue: $queue,
    totalDuration: $totalDuration,
    initialSequence: $initialSequence,
    useSequence: $useSequence
  }
  ''';
}

extension MediaItemListUtils on List<MediaItem> {
  AudioQueue restoreQueue() {
    /* this is an inverse operation of AudioQueue.toMediaItems() */
    assert(this.isNotEmpty);
    return AudioQueue.fromJson(jsonDecode(this.first.extras['audioQueue']));
  }
}

Future<AudioQueue> normalizeRawBook(
  dynamic raw, {
  int currentSequence = 0,
}) async {
  final String bookId = raw['id'];
  final String bookTitle = raw['title'];
  final String authorName = (raw['author'] ?? const {})['name'];
  final String bookThumbnailUri = raw['thumbnailUri'];
  final Duration bookDuration =
      Duration(milliseconds: (raw['duration'] * 1000.0).floor());

  final String mediaUri = raw['mediaUri'];
  final String metadataUri = raw['metadataUri'];

  final int lastPlayedSequence = (raw['myHistory'] ?? const {})['sequence'];
  final Duration lastPlayedPosition = raw['myHistory'] != null
      ? Duration(milliseconds: raw['myHistory']['position'])
      : null;

  final rawMetadata = metadataUri != null
      ? await http
          .get(metadataUri)
          .then((response) => jsonDecode(response.body))
      : null;

  final List<Chapter> chapters = ((rawMetadata ?? const {})['chapters'] as List)
      ?.map((e) => Chapter.fromJson(e))
      ?.toList();
  final bool hasChapters = chapters != null;

  final List<Chapter> chaptersWithMedia =
      chapters?.where((chapter) => chapter.isSequence)?.toList();
  final int Function(Chapter) getSequenceOf =
      (chapter) => chaptersWithMedia?.indexOf(chapter);

  final List<AudioItem> queue = chapters
          ?.where((chapter) => chapter.isSequence)
          ?.map((chapter) => AudioItem(
                mediaUri: chapter.mediaUri,
                bookId: bookId,
                sequence: getSequenceOf(chapter),
                chapterTitle: chapter.title,
                bookTitle: bookTitle,
                authorName: authorName,
                duration: chapter.duration,
                bookThumbnailUri: bookThumbnailUri,
                seekToPosition: lastPlayedSequence == getSequenceOf(chapter)
                    ? lastPlayedPosition
                    : null,
              ))
          ?.toList() ??
      [
        AudioItem(
          mediaUri: mediaUri,
          bookId: bookId,
          sequence: null,
          chapterTitle: bookTitle,
          bookTitle: bookTitle,
          authorName: authorName,
          duration: bookDuration,
          bookThumbnailUri: bookThumbnailUri,
          seekToPosition: lastPlayedPosition,
        ),
      ];

  return AudioQueue(
    chapters: chapters,
    queue: queue,
    totalDuration: bookDuration,
    initialSequence: lastPlayedSequence ?? (hasChapters ? 0 : null),
    useSequence: hasChapters,
  );
}
