import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/sliver/sliver_footer.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class PaginatedSliverList<T, R, K> extends StatefulWidget {
  final QueryOptions queryOptions;
  final List<R> Function(T data) itemsSelector;
  final String Function(T data) cursorSelector;
  final T Function(T prev, T cur) mergeFetchMoreResult;
  final Widget Function({
    Key key,
    R itemData,
    Future<QueryResult> Function() refetch,
  }) itemBuilder;
  final K Function(R itemData) itemKeySelector;
  final Axis scrollDirection;
  final int numPageItems;
  final Widget emptyLabelWidget;
  final bool isScrollViewNested;
  final PageStorageKey pageStorageKey;
  final Widget headerWidget;
  final Widget footerWidget;
  final double marginTop;
  final double contentMarginTop;
  final double contentMarginBottom;
  final double marginBetweenItems;
  PaginatedSliverList({
    Key key,
    @required this.queryOptions,
    @required this.itemsSelector,
    @required this.cursorSelector,
    @required this.mergeFetchMoreResult,
    @required this.itemBuilder,
    @required this.itemKeySelector,
    this.scrollDirection = Axis.vertical,
    this.numPageItems = 20,
    this.emptyLabelWidget,
    this.isScrollViewNested = false,
    this.pageStorageKey,
    this.headerWidget,
    this.footerWidget,
    this.marginTop = 0,
    this.contentMarginTop = 0,
    this.contentMarginBottom = 0,
    this.marginBetweenItems = 16.0,
  })  : assert(queryOptions != null),
        assert(itemsSelector != null),
        assert(cursorSelector != null),
        assert(mergeFetchMoreResult != null),
        assert(itemBuilder != null),
        assert(itemKeySelector != null),
        assert(numPageItems != null),
        assert(isScrollViewNested != null),
        assert(marginTop != null),
        assert(contentMarginTop != null),
        assert(contentMarginBottom != null),
        assert(marginBetweenItems != null),
        super(key: key);

  @override
  _PaginatedSliverListState createState() => _PaginatedSliverListState();
}

class _PaginatedSliverListState<T, R, K>
    extends State<PaginatedSliverList<T, R, K>>
    with RouteAware, RebuildWidgetOnPopNext<PaginatedSliverList<T, R, K>> {
  @override
  Widget build(BuildContext context) {
    return LazyLoadScrollView(
      child: Builder(
        builder: (context) => Query(
          options: widget.queryOptions,
          builder: (result, {refetch, fetchMore}) => _buildView(
            context,
            result: result,
            fetchMore: fetchMore,
            refetch: refetch,
          ),
        ),
      ),
    );
  }

  Widget _buildView(BuildContext context,
      {QueryResult result,
      Future<QueryResult> Function() refetch,
      dynamic Function(FetchMoreOptions) fetchMore}) {
    final data = result.data != null ? widget.itemsSelector(result.data) : null;
    final cursor =
        result.data != null ? widget.cursorSelector(result.data) : null;
    final hasData = data != null;

    if (result.hasException) {
      final logger = Provider.of<EnvironmentProvider>(context).logger;
      logger.error(result.exception, null);
    }

    final fetchNextPage = () {
      if (!result.loading && cursor != null) {
        fetchMore(
          FetchMoreOptions(
            documentNode: widget.queryOptions.documentNode,
            variables: {'cursor': cursor, 'limit': widget.numPageItems},
            updateQuery: (prev, cur) {
              InheritedLazyScrollViewWidget.onDidUpdate(context);
              return widget.mergeFetchMoreResult(prev, cur);
            },
          ),
        );
      } else {
        InheritedLazyScrollViewWidget.onDidUpdate(context);
      }
    };

    InheritedLazyScrollViewWidget.setOnScrollEndCallback(
      context,
      callback: fetchNextPage,
    );

    return CustomScrollView(
      key: widget.pageStorageKey,
      physics: ClampingScrollPhysics(),
      scrollDirection: widget.scrollDirection,
      slivers: [
        if (widget.isScrollViewNested)
          SliverOverlapInjector(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context)),
        SliverToBoxAdapter(child: _buildMargin(widget.marginTop)),

        /* header */
        if (widget.headerWidget != null)
          SliverToBoxAdapter(child: widget.headerWidget),
        SliverToBoxAdapter(child: _buildMargin(widget.contentMarginTop)),

        /* empty */
        if (hasData && data.isEmpty && widget.emptyLabelWidget != null)
          SliverToBoxAdapter(child: widget.emptyLabelWidget),

        /* items */
        if (hasData && data.isNotEmpty)
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, index) {
                if (index.isEven) {
                  final semanticIndex = index ~/ 2;
                  return widget.itemBuilder(
                    key: ValueKey<K>(
                        widget.itemKeySelector(data[semanticIndex])),
                    itemData: data[semanticIndex],
                    refetch: refetch,
                  );
                } else {
                  return _buildMargin(widget.marginBetweenItems);
                }
              },
              childCount: 2 * data.length - 1,
            ),
          ),

        /* loading */
        if (result.loading)
          SliverToBoxAdapter(child: wrappedProgressIndicatorDark),

        /* error */
        if (result.hasException) SliverToBoxAdapter(child: errorWidget),

        /* footer */
        SliverToBoxAdapter(child: _buildMargin(widget.contentMarginBottom)),
        if (widget.footerWidget != null)
          SliverFooter(child: widget.footerWidget),
      ],
    );
  }

  Widget _buildMargin(double margin) {
    if (widget.scrollDirection == Axis.horizontal) {
      return SizedBox(width: margin);
    } else if (widget.scrollDirection == Axis.vertical) {
      return SizedBox(height: margin);
    }

    return Container();
  }
}
