import 'package:meta/meta.dart';

enum ErrorCode {
  Logic,
  LocalStorage,
  Network,
  NotInitialized,
  NotImplemented,
  Server,
  Timeout,
}

enum ExceptionCode {
  AuthAlreadyRegisteredLocally,
  AuthDisabledUser,
  AuthDuplicateEmail,
  AuthEmailNotVerified,
  AuthGoogleSignInUserCancelled,
  AuthInvalidCredential,
  AuthInvalidEmail,
  AuthUnauthorized,
  AuthUserNotFound,
  AuthWeakPassword,
  AuthWrongPassword,
  AuthExpiredActionCode,
  AuthInvalidActionCode,
  MediaUserNotSubscribed,
  MediaNotFound,
  MediaUserExceedLimit,
  MediaUnknown,
  MediaToken,
  ResourceForbidden,
}

class DurannoError extends Error {
  final ErrorCode code;
  final String message;
  DurannoError({@required this.code, this.message});

  @override
  String toString() => '[${code.toString()}] ${message ?? ''}';
}

class DurannoException implements Exception {
  final ExceptionCode code;
  final dynamic message;
  DurannoException({@required this.code, this.message});

  @override
  String toString() => '[${code.toString()}] ${message ?? ''}';
}
