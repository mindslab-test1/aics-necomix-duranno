import 'dart:io';

import 'package:device_info/device_info.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/provider/local_storage_provider.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/uuid.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MachineInfoProvider extends ChangeNotifier {
  final DeviceInfoPlugin deviceInfoPlugin;
  MachineInfoProvider() : deviceInfoPlugin = DeviceInfoPlugin();

  EnvironmentProvider environmentProvider;
  LocalStorageProvider _localStorageProvider;

  String machineId;
  Map<String, dynamic> deviceInfo;

  LocalStorageProvider get localStorageProvider => _localStorageProvider;
  set localStorageProvider(LocalStorageProvider localStorageProvider) {
    _localStorageProvider = localStorageProvider;
    initialize();
  }

  Future<void> initialize() async {
    if (localStorageProvider == null) {
      throw DurannoError(code: ErrorCode.NotInitialized);
    }

    machineId = await localStorageProvider.getString(LocalStorageKey.machineId);

    if (machineId == null) {
      machineId = Uuid.v4;
      await localStorageProvider.setString(
          LocalStorageKey.machineId, machineId);
    }

    await _initializeDeviceInfo();

    if (environmentProvider != null) {
      environmentProvider.logger.info({'machineId': machineId});
      environmentProvider.logger.info(deviceInfo);
    }
  }

  Future<void> _initializeDeviceInfo() async {
    try {
      if (Platform.isAndroid) {
        deviceInfo = _readAndroidBuildData(await deviceInfoPlugin.androidInfo);
      } else if (Platform.isIOS) {
        deviceInfo = _readIosDeviceInfo(await deviceInfoPlugin.iosInfo);
      }
    } on PlatformException {
      deviceInfo = {'Error:': 'Failed to retrieve platform version.'};
    }
  }

  Map<String, dynamic> _readAndroidBuildData(AndroidDeviceInfo build) {
    return <String, dynamic>{
      'version.securityPatch': build.version.securityPatch,
      'version.sdkInt': build.version.sdkInt,
      'version.release': build.version.release,
      'version.previewSdkInt': build.version.previewSdkInt,
      'version.incremental': build.version.incremental,
      'version.codename': build.version.codename,
      'version.baseOS': build.version.baseOS,
      'board': build.board,
      'bootloader': build.bootloader,
      'brand': build.brand,
      'device': build.device,
      'display': build.display,
      'fingerprint': build.fingerprint,
      'hardware': build.hardware,
      'host': build.host,
      'id': build.id,
      'manufacturer': build.manufacturer,
      'model': build.model,
      'product': build.product,
      'supported32BitAbis': build.supported32BitAbis,
      'supported64BitAbis': build.supported64BitAbis,
      'supportedAbis': build.supportedAbis,
      'tags': build.tags,
      'type': build.type,
      'isPhysicalDevice': build.isPhysicalDevice,
      'androidId': build.androidId,
      'systemFeatures': build.systemFeatures,
    };
  }

  Map<String, dynamic> _readIosDeviceInfo(IosDeviceInfo data) {
    return <String, dynamic>{
      'name': data.name,
      'systemName': data.systemName,
      'systemVersion': data.systemVersion,
      'model': data.model,
      'localizedModel': data.localizedModel,
      'identifierForVendor': data.identifierForVendor,
      'isPhysicalDevice': data.isPhysicalDevice,
      'utsname.sysname:': data.utsname.sysname,
      'utsname.nodename:': data.utsname.nodename,
      'utsname.release:': data.utsname.release,
      'utsname.version:': data.utsname.version,
      'utsname.machine:': data.utsname.machine,
    };
  }
}
