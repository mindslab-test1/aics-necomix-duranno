import 'dart:async';
import 'dart:convert';

import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageKey {
  static const String machineId = 'MACHINE_ID';
  static const String recentSearchId = 'RecentKeyId';
}

class LocalStorageProvider extends ChangeNotifier {
  EnvironmentProvider environmentProvider;

  SharedPreferences _sharedPreferences;
  Future<SharedPreferences> getSharedPreferences() async {
    if (_sharedPreferences == null) {
      _sharedPreferences = await SharedPreferences.getInstance();
    }
    return _sharedPreferences;
  }

  Future<void> log() async {
    if (environmentProvider == null) {
      throw DurannoError(code: ErrorCode.NotInitialized);
    }

    final prefs = await getSharedPreferences();
    final values = Map<String, dynamic>.fromIterable(prefs.getKeys(),
        key: (key) => key, value: (key) => prefs.get(key));
    environmentProvider.logger.debug(values);
  }

  Future<T> _getTask<T>(
      FutureOr<T> Function(SharedPreferences) task, String key) async {
    if (environmentProvider == null) {
      throw DurannoError(code: ErrorCode.NotInitialized);
    }

    try {
      return await getSharedPreferences().then(task).then((value) {
        environmentProvider.logger.debug('get $value := localStorage[$key]');
        return value;
      });
    } catch (e) {
      environmentProvider.logger.error(e, null);
      throw DurannoError(
          code: ErrorCode.LocalStorage,
          message: 'failed: get localStorage[$key]');
    }
  }

  String _defaultFormatSuccessMessage(String key, dynamic value) =>
      'set localStorage[$key] := ${value.toString()}';
  String _defaultFormatErrorMessage(String key) =>
      'failed: set localStorage[$key]';

  Future<void> _setOrClearTask<T>(
      FutureOr<bool> Function(SharedPreferences) task, String key, T value,
      {String Function(String, T) formatSuccessMessage,
      String Function(String) formatErrorMessage}) async {
    if (environmentProvider == null) {
      throw DurannoError(code: ErrorCode.NotInitialized);
    }

    await getSharedPreferences().then(task).then((success) {
      if (success) {
        environmentProvider.logger.debug(
            (formatSuccessMessage ?? _defaultFormatSuccessMessage)(key, value));
      } else {
        throw DurannoError(
            code: ErrorCode.LocalStorage,
            message: (formatErrorMessage ?? _defaultFormatErrorMessage)(key));
      }
    });
  }

  Future<int> getInt(String key) async {
    assert(key != null && key.isNotEmpty);
    return _getTask<int>((prefs) => prefs.getInt(key), key);
  }

  Future<bool> getBool(String key) async {
    assert(key != null && key.isNotEmpty);
    return _getTask<bool>((prefs) => prefs.getBool(key), key);
  }

  Future<String> getString(String key) async {
    assert(key != null && key.isNotEmpty);
    return _getTask<String>((prefs) => prefs.getString(key), key);
  }

  Future<Map<String, dynamic>> getObject(String key) async {
    assert(key != null && key.isNotEmpty);
    return _getTask<Map<String, dynamic>>((prefs) {
      final value = prefs.getString(key);
      return value != null ? jsonDecode(value) : null;
    }, key);
  }

  Future<List<dynamic>> getList(String key) async {
    assert(key != null && key.isNotEmpty);
    return _getTask<List<dynamic>>((prefs) {
      final value = prefs.getString(key);
      return value != null ? jsonDecode(value) : null;
    }, key);
  }

  Future<void> clear() =>
      _setOrClearTask<void>((prefs) => prefs.clear(), null, null,
          formatSuccessMessage: (key, value) => 'clear localStorage',
          formatErrorMessage: (key) => 'failed: clear localStorage');

  Future<void> clearKey(String key) =>
      _setOrClearTask<void>((prefs) => prefs.remove(key), key, null,
          formatSuccessMessage: (key, value) => 'clear localStorage[$key]',
          formatErrorMessage: (key) => 'failed: clear localStorage[$key]');

  Future<void> setInt(String key, int value) async {
    assert(key != null && key.isNotEmpty);
    assert(value != null);
    await _setOrClearTask<int>((prefs) => prefs.setInt(key, value), key, value);
  }

  Future<void> setBool(String key, bool value) async {
    assert(key != null && key.isNotEmpty);
    assert(value != null);
    await _setOrClearTask<bool>(
        (prefs) => prefs.setBool(key, value), key, value);
  }

  Future<void> setString(String key, String value) async {
    assert(key != null && key.isNotEmpty);
    assert(value != null);
    await _setOrClearTask<String>(
        (prefs) => prefs.setString(key, value), key, value);
  }

  Future<void> setObject(String key, Map<String, dynamic> value) async {
    assert(key != null && key.isNotEmpty);
    assert(value != null);
    await _setOrClearTask<Map<String, dynamic>>(
        (prefs) => prefs.setString(key, jsonEncode(value)), key, value);
  }

  Future<void> setList(String key, List<dynamic> value) async {
    assert(key != null && key.isNotEmpty);
    assert(value != null);
    await _setOrClearTask<List<dynamic>>(
        (prefs) => prefs.setString(key, jsonEncode(value)), key, value);
  }
}
