import 'package:flutter/widgets.dart';

class RouteProvider extends ChangeNotifier {
  final RouteObserver observer;

  RouteProvider({@required this.observer});
}
