import 'package:duranno_app/app.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';

class AnalyticsCategories {
  static const String auth = 'auth';
}

class AnalyticsEvents {
  static const String linkWithGoogle = 'link_with_google';
  static const String linkWithGoogleCancelled = 'link_with_google_cancelled';
  static const String signInAnonymously = 'sign_in_anonymously';
  static const String signInLocalUser = 'sign_in_local_user';
  static const String signInGoogleUser = 'sign_in_google_user';
  static const String signInGoogleUserCancelled =
      'sign_in_google_user_cancelled';
  static const String signOut = 'sign_out';
  static const String registerLocalUser = 'register_local_user';
  static const String requestEmailVerification = 'request_email_verification';
  static const String requestPasswordResetEmail =
      'request_password_reset_email';
  static const String resetPasswordWithCode = 'reset_password_with_code';
}

class AnalyticsProvider extends ChangeNotifier {
  bool _initialized = false;
  String _machineId;
  FirebaseAnalytics _analytics;

  AnalyticsProvider() {
    _analytics = FirebaseAnalytics();
  }

  Future<void> initialize({
    String machineId,
  }) async {
    await setCurrentScreen(App.initialRouteName);
    _machineId = machineId;
    _initialized = true;
  }

  Future<void> event(
    String name, {
    String category,
    Map<String, dynamic> parameters = const {},
  }) async {
    assert(name != null);

    if (!_initialized) {
      throw DurannoError(code: ErrorCode.NotInitialized);
    }

    await _analytics.logEvent(
        name: _formatEventName(
          name: name,
          category: category,
        ),
        parameters: _applyDefaultParameters(parameters));
  }

  Future<void> setCurrentScreen(String routeName) async {
    assert(routeName != null);

    await _analytics.setCurrentScreen(screenName: routeName);
  }

  Future<void> setUserId(String id) async {
    await _analytics.setUserId(id);
  }

  String _formatEventName({@required String name, String category}) {
    assert(name != null);

    if (category != null) {
      return '${category}_$name';
    }

    return name;
  }

  Map<String, dynamic> _applyDefaultParameters(
          Map<String, dynamic> parameters) =>
      {
        ...parameters,
        'machine_id': _machineId,
      };
}
