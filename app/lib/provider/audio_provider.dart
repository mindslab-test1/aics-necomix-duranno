import 'dart:async';

import 'package:audio_service/audio_service.dart';
import 'package:duranno_app/graphql/query/book.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/audio/audio_controller.dart';
import 'package:duranno_app/utils/audio/audio_service_controller.dart';
import 'package:duranno_app/utils/book.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tuple/tuple.dart';

enum AudioQueryState { Pending, Completed, Error }

class AudioProvider extends ChangeNotifier {
  StreamSubscription<AudioControllerState> _controllerStateSubscription;
  StreamSubscription<MediaItem> _mediaItemFromAudioControllerSubscription;

  AudioProvider({
    @required BuildContext context,
  }) {
    assert(context != null);

    _contextSubject.add(context);
    _contextSubject.stream.listen(controller?.onContextChanged);

    /* logging */
    final logger =
        Provider.of<EnvironmentProvider>(context, listen: false).logger;

    onControllerChanged.listen(
        (controller) => logger.verbose('Controller updated: $controller'));
    onAudioQueueChanged.listen(
        (audioQueue) => logger.verbose('Audio queue updated: $audioQueue'));
    onMediaItemChanged.listen(
        (mediaItem) => logger.verbose('Media item updated: $mediaItem'));
    onAudioQueryStateChanged.listen((state) =>
        logger.verbose('Audio query state changed: ${state.toString()}'));

    _audioQueueFromNetworkSubject.stream.listen(
      (tuple) async {
        if (tuple == null) {
          return;
        }

        final audioQueue = tuple.item1;
        final initialSequence = tuple.item2 ?? audioQueue.initialSequence;

        _controllerStateSubscription?.cancel();

        final safeController =
            controller ?? (AudioServiceController()..initialize(this.context));
        _controllerSubject.add(safeController);

        await safeController.prepare(
          audioQueue: audioQueue,
          initialSequence: initialSequence,
        );

        _controllerStateSubscription = safeController.onStateChanged
            .where((state) => state == AudioControllerState.Stopped)
            .listen((_) => _onStop());
      },
    );

    onControllerChanged.listen(
      (controller) {
        _mediaItemFromAudioControllerSubscription?.cancel();
        _mediaItemFromAudioControllerSubscription = controller
            ?.onMediaItemChanged
            ?.listen(_mediaItemFromAudioControllerSubject.add);
      },
    );

    Rx.merge([
      _audioQueueFromNetworkSubject.stream.map(
        (tuple) {
          if (tuple == null) {
            return null;
          }

          final audioQueue = tuple.item1;
          final initialSequence = tuple.item2 ?? audioQueue.initialSequence;
          final initialItem = audioQueue.queue[initialSequence ?? 0];

          return MediaItem(
            id: null,
            album: initialItem.bookTitle,
            title: initialItem.chapterTitle ?? initialItem.bookTitle,
            artist: initialItem.authorName,
            duration: initialItem.duration,
            artUri: initialItem.bookThumbnailUri,
          );
        },
      ),
      _onMediaItemFromAudioControllerChanged
          .where((mediaItem) => mediaItem != null),
    ]).listen(_mediaItemSubject.add);

    _audioQueryStateSubject.add(AudioQueryState.Pending);

    _tryRestoringOrphanedController(context);
  }

  BehaviorSubject<BuildContext> _contextSubject =
      BehaviorSubject<BuildContext>();

  BuildContext get context => _contextSubject.value;

  BehaviorSubject<IAudioController> _controllerSubject =
      BehaviorSubject<IAudioController>();

  /// Current [AudioController].
  IAudioController get controller => _controllerSubject.value;

  /// A [Stream] that exposes the current [AudioController] object.
  Stream<IAudioController> get onControllerChanged =>
      _controllerSubject.stream.distinct();

  BehaviorSubject<MediaItem> _mediaItemFromAudioControllerSubject =
      BehaviorSubject<MediaItem>();

  Stream<MediaItem> get _onMediaItemFromAudioControllerChanged =>
      _mediaItemFromAudioControllerSubject.stream.distinct();

  BehaviorSubject<Tuple2<AudioQueue, int>> _audioQueueFromNetworkSubject =
      BehaviorSubject<Tuple2<AudioQueue, int>>();

  BehaviorSubject<AudioQueue> _audioQueueFromRestorationSubject =
      BehaviorSubject<AudioQueue>();

  /// Current [AudioQueue] object.
  AudioQueue get audioQueue =>
      _audioQueueFromRestorationSubject.value ??
      _audioQueueFromNetworkSubject.value?.item1;

  /// A [Stream] that exposes the current [AudioQueue] object.
  Stream<AudioQueue> get onAudioQueueChanged => Rx.merge([
        _audioQueueFromNetworkSubject.stream
            .map((tuple) => tuple?.item1)
            .distinct(),
        _audioQueueFromRestorationSubject.stream,
      ]);

  BehaviorSubject<MediaItem> _mediaItemSubject = BehaviorSubject<MediaItem>();

  /// Current [MediaItem].
  MediaItem get mediaItem => _mediaItemSubject.value;

  /// A [Stream] that exposes the current [MediaItem].
  ///
  /// The value is also available before media is actually ready: the temporary [MediaItem] is created from the audio metadata.
  Stream<MediaItem> get onMediaItemChanged => _mediaItemSubject.stream;

  BehaviorSubject<AudioQueryState> _audioQueryStateSubject =
      BehaviorSubject<AudioQueryState>();

  /// Current query state.
  AudioQueryState get audioQueryState => _audioQueryStateSubject.value;

  /// A [Stream] that exposes current query state.
  Stream<AudioQueryState> get onAudioQueryStateChanged =>
      _audioQueryStateSubject.stream;

  /// Fetch audio metadata from the server, and prepares audio controller.
  /// The audio metadata is available at [onAudioQueueChanged].
  Future<void> fetch({
    @required String bookId,
    int initialSequence,
  }) async {
    assert(bookId != null);

    if (bookId == audioQueue?.bookId) {
      if (initialSequence != audioQueue.getSequenceOfMediaItem(mediaItem)) {
        await controller.seekToSequence(initialSequence);
      }
      return;
    }

    _audioQueryStateSubject.add(AudioQueryState.Pending);

    return BookMediaQuery.instance.execute(
      context,
      args: {'bookId': bookId},
      onCompleted: (data) async {
        _audioQueueFromNetworkSubject.add(
          Tuple2(await normalizeRawBook(data), initialSequence),
        );
        _audioQueryStateSubject.add(AudioQueryState.Completed);
      },
      onError: (err) {
        _audioQueryStateSubject.add(AudioQueryState.Error);
      },
    );
  }

  List<void Function()> _listeners = [];

  void addOnStopListener(void Function() listener) {
    _listeners.add(listener);
  }

  void removeOnStopListener(void Function() listener) {
    _listeners.remove(listener);
  }

  Future<void> _onStop() async {
    await controller?.disconnect();

    _audioQueueFromNetworkSubject.add(null);
    _audioQueueFromRestorationSubject.add(null);
    _controllerSubject.add(null);

    _listeners.forEach((listener) => listener());
  }

  Future<void> _tryRestoringOrphanedController(BuildContext context) async {
    final controller = AudioServiceController()..initialize(context);
    final restored = await controller.restoreFromService();
    if (restored) {
      _audioQueueFromRestorationSubject.add(controller.audioQueue);
      _mediaItemSubject.add(await controller.mediaItem);
      _controllerSubject.add(controller);
      _audioQueryStateSubject.add(AudioQueryState.Completed);
      _controllerStateSubscription?.cancel();
      _controllerStateSubscription = controller.onStateChanged
          .where((state) => state == AudioControllerState.Stopped)
          .listen((_) => _onStop());
    }
  }

  @override
  void dispose() {
    super.dispose();

    controller?.dispose();

    _contextSubject.close();
    _controllerSubject.close();
    _mediaItemFromAudioControllerSubject.close();
    _mediaItemSubject.close();
    _audioQueueFromNetworkSubject.close();
    _audioQueueFromRestorationSubject.close();
    _audioQueryStateSubject.close();
  }
}
