import 'dart:convert';
import 'dart:io';

import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Api {
  static const String heartbeat = '/';

  static const String authCheck = '/auth/check';
}

enum HttpMethod { Get, Post, Put, Delete }

class ApiProvider extends ChangeNotifier {
  String apiUri;
  Logger logger;

  ApiProvider();

  static const Duration _timeout = const Duration(milliseconds: 10000);

  void _log(Object message) {
    if (logger == null) {
      return;
    }

    logger.debug(message);
  }

  Future<http.Response> Function(
    dynamic, {
    Map<String, String> headers,
    dynamic body,
  }) _getMethodFunc(HttpMethod method) {
    switch (method) {
      case HttpMethod.Get:
        return (uri, {headers, body}) => http.get(uri, headers: headers);
      case HttpMethod.Post:
        return http.post;
      case HttpMethod.Delete:
        return (uri, {headers, body}) => http.delete(uri, headers: headers);
      case HttpMethod.Put:
        return http.put;
      default:
        throw DurannoError(code: ErrorCode.Logic);
    }
  }

  Future<Map<String, dynamic>> _request(
    HttpMethod method,
    String relativeUri, {
    Map<String, String> headers,
    String idToken,
  }) async {
    final String methodStr = method.toString().split('.').last.toUpperCase();
    final methodFunc = _getMethodFunc(method);

    _log('Request to $methodStr ${apiUri + relativeUri} ...');
    try {
      return await methodFunc(apiUri + relativeUri, headers: {
        ...(headers ?? {}),
        'Authorization': idToken != null ? 'Bearer $idToken' : null,
      }).then((response) {
        _log('$methodStr ${apiUri + relativeUri}: ${response.statusCode}');

        if (response.statusCode.toString().startsWith('5')) {
          throw DurannoError(
              code: ErrorCode.Server,
              message:
                  'internal server error while $methodStr ${apiUri + relativeUri}');
        }

        final body = jsonDecode(response.body);
        _log(body);
        return body;
      }).timeout(_timeout, onTimeout: () {
        throw DurannoError(
            code: ErrorCode.Timeout,
            message: 'timeout while $methodStr ${apiUri + relativeUri}');
      });
    } on SocketException {
      throw DurannoError(code: ErrorCode.Network);
    }
  }

  Future<Map<String, dynamic>> heartbeat() =>
      _request(HttpMethod.Get, Api.heartbeat);

  Future<Map<String, dynamic>> authCheck(
      AuthenticationProvider authentication) async {
    return _request(
      HttpMethod.Get,
      Api.authCheck,
      idToken: await authentication.idToken,
    );
  }
}
