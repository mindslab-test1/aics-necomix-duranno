import 'package:duranno_app/provider/analytics_provider.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/logger.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';

class FirebaseAuthErrorCode {
  static const String operationNotAllowed = 'operation-not-allowed';
  static const String emailAlreadyInUse = 'email-already-in-use';
  static const String invalidEmail = 'invalid-email';
  static const String weakPassword = 'weak-password';
  static const String userDisabled = 'user-disabled';
  static const String userNotFound = 'user-not-found';
  static const String wrongPassword = 'wrong-password';
  static const String accountExistsWithDifferentCredential =
      'account-exists-with-different-credential';
  static const String invalidCredential = 'invalid-credential';
  static const String providerAlreadyLinked = 'provider-already-linked';
  static const String credentialAlreadyInUse = 'credential-already-in-use';
  static const String expiredActionCode = 'expired-action-code';
  static const String invalidActionCode = 'invalid-action-code';
}

class AuthenticationProvider extends ChangeNotifier {
  Logger logger;
  AnalyticsProvider analytics;

  User _user;

  /// Retrieves currently signed user.
  ///
  /// Throws:
  ///   - [ExceptionCode.AuthUnauthorized]: if user is not signed
  ///   - [ExceptionCode.AuthEmailNotVerified]: if user is not anonymous, but email address is not verified
  User get user {
    if (_user == null) {
      throw DurannoException(code: ExceptionCode.AuthUnauthorized);
    }
    if (!_user.isAnonymous && !_user.emailVerified) {
      throw DurannoException(code: ExceptionCode.AuthEmailNotVerified);
    }

    return _user;
  }

  Future<String> get idToken {
    if (_user == null) {
      throw DurannoException(code: ExceptionCode.AuthUnauthorized);
    }

    return _user.getIdToken();
  }

  bool get isSigned => _user != null;
  bool get isAnonymous => isSigned && _user.isAnonymous;
  bool get isEmailVerified => isSigned && _user.emailVerified;
  bool get isGoogleAccount =>
      isSigned &&
      _user.providerData.any((data) => data.providerId.contains('google.com'));
  bool get isAppleAccount =>
      isSigned &&
      _user.providerData.any((data) => data.providerId.contains('apple.com'));
  bool get isSocialAccount => isGoogleAccount || isAppleAccount;

  AuthenticationProvider();

  Future<void> initialize() async {
    FirebaseAuth.instance.userChanges().listen(_onAuthStateChange);
  }

  /// Signs in user anonymously.
  Future<void> signInAnonymously() async {
    try {
      await FirebaseAuth.instance.signInAnonymously();
      await _event(
        AnalyticsEvents.signInAnonymously,
        category: AnalyticsCategories.auth,
      );
    } on FirebaseAuthException catch (e, stackTrace) {
      if (e.code == FirebaseAuthErrorCode.operationNotAllowed) {
        throw DurannoError(
            code: ErrorCode.NotImplemented,
            message: 'Anonymous sign-in is not allowed in this project.');
      }

      logger.error(e, stackTrace);
    }
  }

  /// Signs out currently signed user.
  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    await _event(
      AnalyticsEvents.signOut,
      category: AnalyticsCategories.auth,
    );
    _user = null;
  }

  /// Create an user with given email address.
  ///
  /// [name] parameter is optional. If [name] parameter is given, the [displayName] property of [User] will be set.
  ///
  /// Throws:
  ///   - [ExceptionCode.AuthInvalidEmail]
  ///   - [ExcpetionCode.AuthDuplicateEmail]
  ///   - [ExceptionCode.AuthWeakPassword]
  Future<void> registerLocalUser({
    @required String email,
    @required String password,
    String name,
  }) async {
    assert(email != null);
    assert(password != null);

    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );

      if (name != null) {
        assert(_user != null);
        await _user.updateProfile(displayName: name);
      }

      await _event(
        AnalyticsEvents.registerLocalUser,
        category: AnalyticsCategories.auth,
      );
    } on FirebaseAuthException catch (e, stackTrace) {
      if (e.code == FirebaseAuthErrorCode.operationNotAllowed) {
        throw DurannoError(
          code: ErrorCode.NotImplemented,
          message:
              'Creating user with email and password is not allowed in this project.',
        );
      }
      if (e.code == FirebaseAuthErrorCode.invalidEmail) {
        throw DurannoException(
          code: ExceptionCode.AuthInvalidEmail,
        );
      }
      if (e.code == FirebaseAuthErrorCode.emailAlreadyInUse) {
        throw DurannoException(
          code: ExceptionCode.AuthDuplicateEmail,
        );
      }
      if (e.code == FirebaseAuthErrorCode.weakPassword) {
        throw DurannoException(
          code: ExceptionCode.AuthWeakPassword,
          message: 'should use password longer or equal than 6 characters',
        );
      }

      logger.error(e, stackTrace);
    }
  }

  /// Sign in with email address and password.
  ///
  /// Throws:
  ///   - [ExceptionCode.AuthInvalidEmail]
  ///   - [ExceptionCode.AuthDisabledUser]
  ///   - [ExceptionCode.AuthUserNotFound]
  ///   - [ExceptionCode.AuthWrongPassword]
  Future<void> signInWithLocalUser({
    @required String email,
    @required String password,
  }) async {
    assert(email != null);
    assert(password != null);

    try {
      await FirebaseAuth.instance.signInWithEmailAndPassword(
        email: email,
        password: password,
      );
      await _event(
        AnalyticsEvents.signInLocalUser,
        category: AnalyticsCategories.auth,
      );
    } on FirebaseAuthException catch (e, stackTrace) {
      if (e.code == FirebaseAuthErrorCode.invalidEmail) {
        throw DurannoException(
          code: ExceptionCode.AuthInvalidEmail,
        );
      }
      if (e.code == FirebaseAuthErrorCode.userDisabled) {
        throw DurannoException(
          code: ExceptionCode.AuthDisabledUser,
        );
      }
      if (e.code == FirebaseAuthErrorCode.userNotFound) {
        throw DurannoException(
          code: ExceptionCode.AuthUserNotFound,
        );
      }
      if (e.code == FirebaseAuthErrorCode.wrongPassword) {
        throw DurannoException(
          code: ExceptionCode.AuthWrongPassword,
        );
      }

      logger.error(e, stackTrace);
    }
  }

  /// Request an email verification with currently signed user.
  ///
  /// [requestEmailVerification] can be called only during local account registration, since all [user] retrieval from [AuthenticationProvider] requires an user whose email address is already verified.
  /// [requestEmailVerification] immediately returns if user's email address is already verified.
  Future<void> requestEmailVerification() async {
    if (_user == null) {
      throw DurannoError(
        code: ErrorCode.Logic,
        message: 'user is not signed in',
      );
    }

    if (_user.emailVerified) {
      return;
    }

    await _user.sendEmailVerification();
    await _event(
      AnalyticsEvents.requestEmailVerification,
      category: AnalyticsCategories.auth,
    );
  }

  /// Refreshes current user.
  ///
  /// [refresh] is useful when user context can be updated outside application: e.g. verifying user email address.
  /// Throws:
  ///   - [ExceptionCode.AuthUnauthorized]
  Future<void> refresh() async {
    if (_user == null) {
      throw DurannoException(code: ExceptionCode.AuthUnauthorized);
    }

    await _user.reload();
  }

  /// Signs in user using google account.
  ///
  /// The google login prompt will be shown.
  ///
  /// Throws:
  ///   - [ExceptionCode.AuthDisabledUser]: if the account is disabled or deleted
  ///   - [ExceptionCode.AuthInvalidCredential]: if somehow the google credential returned from prompt is invalid
  ///   - [ExceptionCode.AuthAlreadyRegisteredLocally]: if account with given email address already exists, with email/password login method. The application should redirect user to sign in using email and password.
  ///   - [ExceptionCode.AuthGoogleSignInUserCancelled]: if user cancelled during google sign in
  Future<void> signInWithGoogle() async {
    try {
      await FirebaseAuth.instance
          .signInWithCredential(await _googleAuthCredential);
      await _event(
        AnalyticsEvents.signInGoogleUser,
        category: AnalyticsCategories.auth,
      );
    } on FirebaseAuthException catch (e, stackTrace) {
      if (e.code == FirebaseAuthErrorCode.operationNotAllowed) {
        throw DurannoError(
          code: ErrorCode.NotImplemented,
          message: 'Signing with google is not allowed in this project.',
        );
      }
      if (e.code == FirebaseAuthErrorCode.userDisabled) {
        throw DurannoException(
          code: ExceptionCode.AuthDisabledUser,
        );
      }
      if (e.code == FirebaseAuthErrorCode.invalidCredential) {
        throw DurannoException(
          code: ExceptionCode.AuthInvalidCredential,
        );
      }
      if (e.code ==
          FirebaseAuthErrorCode.accountExistsWithDifferentCredential) {
        final email = e.email;
        final userSignInMethods =
            await FirebaseAuth.instance.fetchSignInMethodsForEmail(email);

        if (userSignInMethods.first == 'password') {
          throw DurannoException(
            code: ExceptionCode.AuthAlreadyRegisteredLocally,
          );
        }

        logger.warning(e);
      }

      logger.error(e, stackTrace);
    } on DurannoException catch (e) {
      if (e.code == ExceptionCode.AuthGoogleSignInUserCancelled) {
        await _event(
          AnalyticsEvents.signInGoogleUserCancelled,
          category: AnalyticsCategories.auth,
        );
      }

      throw e;
    }
  }

  /// Links currently signed user with google account.
  ///
  /// The google login prompt will be shown.
  ///
  /// Throws:
  ///   - [ExceptionCode.AuthInvalidCredential]: if somehow the google credential returned from prompt is invalid
  ///   - [ExceptionCode.AuthGoogleSignInUserCancelled]: if user cancelled during google sign in
  Future<void> linkWithGoogle() async {
    try {
      await user.linkWithCredential(await _googleAuthCredential);
      await _event(
        AnalyticsEvents.linkWithGoogle,
        category: AnalyticsCategories.auth,
      );
    } on FirebaseAuthException catch (e, stackTrace) {
      if (e.code == FirebaseAuthErrorCode.operationNotAllowed) {
        throw DurannoError(
          code: ErrorCode.NotImplemented,
          message: 'Signing with google is not allowed in this project',
        );
      }
      if (e.code == FirebaseAuthErrorCode.providerAlreadyLinked) {
        return;
      }
      if (e.code == FirebaseAuthErrorCode.invalidCredential) {
        throw DurannoException(
          code: ExceptionCode.AuthInvalidCredential,
        );
      }
      if (e.code == FirebaseAuthErrorCode.credentialAlreadyInUse) {
        await FirebaseAuth.instance.signInWithCredential(e.credential);
        await _event(
          AnalyticsEvents.linkWithGoogle,
          category: AnalyticsCategories.auth,
        );
      }

      logger.error(e, stackTrace);
    } on DurannoException catch (e) {
      if (e.code == ExceptionCode.AuthGoogleSignInUserCancelled) {
        await _event(
          AnalyticsEvents.linkWithGoogleCancelled,
          category: AnalyticsCategories.auth,
        );
      }

      throw e;
    }
  }

  /// Signs in user using apple account.
  ///
  /// The apple login prompt will be shown.
  ///
  /// Throws:
  ///   - [ExceptionCode.AuthDisabledUser]: if the account is disabled or deleted
  ///   - [ExceptionCode.AuthInvalidCredential]: if somehow the apple credential returned from prompt is invalid
  ///   - [ExceptionCode.AuthAlreadyRegisteredLocally]: if account with given email address already exists, with email/password login method. The application should redirect user to sign in using email and password.
  ///   - [ExceptionCode.AuthGoogleSignInUserCancelled]: if user cancelled during apple sign in
  Future<void> signInWithApple() async {
    throw DurannoError(
      code: ErrorCode.NotImplemented,
    );
  }

  /// Links currently signed user with apple account.
  ///
  /// The apple login prompt will be shown.
  ///
  /// Throws:
  ///   - [ExceptionCode.AuthInvalidCredential]: if somehow the apple credential returned from prompt is invalid
  ///   - [ExceptionCode.AuthGoogleSignInUserCancelled]: if user cancelled during apple sign in

  Future<void> linkWithApple() async {
    throw DurannoError(
      code: ErrorCode.NotImplemented,
    );
  }

  /// Requests an email for resetting password of an existing user.
  ///
  /// The email containing the confirmation code will be sent to the given email, if the user registered with given email exists.
  /// Currently, FirebaseAuth does not provide whether the user with given email exists.

  Future<void> requestPasswordResetEmail({
    @required String email,
  }) async {
    assert(email != null);

    await FirebaseAuth.instance.sendPasswordResetEmail(email: email);
    await _event(
      AnalyticsEvents.requestPasswordResetEmail,
      category: AnalyticsCategories.auth,
    );
  }

  /// Resets password with confirmation code received via [requestPasswordResetEmail].
  ///
  /// Throws:
  ///   - [ExceptionCode.AuthDisabledUser]: if the account is disabled
  ///   - [ExceptionCode.AuthUserNotFound]: if the account is deleted
  ///   - [ExceptionCode.AuthWeakPassword]: if given [newPassword] is weak (less than 6 characters)
  ///   - [ExceptionCode.AuthExpiredActionCode]: if given [code] has expired or already used. Ensure that user should be able to request new confirmation code.
  ///   - [ExceptionCode.AuthInvalidActionCode]: if given [code] was wrong.

  Future<void> resetPasswordWithConfirmationCode({
    @required String code,
    @required String newPassword,
  }) async {
    assert(code != null);
    assert(newPassword != null);

    try {
      await FirebaseAuth.instance
          .confirmPasswordReset(code: code, newPassword: newPassword);
      await _event(
        AnalyticsEvents.resetPasswordWithCode,
        category: AnalyticsCategories.auth,
      );
    } on FirebaseAuthException catch (e, stackTrace) {
      if (e.code == FirebaseAuthErrorCode.userDisabled) {
        throw DurannoException(
          code: ExceptionCode.AuthDisabledUser,
        );
      }
      if (e.code == FirebaseAuthErrorCode.userNotFound) {
        throw DurannoException(
          code: ExceptionCode.AuthUserNotFound,
        );
      }
      if (e.code == FirebaseAuthErrorCode.weakPassword) {
        throw DurannoException(
          code: ExceptionCode.AuthWeakPassword,
        );
      }
      if (e.code == FirebaseAuthErrorCode.expiredActionCode) {
        throw DurannoException(
          code: ExceptionCode.AuthExpiredActionCode,
        );
      }
      if (e.code == FirebaseAuthErrorCode.invalidActionCode) {
        throw DurannoException(
          code: ExceptionCode.AuthInvalidActionCode,
        );
      }

      logger.error(e, stackTrace);
    }
  }

  Future<void> _event(
    String name, {
    String category,
    Map<String, dynamic> parameters = const {},
  }) async {
    if (analytics == null) {
      logger.warning('Logger in AuthenticationProvider is not initialized.');
    }

    await analytics.event(name, category: category, parameters: parameters);
  }

  Future<void> _onAuthStateChange(User user) async {
    _user = user;
    logger.debug('Auth update: user := $user');

    if (analytics != null) {
      if (user != null) {
        if (user.isAnonymous) {
          await analytics.setUserId(user.uid);
        }
      } else {
        await analytics.setUserId(null);
      }
    }

    notifyListeners();
  }

  Future<OAuthCredential> get _googleAuthCredential async {
    final googleUser = await GoogleSignIn(scopes: ['email']).signIn();

    if (googleUser == null) {
      throw DurannoException(
        code: ExceptionCode.AuthGoogleSignInUserCancelled,
      );
    }

    final googleAuth = await googleUser.authentication;
    final googleAuthCredential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );

    return googleAuthCredential;
  }
}
