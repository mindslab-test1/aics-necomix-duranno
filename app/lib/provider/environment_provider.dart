import 'dart:io';

import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

enum AppStage { Development, Beta, Production }
enum ApiStage { Dev, Beta, Release }

extension AppStageUtils on AppStage {
  String get name => this.toString().split('.').last.toLowerCase();

  static AppStage parse(String value) => AppStage.values.firstWhere(
        (appStage) => appStage.name == value,
        orElse: () {
          print("""
  [WARN] Invalid "APP_STAGE" envrionment variable: got $value
  [WARN] Using fallback AppStage.Development.
          """);
          return AppStage.Development;
        },
      );
}

extension ApiStageUtils on ApiStage {
  String get name => this.toString().split('.').last.toLowerCase();

  static ApiStage parse(String value) => ApiStage.values.firstWhere(
        (apiStage) => apiStage.name == value,
        orElse: () {
          print("""
  [WARN] Invalid "API_STAGE" environment variable: got $value
  [WARN] Using fallback ApiStage.Dev.
      """);
          return ApiStage.Dev;
        },
      );
}

class EnvironmentProvider extends ChangeNotifier {
  final AppStage appStage;
  final String apiName;
  final String mediaAuthApiName;
  final ApiStage apiStage;
  final String apiUri;
  final String resourceUri;
  final Logger logger;
  EnvironmentProvider({
    @required this.appStage,
    @required this.apiName,
    @required this.mediaAuthApiName,
    @required this.apiStage,
    @required this.apiUri,
    @required this.resourceUri,
  }) : logger = Logger(level: _getLogLevel(appStage), appStage: appStage);

  factory EnvironmentProvider.fromDotEnv() {
    final env = DotEnv().env;

    final appStage = AppStageUtils.parse(env['APP_STAGE']);

    final apiName = env['API_NAME'];
    if (apiName == null) {
      throw DurannoError(
        code: ErrorCode.NotImplemented,
        message: 'API_NAME environment variable is missing',
      );
    }

    final mediaAuthApiName = env['MEDIA_AUTH_API_NAME'];
    if (mediaAuthApiName == null) {
      throw DurannoError(
        code: ErrorCode.NotImplemented,
        message: 'MEDIA_AUTH_API_NAME environment variable is missing',
      );
    }

    final apiStage = ApiStageUtils.parse(env['API_STAGE']);

    final apiUri = env['API_URI'];
    if (apiUri == null) {
      throw DurannoError(
        code: ErrorCode.NotImplemented,
        message: 'API_URI environment variable is missing',
      );
    }

    final resourceUri = env['RESOURCE_URI'];
    if (resourceUri == null) {
      throw DurannoError(
        code: ErrorCode.NotImplemented,
        message: 'RESOURCE_URI environment variable is missing',
      );
    }

    return EnvironmentProvider(
      appStage: appStage,
      apiName: apiName,
      mediaAuthApiName: mediaAuthApiName,
      apiStage: apiStage,
      apiUri: apiUri,
      resourceUri: resourceUri,
    );
  }

  /// Attaches a baseUrl to a given relative [uri], forming a full url for the resource.
  String getResourceUri(String uri) => '$resourceUri$uri';

  /// Fetches a file from the resource bucket, which baseUrl is stored at [resourceUri].
  Future<File> getResource(String uri) {
    return DefaultCacheManager().getSingleFile(getResourceUri(uri));
  }

  /// Fetches a file, and parses the content into String.
  ///
  /// Throws [ExceptionCode.ResourceForbidden] if the ACL is not allowed for public read, or the resource does not exist.
  Future<String> parseResource(String uri) async {
    final file = await getResource(uri);
    final content = await file.readAsString();

    if (content.contains('Access Denied')) {
      throw DurannoException(code: ExceptionCode.ResourceForbidden);
    }

    return content;
  }

  static LogLevel _getLogLevel(AppStage appStage) {
    switch (appStage) {
      case AppStage.Development:
        return LogLevel.Debug;
      case AppStage.Beta:
        return LogLevel.Info;
      case AppStage.Production:
        return LogLevel.Info;
      default:
        return LogLevel.Debug;
    }
  }
}
