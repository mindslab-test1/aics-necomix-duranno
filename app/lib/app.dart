import 'package:duranno_app/graphql_app.dart';
import 'package:duranno_app/provider/analytics_provider.dart';
import 'package:duranno_app/provider/api_provider.dart';
import 'package:duranno_app/provider/audio_provider.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/provider/local_storage_provider.dart';
import 'package:duranno_app/provider/machine_info_provider.dart';
import 'package:duranno_app/provider/route_provider.dart';
import 'package:duranno_app/route/audio/audio_player_route.dart';
import 'package:duranno_app/route/audio/audio_sequence_route.dart';
import 'package:duranno_app/route/customer_service_route.dart';
import 'package:duranno_app/route/book_detail_route.dart';
import 'package:duranno_app/route/find/confirm_reset_password_route.dart';
import 'package:duranno_app/route/find/find_password_route.dart';
import 'package:duranno_app/route/home_route.dart';
import 'package:duranno_app/route/init_route.dart';
import 'package:duranno_app/route/library/comment_details_route.dart';
import 'package:duranno_app/route/library/library_home_route.dart';
import 'package:duranno_app/route/library/playlist_details_route.dart';
import 'package:duranno_app/route/login_route.dart';
import 'package:duranno_app/route/membership/home_route.dart';
import 'package:duranno_app/route/membership/terms_of_service_route.dart';
import 'package:duranno_app/route/membership/unsubscribe_route.dart';
import 'package:duranno_app/route/membership/user_agreement_route.dart';
import 'package:duranno_app/route/privacy_policy_route.dart';
import 'package:duranno_app/route/profile/announcement_route.dart';
import 'package:duranno_app/route/profile/my_profile_route.dart';
import 'package:duranno_app/route/register/email_confirmation_route.dart';
import 'package:duranno_app/route/register/registration_route.dart';
import 'package:duranno_app/route/register/user_agreement_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/route/settings_route.dart';
import 'package:duranno_app/route/search_route.dart';
import 'package:duranno_app/route/terms_of_service_route.dart';
import 'package:duranno_app/theme/duranno/colors.dart';
import 'package:duranno_app/theme/duranno/text_field.dart';
import 'package:duranno_app/theme/neocomix/typography.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/reporter/app_runner.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();

  static String get initialRouteName => RouteName.init;
}

class _AppState extends State<App> {
  RouteObserver<PageRoute> _routeObserver;

  @override
  void initState() {
    super.initState();

    _routeObserver = RouteObserver<PageRoute>();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<RouteProvider>(
          create: (_) => RouteProvider(observer: _routeObserver),
          lazy: false,
        ),
        ChangeNotifierProvider<EnvironmentProvider>(
          create: (_) => EnvironmentProvider.fromDotEnv(),
          lazy: false,
        ),
        ChangeNotifierProvider<AnalyticsProvider>(
          create: (_) => AnalyticsProvider(),
          lazy: false,
        ),
        ChangeNotifierProxyProvider<EnvironmentProvider, LocalStorageProvider>(
          create: (context) => LocalStorageProvider(),
          update: (context, environmentProvider, oldLocalStorageProvider) =>
              oldLocalStorageProvider
                ..environmentProvider = environmentProvider,
          lazy: false,
        ),
        ChangeNotifierProxyProvider2<EnvironmentProvider, LocalStorageProvider,
            MachineInfoProvider>(
          create: (context) => MachineInfoProvider(),
          update: (context, environmentProvider, localStorageProvider,
                  oldMachineInfoProvider) =>
              oldMachineInfoProvider
                ..environmentProvider = environmentProvider
                ..localStorageProvider = localStorageProvider,
          lazy: false,
        ),
        ChangeNotifierProxyProvider<EnvironmentProvider, ApiProvider>(
          create: (context) => ApiProvider(),
          update: (context, environmentProvider, oldApiProvider) =>
              oldApiProvider
                ..apiUri = environmentProvider.apiUri
                ..logger = environmentProvider.logger,
          lazy: false,
        ),
        ChangeNotifierProxyProvider2<EnvironmentProvider, AnalyticsProvider,
            AuthenticationProvider>(
          create: (context) => AuthenticationProvider(),
          update: (context, environmentProvider, analyticsProvider,
                  oldAuthenticationProvider) =>
              oldAuthenticationProvider
                ..logger = environmentProvider.logger
                ..analytics = analyticsProvider,
          lazy: false,
        ),
      ],
      builder: (context, child) => _buildFirebaseApp(
        context,
        child: GraphQLApp(
          child: ChangeNotifierProvider<AudioProvider>(
            create: (context) => AudioProvider(context: context),
            builder: (context, child) => _buildMaterialApp(context),
          ),
        ),
      ),
    );
  }

  Widget _buildFirebaseApp(BuildContext context, {@required Widget child}) {
    assert(child != null);

    final analytics = Provider.of<AnalyticsProvider>(context);
    final authentication = Provider.of<AuthenticationProvider>(
      context,
      listen: false,
    );
    final machineInfo = Provider.of<MachineInfoProvider>(context);

    return FutureBuilder<FirebaseApp>(
      future: Firebase.initializeApp().then((app) async {
        await analytics.initialize(machineId: machineInfo.machineId);
        await analytics.event('init');
        await authentication.initialize();
        return app;
      }),
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          throw DurannoError(
            code: ErrorCode.NotInitialized,
            message: 'failed to initialize firebase app.',
          );
        }

        if (snapshot.connectionState == ConnectionState.done) {
          return child;
        }

        return DecoratedBox(
          decoration: BoxDecoration(color: _theme.scaffoldBackgroundColor),
        );
      },
    );
  }

  Widget _buildMaterialApp(BuildContext context) => MaterialApp(
        initialRoute: App.initialRouteName,
        routes: {
          RouteName.init: (context) => InitRoute(),
          RouteName.home: (context) => HomeRoute(),
          RouteName.login: (context) => LoginRoute(),
          RouteName.library: (context) => LibraryHomeRoute(),
          RouteName.playlistDetails: (context) => PlaylistDetailsRoute(),
          RouteName.commentDetails: (context) => CommentDetailsRoute(),
          RouteName.membershipHome: (context) => MembershipHomeRoute(),
          RouteName.membershipUserAgreement: (context) =>
              MembershipUserAgreementRoute(),
          RouteName.membershipTermsOfService: (context) =>
              MembershipTermsOfServiceRoute(),
          RouteName.registrationEmailConfirmation: (context) =>
              EmailConfirmationRoute(),
          RouteName.registrationForm: (context) => RegistrationRoute(),
          RouteName.registrationUserAgreement: (context) =>
              UserAgreementRoute(),
          RouteName.customerService: (context) => CustomerServiceRoute(),
          RouteName.settings: (context) => SettingsRoute(),
          RouteName.search: (context) => SearchRoute(),
          RouteName.bookDetail: (context) => BookDetailRoute(),
          RouteName.audioPlayer: (context) => AudioPlayerRoute(),
          RouteName.audioSequence: (context) => AudioSequenceRoute(),
          RouteName.myProfile: (context) => MyProfileRoute(),
          RouteName.termsOfService: (context) => TermsOfServiceRoute(),
          RouteName.privacyPolicy: (context) => PrivacyPolicyRoute(),
          RouteName.announcement: (context) => AnnouncementRoute(),
          RouteName.findPassword: (context) => FindPasswordRoute(),
          RouteName.confirmResetPassword: (context) =>
              ConfirmResetPasswordRoute(),
          RouteName.unsubscribe: (context) => UnsubscribeRoute(),
        },
        theme: _theme,
        debugShowCheckedModeBanner: false,
        navigatorKey: AppRunner.navigatorKey,
        navigatorObservers: [
          _routeObserver,
        ],
      );

  static final _theme = ThemeData(
    scaffoldBackgroundColor: DurannoThemeColors.white,
    accentColor: Colors.transparent,
    textTheme: typography,
    inputDecorationTheme: inputDecorationTheme,
    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
  );
}
