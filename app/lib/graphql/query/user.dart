import 'package:duranno_app/graphql/util/query.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class UserQuery {
  static final me = gql(r'''
    query Me {
      me {
        id
        email
        emailVerified
        registered
        subscribed
      }
    }
  ''');

  static final myProfile = gql(r'''
    query MyProfile {
      me {
        id
        email
        emailVerified
        registered
        subscribed
        activeSubscription {
          id
          updatedAt
          provider
          productId
          level
          title
          limit
          metadata
        }
      }
    }
  ''');

  static final myCommentsSummary = gql(r'''
  query MyCommentsSummary {
    MyComments {
      count
    }
    myComments(limit: 3) {
      comments {
        id
        book {
          id
          title
          thumbnailUri
        }
        content
        rating
      }
    }
  }
  ''');

  static final myCommentsCount = gql(r'''
  query MyCommentsCount {
    MyComments {
      count
    }
  }
  ''');

  static final myCommentsPage = gql(r'''
  query MyComments($limit: Int, $cursor: ID) {
    myComments(limit: $limit, cursor: $cursor) {
      comments {
        id
        book {
          id
          title
          thumbnailUri
        }
        content
        rating
      }
      cursor
    }
  }
  ''');
}

class MeQuery extends QueryHandler {
  static MeQuery _instance;
  static MeQuery get instance {
    if (_instance == null) {
      _instance = MeQuery();
    }

    return _instance;
  }

  final _gql = gql(r'''
  query Me {
    me {
      __typename
      id
      email
      emailVerified
      registered
      subscribed
    }
  }
  ''');

  @override
  dynamic get documentNode => _gql;

  @override
  dynamic dataSelector(dynamic raw) => raw['me'];
}

class MyHistoryQuery extends PaginatedQueryHandler {
  static MyHistoryQuery _instance;
  static MyHistoryQuery get instance {
    if (_instance == null) {
      _instance = MyHistoryQuery();
    }

    return _instance;
  }

  final _gql = gql(r'''
  query MyHistory($limit: Int, $cursor: ID) {
    History {
      __typename
      id
      list(limit: $limit, cursor: $cursor) {
        items {
          __typename
          id
          book {
            __typename
            id
            title
            author {
              name
            }
            thumbnailUri
          }
          sequence
          chapterTitle
          mediaUri
          position
        }
        cursor
      }
    }
  }
  ''');

  @override
  dynamic get documentNode => _gql;

  @override
  dynamic dataSelector(dynamic raw) =>
      getSafeField(raw, fields: ['History', 'list']);

  @override
  List<dynamic> itemsSelector(dynamic raw) =>
      (dataSelector(raw) ?? const {})['items'];

  @override
  String cursorSelector(dynamic raw) =>
      (dataSelector(raw) ?? const {})['cursor'];

  @override
  dynamic mergeFetchMoreResult(dynamic prev, dynamic cur) {
    return Map<String, dynamic>.from(cur)
      ..addAll({
        'History': Map<String, dynamic>.from(cur['History'])
          ..addAll({
            'list': Map<String, dynamic>.from(cur['History']['list'])
              ..addAll({
                'items': List<dynamic>.from(cur['History']['list']['items'])
                  ..insertAll(0, prev['History']['list']['items'])
              }),
          }),
      });
  }
}
