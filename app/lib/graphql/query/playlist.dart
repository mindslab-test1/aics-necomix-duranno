import 'package:duranno_app/graphql/util/query.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class PlaylistSummaryQuery extends QueryHandler {
  static PlaylistSummaryQuery _instance;
  static PlaylistSummaryQuery get instance {
    if (_instance == null) {
      _instance = PlaylistSummaryQuery();
    }

    return _instance;
  }

  static final _gql = gql(r'''
  query PlaylistSummary {
    Playlist {
      __typename
      id
      count
      list(limit: 3) {
        items {
          __typename
          id
          book {
            __typename
            id
            title
            author {
              name
            }
            rating {
              average
            }
            numViews
            thumbnailUri
            tags {
              id
              text
              color
              backgroundColor
            }
          }
        }
      }
    }
  }
  ''');

  @override
  dynamic get documentNode => _gql;

  @override
  dynamic dataSelector(dynamic raw) => raw['Playlist'];
}

class PlaylistCountQuery extends QueryHandler {
  static PlaylistCountQuery _instance;
  static PlaylistCountQuery get instance {
    if (_instance == null) {
      _instance = PlaylistCountQuery();
    }

    return _instance;
  }

  static final _gql = gql(r'''
  query PlaylistCount {
    Playlist {
      __typename
      id
      count
    }
  }
  ''');

  @override
  dynamic get documentNode => _gql;

  @override
  dynamic dataSelector(dynamic raw) => raw['Playlist'];
}

class PlaylistQuery {
  static final count = gql(r'''
  query PlaylistCount {
    Playlist {
      __typename
      id
      count
    }
  }
  ''');

  static final page = gql(r'''
  query PlaylistPage($limit: Int, $cursor: ID) {
    Playlist {
      __typename
      id
      list(limit: $limit, cursor: $cursor) {
        items {
          __typename
          id
          book {
            __typename
            id
            title
            author {
              name
            }
            rating {
              average
            }
            numViews
            thumbnailUri
            tags {
              id
              text
              color
              backgroundColor
            }
          }
        }
        cursor
      }
    }
  }
  ''');

  static List<dynamic> playlistItemsSelector(dynamic data) =>
      data['Playlist']['list']['items'];

  static String playlistCursorSelector(dynamic data) =>
      data['Playlist']['list']['cursor'];

  static dynamic mergePlaylistFetchMore(dynamic prev, dynamic cur) => {
        'Playlist': {
          ...cur,
          'list': {
            ...cur,
            'items': [
              ...prev['Playlist']['list']['items'],
              ...cur['Playlist']['list']['items'],
            ]
          }
        },
      };
}
