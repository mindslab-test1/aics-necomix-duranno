import 'package:graphql_flutter/graphql_flutter.dart';

class SearchQuery {
  static final keywordBookCount = gql(r'''
  query SearchKeywordBookCount($keyword: String!) {
    countAll: searchBookCount(
      input: {
        keyword: $keyword
      }
    )
    countTitle: searchBookCount(
      input: {
        keyword: $keyword,
        keywordScope: ["title"]
      }
    )
    countAuthor: searchBookCount(
      input: {
        keyword: $keyword,
        keywordScope: ["authorName"]
      }
    )
  }
  ''');

  static final books = gql(r'''
  query SearchBooks(
    $keyword: String,
    $keywordScope: [String],
    $tag: String,
    $orderBy: String,
    $limit: Int,
    $cursor: ID
  ) {
    searchBooks(
      input: {
        keyword: $keyword
        keywordScope: $keywordScope
        tag: $tag
        orderBy: $orderBy
      },
      limit: $limit,
      cursor: $cursor
    ) {
      items {
        id
        title
        author {
          name
        }
        tags {
          text
          color
          backgroundColor
        }
        numViews
        rating {
          average
        }
        thumbnailUri
      }
      cursor
    }
  }
  ''');
}
