import 'package:graphql_flutter/graphql_flutter.dart';

class SubscriptionQuery {
  static final token = gql(r'''
    query SubscriptionToken($bookId: ID!, $sequence: Int) {
      subscriptionToken(bookId: $bookId, sequence: $sequence) {
        token
        error
      }
    }
  ''');
}
