import 'package:duranno_app/graphql/util/query.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class BookQuery {
  static final featured = gql(r'''
  query FeaturedBooks($limit: Int) {
    featuredBooks(limit: $limit) {
      __typename
      id
      title
      author {
        name
      }
      thumbnailUri
      rating {
        average
      }
    }
  }
  ''');

  static final header = gql(r'''
  query BookBanner($bookId: String!) {
    book(bookId: $bookId) {
      __typename
      id
      title
    }
  }
  ''');

  static final banner = gql(r'''
  query BookBanner($bookId: String!) {
    book(bookId: $bookId) {
      __typename
      id
      thumbnailUri
      title
      author {
        name
      }
    }
  }
  ''');

  static final primaryAction = gql(r'''
  query BookPrimaryAction($bookId: String!) {
    me {
      subscribed
    }
    book(bookId: $bookId) {
      __typename
      id
      title
      isInMyPlaylist
      mediaUri
      metadataUri
      myHistory {
        sequence
        chapterTitle
        mediaUri
        position
      }
    }
  }
  ''');

  static final info = gql(r'''
  query BookInfo($bookId: String!) {
    book(bookId: $bookId) {
      __typename
      id
      tags {
        text
        color
        backgroundColor
      }
      description
      author {
        name
        profileImageUri
        description
      }
    }
  }
  ''');

  static final tableOfContents = gql(r'''
  query BookTableOfContents($bookId: String!) {
    book(bookId: $bookId) {
      __typename
      id
      title,
      duration
      mediaUri
      metadataUri
    }
  }
  ''');

  static final rating = gql(r'''
  query BookRating($bookId: String!) {
    me {
      __typename
      id
      emailVerified
      subscribed
    }
    book(bookId: $bookId) {
      __typename
      id
      rating {
        average
        me
        numSubmissions
        items {
          value
          numSubmissions
        }
      }
    }
  }
  ''');

  static final comments = gql(r'''
  query BookComments($bookId: String!, $cursor: ID, $limit: Int) {
    me {
      __typename
      id
      emailVerified
      subscribed
    }
    bookComments(bookId: $bookId, cursor: $cursor, limit: $limit) {
      __typename
      id
      comments {
        __typename
        id
        user {
          id
          email
        }
        updatedAt
        content
      }
      cursor
    }
  }
  ''');
}

class BookMediaQuery extends QueryHandler {
  static BookMediaQuery _instance;
  static BookMediaQuery get instance {
    if (_instance == null) {
      _instance = BookMediaQuery();
    }

    return _instance;
  }

  static final _gql = gql(r'''
  query Media($bookId: String!) {
    book(bookId: $bookId) {
      __typename
      id
      title
      author {
        name
      }
      thumbnailUri
      duration
      mediaUri
      metadataUri
      myHistory {
        sequence
        chapterTitle
        mediaUri
        position
      }
    }
  }
  ''');

  @override
  dynamic get documentNode => _gql;

  @override
  dynamic dataSelector(dynamic raw) => raw['book'];
}
