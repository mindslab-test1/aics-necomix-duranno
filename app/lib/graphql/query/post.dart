import 'package:graphql_flutter/graphql_flutter.dart';

class PostQuery {
  static final recentAnnouncements = gql(r'''
  query RecentAnnouncements {
    announcements(limit: 3) {
      items {
        id
        title
        content
      }
    }
  }
  ''');

  static final announcements = gql(r'''
  query Annoucements($cursor: ID, $limit: Int) {
    announcements(cursor: $cursor, limit: $limit) {
      items {
        id
        title
        content
        updatedAt
      }
      cursor
    }
  }
  ''');
}
