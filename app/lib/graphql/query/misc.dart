import 'package:duranno_app/graphql/util/query.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class HomeBannersQuery extends QueryHandler {
  static HomeBannersQuery _instance;
  static HomeBannersQuery get instance {
    if (_instance == null) {
      _instance = HomeBannersQuery();
    }

    return _instance;
  }

  static final _gql = gql(r'''
  query HomeBanners {
    HomeBanner {
      all {
        id
        bannerImageUri
        linkRoute
        linkRouteArgs
      }
    }
  }
  ''');

  @override
  dynamic get documentNode => _gql;

  @override
  dynamic dataSelector(dynamic raw) => raw['HomeBanner']['all'];
}
