import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

abstract class QueryHandler {
  dynamic get documentNode;
  dynamic dataSelector(dynamic raw);

  Future<QueryResult> execute(
    BuildContext context, {
    void Function(dynamic) onCompleted,
    void Function(OperationException) onError,
    Map<String, dynamic> args,
  }) async {
    assert(documentNode != null);
    assert(context != null);

    final client = GraphQLProvider.of(context).value;

    final result = await client.query(
      QueryOptions(
        documentNode: documentNode,
        variables: args,
      ),
    );

    if (result.hasException) {
      final logger =
          Provider.of<EnvironmentProvider>(context, listen: false).logger;
      logger.error(result.exception, null);

      if (onError != null) {
        onError(result.exception);
      }
    } else {
      if (onCompleted != null) {
        onCompleted(dataSelector(result.data));
      }
    }

    return result;
  }

  Widget build(
    BuildContext context, {
    Widget Function(BuildContext context) pendingBuilder,
    Widget Function(
      BuildContext context, {
      OperationException error,
    })
        errorBuilder,
    @required
        Widget Function(
      BuildContext context, {
      dynamic data,
      Future<QueryResult> Function() refetch,
    })
            builder,
    Map<String, dynamic> args,
  }) {
    assert(documentNode != null);
    assert(context != null);
    assert(builder != null);

    return Query(
      options: QueryOptions(
        documentNode: documentNode,
        variables: args,
      ),
      builder: (result, {refetch, fetchMore}) {
        if (result.loading) {
          if (pendingBuilder != null) {
            return pendingBuilder(context);
          }

          return Center(child: wrappedProgressIndicatorDark);
        }

        if (result.hasException) {
          final logger = Provider.of<EnvironmentProvider>(context).logger;
          logger.error(result.exception, null);

          if (errorBuilder != null) {
            return errorBuilder(context, error: result.exception);
          }

          return Center(child: errorWidget);
        }

        return _QueryRebuilder(
          builder: (context) => builder(
            context,
            data: dataSelector(result.data),
            refetch: refetch,
          ),
        );
      },
    );
  }
}

abstract class PaginatedQueryHandler extends QueryHandler {
  List<dynamic> itemsSelector(dynamic raw);
  String cursorSelector(dynamic raw);
  dynamic mergeFetchMoreResult(dynamic prev, dynamic cur);

  String itemKeySelector(dynamic item) => item.id;
}

class _QueryRebuilder extends StatefulWidget {
  final Widget Function(BuildContext context) builder;
  _QueryRebuilder({@required this.builder});

  @override
  _QueryRebuilderState createState() => _QueryRebuilderState();
}

class _QueryRebuilderState extends State<_QueryRebuilder>
    with RouteAware, RebuildWidgetOnPopNext<_QueryRebuilder> {
  @override
  Widget build(BuildContext context) => widget.builder(context);
}
