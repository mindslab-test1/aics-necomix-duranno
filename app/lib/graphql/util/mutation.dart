import 'package:duranno_app/provider/environment_provider.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:provider/provider.dart';

typedef MutationBuilderDelegate = Widget Function({
  void Function() runMutation,
  QueryResult result,
});

abstract class MutationHandler {
  dynamic get documentNode;
  void updateCache(
    BuildContext context, {
    Cache cache,
    dynamic data,
    Map<String, dynamic> args,
  });
  bool selectSuccess(dynamic raw);
  dynamic selectData(dynamic raw);

  Future<QueryResult> execute(
    BuildContext context, {
    void Function(dynamic) onCompleted,
    void Function(OperationException) onError,
    Map<String, dynamic> args,
  }) async {
    assert(documentNode != null);

    final client = GraphQLProvider.of(context).value;

    return client.mutate(
      MutationOptions(
        documentNode: documentNode,
        variables: args,
        onError: (error) {
          final logger =
              Provider.of<EnvironmentProvider>(context, listen: false).logger;
          logger.error(error, null);

          if (onError != null) {
            onError(error);
          }
        },
        update: (cache, result) {
          if (!result.hasException) {
            final bool success = selectSuccess(result.data);
            if (success) {
              final data = selectData(result.data);
              updateCache(
                context,
                cache: cache,
                data: data,
                args: args,
              );
              if (onCompleted != null) {
                onCompleted(data);
              }
            } else {
              if (onError != null) {
                onError(null);
              }
            }
          }
        },
      ),
    );
  }

  Widget build(
    BuildContext context, {
    void Function(dynamic) onCompleted,
    void Function(OperationException) onError,
    @required MutationBuilderDelegate builder,
    Map<String, dynamic> args,
  }) {
    assert(documentNode != null);
    assert(builder != null);

    return Mutation(
      options: MutationOptions(
        documentNode: documentNode,
        onError: (error) {
          final logger =
              Provider.of<EnvironmentProvider>(context, listen: false).logger;
          logger.error(error, null);

          if (onError != null) {
            onError(error);
          }
        },
        update: (cache, result) {
          if (!result.hasException) {
            final bool success = selectSuccess(result.data);
            if (success) {
              final data = selectData(result.data);
              updateCache(
                context,
                cache: cache,
                data: data,
                args: args,
              );
              if (onCompleted != null) {
                onCompleted(data);
              }
            } else {
              if (onError != null) {
                onError(null);
              }
            }
          }
        },
      ),
      builder: (runMutation, result) => builder(
        runMutation: () {
          runMutation(args);
        },
        result: result,
      ),
    );
  }
}
