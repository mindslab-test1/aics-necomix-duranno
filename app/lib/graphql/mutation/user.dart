import 'package:duranno_app/graphql/util/mutation.dart';
import 'package:duranno_app/model/history/history.dart';
import 'package:duranno_app/model/history/user_history.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class UserMutation {
  static final register = gql(r'''
    mutation Register($isAllowedPromotionEmail: Boolean) {
      register(input: {
        isAllowedPromotionEmail: $isAllowedPromotionEmail
      })
    }
  ''');

  static final mockSubscribe = gql(r'''
    mutation MockSubscribe($promotionEnabled: Boolean) {
      mockSubscribe(input: {
        productLevel: "BASIC",
        promotionEnabled: $promotionEnabled
      }) {
        id
        createdAt
        updatedAt
        promotionEnabled
        provider
        productId
      }
    }
  ''');
}

class UpdateHistoryMutation extends MutationHandler {
  static final _gql = gql(r'''
  mutation UpdateHistory(
    $bookId: ID!,
    $mediaUri: String!,
    $position: Int!,
    $sequence: Int,
    $chapterTitle: String
  ) {
    History {
      update(
        bookId: $bookId,
        input: {
          mediaUri: $mediaUri,
          position: $position,
          sequence: $sequence,
          chapterTitle: $chapterTitle
        }
      ) {
        __typename
        id
        book {
          __typename
          id
        }
        sequence
        chapterTitle
        mediaUri
        position
      }
    }
  }
  ''');

  static UpdateHistoryMutation _instance;
  static UpdateHistoryMutation get instance {
    if (_instance == null) {
      _instance = UpdateHistoryMutation();
    }

    return _instance;
  }

  @override
  dynamic get documentNode => _gql;

  @override
  void updateCache(
    BuildContext context, {
    Cache cache,
    dynamic data,
    Map<String, dynamic> args,
  }) {
    final itemId = data['id'];
    final userId = args['userId'];

    final item = UserHistoryModel(id: itemId, cache: cache)
      ..from(data)
      ..commit();

    HistoryModel(id: userId, cache: cache)
      ..update(item)
      ..commit();
  }

  @override
  bool selectSuccess(dynamic raw) => raw['History']['update'] != null;

  @override
  dynamic selectData(dynamic raw) => raw['History']['update'];
}
