import 'package:duranno_app/graphql/util/mutation.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class BookMutation {
  static final rateBook = gql(r'''
  mutation RateBook($bookId: ID!, $value: Int!) {
    rateBook(bookId: $bookId, value: $value)
  }
  ''');

  static final deleteRating = gql(r'''
  mutation DeleteRating($bookId: ID!) {
    deleteRating(bookId: $bookId)
  }
  ''');

  static final writeComment = gql(r'''
  mutation WriteComment($bookId: ID!, $content: String!) {
    writeComment(
      bookId: $bookId,
      content: $content
    ) {
      id
      user {
        id
        email
      }
      updatedAt
      content
    }
  }
  ''');
}

class DeleteCommentMutation extends MutationHandler {
  static DeleteCommentMutation _instance;
  static DeleteCommentMutation get instance {
    if (_instance == null) {
      _instance = DeleteCommentMutation();
    }

    return _instance;
  }

  static final _gql = gql(r'''
  mutation DeleteComment($commentId: ID!) {
    deleteComment(commentId: $commentId)
  }
  ''');

  @override
  dynamic get documentNode => _gql;

  @override
  void updateCache(
    BuildContext context, {
    Cache cache,
    dynamic data,
    Map<String, dynamic> args,
  }) {}

  @override
  bool selectSuccess(dynamic raw) {
    return raw['deleteComment'];
  }

  @override
  dynamic selectData(dynamic raw) {
    return raw['deleteComment'];
  }
}
