import 'package:duranno_app/graphql/util/mutation.dart';
import 'package:duranno_app/model/book/book.dart';
import 'package:duranno_app/model/playlist/playlist.dart';
import 'package:duranno_app/model/playlist/playlist_item.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class AddToPlaylistMutation extends MutationHandler {
  static AddToPlaylistMutation _instance;
  static AddToPlaylistMutation get instance {
    if (_instance == null) {
      _instance = AddToPlaylistMutation();
    }

    return _instance;
  }

  final _gql = gql(r'''
  mutation AddToPlaylist($bookId: ID!) {
    Playlist {
      add(bookId: $bookId) {
        __typename
        id
        book {
          __typename
          id
          title
          author {
            name
          }
          rating {
            average
          }
          numViews
          thumbnailUri
          tags {
            id
            text
            color
            backgroundColor
          }
        }
      }
    }
  }
  ''');

  @override
  dynamic get documentNode => _gql;

  @override
  void updateCache(
    BuildContext context, {
    Cache cache,
    dynamic data,
    Map<String, dynamic> args,
  }) {
    final itemId = data['id'];

    final item = PlaylistItemModel(id: itemId, cache: cache)
      ..from(data)
      ..commit();

    final uid =
        Provider.of<AuthenticationProvider>(context, listen: false).user.uid;

    PlaylistModel(id: uid, cache: cache)
      ..add(item)
      ..commit();

    BookModel(id: data['book']['id'], cache: cache)
      ..addToPlaylist()
      ..commit();
  }

  @override
  bool selectSuccess(dynamic raw) {
    return raw['Playlist']['add'] != null;
  }

  @override
  dynamic selectData(dynamic raw) {
    return raw['Playlist']['add'];
  }
}

class DeleteFromPlaylistMutation extends MutationHandler {
  static DeleteFromPlaylistMutation _instance;
  static DeleteFromPlaylistMutation get instance {
    if (_instance == null) {
      _instance = DeleteFromPlaylistMutation();
    }

    return _instance;
  }

  final _gql = gql(r'''
  mutation DeleteFromPlaylist($bookId: ID!) {
    Playlist {
      remove(bookId: $bookId)
    }
  }
  ''');

  @override
  dynamic get documentNode => _gql;

  @override
  void updateCache(
    BuildContext context, {
    Cache cache,
    dynamic data,
    Map<String, dynamic> args,
  }) {
    final String itemId = data;

    final uid =
        Provider.of<AuthenticationProvider>(context, listen: false).user.uid;

    PlaylistModel(id: uid, cache: cache)
      ..delete(itemId)
      ..commit();

    BookModel(id: args['bookId'], cache: cache)
      ..removeFromPlaylist()
      ..commit();
  }

  @override
  bool selectSuccess(dynamic raw) {
    return raw['Playlist']['remove'] != null;
  }

  @override
  dynamic selectData(dynamic raw) {
    return raw['Playlist']['remove'];
  }
}
