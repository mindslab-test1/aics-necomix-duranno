import 'package:graphql_flutter/graphql_flutter.dart';

class FeedbackMutation {
  static final createFeedback = gql(r'''
  mutation CreateFeedback(
    $type: String!,
    $category: String!,
    $name: String!,
    $email: String!,
    $title: String!,
    $content: String
  ) {
    createFeedback(input: {
      type: $type,
      category: $category,
      name: $name,
      email: $email,
      title: $title,
      content: $content
    }) {
      id
    }
  }
  ''');
}
