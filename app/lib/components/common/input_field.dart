import 'dart:ui';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

class InputField extends StatefulWidget {
  final TextEditingController controller;
  final String errorMessage;
  final String textLabel;
  final String actionTextLabel;
  final Function() onClickActionButton;
  final Function() onEditingComplete;
  final bool disableActionButton;
  final bool isPassword;
  final ThemeIconData icon;
  final bool clearable;
  final int maxLine;
  final bool autoExpand;
  final String hintText;
  InputField({
    Key key,
    this.maxLine = 1,
    this.textLabel,
    this.actionTextLabel,
    this.onClickActionButton,
    this.onEditingComplete,
    this.disableActionButton,
    @required this.controller,
    @required this.errorMessage,
    this.isPassword = false,
    this.icon,
    this.clearable = false,
    this.autoExpand = false,
    this.hintText,
  })  : assert(controller != null),
        assert((actionTextLabel == null &&
                onClickActionButton == null &&
                disableActionButton == null) ||
            (actionTextLabel != null &&
                onClickActionButton != null &&
                disableActionButton != null)),
        assert(isPassword && maxLine == 1 || !isPassword && maxLine >= 1),
        assert(autoExpand != null),
        super(key: key);

  @override
  _InputFieldState createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> with TickerProviderStateMixin {
  FocusNode _focusNode;
  AnimationController _animationController;
  GlobalKey _keyTextField = GlobalKey();
  final _borderRadiusValue = 2.0;

  double _inputFieldHeight = 0.0;
  bool _disposed = false;

  void _updateInputFieldSize() {
    if (_disposed) {
      return;
    }

    final RenderBox renderBoxInputField =
        _keyTextField.currentContext?.findRenderObject();

    setState(() {
      _inputFieldHeight = renderBoxInputField?.size?.height ?? 0.0;
    });
  }

  bool get _showClearActionButton =>
      widget.clearable &&
      widget.controller.text.isNotEmpty &&
      _focusNode.hasFocus;

  bool _isTextEmpty;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 150),
      reverseDuration: const Duration(milliseconds: 150),
    );

    _focusNode = FocusNode();
    _focusNode.addListener(() {
      if (_focusNode.hasFocus) {
        if (widget.controller.text.isEmpty) {
          _animationController.forward();
        }
      } else {
        if (widget.controller.text.isEmpty) {
          _animationController.reverse();
        }
      }
      setState(() {});
    });

    widget.controller.addListener(_onValueChanged);

    _isTextEmpty = widget.controller.text.isEmpty;

    WidgetsBinding.instance
        .addPostFrameCallback((_) => _updateInputFieldSize());

    super.initState();
  }

  @override
  void didUpdateWidget(InputField oldWidget) {
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.removeListener(_onValueChanged);
      widget.controller.addListener(_onValueChanged);
      _isTextEmpty = widget.controller.text.isEmpty;
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _disposed = true;
    _animationController.dispose();
    widget.controller.removeListener(_onValueChanged);

    super.dispose();
  }

  void _onValueChanged() {
    if (_isTextEmpty != widget.controller.text.isEmpty) {
      setState(() {
        _isTextEmpty = widget.controller.text.isEmpty;
      });
    }

    if (widget.autoExpand) {
      WidgetsBinding.instance
          .addPostFrameCallback((_) => _updateInputFieldSize());
    }
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    final hasIcon = widget.icon != null;
    final iconColor = NeocomixThemeColors.secondaryLight;

    final contentPaddingAdjustment =
        0.5 * (textTheme.subtitle1.height - 1.0) * textTheme.subtitle1.fontSize;

    final contentIdlePadding = EdgeInsets.only(
      top: 16.0 + contentPaddingAdjustment,
      bottom: 16.0 + contentPaddingAdjustment,
      left: hasIcon ? 52.0 : 16.0,
    );

    final contentFocusedPadding = widget.textLabel != null
        ? EdgeInsets.only(
            top: 24.0 + contentPaddingAdjustment,
            bottom: 8.0 + contentPaddingAdjustment,
            left: hasIcon ? 52.0 : 16.0,
            right: widget.clearable ? 58 : 0)
        : contentIdlePadding;

    final iconPosition = Rect.fromLTWH(16.0, 16.0, 20.0, 20.0);

    final hintIdleTextStyle = textTheme.subtitle1.copyWith(
      color: NeocomixThemeColors.secondaryLight,
      height: 1.0,
    );

    final hintFocusedTextStyle = textTheme.bodyText2.copyWith(
      color: NeocomixThemeColors.secondaryLight,
      height: 1.0,
    );

    final hintIdlePositionAdjustment =
        0.5 * (textTheme.subtitle1.height - 1.0) * textTheme.subtitle1.fontSize;
    final hintFocusedPositionAdjustment =
        0.5 * (textTheme.bodyText2.height - 1.0) * textTheme.bodyText2.fontSize;

    final hintIdlePosition = Rect.fromLTWH(
        hasIcon ? 52.0 : 16.0,
        16.0 + hintIdlePositionAdjustment,
        200.0,
        textTheme.subtitle1.fontSize + 4.0);

    final hintFocusedPosition = Rect.fromLTWH(
        hasIcon ? 52.0 : 16.0,
        6.0 + hintFocusedPositionAdjustment,
        200.0,
        textTheme.bodyText2.fontSize + 4.0);

    final clearActionButton = GestureDetector(
      child: AbsorbPointer(
        child: SizedBox(
          width: 56.0,
          height: 24.0,
          child: Align(
            alignment: FractionalOffset.bottomRight,
            child: Padding(
              padding: EdgeInsets.only(right: 16, bottom: 14, top: 14),
              child: Icon(Icons.clear, color: iconColor, size: 20.0),
            ),
          ),
        ),
      ),
      onTap: () {
        widget.controller.text = '';
        setState(() {});
      },
    );

    final textActionButton = widget.actionTextLabel != null
        ? GestureDetector(
            onTap:
                widget.disableActionButton ? null : widget.onClickActionButton,
            child: AbsorbPointer(
              child: Container(
                decoration: BoxDecoration(
                    color: widget.disableActionButton
                        ? NeocomixThemeColors.background
                        : NeocomixThemeColors.primaryLight,
                    borderRadius: BorderRadius.only(
                        topRight: Radius.circular(_borderRadiusValue),
                        bottomRight: Radius.circular(_borderRadiusValue))),
                height: _inputFieldHeight,
                child: Align(
                  alignment: FractionalOffset.bottomLeft,
                  child: Padding(
                    padding: EdgeInsets.only(left: 16, bottom: 17, right: 16),
                    child: Text(widget.actionTextLabel,
                        style: Theme.of(context).textTheme.headline6.copyWith(
                            color: widget.disableActionButton
                                ? NeocomixThemeColors.secondaryLight
                                : NeocomixThemeColors.white)),
                  ),
                ),
              ),
            ),
          )
        : null;

    final border = OutlineInputBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(_borderRadiusValue),
            bottomLeft: Radius.circular(_borderRadiusValue),
            bottomRight: widget.actionTextLabel == null
                ? Radius.circular(_borderRadiusValue)
                : Radius.circular(0),
            topRight: widget.actionTextLabel == null
                ? Radius.circular(_borderRadiusValue)
                : Radius.circular(0)),
        borderSide: BorderSide(color: NeocomixThemeColors.background));

    return AnimatedBuilder(
      animation: _animationController,
      builder: (context, child) => Column(children: [
        Stack(
          children: [
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Stack(children: [
                    Container(
                      key: _keyTextField,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(_borderRadiusValue),
                          topRight: widget.actionTextLabel == null
                              ? Radius.circular(_borderRadiusValue)
                              : Radius.circular(0),
                          bottomLeft: Radius.circular(_borderRadiusValue),
                          bottomRight: widget.actionTextLabel == null
                              ? Radius.circular(_borderRadiusValue)
                              : Radius.circular(0),
                        ),
                        border: widget.errorMessage != null
                            ? Border.all(color: NeocomixThemeColors.error)
                            : null,
                      ),
                      child: TextField(
                        onEditingComplete: widget.onEditingComplete,
                        keyboardType:
                            widget.maxLine > 1 ? TextInputType.multiline : null,
                        maxLines: widget.autoExpand ? null : widget.maxLine,
                        textAlignVertical: TextAlignVertical.top,
                        focusNode: _focusNode,
                        obscureText: widget.isPassword,
                        style: Theme.of(context)
                            .textTheme
                            .subtitle1
                            .copyWith(height: 1.0),
                        decoration: InputDecoration(
                          isCollapsed: true,
                          contentPadding: EdgeInsets.lerp(
                            contentIdlePadding,
                            contentFocusedPadding,
                            _animationController.value,
                          ),
                          enabledBorder: border,
                          filled: true,
                          fillColor: NeocomixThemeColors.background,
                          focusedBorder: border,
                          hintText:
                              widget.textLabel == null ? widget.hintText : null,
                          hintStyle: textTheme.subtitle1.copyWith(
                            height: 1.0,
                            color: NeocomixThemeColors.secondaryLight,
                          ),
                        ),
                        controller: widget.controller,
                      ),
                    ),
                    if (hasIcon)
                      Positioned.fromRect(
                        rect: iconPosition,
                        child: IgnorePointer(
                          child: ThemeIcon(
                            widget.icon,
                            color: iconColor,
                            size: 20.0,
                          ),
                        ),
                      ),
                    if (widget.textLabel != null)
                      Positioned.fromRect(
                        rect: Rect.lerp(
                          hintIdlePosition,
                          hintFocusedPosition,
                          _animationController.value,
                        ),
                        child: IgnorePointer(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Spacer(),
                              Text(
                                widget.textLabel,
                                style: TextStyle.lerp(
                                  hintIdleTextStyle,
                                  hintFocusedTextStyle,
                                  _animationController.value,
                                ),
                              ),
                              Spacer(),
                            ],
                          ),
                        ),
                      ),
                    if (_showClearActionButton)
                      Positioned(
                        top: 0,
                        bottom: 0,
                        right: 0,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(_borderRadiusValue),
                              topRight: Radius.circular(0),
                              bottomLeft: Radius.circular(_borderRadiusValue),
                              bottomRight: Radius.circular(0),
                            ),
                          ),
                          child: clearActionButton,
                        ),
                      )
                  ]),
                ),
                if (widget.actionTextLabel != null) textActionButton
              ],
            ),
          ],
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: widget.errorMessage != null && widget.errorMessage != ''
              ? Padding(
                  padding: EdgeInsets.only(top: 6),
                  child: Text(
                    widget.errorMessage,
                    style: Theme.of(context).textTheme.headline6.copyWith(
                          color: NeocomixThemeColors.primaryLight,
                          fontWeight: FontWeight.normal,
                        ),
                  ),
                )
              : null,
        ),
      ]),
    );
  }
}
