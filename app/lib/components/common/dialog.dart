import 'dart:async';

import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:flutter/material.dart';

enum DialogType {
  OneAction,
  TwoActions,
}

enum DialogResult {
  Confirm,
  Cancel,
}

class ThemeDialog extends StatelessWidget {
  final DialogType type;
  final String title;
  final String content;
  final Widget contentWidget;
  final String confirmLabel;
  final String cancelLabel;
  final FutureOr<void> Function(BuildContext) onConfirm;
  final FutureOr<void> Function(BuildContext) onCancel;
  final bool overrideDefaultConfirmCallback;
  final bool overrideDefaultCancelCallback;
  final Widget confirmButton;
  final Widget cancelButton;

  ThemeDialog({
    Key key,
    @required this.type,
    @required this.title,
    @required this.confirmLabel,
    this.cancelLabel,
    this.onConfirm,
    this.onCancel,
    this.content,
    this.contentWidget,
    @required this.overrideDefaultConfirmCallback,
    this.overrideDefaultCancelCallback,
    this.confirmButton,
    this.cancelButton,
  })  : assert(type != null),
        assert(title != null && title.isNotEmpty),
        assert(confirmLabel != null && confirmLabel.isNotEmpty),
        assert(overrideDefaultConfirmCallback != null &&
            !(overrideDefaultConfirmCallback && onConfirm == null)),
        assert(!(type == DialogType.TwoActions &&
            (cancelLabel == null || cancelLabel.isEmpty))),
        assert(!(type == DialogType.TwoActions &&
            (overrideDefaultCancelCallback == null ||
                (overrideDefaultCancelCallback && onCancel == null)))),
        super(key: key);

  factory ThemeDialog.oneAction({
    Key key,
    @required String title,
    String content,
    Widget contentWidget,
    @required String confirmLabel,
    FutureOr<void> Function(BuildContext) onConfirm,
    @required bool overrideDefaultConfirmCallback,
    Widget confirmButton,
  }) =>
      ThemeDialog(
        type: DialogType.OneAction,
        key: key,
        title: title,
        content: content,
        contentWidget: contentWidget,
        confirmLabel: confirmLabel,
        onConfirm: onConfirm,
        overrideDefaultConfirmCallback: overrideDefaultConfirmCallback,
        confirmButton: confirmButton,
      );

  factory ThemeDialog.twoActions({
    Key key,
    @required String title,
    String content,
    Widget contentWidget,
    @required String confirmLabel,
    @required String cancelLabel,
    FutureOr<void> Function(BuildContext) onConfirm,
    FutureOr<void> Function(BuildContext) onCancel,
    @required bool overrideDefaultConfirmCallback,
    @required bool overrideDefaultCancelCallback,
    Widget confirmButton,
    Widget cancelButton,
  }) =>
      ThemeDialog(
        type: DialogType.TwoActions,
        key: key,
        title: title,
        content: content,
        contentWidget: contentWidget,
        confirmLabel: confirmLabel,
        cancelLabel: cancelLabel,
        onConfirm: onConfirm,
        onCancel: onCancel,
        overrideDefaultConfirmCallback: overrideDefaultConfirmCallback,
        overrideDefaultCancelCallback: overrideDefaultCancelCallback,
        confirmButton: confirmButton,
        cancelButton: cancelButton,
      );

  @override
  Widget build(BuildContext context) {
    final titleWidget =
        Text(title, style: Theme.of(context).textTheme.headline3);
    final contentWidgetOrText = contentWidget != null
        ? contentWidget
        : content != null
            ? Text(content, style: Theme.of(context).textTheme.bodyText1)
            : null;
    final confirmButtonWidget = confirmButton ??
        ThemeFlatButton.primary(
          label: confirmLabel,
          onPressed: _onConfirm(context),
        );
    final cancelButtonWidget = type == DialogType.TwoActions
        ? cancelButton ??
            ThemeFlatButton.secondary(
              label: cancelLabel,
              onPressed: _onCancel(context),
            )
        : null;

    return Dialog(
      backgroundColor: NeocomixThemeColors.white,
      elevation: 12.0,
      shape: RoundedRectangleBorder(
        side: BorderSide(
          color: NeocomixThemeColors.border,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      insetPadding: const EdgeInsets.all(20.0),
      child: SafeArea(
        minimum: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 16.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            titleWidget,
            if (contentWidgetOrText != null) SizedBox(height: 8.0),
            if (contentWidgetOrText != null) contentWidgetOrText,
            SizedBox(height: 32.0),
            Row(
              children: [
                if (type == DialogType.TwoActions)
                  SizedBox(
                    width: 100.0,
                    child: cancelButtonWidget,
                  ),
                if (type == DialogType.TwoActions) SizedBox(width: 8.0),
                Expanded(child: confirmButtonWidget),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Future<void> Function() _onConfirm(BuildContext context) => () async {
        if (overrideDefaultConfirmCallback) {
          await onConfirm(context);
          return;
        }

        if (onConfirm != null) {
          await onConfirm(context);
        }
        Navigator.of(context).pop<DialogResult>(DialogResult.Confirm);
      };

  Future<void> Function() _onCancel(BuildContext context) => () async {
        if (type != DialogType.TwoActions) {
          throw DurannoError(code: ErrorCode.Logic);
        }

        if (overrideDefaultCancelCallback) {
          await onCancel(context);
          return;
        }

        if (onCancel != null) {
          await onCancel(context);
        }
        Navigator.of(context).pop<DialogResult>(DialogResult.Cancel);
      };
}

Future<DialogResult> showThemeDialog(
  DialogType type, {
  @required BuildContext context,
  @required String title,
  String content,
  Widget contentWidget,
  String confirmLabel = '확인',
  String cancelLabel = '취소',
  FutureOr<void> Function(BuildContext) onConfirm,
  FutureOr<void> Function(BuildContext) onCancel,
  bool overrideDefaultConfirmCallback = false,
  bool overrideDefaultCancelCallback = false,
  Widget confirmButton,
  Widget cancelButton,
}) {
  assert(type != null);

  return showDialog<DialogResult>(
    context: context,
    builder: (context) {
      switch (type) {
        case DialogType.OneAction:
          return ThemeDialog.oneAction(
            title: title,
            content: content,
            contentWidget: contentWidget,
            confirmLabel: confirmLabel,
            onConfirm: onConfirm,
            overrideDefaultConfirmCallback: overrideDefaultConfirmCallback,
            confirmButton: confirmButton,
          );
        case DialogType.TwoActions:
          return ThemeDialog.twoActions(
            title: title,
            content: content,
            contentWidget: contentWidget,
            confirmLabel: confirmLabel,
            cancelLabel: cancelLabel,
            onConfirm: onConfirm,
            onCancel: onCancel,
            overrideDefaultConfirmCallback: overrideDefaultConfirmCallback,
            overrideDefaultCancelCallback: overrideDefaultCancelCallback,
            confirmButton: confirmButton,
            cancelButton: cancelButton,
          );
        default:
          throw DurannoError(code: ErrorCode.Logic);
      }
    },
  );
}
