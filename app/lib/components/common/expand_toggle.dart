import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

class ExpandToggle extends StatelessWidget {
  final bool expanded;
  ExpandToggle({
    Key key,
    @required this.expanded,
  })  : assert(expanded != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final labelColor = NeocomixThemeColors.secondaryLight;
    return SizedBox(
      height: 40.0,
      child: Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextLabel.p(expanded ? '접기' : '더보기', color: labelColor),
            SizedBox(width: 4.0),
            ThemeIcon(
              expanded ? ThemeIconData.arrow_up : ThemeIconData.arrow_down,
              size: 12.0,
              color: labelColor,
            ),
          ],
        ),
      ),
    );
  }
}
