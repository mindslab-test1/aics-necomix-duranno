import 'package:duranno_app/components/common/tab_page/tab_page_controller.dart';
import 'package:flutter/material.dart';

class TabPage extends StatefulWidget {
  final TabPageController controller;
  final bool useCache;
  final Function(BuildContext, int) builder;
  TabPage({
    Key key,
    @required this.controller,
    this.useCache = true,
    @required this.builder,
  })  : assert(controller != null),
        assert(builder != null),
        super(key: key);

  @override
  _TabPageState createState() => _TabPageState();
}

class _TabPageState extends State<TabPage> {
  List<Widget> _pageWidgetCaches;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _pageWidgetCaches =
        List<Widget>.generate(widget.controller.numItems, (index) => null);
  }

  @override
  void didUpdateWidget(TabPage oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (widget.controller != oldWidget.controller ||
        widget.builder != oldWidget.builder) {
      _pageWidgetCaches =
          List<Widget>.generate(widget.controller.numItems, (index) => null);
    }
  }

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: widget.controller.activeListenable,
        builder: (context, index, child) {
          if (_pageWidgetCaches[index] == null || widget.useCache) {
            _pageWidgetCaches[index] = widget.builder(context, index);
          }
          return _pageWidgetCaches[index];
        });
  }
}
