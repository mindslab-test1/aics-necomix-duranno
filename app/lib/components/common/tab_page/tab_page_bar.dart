import 'dart:ui';

import 'package:duranno_app/components/common/tab_page/tab_page_controller.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';

class TabPageBar extends StatefulWidget {
  final TabPageController controller;
  final List<String> labels;
  final double verticalPadding;
  final bool shrink;
  final TextStyle labelTextStyle;
  TabPageBar({
    Key key,
    @required this.controller,
    @required this.labels,
    this.verticalPadding = 10.0,
    this.shrink = false,
    this.labelTextStyle,
  })  : assert(controller != null),
        assert(labels != null),
        assert(labels.length == controller.numItems),
        assert(verticalPadding >= 0.0),
        super(key: key);

  @override
  _TabPageBarState createState() => _TabPageBarState();
}

class _TabPageBarState extends State<TabPageBar> {
  GlobalKey _containerKey;
  List<GlobalKey> _labelWidgetKeys;

  ValueNotifier<Size> _containerSize;
  ValueNotifier<List<Offset>> _labelPositions;
  ValueNotifier<List<Size>> _labelSizes;

  @override
  void initState() {
    super.initState();

    _containerSize = ValueNotifier<Size>(null);
    _labelPositions = ValueNotifier<List<Offset>>(null);
    _labelSizes = ValueNotifier<List<Size>>(null);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    _containerKey = GlobalKey();
    _labelWidgetKeys =
        List.generate(widget.controller.numItems, (index) => GlobalKey());

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      _updateDimensions();
    });
  }

  @override
  void didUpdateWidget(TabPageBar oldWidget) {
    super.didUpdateWidget(oldWidget);

    if (oldWidget.labels != widget.labels) {
      WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
        _updateDimensions();
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final separatorWidget = Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(width: widget.shrink ? 12.0 : 9.0),
          Container(
            color: NeocomixThemeColors.border,
            width: 1.0,
            height: 10.0,
          ),
          SizedBox(width: widget.shrink ? 12.0 : 9.0),
        ]);

    if (widget.shrink) {
      return SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        padding: const EdgeInsets.symmetric(horizontal: 32.0),
        child: AnimatedBuilder(
          animation: widget.controller.animation,
          builder: (context, child) => Stack(
            children: [
              _buildHighlight(context),
              child,
            ],
          ),
          child: ValueListenableBuilder<int>(
            valueListenable: widget.controller.activeListenable,
            builder: (context, value, child) => Row(
              key: _containerKey,
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ...IterableUtils.join<Widget>(
                  Iterable<int>.generate(widget.controller.numItems)
                      .map((index) => _buildLabel(context, index)),
                  separator: separatorWidget,
                ),
              ],
            ),
          ),
        ),
      );
    } else {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: AnimatedBuilder(
          animation: widget.controller.animation,
          builder: (context, child) => Stack(
            children: [
              _buildHighlight(context),
              child,
            ],
          ),
          child: ValueListenableBuilder<int>(
            valueListenable: widget.controller.activeListenable,
            builder: (context, value, child) => Row(
              key: _containerKey,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ...IterableUtils.join<Widget>(
                  Iterable<int>.generate(widget.controller.numItems)
                      .map((index) => _buildLabel(context, index)),
                  separator: separatorWidget,
                ),
              ],
            ),
          ),
        ),
      );
    }
  }

  Widget _buildLabel(BuildContext context, int index) {
    final label = widget.labels[index];

    final labelWidget = Container(
      key: _labelWidgetKeys[index],
      padding: EdgeInsets.symmetric(vertical: widget.verticalPadding),
      child: widget.controller.active == index
          ? TextLabel.h5(label, color: NeocomixThemeColors.primary)
          : TextLabel.h5(
              label,
              color: NeocomixThemeColors.secondaryLight,
              fontWeight: FontWeight.normal,
            ),
    );

    if (widget.shrink) {
      return GestureDetector(
        child: labelWidget,
        onTap: () {
          widget.controller.setActive(index);
        },
        behavior: HitTestBehavior.translucent,
      );
    } else {
      return Expanded(
        child: GestureDetector(
          child: Center(child: labelWidget),
          onTap: () {
            widget.controller.setActive(index);
          },
          behavior: HitTestBehavior.translucent,
        ),
      );
    }
  }

  Widget _buildHighlight(BuildContext context) {
    if (_containerSize.value == null ||
        _labelSizes.value == null ||
        _labelPositions.value == null) {
      return Container();
    }

    final beginWidth = _labelSizes.value[widget.controller.begin].width;
    final endWidth = _labelSizes.value[widget.controller.end].width;

    final beginLeft = _labelPositions.value[widget.controller.begin].dx;
    final endLeft = _labelPositions.value[widget.controller.end].dx;

    return Positioned(
      left: lerpDouble(
        beginLeft,
        endLeft,
        widget.controller.animationProgress,
      ),
      bottom: 0,
      width: lerpDouble(
        beginWidth,
        endWidth,
        widget.controller.animationProgress,
      ),
      height: 4,
      child: Container(color: NeocomixThemeColors.primary),
    );
  }

  void _updateDimensions() {
    final numItems = widget.controller.numItems;

    _labelPositions.value = List<Offset>.generate(numItems, (index) => null);
    _labelSizes.value = List<Size>.generate(numItems, (index) => null);

    final RenderBox containerRenderBox =
        _containerKey.currentContext.findRenderObject();
    final Offset containerOffset =
        containerRenderBox.globalToLocal(Offset.zero);
    _containerSize.value = containerRenderBox.size;

    Iterable<int>.generate(numItems).forEach((index) {
      final labelWidgetKey = _labelWidgetKeys[index];
      final RenderBox labelRenderBox =
          labelWidgetKey.currentContext.findRenderObject();
      _labelPositions.value[index] =
          labelRenderBox.localToGlobal(containerOffset);
      _labelSizes.value[index] = labelRenderBox.size;
    });

    setState(() {});
  }
}
