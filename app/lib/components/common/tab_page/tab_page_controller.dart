import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

class TabPageController {
  final int numItems;
  final int initialActive;
  final Duration animationDuration;

  final ValueNotifier<int> _active;

  Tween<double> _tween;
  AnimationController _animationController;
  Animation<double> _curvedAnimation;
  Animation<double> _tweenAnimation;
  int _count;

  bool _disposed = false;

  TabPageController({
    @required this.numItems,
    this.initialActive = 0,
    this.animationDuration = const Duration(milliseconds: 150),
  })  : assert(numItems != null && numItems > 0),
        _active = ValueNotifier<int>(initialActive);

  void initialize({
    @required TickerProvider vsync,
  }) {
    assert(vsync != null);

    _animationController = AnimationController(
      vsync: vsync,
      duration: animationDuration,
    );
    _curvedAnimation = CurvedAnimation(
      curve: Curves.ease,
      parent: _animationController,
    );

    _count = 0;

    _tween = Tween<double>(
      begin: initialActive.toDouble(),
      end: initialActive.toDouble(),
    );
    _tweenAnimation = _tween.animate(_curvedAnimation);
  }

  int get active => _active.value;
  int get begin => _tween.begin.toInt();
  int get end => _tween.end.toInt();
  double get animatedValue => _tweenAnimation.value;
  double get animationProgress => _curvedAnimation.value;
  ValueListenable<int> get activeListenable => _active;
  Animation<double> get animation => _animationController;

  void setActive(int index) {
    assert(index != null);
    assert(index >= 0 && index < numItems);

    if (_active.value == index) {
      return;
    }

    _active.value = index;

    if (_count % 2 == 0) {
      _tween.end = index.toDouble();
      _animationController.forward();
    } else {
      _tween.begin = index.toDouble();
      _animationController.reverse();
    }
    _count++;
  }

  void dispose() {
    if (_disposed) {
      return;
    }

    _disposed = true;
    _active.dispose();
    _animationController.dispose();
  }
}
