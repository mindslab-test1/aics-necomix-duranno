import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

class ResponsiveImage extends StatelessWidget {
  final double originalWidth;
  final double originalHeight;
  final String imageUri1x;
  final String imageUri2x;
  final String imageUri3x;
  final BoxFit boxFit;
  ResponsiveImage({
    Key key,
    @required this.originalWidth,
    @required this.originalHeight,
    @required this.imageUri1x,
    this.imageUri2x,
    this.imageUri3x,
    this.boxFit = BoxFit.cover,
  })  : assert(originalWidth != null && originalWidth > 0),
        assert(originalHeight != null && originalHeight > 0),
        assert(imageUri1x != null),
        assert(boxFit != null),
        super(key: key);

  double get aspectRatio => originalWidth / originalHeight;
  double get inverseAspectRatio => originalHeight / originalWidth;

  double targetWidth(BoxConstraints constraints) {
    if (constraints.maxWidth.isInfinite && constraints.maxHeight.isInfinite) {
      return originalWidth;
    }

    if (constraints.maxWidth.isInfinite) {
      switch (boxFit) {
        case BoxFit.contain:
        case BoxFit.cover:
        case BoxFit.fill:
        case BoxFit.fitHeight:
        case BoxFit.fitWidth:
          return aspectRatio * constraints.maxHeight;
        case BoxFit.none:
          return originalWidth;
        case BoxFit.scaleDown:
          return min(aspectRatio * constraints.maxHeight, originalWidth);
        default:
          return 0;
      }
    }

    if (constraints.maxHeight.isInfinite) {
      switch (boxFit) {
        case BoxFit.contain:
        case BoxFit.cover:
        case BoxFit.fill:
        case BoxFit.fitHeight:
        case BoxFit.fitWidth:
          return constraints.maxWidth;
        case BoxFit.none:
          return originalWidth;
        case BoxFit.scaleDown:
          return min(constraints.maxWidth, originalWidth);
        default:
          return 0;
      }
    }

    switch (boxFit) {
      case BoxFit.contain:
        return min(constraints.maxWidth, aspectRatio * constraints.maxHeight);
      case BoxFit.cover:
        return max(constraints.maxWidth, aspectRatio * constraints.maxHeight);
      case BoxFit.fill:
        return constraints.maxWidth;
      case BoxFit.fitHeight:
        return aspectRatio * constraints.maxHeight;
      case BoxFit.fitWidth:
        return constraints.maxWidth;
      case BoxFit.none:
        return originalWidth;
      case BoxFit.scaleDown:
        return min(
            min(constraints.maxWidth, aspectRatio * constraints.maxHeight),
            originalWidth);
      default:
        return 0;
    }
  }

  double targetHeight(BoxConstraints constraints) {
    if (constraints.maxWidth.isInfinite && constraints.maxHeight.isInfinite) {
      return originalHeight;
    }

    if (constraints.maxWidth.isInfinite) {
      switch (boxFit) {
        case BoxFit.contain:
        case BoxFit.cover:
        case BoxFit.fill:
        case BoxFit.fitHeight:
        case BoxFit.fitWidth:
          return constraints.maxHeight;
        case BoxFit.none:
          return originalHeight;
        case BoxFit.scaleDown:
          return min(constraints.maxHeight, originalHeight);
        default:
          return 0;
      }
    }

    if (constraints.maxHeight.isInfinite) {
      switch (boxFit) {
        case BoxFit.contain:
        case BoxFit.cover:
        case BoxFit.fill:
        case BoxFit.fitHeight:
        case BoxFit.fitWidth:
          return inverseAspectRatio * constraints.maxWidth;
        case BoxFit.none:
          return originalHeight;
        case BoxFit.scaleDown:
          return min(inverseAspectRatio * constraints.maxWidth, originalHeight);
        default:
          return 0;
      }
    }

    switch (boxFit) {
      case BoxFit.contain:
        return min(
            constraints.maxHeight, inverseAspectRatio * constraints.maxWidth);
      case BoxFit.cover:
        return max(
            constraints.maxHeight, inverseAspectRatio * constraints.maxWidth);
      case BoxFit.fill:
        return constraints.maxHeight;
      case BoxFit.fitHeight:
        return constraints.maxHeight;
      case BoxFit.fitWidth:
        return inverseAspectRatio * constraints.maxWidth;
      case BoxFit.none:
        return originalHeight;
      case BoxFit.scaleDown:
        return min(
            min(constraints.maxHeight,
                inverseAspectRatio * constraints.maxWidth),
            originalHeight);
      default:
        return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(builder: (context, constraints) {
      final width = targetWidth(constraints);
      final height = targetHeight(constraints);

      final scale = max(width / originalWidth, height / originalHeight);
      final devicePixelRatio = WidgetsBinding.instance.window.devicePixelRatio;
      final uri = (scale >= (2.5 / devicePixelRatio) ? imageUri3x : null) ??
          (scale >= (1.5 / devicePixelRatio) ? imageUri2x : null) ??
          imageUri1x;

      return CachedNetworkImage(
        imageUrl: uri,
        imageBuilder: (context, imageProvider) {
          return Image(
            image: imageProvider,
            fit: boxFit,
            width: width,
            height: height,
          );
        },
        errorWidget: (context, url, error) {
          return Container(
            width: width,
            height: height,
            child: Center(
              child: Icon(
                Icons.error,
                size: 18.0,
                color: NeocomixThemeColors.secondaryDarkest,
              ),
            ),
          );
        },
      );
    });
  }
}
