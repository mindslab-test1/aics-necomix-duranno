import 'package:duranno_app/components/common/checkbox_controller.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

class AgreementCheckBox extends StatefulWidget {
  final String text;
  final String subTitle;
  final bool isTitle;
  final int index;
  final bool isOptional;
  final CheckBoxController controller;
  final Function onTap;
  final Function onChange;
  final bool textOverflow;
  AgreementCheckBox({
    Key key,
    @required this.text,
    @required this.controller,
    this.isTitle = false,
    this.index,
    this.isOptional = false,
    this.textOverflow = false,
    @required this.onChange,
    this.onTap,
    this.subTitle = '',
  })  : assert(text != null),
        assert((isTitle && isOptional == false) || (!isTitle && index != null)),
        super(key: key);

  @override
  _AgreementCheckBoxState createState() => _AgreementCheckBoxState();
}

class _AgreementCheckBoxState extends State<AgreementCheckBox> {
  @override
  void initState() {
    if (widget.isOptional) {
      widget.controller.addOptional(widget.index);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: () {
              setState(() {
                widget.isTitle
                    ? widget.controller.switchAll()
                    : widget.controller.switchActive(widget.index);
                widget.onChange();
                widget.controller.allSelect();
              });
            },
            child: (widget.isTitle
                    ? widget.controller.isTitleCheck
                    : widget.controller.isSelected[widget.index])
                ? ThemeIcon(
                    ThemeIconData.check,
                    color: null,
                  )
                : ThemeIcon(
                    ThemeIconData.uncheck,
                    color: null,
                  ),
          ),
          SizedBox(width: 12),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                if (widget.isTitle)
                  TextLabel.h5(widget.text)
                else ...[
                  if (widget.textOverflow)
                    Text(
                      widget.text,
                      style: Theme.of(context).textTheme.subtitle1.copyWith(
                            fontWeight: FontWeight.normal,
                          ),
                      overflow: TextOverflow.visible,
                    )
                  else ...[
                    SizedBox(height: 2.0),
                    TextLabel.p(widget.text),
                    SizedBox(height: 2.0),
                  ]
                ],
                widget.subTitle != ''
                    ? Column(
                        children: [
                          SizedBox(height: 6),
                          Text(
                            widget.subTitle,
                            style: Theme.of(context)
                                .textTheme
                                .headline6
                                .copyWith(
                                    fontWeight: FontWeight.normal,
                                    color: NeocomixThemeColors.secondaryLight),
                          ),
                        ],
                      )
                    : Container(),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 18),
            child: widget.onTap == null
                ? null
                : GestureDetector(
                    child: ThemeIcon(
                      ThemeIconData.arrow_right,
                      color: null,
                    ),
                    onTap: widget.onTap,
                  ),
          )
        ],
      ),
    );
  }
}
