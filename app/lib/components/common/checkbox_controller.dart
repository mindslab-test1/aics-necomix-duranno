import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class CheckBoxController {
  final int totalNum;
  List<bool> isSelected;
  List<int> optional;
  bool isTitleCheck;
  CheckBoxController({
    @required this.totalNum,
  }) {
    isSelected = List<bool>.generate(totalNum, (index) => false);
    isTitleCheck = false;
    optional = List<int>();
  }

  void addOptional(int index) {
    optional.add(index);
  }

  void switchActive(int index) {
    isSelected[index] = !isSelected[index];
  }

  void switchAll() {
    bool switchBool = !isSelected.every((e) => e);
    isSelected = List<bool>.generate(totalNum, (index) => switchBool);
    isTitleCheck = switchBool;
  }

  bool allSelect() {
    for (int i = 0; i < isSelected.length; i++) {
      if (isSelected[i] != true) //false
      {
        if (!optional.contains(i)) {
          return false;
        }
      }
    }
    return true;
  }
}
