import 'package:duranno_app/components/common/expand_toggle.dart';
import 'package:flutter/material.dart';

class ExpandableText extends StatefulWidget {
  final String text;
  final TextStyle textStyle;
  ExpandableText({
    Key key,
    @required this.text,
    this.textStyle,
  })  : assert(text != null),
        super(key: key);

  @override
  _ExpandableTextState createState() => _ExpandableTextState();
}

class _ExpandableTextState extends State<ExpandableText> {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    final textWidget = Flexible(
      child: Text(
        widget.text,
        style: widget.textStyle ?? Theme.of(context).textTheme.bodyText1,
        softWrap: _expanded,
        overflow: _expanded ? null : TextOverflow.ellipsis,
        maxLines: _expanded ? null : 3,
      ),
    );

    final toggleWidget = GestureDetector(
      child: ExpandToggle(expanded: _expanded),
      behavior: HitTestBehavior.translucent,
      onTap: () {
        setState(() {
          _expanded = !_expanded;
        });
      },
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        textWidget,
        SizedBox(height: 12.0),
        toggleWidget,
      ],
    );
  }
}
