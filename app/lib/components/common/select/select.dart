import 'dart:async';

import 'package:duranno_app/components/common/select/select_controller.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

class Select<T> extends StatefulWidget {
  final String hintText;
  final SelectController<T> controller;
  Select({
    Key key,
    @required this.controller,
    this.hintText,
  })  : assert(controller != null),
        super(key: key);

  @override
  _SelectState<T> createState() => _SelectState<T>();
}

class _SelectState<T> extends State<Select<T>> {
  StreamSubscription _onCurrentChangedSubscription;

  @override
  void initState() {
    super.initState();

    _onCurrentChangedSubscription =
        widget.controller.onCurrentChanged.listen((event) {
      setState(() {});
    });
  }

  @override
  void didUpdateWidget(Select<T> oldWidget) {
    if (oldWidget.controller != widget.controller) {
      _onCurrentChangedSubscription.cancel();
      _onCurrentChangedSubscription =
          widget.controller.onCurrentChanged.listen((event) {
        setState(() {});
      });
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    _onCurrentChangedSubscription.cancel();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final boxDecoration = BoxDecoration(
      color: NeocomixThemeColors.background,
      borderRadius: BorderRadius.circular(2.0),
    );

    return Container(
      height: 52.0,
      decoration: boxDecoration,
      padding: const EdgeInsets.only(left: 16.0, right: 12.0),
      child: Align(
        alignment: Alignment.centerLeft,
        child: DropdownButton<T>(
          value: widget.controller.current,
          icon: ThemeIcon(
            ThemeIconData.arrow_down,
            size: 16.0,
            color: NeocomixThemeColors.secondaryLight,
          ),
          underline: Container(),
          hint: widget.hintText != null ? Text(widget.hintText) : null,
          isExpanded: true,
          items: widget.controller.options
              .map(
                (option) => DropdownMenuItem(
                  child: TextLabel.p(widget.controller.optionsLabel[option]),
                  value: option,
                ),
              )
              .toList(),
          onChanged: (value) {
            setState(() {
              widget.controller.current = value;
            });
          },
        ),
      ),
    );
  }
}
