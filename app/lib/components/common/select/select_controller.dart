import 'dart:async';

import 'package:meta/meta.dart';

class SelectController<T> {
  final List<T> options;
  final Map<T, String> optionsLabel;
  final T initialOption;

  final StreamController<T> _currentStreamController =
      StreamController<T>.broadcast();
  T _current;

  SelectController({
    @required this.options,
    @required this.optionsLabel,
    this.initialOption,
  }) {
    _currentStreamController.add(initialOption);
    _current = initialOption;
  }

  void clear() {
    current = initialOption;
  }

  void dispose() {
    _currentStreamController.close();
  }

  T get current => _current;
  set current(T value) {
    _currentStreamController.add(value);
    _current = value;
  }

  Stream<T> get onCurrentChanged => _currentStreamController.stream;
}
