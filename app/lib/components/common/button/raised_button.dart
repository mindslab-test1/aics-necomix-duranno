import 'dart:async';

import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';

class ThemeRaisedButton extends StatelessWidget {
  final String label;
  final ThemeIconData icon;
  final IconData materialIcon;
  final bool enabled;
  final FutureOr<void> Function() onPressed;
  ThemeRaisedButton({
    Key key,
    @required this.label,
    this.icon,
    this.materialIcon,
    this.enabled = true,
    @required this.onPressed,
  })  : assert(label != null),
        assert(onPressed != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final bool hasIcon = icon != null || materialIcon != null;
    const Color color = NeocomixThemeColors.white;
    final iconWidget = hasIcon
        ? icon != null
            ? ThemeIcon(
                icon,
                size: 24.0,
                color: color,
              )
            : Icon(
                materialIcon,
                size: 24.0,
                color: color,
              )
        : null;

    return SizedBox(
      width: double.infinity,
      child: RaisedButton(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ...IterableUtils.join<Widget>(
              [
                if (hasIcon) iconWidget,
                TextLabel.h5(label, color: color),
              ],
              separator: SizedBox(width: 6.0),
            )
          ],
        ),
        onPressed: enabled ? onPressed : null,
        color: NeocomixThemeColors.primary,
        padding: const EdgeInsets.symmetric(vertical: 16.0),
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0)),
        elevation: 8.0,
        highlightElevation: 8.0,
      ),
    );
  }
}
