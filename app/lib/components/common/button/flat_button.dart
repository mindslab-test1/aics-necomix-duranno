import 'dart:async';

import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';

enum FlatButtonType { Primary, Secondary }

class ThemeFlatButton extends StatelessWidget {
  final FlatButtonType type;
  final String label;
  final ThemeIconData icon;
  final IconData materialIcon;
  final bool enabled;
  final FutureOr<void> Function() onPressed;
  final Color color;
  final EdgeInsets contentPadding;
  ThemeFlatButton({
    Key key,
    @required this.type,
    @required this.label,
    this.icon,
    this.materialIcon,
    this.enabled,
    @required this.onPressed,
    this.color,
    this.contentPadding,
  })  : assert(type != null),
        assert(label != null),
        assert(onPressed != null),
        super(key: key);

  factory ThemeFlatButton.primary({
    @required String label,
    ThemeIconData icon,
    IconData materialIcon,
    bool enabled = true,
    FutureOr<void> Function() onPressed,
    Color color,
    EdgeInsets contentPadding,
  }) =>
      ThemeFlatButton(
        type: FlatButtonType.Primary,
        label: label,
        icon: icon,
        materialIcon: materialIcon,
        enabled: enabled,
        onPressed: onPressed,
        color: color,
        contentPadding: contentPadding,
      );

  factory ThemeFlatButton.secondary({
    @required String label,
    ThemeIconData icon,
    IconData materialIcon,
    bool enabled = true,
    FutureOr<void> Function() onPressed,
    Color color,
    EdgeInsets contentPadding,
  }) =>
      ThemeFlatButton(
        type: FlatButtonType.Secondary,
        label: label,
        icon: icon,
        materialIcon: materialIcon,
        enabled: enabled,
        onPressed: onPressed,
        color: color,
        contentPadding: contentPadding,
      );

  static Map<FlatButtonType, Color> _textColors = {
    FlatButtonType.Primary: NeocomixThemeColors.white,
    FlatButtonType.Secondary: NeocomixThemeColors.secondaryLight,
  };

  static Map<FlatButtonType, Color> _backgroundColors = {
    FlatButtonType.Primary: NeocomixThemeColors.primaryLight,
    FlatButtonType.Secondary: NeocomixThemeColors.white,
  };

  @override
  Widget build(BuildContext context) {
    final bool hasIcon = icon != null || materialIcon != null;
    final iconWidget = hasIcon
        ? SizedBox(
            width: 20.0,
            height: 20.0,
            child: icon != null
                ? ThemeIcon(
                    icon,
                    size: 20.0,
                    color: _textColors[type],
                  )
                : Icon(
                    materialIcon,
                    size: 20.0,
                    color: _textColors[type],
                  ),
          )
        : null;

    return SizedBox(
      width: double.infinity,
      child: FlatButton(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ...IterableUtils.join<Widget>(
              [
                if (hasIcon) iconWidget,
                TextLabel.button(label, color: _textColors[type]),
              ],
              separator: SizedBox(width: 4.0),
            ),
          ],
        ),
        onPressed: enabled ? onPressed : null,
        color: color ?? _backgroundColors[type],
        disabledColor: NeocomixThemeColors.disabled,
        padding: contentPadding ?? const EdgeInsets.symmetric(vertical: 14.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(4.0),
          side: type == FlatButtonType.Secondary
              ? BorderSide(
                  width: 1.0,
                  color: NeocomixThemeColors.secondaryLightest,
                )
              : BorderSide.none,
        ),
      ),
    );
  }
}
