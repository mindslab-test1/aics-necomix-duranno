import 'dart:async';
import 'dart:math';

import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

class InlineButton extends StatelessWidget {
  final Color color;
  final Color borderColor;
  final Color textColor;
  final Color disabledColor;
  final Color disabledBorderColor;
  final Color disabledTextColor;
  final EdgeInsets contentPadding;
  final double height;
  final double borderRadius;
  final ThemeIconData icon;
  final IconData materialIcon;
  final double iconSize;
  final Widget iconWidget;
  final String text;
  final bool enabled;
  final FutureOr<void> Function() onPressed;
  InlineButton({
    Key key,
    this.color = NeocomixThemeColors.secondaryDark,
    this.borderColor,
    this.textColor,
    this.disabledColor = NeocomixThemeColors.secondaryLight,
    this.disabledBorderColor,
    this.disabledTextColor,
    this.contentPadding,
    this.height,
    this.borderRadius = 2.0,
    this.icon,
    this.materialIcon,
    this.iconSize = 16.0,
    this.iconWidget,
    @required this.text,
    this.enabled = true,
    @required this.onPressed,
  })  : assert(borderRadius != null),
        assert(iconSize != null),
        assert(text != null),
        assert(enabled != null),
        assert(onPressed != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final enabledTextColorSafe = textColor ?? color;
    final disabledTextColorSafe = disabledTextColor ?? textColor ?? color;
    final enabledBorderColorSafe = borderColor ?? color;
    final disabledBorderColorSafe = disabledBorderColor ?? borderColor ?? color;
    final textColorSafe =
        enabled ? enabledTextColorSafe : disabledTextColorSafe;
    final borderColorSafe =
        enabled ? enabledBorderColorSafe : disabledBorderColorSafe;

    Widget iconWidget = icon != null
        ? ThemeIcon(icon, size: iconSize, color: textColorSafe)
        : materialIcon != null
            ? Icon(materialIcon, size: iconSize, color: textColorSafe)
            : null;
    iconWidget = iconWidget != null
        ? SizedBox(
            width: max(iconSize, 20.0),
            height: max(iconSize, 20.0),
            child: Center(child: iconWidget),
          )
        : null;

    iconWidget ??= this.iconWidget;

    Widget content = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        if (iconWidget != null) iconWidget,
        if (iconWidget != null) SizedBox(width: 4.0),
        TextLabel.h6(text, color: textColorSafe),
      ],
    );

    content = Container(
      decoration: BoxDecoration(
        border: Border.all(color: borderColorSafe, width: 1.0),
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      padding: (contentPadding ??
              (iconWidget != null
                  ? const EdgeInsets.fromLTRB(12.0, 4.0, 16.0, 4.0)
                  : const EdgeInsets.symmetric(
                      vertical: 4.0, horizontal: 12.0)))
          .copyWith(
        top: height != null ? 0 : null,
        bottom: height != null ? 0 : null,
      ),
      height: height,
      child: height != null
          ? Column(
              children: [
                Spacer(),
                content,
                Spacer(),
              ],
            )
          : content,
    );

    return GestureDetector(
      child: content,
      behavior: HitTestBehavior.translucent,
      onTap: () {
        if (enabled) {
          onPressed();
        }
      },
    );
  }
}
