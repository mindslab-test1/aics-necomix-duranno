import 'dart:io';

import 'package:flutter/material.dart';

enum TextLabelType {
  headline1,
  headline2,
  headline3,
  headline4,
  headline5,
  headline6,
  bodyText1,
  bodyText2,
  button,
  subtitle,
}

class TextLabel extends StatelessWidget {
  final String text;
  final TextLabelType type;
  final Color color;
  final FontWeight fontWeight;
  final TextOverflow textOverflow;
  final TextAlign textAlign;
  final bool expanded;
  TextLabel(
    this.text, {
    Key key,
    @required this.type,
    this.color,
    this.fontWeight,
    this.textOverflow = TextOverflow.ellipsis,
    this.textAlign = TextAlign.left,
    this.expanded = false,
  })  : assert(text != null),
        assert(type != null),
        assert(expanded != null),
        assert(textOverflow != null),
        assert(textAlign != null),
        super(key: key);

  factory TextLabel.h1(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.headline1,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  factory TextLabel.h2(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.headline2,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  factory TextLabel.h3(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.headline3,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  factory TextLabel.h4(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.headline4,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  factory TextLabel.h5(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.headline5,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  factory TextLabel.h6(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.headline6,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  factory TextLabel.span(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.bodyText1,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  factory TextLabel.smallSpan(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.bodyText2,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  factory TextLabel.button(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.button,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  factory TextLabel.p(
    String text, {
    Key key,
    Color color,
    FontWeight fontWeight,
    TextOverflow textOverflow = TextOverflow.ellipsis,
    TextAlign textAlign = TextAlign.left,
    bool expanded = false,
  }) =>
      TextLabel(
        text,
        type: TextLabelType.subtitle,
        color: color,
        fontWeight: fontWeight,
        textOverflow: textOverflow,
        textAlign: textAlign,
        expanded: expanded,
      );

  TextStyle getDefaultTextStyle(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    switch (type) {
      case TextLabelType.headline1:
        return textTheme.headline1;
      case TextLabelType.headline2:
        return textTheme.headline2;
      case TextLabelType.headline3:
        return textTheme.headline3;
      case TextLabelType.headline4:
        return textTheme.headline4;
      case TextLabelType.headline5:
        return textTheme.headline5;
      case TextLabelType.headline6:
        return textTheme.headline6;
      case TextLabelType.bodyText1:
        return textTheme.bodyText1;
      case TextLabelType.bodyText2:
        return textTheme.bodyText2;
      case TextLabelType.button:
        return textTheme.button;
      case TextLabelType.subtitle:
        return textTheme.subtitle1;
      default:
        throw Error();
    }
  }

  double getHeight(BuildContext context) {
    final textStyle = getDefaultTextStyle(context);
    return textStyle.fontSize * textStyle.height;
  }

  TextStyle getTextStyle(BuildContext context) {
    return getDefaultTextStyle(context).copyWith(
      color: color,
      fontWeight: fontWeight,
      height: Platform.isIOS ? null : 1.0,
    );
  }

  Alignment get alignment {
    switch (textAlign) {
      case TextAlign.center:
        return Alignment.center;
      case TextAlign.right:
      case TextAlign.end:
        return Alignment.centerRight;
      default:
        return Alignment.centerLeft;
    }
  }

  @override
  Widget build(BuildContext context) {
    final textWidget = Text(
      text,
      style: getTextStyle(context),
      overflow: expanded ? textOverflow : null,
    );

    if (Platform.isIOS) {
      return textWidget;
    }

    if (expanded) {
      return Container(
        height: getHeight(context),
        child: Align(alignment: alignment, child: textWidget),
      );
    } else {
      return Container(
        height: getHeight(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Spacer(),
            textWidget,
            Spacer(),
          ],
        ),
      );
    }
  }
}
