import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

const double _singleLineVerticalPadding = 14.0;
const double _snackBarPadding = 20.0;

const Curve _snackBarFadeInCurve =
    Interval(0.45, 1.0, curve: Curves.fastOutSlowIn);
const Curve _snackBarFadeOutCurve =
    Interval(0.72, 1.0, curve: Curves.fastOutSlowIn);

class ThemeSnackBar extends StatefulWidget implements SnackBar {
  final Widget content;
  final SnackBarAction action;
  final Color backgroundColor;
  final Duration duration;
  final Animation<double> animation;
  final VoidCallback onVisible;
  ThemeSnackBar({
    Key key,
    @required this.content,
    this.action,
    this.backgroundColor = NeocomixThemeColors.secondaryDark,
    this.duration = const Duration(milliseconds: 2000),
    this.animation,
    this.onVisible,
  })  : assert(content != null),
        assert(duration != null),
        super(key: key);

  double get elevation => 0.0;
  ShapeBorder get shape =>
      RoundedRectangleBorder(borderRadius: BorderRadius.circular(4.0));
  SnackBarBehavior get behavior => SnackBarBehavior.floating;
  EdgeInsetsGeometry get margin => null;
  EdgeInsetsGeometry get padding => null;
  double get width => null;

  @override
  _ThemeSnackBarState createState() => _ThemeSnackBarState();

  @override
  ThemeSnackBar withAnimation(
    Animation<double> newAnimation, {
    Key fallbackKey,
  }) {
    return ThemeSnackBar(
      key: key ?? fallbackKey,
      content: content,
      backgroundColor: backgroundColor,
      duration: duration,
      animation: newAnimation,
    );
  }
}

class _ThemeSnackBarState extends State<ThemeSnackBar> {
  bool _wasVisible = false;

  @override
  void initState() {
    super.initState();
    widget.animation.addStatusListener(_onAnimationStatusChanged);
  }

  @override
  void didUpdateWidget(ThemeSnackBar oldWidget) {
    if (widget.animation != oldWidget.animation) {
      oldWidget.animation.removeStatusListener(_onAnimationStatusChanged);
      widget.animation.addStatusListener(_onAnimationStatusChanged);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    widget.animation.removeStatusListener(_onAnimationStatusChanged);
    super.dispose();
  }

  void _onAnimationStatusChanged(AnimationStatus animationStatus) {
    switch (animationStatus) {
      case AnimationStatus.dismissed:
      case AnimationStatus.forward:
      case AnimationStatus.reverse:
        break;
      case AnimationStatus.completed:
        if (widget.onVisible != null && !_wasVisible) {
          widget.onVisible();
        }
        _wasVisible = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData mediaQueryData = MediaQuery.of(context);
    assert(widget.animation != null);
    final ThemeData theme = Theme.of(context);

    final TextStyle contentTextStyle = theme.textTheme.subtitle1.copyWith(
      color: NeocomixThemeColors.white,
    );

    final CurvedAnimation fadeInAnimation = CurvedAnimation(
      parent: widget.animation,
      curve: _snackBarFadeInCurve,
    );
    final CurvedAnimation fadeOutAnimation = CurvedAnimation(
      parent: widget.animation,
      curve: _snackBarFadeOutCurve,
      reverseCurve: const Threshold(0.0),
    );

    Widget snackBar = SafeArea(
      top: false,
      bottom: false,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          SizedBox(width: _snackBarPadding),
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(
                  vertical: _singleLineVerticalPadding),
              child: DefaultTextStyle(
                style: contentTextStyle,
                child: widget.content,
              ),
            ),
          ),
          SizedBox(width: _snackBarPadding),
        ],
      ),
    );

    snackBar = Material(
      shape: widget.shape,
      elevation: widget.elevation,
      color: widget.backgroundColor,
      child: mediaQueryData.accessibleNavigation
          ? snackBar
          : FadeTransition(
              opacity: fadeOutAnimation,
              child: snackBar,
            ),
    );

    snackBar = Padding(
      padding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 14.0),
      child: snackBar,
    );

    snackBar = Semantics(
      container: true,
      liveRegion: true,
      onDismiss: () {
        Scaffold.of(context)
            .removeCurrentSnackBar(reason: SnackBarClosedReason.dismiss);
      },
      child: Dismissible(
        key: const Key('dismissible'),
        direction: DismissDirection.down,
        resizeDuration: null,
        onDismissed: (DismissDirection direction) {
          Scaffold.of(context)
              .removeCurrentSnackBar(reason: SnackBarClosedReason.swipe);
        },
        child: snackBar,
      ),
    );

    Widget snackBarTransition;
    if (mediaQueryData.accessibleNavigation) {
      snackBarTransition = snackBar;
    } else {
      snackBarTransition = FadeTransition(
        opacity: fadeInAnimation,
        child: snackBar,
      );
    }

    return ClipRect(child: snackBarTransition);
  }
}
