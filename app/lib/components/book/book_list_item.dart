import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/components/book/book_tag.dart';
import 'package:duranno_app/components/common/snack_bar.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/mutation/playlist.dart';
import 'package:duranno_app/route/book_detail_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

final _ratingIcon = SvgPicture.asset(
  'asset/image/ico-star-middle-black.svg',
  width: 20.0,
  height: 20.0,
  color: NeocomixThemeColors.secondaryLightest,
);

const double _tagListHeight = 18.0;
const double _actionButtonSize = 24.0;

enum BookAction { DeleteFromPlaylist }

extension BookActionUtils on BookAction {
  String get label {
    switch (this) {
      case BookAction.DeleteFromPlaylist:
        {
          return '삭제';
        }
    }

    return null;
  }
}

class BookListItem extends StatelessWidget {
  final Map<String, dynamic> data;
  final bool useRouteLink;
  final List<BookAction> actions;
  final void Function({
    BookAction type,
    dynamic data,
    Map<String, dynamic> args,
  }) onActionComplete;
  BookListItem({
    Key key,
    @required this.data,
    this.useRouteLink = true,
    this.actions = const <BookAction>[],
    this.onActionComplete,
  })  : assert(data != null),
        assert(data['id'] != null),
        assert(data['title'] != null),
        assert(data['author'] == null || data['author']['name'] != null),
        assert(data['tags'] != null),
        assert(data['numViews'] != null),
        assert(useRouteLink != null),
        assert(actions != null),
        super(key: key);

  List<dynamic> get _tags => data['tags'];

  @override
  Widget build(BuildContext context) {
    final textColor = NeocomixThemeColors.secondaryDarkest;

    final thumbnailErrorWidget = Icon(
      Icons.error,
      size: 18.0,
      color: textColor,
    );

    final thumbnailWidget = data['thumbnailUri'] != null
        ? Material(
            elevation: 4.0,
            child: CachedNetworkImage(
              imageUrl: data['thumbnailUri'],
              imageBuilder: (context, imageProvider) => Container(
                child: Image(
                  image: imageProvider,
                  fit: BoxFit.cover,
                  width: 70.0,
                  height: 104.0,
                ),
              ),
              errorWidget: (context, url, error) => thumbnailErrorWidget,
            ),
          )
        : Container(
            width: 70.0,
            height: 104.0,
            color: NeocomixThemeColors.background,
            child: Center(child: thumbnailErrorWidget),
          );

    Widget tagList = Row(
      children: [
        ...IterableUtils.join<Widget>(
          _tags.map<Widget>((tagData) => BookTag(data: tagData)),
          separator: SizedBox(width: 6.0),
        ),
      ],
    );

    tagList = Container(
      height: _tagListHeight,
      child: ScrollConfiguration(
        behavior: NeverGlowScrollBehavior(),
        child: SingleChildScrollView(
          key: PageStorageKey<String>(data['id']),
          child: tagList,
          scrollDirection: Axis.horizontal,
          physics: ClampingScrollPhysics(),
        ),
      ),
    );

    final titleWidget = TextLabel.p(
      data['title'],
      fontWeight: FontWeight.bold,
      expanded: true,
    );

    final authorNameWidget = TextLabel.h6(
      (data['author'] ?? const {})['name'] ?? '',
      fontWeight: FontWeight.normal,
      color: NeocomixThemeColors.secondaryLight,
      expanded: true,
    );

    final numViewsWidget = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        ThemeIcon(
          ThemeIconData.play,
          size: 20.0,
          color: NeocomixThemeColors.secondaryLightest,
        ),
        SizedBox(width: 2.0),
        TextLabel.h6(
          data['numViews'].toString(),
          fontWeight: FontWeight.normal,
          color: NeocomixThemeColors.secondaryLight,
        ),
      ],
    );

    final ratingAverageWidget = data['rating']['average'] != null
        ? Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              _ratingIcon,
              SizedBox(width: 2.0),
              TextLabel.h6(
                (data['rating']['average'] as num).toStringAsFixed(1),
                fontWeight: FontWeight.normal,
                color: NeocomixThemeColors.secondaryLight,
              ),
            ],
          )
        : null;

    final actionButtonWidget = PopupMenuButton<BookAction>(
      child: ThemeIcon(ThemeIconData.more,
          color: NeocomixThemeColors.secondaryLight, size: _actionButtonSize),
      onSelected: (value) {
        switch (value) {
          case BookAction.DeleteFromPlaylist:
            {
              final args = {
                'bookId': data['id'],
              };

              DeleteFromPlaylistMutation.instance.execute(
                context,
                args: args,
                onCompleted: (data) {
                  Scaffold.of(context).showSnackBar(
                    ThemeSnackBar(
                      content: Text('삭제되었습니다.'),
                    ),
                  );

                  if (onActionComplete != null) {
                    onActionComplete(
                      type: BookAction.DeleteFromPlaylist,
                      data: data,
                      args: args,
                    );
                  }
                },
                onError: (error) {
                  Scaffold.of(context).showSnackBar(
                    ThemeSnackBar(
                      content: Text('오류: 책을 삭제하지 못했습니다.'),
                    ),
                  );
                },
              );
              break;
            }
        }
      },
      itemBuilder: (context) => actions
          .map(
            (action) => PopupMenuItem<BookAction>(
              value: BookAction.DeleteFromPlaylist,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  TextLabel.p(
                    action.label,
                    color: NeocomixThemeColors.primary,
                    fontWeight: FontWeight.bold,
                  ),
                ],
              ),
              height: 68.0,
            ),
          )
          .toList(),
      elevation: 4.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
        side: BorderSide(
          width: 1.0,
          color: NeocomixThemeColors.border,
        ),
      ),
    );

    final bookListItem = Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        thumbnailWidget,
        SizedBox(width: 12.0),
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  if (_tags.isNotEmpty) ...[
                    Expanded(child: tagList),
                    SizedBox(width: 8.0),
                  ] else
                    Expanded(child: Container(height: _tagListHeight)),
                  if (actions.isNotEmpty)
                    actionButtonWidget
                  else
                    Container(height: _actionButtonSize),
                ],
              ),
              titleWidget,
              SizedBox(height: 6.0),
              authorNameWidget,
              SizedBox(height: 16.0),
              Row(
                children: [
                  ...IterableUtils.join<Widget>(
                    [
                      numViewsWidget,
                      if (data['rating']['average'] != null)
                        ratingAverageWidget,
                    ],
                    separator: SizedBox(width: 8.0),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );

    if (useRouteLink) {
      return GestureDetector(
        child: bookListItem,
        behavior: HitTestBehavior.translucent,
        onTap: () {
          Navigator.of(context).pushNamed(
            RouteName.bookDetail,
            arguments: BookDetailRouteArguments(bookId: data['id']),
          );
        },
      );
    } else {
      return bookListItem;
    }
  }
}
