import 'package:duranno_app/components/book/rating_picker/rating_picker.dart';
import 'package:duranno_app/components/book/rating_picker/rating_picker_confirm_button.dart';
import 'package:duranno_app/components/book/rating_picker/rating_picker_controller.dart';
import 'package:duranno_app/components/common/button/inline_button.dart';
import 'package:duranno_app/components/common/dialog.dart';
import 'package:duranno_app/components/common/snack_bar.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/mutation/book.dart';
import 'package:duranno_app/graphql/query/book.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

final _starMiddleImage = SvgPicture.asset(
  'asset/image/ico-star-middle-black.svg',
  width: 20.0,
  height: 20.0,
);

final _filledStarImage = SvgPicture.asset(
  'asset/image/ico-star-small-filled.svg',
  width: 16.0,
  height: 16.0,
);

final _blankStarImage = SvgPicture.asset(
  'asset/image/ico-star-small-blank.svg',
  width: 16.0,
  height: 16.0,
);

final _filledStarLargeImage = SvgPicture.asset(
  'asset/image/ico-star-large-filled.svg',
  width: 36.0,
  height: 36.0,
);

final _blankStarLargeImage = SvgPicture.asset(
  'asset/image/ico-star-large-blank.svg',
  width: 36.0,
  height: 36.0,
);

final _progressIndicator = SizedBox(
  child: CircularProgressIndicator(
    valueColor: AlwaysStoppedAnimation<Color>(NeocomixThemeColors.border),
    strokeWidth: 4.0,
  ),
  width: 24.0,
  height: 24.0,
);

final _rateBookFailedSnackBar = ThemeSnackBar(
  content: Text('이미 별점을 남겼습니다.'),
);

final _deleteBookSuccessSnackBar = ThemeSnackBar(
  content: Text('삭제되었습니다.'),
);

final _deleteBookFailedSnackBar = ThemeSnackBar(
  content: Text('이미 별점이 삭제되었습니다.'),
);

class BookRating extends StatelessWidget {
  final String bookId;
  BookRating({Key key, @required this.bookId})
      : assert(bookId != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Query(
        options: QueryOptions(
          documentNode: BookQuery.rating,
          variables: {
            'bookId': bookId,
          },
        ),
        builder: (result, {refetch, fetchMore}) {
          if (result.hasException) {
            throw result.exception;
          }

          final me = (result.data ?? const {})['me'];
          final isEmailVerified = (me ?? const {})['emailVerified'];
          final isSubscribed = (me ?? const {})['subscribed'];
          final canUseRateActions =
              (isEmailVerified ?? false) && (isSubscribed ?? false);

          return _buildFromData(
            context,
            data: ((result.data ?? const {})['book'] ?? const {})['rating'],
            canUseRateActions: canUseRateActions,
            refresh: refetch,
          );
        });
  }

  Widget _buildFromData(
    BuildContext context, {
    @required Map<String, dynamic> data,
    @required bool canUseRateActions,
    @required Future<QueryResult> Function() refresh,
  }) {
    final textTheme = Theme.of(context).textTheme;

    final title = TextLabel.h4('별점', expanded: true);

    final rateBookButton = Mutation(
      options: MutationOptions(
        documentNode: BookMutation.rateBook,
        onCompleted: (resultData) {
          if (resultData['rateBook']) {
            refresh();
          } else {
            Scaffold.of(context).showSnackBar(_rateBookFailedSnackBar);
          }
        },
      ),
      builder: (runMutation, result) {
        if (result.hasException) {
          throw result.exception;
        }

        return InlineButton(
          text: '별점 남기기',
          textColor: NeocomixThemeColors.secondaryDark,
          borderColor: NeocomixThemeColors.secondaryLight,
          iconWidget: _starMiddleImage,
          borderRadius: 4.0,
          onPressed: () async {
            final ratingPickerController = RatingPickerController();
            final ratingPicker =
                RatingPicker(controller: ratingPickerController);

            await showThemeDialog(
              DialogType.TwoActions,
              context: context,
              contentWidget: Column(
                children: [
                  SizedBox(height: 8.0),
                  ratingPicker,
                ],
              ),
              title: '오디오북은 어떠셨나요?',
              confirmButton: Builder(
                builder: (context) => RatingPickerConfirmButton(
                  controller: ratingPickerController,
                  onPressed: () {
                    runMutation({
                      'bookId': bookId,
                      'value': ratingPickerController.rating,
                    });
                    Navigator.of(context)
                        .pop<DialogResult>(DialogResult.Confirm);
                  },
                ),
              ),
            );
          },
        );
      },
    );

    final deleteRatingButton = Mutation(
      options: MutationOptions(
        documentNode: BookMutation.deleteRating,
        onCompleted: (resultData) {
          if (resultData['deleteRating']) {
            refresh();
            Scaffold.of(context).showSnackBar(_deleteBookSuccessSnackBar);
          } else {
            Scaffold.of(context).showSnackBar(_deleteBookFailedSnackBar);
          }
        },
      ),
      builder: (runMutation, result) {
        if (result.hasException) {
          throw result.exception;
        }

        return InlineButton(
          text: '별점 삭제',
          textColor: NeocomixThemeColors.secondaryDark,
          borderColor: NeocomixThemeColors.secondaryLight,
          iconWidget: _starMiddleImage,
          borderRadius: 4.0,
          onPressed: () async {
            await showThemeDialog(
              DialogType.TwoActions,
              context: context,
              title: '별점을 삭제하시겠어요?',
              confirmLabel: '삭제',
              onConfirm: (context) => runMutation({
                'bookId': bookId,
              }),
            );
          },
        );
      },
    );

    final averageRatingLabel = data != null
        ? Container(
            height: 60.0,
            child: Center(
              child: Text(
                data['average']?.toStringAsFixed(1) ?? '0.0',
                style: textTheme.headline1.copyWith(
                  fontSize: 40.0,
                  height: 1.0,
                  color: NeocomixThemeColors.secondaryLight,
                ),
              ),
            ),
          )
        : _progressIndicator;

    final averageRatingStars = Row(
      children: [
        ...IterableUtils.join<Widget>(
          Iterable<int>.generate(5).map((idx) => data != null &&
                  data['average'] != null &&
                  (idx + 1 <= data['average'])
              ? _filledStarImage
              : _blankStarImage),
          separator: SizedBox(width: 2.0),
        ),
      ],
    );

    final ratingNumSubmissions = data != null && data['numSubmissions'] != null
        ? TextLabel.h6(
            '(${data['numSubmissions']})',
            color: NeocomixThemeColors.secondaryLight,
          )
        : SizedBox(
            height: textTheme.headline6.fontSize * textTheme.headline6.height);

    final averageRatingWidget = SizedBox(
      width: 88.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          averageRatingLabel,
          SizedBox(height: 4.0),
          averageRatingStars,
          SizedBox(height: 2.0),
          Align(alignment: Alignment.centerLeft, child: ratingNumSubmissions),
        ],
      ),
    );

    final ratingStatsWidget = SizedBox(
      width: 200.0,
      child: Column(children: [
        ...IterableUtils.join<Widget>(
          Iterable<int>.generate(5, (idx) => 5 - idx).map(
            (value) {
              final item = data != null
                  ? data['items']?.firstWhere(
                      (item) => item['value'] == value,
                      orElse: () => null,
                    )
                  : null;
              final ratio = data != null && data['numSubmissions'] > 0
                  ? (item != null ? item['numSubmissions'] : 0) /
                      data['numSubmissions']
                  : null;

              final indexLabelWidget = TextLabel.h6(
                value.toString(),
                color: NeocomixThemeColors.secondaryLight,
              );

              final statsBarWidget = LayoutBuilder(
                builder: (context, constraint) => ClipRRect(
                  borderRadius: BorderRadius.circular(2.0),
                  child: Stack(
                    children: [
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          color: NeocomixThemeColors.border,
                          height: 10.0,
                        ),
                      ),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Container(
                          color: NeocomixThemeColors.primaryLight,
                          width:
                              ratio != null ? ratio * constraint.maxWidth : 0,
                          height: 10.0,
                        ),
                      ),
                    ],
                  ),
                ),
              );

              final percentageWidget = Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  SizedBox(
                    width: 36.0,
                    child: Align(
                      alignment: Alignment.centerRight,
                      child: TextLabel.h6(
                        ratio != null
                            ? (ratio * 100).toStringAsFixed(1)
                            : '0.0',
                        color: NeocomixThemeColors.secondaryLight,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ),
                  SizedBox(width: 2.0),
                  TextLabel.smallSpan(
                    '%',
                    color: NeocomixThemeColors.secondaryLight,
                  ),
                ],
              );

              return Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  indexLabelWidget,
                  SizedBox(width: 10.0),
                  Expanded(child: statsBarWidget),
                  SizedBox(width: 6.0),
                  percentageWidget,
                ],
              );
            },
          ),
          separator: SizedBox(height: 4.0),
        ),
      ]),
    );

    final myRatingWidget = data != null && data['me'] != null
        ? Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: NeocomixThemeColors.border,
                width: 1.0,
              ),
              borderRadius: BorderRadius.circular(4.0),
            ),
            padding:
                const EdgeInsets.symmetric(horizontal: 36.0, vertical: 20.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                TextLabel.p('별점을 남겨주셔서 감사합니다.'),
                SizedBox(height: 8.0),
                Row(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    ...IterableUtils.join<Widget>(
                      Iterable<int>.generate(5).map((idx) =>
                          data['me'] != null && (idx + 1 <= data['me'])
                              ? _filledStarLargeImage
                              : _blankStarLargeImage),
                      separator: SizedBox(width: 8.0),
                    ),
                  ],
                ),
              ],
            ),
          )
        : null;

    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            title,
            Spacer(),
            if (canUseRateActions && data != null)
              if (data['me'] != null) deleteRatingButton else rateBookButton,
          ],
        ),
        SizedBox(height: 16.0),
        Row(
          children: [
            averageRatingWidget,
            Spacer(),
            ratingStatsWidget,
          ],
        ),
        if (data != null && data['me'] != null) SizedBox(height: 16.0),
        if (data != null && data['me'] != null) myRatingWidget,
      ],
    );
  }
}
