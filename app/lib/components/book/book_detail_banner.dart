import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/components/book/blurred_background_image.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/query/book.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

final _bookThumbnailBoxShadow = [
  BoxShadow(
    offset: Offset(0.0, 4.0),
    blurRadius: 8.0,
    color: const Color.fromRGBO(0, 0, 0, 0.25),
  ),
];

class BookDetailBanner extends StatelessWidget {
  final String bookId;
  BookDetailBanner({
    Key key,
    @required this.bookId,
  })  : assert(bookId != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
        documentNode: BookQuery.banner,
        variables: {
          'bookId': bookId,
        },
      ),
      builder: (result, {refetch, fetchMore}) => _buildView(context, result),
    );
  }

  Widget _buildView(
    BuildContext context,
    QueryResult result,
  ) {
    final textColor = NeocomixThemeColors.white;

    final hasData = result.data != null && result.data['book'] != null;

    final backgroundWidget = BlurredBackgroundImage(
      imageUri: getSafeField(
        result.data,
        fields: ['book', 'thumbnailUri'],
      ),
    );

    final thumbnailErrorWidget = Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.error,
            size: 18.0,
            color: textColor,
          ),
          SizedBox(height: 4.0),
          TextLabel.smallSpan('이미지를 찾을 수 없습니다.', color: textColor),
        ],
      ),
    );

    final queryErrorWidget = Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Icon(
            Icons.error,
            size: 24.0,
            color: textColor,
          ),
          SizedBox(height: 4.0),
          TextLabel.span('정보를 불러올 수 없습니다.', color: textColor),
        ],
      ),
    );

    final thumbnailWidget = result.loading
        ? wrappedProgressIndicatorLight
        : hasData
            ? result.data['book']['thumbnailUri'] != null
                ? CachedNetworkImage(
                    imageUrl: result.data['book']['thumbnailUri'],
                    imageBuilder: (context, imageProvider) => Container(
                      decoration: BoxDecoration(
                        boxShadow: _bookThumbnailBoxShadow,
                      ),
                      child: Image(
                        image: imageProvider,
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                    errorWidget: (context, url, error) => thumbnailErrorWidget,
                  )
                : thumbnailErrorWidget
            : queryErrorWidget;

    final titleWidget = TextLabel.h3(
      ((result.data ?? const {})['book'] ?? const {})['title'] ?? '',
      color: textColor,
    );

    final authorNameWidget = TextLabel.span(
      (((result.data ?? const {})['book'] ?? const {})['author'] ??
              const {})['name'] ??
          '',
      color: textColor,
    );

    return SizedBox(
      width: double.infinity,
      height: 395.0,
      child: Stack(
        children: [
          Positioned.fill(child: backgroundWidget),
          Positioned.fill(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 76.0),
                Expanded(child: thumbnailWidget),
                SizedBox(height: 24.0),
                titleWidget,
                SizedBox(height: 6.0),
                authorNameWidget,
                SizedBox(height: 36.0),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
