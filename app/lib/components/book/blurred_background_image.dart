import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class BlurredBackgroundImage extends StatelessWidget {
  final String imageUri;
  BlurredBackgroundImage({
    Key key,
    this.imageUri,
  }) : super(key: key);

  static final _backgroundOverlayWidget = Stack(
    children: [
      Positioned.fill(
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: const Color.fromRGBO(255, 255, 255, 0.3),
          ),
        ),
      ),
      Positioned.fill(
        child: DecoratedBox(
          decoration: BoxDecoration(
            color: const Color.fromRGBO(0, 0, 0, 0.3),
          ),
        ),
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    if (imageUri == null) {
      return _backgroundOverlayWidget;
    }

    return CachedNetworkImage(
      imageUrl: imageUri,
      imageBuilder: (context, imageProvider) => ClipRect(
        // TODO: implement better image scaling & blurring using canvas
        child: Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: BoxFit.cover,
            ),
          ),
          child: BackdropFilter(
            filter: ImageFilter.blur(sigmaX: 20.0, sigmaY: 20.0),
            child: _backgroundOverlayWidget,
          ),
        ),
      ),
      placeholder: (context, uri) => _backgroundOverlayWidget,
      errorWidget: (context, uri, error) => _backgroundOverlayWidget,
    );
  }
}
