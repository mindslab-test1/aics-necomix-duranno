import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/route/book_detail_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

const double _widgetWidth = 88.0;
const double _thumbnailImageHeight = 132.0;

class BookSimpleItem extends StatelessWidget {
  final Map<String, dynamic> data;
  final bool useRouteLink;
  BookSimpleItem({
    Key key,
    @required this.data,
    this.useRouteLink = true,
  })  : assert(data != null),
        assert(data['id'] != null),
        assert(data['title'] != null),
        super(key: key);

  String get _id => data['id'];
  String get _title => data['title'];
  String get _thumbnailUri => data['thumbnailUri'];

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final textColor = NeocomixThemeColors.secondaryDarkest;
    final titleTextStyle = textTheme.subtitle1.copyWith(
      fontWeight: FontWeight.bold,
    );
    final titleWidgetHeight =
        3.0 * titleTextStyle.fontSize * titleTextStyle.height + 12.0;

    final thumbnailErrorWidget = Icon(
      Icons.error,
      size: 18.0,
      color: textColor,
    );

    final thumbnailWidget = _thumbnailUri != null
        ? Material(
            elevation: 2.0,
            child: CachedNetworkImage(
              imageUrl: _thumbnailUri,
              imageBuilder: (context, imageProvider) => Container(
                child: Image(
                  image: imageProvider,
                  fit: BoxFit.cover,
                  width: _widgetWidth,
                  height: _thumbnailImageHeight,
                ),
              ),
              errorWidget: (context, url, error) => thumbnailErrorWidget,
            ),
          )
        : Container(
            width: _widgetWidth,
            height: _thumbnailImageHeight,
            color: NeocomixThemeColors.background,
            child: Center(child: thumbnailErrorWidget),
          );

    final widget = Container(
      width: _widgetWidth,
      child: Column(
        children: [
          thumbnailWidget,
          SizedBox(height: 8.0),
          Container(
            height: titleWidgetHeight,
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                _title,
                maxLines: 3,
                overflow: TextOverflow.ellipsis,
                style: textTheme.subtitle1.copyWith(
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
        ],
      ),
    );

    if (useRouteLink) {
      return GestureDetector(
        child: widget,
        behavior: HitTestBehavior.translucent,
        onTap: () {
          Navigator.of(context).pushNamed(
            RouteName.bookDetail,
            arguments: BookDetailRouteArguments(bookId: _id),
          );
        },
      );
    } else {
      return widget;
    }
  }
}
