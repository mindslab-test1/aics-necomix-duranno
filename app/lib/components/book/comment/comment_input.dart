import 'package:duranno_app/components/common/input_field.dart';
import 'package:duranno_app/components/common/snack_bar.dart';
import 'package:duranno_app/graphql/mutation/book.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class CommentInput extends StatefulWidget {
  final String bookId;
  final Map<String, dynamic> commentsCache;
  CommentInput({
    Key key,
    @required this.bookId,
    this.commentsCache,
  })  : assert(bookId != null),
        assert(commentsCache != null),
        super(key: key);

  @override
  _CommentInputState createState() => _CommentInputState();
}

class _CommentInputState extends State<CommentInput> {
  TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();

    _controller.addListener(_onValueChanged);
  }

  @override
  void dispose() {
    _controller.removeListener(_onValueChanged);
    _controller.dispose();

    super.dispose();
  }

  bool _canSubmit = false;
  bool _isPending = false;

  void _onValueChanged() {
    final canSubmit = _controller.text.isNotEmpty;
    if (_canSubmit != canSubmit) {
      setState(() {
        _canSubmit = canSubmit;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Mutation(
      options: MutationOptions(
        documentNode: BookMutation.writeComment,
        onCompleted: (data) {
          setState(() {
            _isPending = false;
          });
        },
        onError: (err) {
          final logger =
              Provider.of<EnvironmentProvider>(context, listen: false).logger;
          logger.error(err, null);

          Scaffold.of(context).showSnackBar(
            ThemeSnackBar(
              content: Text('오류: 댓글을 작성하지 못했습니다.'),
            ),
          );

          setState(() {
            _isPending = false;
          });
        },
        update: (cache, result) {
          if (!result.hasException) {
            final comment = result.data['writeComment'];

            _controller.text = '';

            if (widget.commentsCache != null) {
              final updated = {
                ...widget.commentsCache,
                'comments': [
                  comment,
                  ...widget.commentsCache['comments'],
                ],
              };

              cache.write(typenameDataIdFromObject(updated), updated);
            }
          }
        },
      ),
      builder: (runMutation, result) {
        return InputField(
          controller: _controller,
          hintText: '의견을 남겨주세요.',
          actionTextLabel: '등록',
          disableActionButton: !_canSubmit || _isPending,
          onClickActionButton: () async {
            setState(() {
              _isPending = true;
            });
            runMutation({
              'bookId': widget.bookId,
              'content': _controller.text,
            });
          },
          autoExpand: true,
          errorMessage: null,
        );
      },
    );
  }
}
