import 'dart:math';

import 'package:duranno_app/components/book/comment/book_comment.dart';
import 'package:duranno_app/components/book/comment/comment_input.dart';
import 'package:duranno_app/components/common/expand_toggle.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/query/book.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/constants.dart';
import 'package:duranno_app/utils/lazy_load_scrollview/lazy_load_scrollview.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

const _initialVisibleNumComments = 3;
const _commentsPagination = 5;

class BookCommentList extends StatefulWidget {
  final String bookId;
  BookCommentList({
    Key key,
    @required this.bookId,
  })  : assert(bookId != null),
        super(key: key);

  @override
  _BookCommentListState createState() => _BookCommentListState();
}

class _BookCommentListState extends State<BookCommentList> {
  bool _expanded = false;

  @override
  Widget build(BuildContext context) {
    final titleWidget = Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: TextLabel.h4('댓글', expanded: true),
    );

    final authentication = Provider.of<AuthenticationProvider>(context);
    final userId = authentication.user.uid;

    return Query(
      options: QueryOptions(
        documentNode: BookQuery.comments,
        variables: {
          'bookId': widget.bookId,
          'limit': _initialVisibleNumComments,
        },
      ),
      builder: (result, {refetch, fetchMore}) {
        final hasData = result.data != null;
        final List<dynamic> comments =
            ((result.data ?? const {})['bookComments'] ?? const {})['comments'];
        final bool isEmailVerified =
            ((result.data ?? const {})['me'] ?? const {})['emailVerified'] ??
                false;
        final bool isSubscribed =
            ((result.data ?? const {})['me'] ?? const {})['subscribed'] ??
                false;

        if (!hasData) {
          return SliverList(
            delegate: SliverChildListDelegate([
              titleWidget,
              if (result.loading) wrappedProgressIndicatorDark,
              SizedBox(height: 24.0),
            ]),
          );
        }

        final canWriteComment = isEmailVerified && isSubscribed;

        final fetchNextPage = () {
          if (_expanded &&
              !result.loading &&
              result.data['bookComments']['cursor'] != null) {
            fetchMore(
              _generateFetchMoreOptions(
                context,
                resultData: result.data,
                fetchMore: fetchMore,
              ),
            );
          } else {
            InheritedLazyScrollViewWidget.onDidUpdate(context);
          }
        };

        final toggleWidget = GestureDetector(
          child: ExpandToggle(expanded: _expanded),
          behavior: HitTestBehavior.translucent,
          onTap: () {
            setState(() {
              _expanded = !_expanded;
              if (comments.length == _initialVisibleNumComments) {
                fetchNextPage();
              }
            });
          },
        );

        InheritedLazyScrollViewWidget.setOnScrollEndCallback(
          context,
          callback: fetchNextPage,
        );

        final expandable = comments.length >= _initialVisibleNumComments;
        final visibleNumComments = _expanded
            ? comments.length
            : min(_initialVisibleNumComments, comments.length);

        final sliverListChildCount = visibleNumComments * 2 +
            (expandable ? 1 : 0) +
            (canWriteComment ? 2 : 0) +
            (result.loading ? 1 : 0) +
            (comments.isEmpty ? 1 : 0) +
            2;

        return SliverList(
          delegate: SliverChildBuilderDelegate(
            (context, index) {
              /* title */
              if (index == 0) {
                return titleWidget;
              }

              /* title bottom margin */
              if (index == 1) {
                return SizedBox(height: 16.0);
              }

              if (canWriteComment && index == 2) {
                return Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: CommentInput(
                    bookId: widget.bookId,
                    commentsCache: result.data['bookComments'],
                  ),
                );
              }

              if (canWriteComment && index == 3) {
                return SizedBox(height: 36.0);
              }

              /* progress indicator */
              if (result.loading &&
                  index == sliverListChildCount - 1 - (expandable ? 1 : 0)) {
                return wrappedProgressIndicatorDark;
              }

              /* expand toggle */
              if (expandable && index == sliverListChildCount - 1) {
                return Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 24.0),
                  child: toggleWidget,
                );
              }

              final listStartIndex = 2 + (canWriteComment ? 2 : 0);

              /* empty comment list */
              if (index == listStartIndex &&
                  comments.isEmpty &&
                  !result.loading)
                return Padding(
                  padding: const EdgeInsets.symmetric(vertical: 52.0),
                  child: Center(
                    child: TextLabel.p(
                      '아직 댓글이 없습니다.',
                      color: NeocomixThemeColors.secondaryLight,
                    ),
                  ),
                );

              if (index.isEven) {
                /* comment item */
                final comment = comments[(index - listStartIndex) ~/ 2];
                final String commentWriterId = comment['user']['id'];
                final canUseCommentActions = isEmailVerified &&
                    isSubscribed &&
                    commentWriterId == userId;

                return Padding(
                  padding: contentPadding,
                  child: BookComment(
                    key: ValueKey(comment['id']),
                    data: comment,
                    canUseActions: canUseCommentActions,
                    refetch: refetch,
                  ),
                );
              } else {
                /* comment bottom margin */
                return SizedBox(height: 28.0);
              }
            },
            childCount: sliverListChildCount,
          ),
        );
      },
    );
  }

  FetchMoreOptions _generateFetchMoreOptions(
    BuildContext context, {
    @required dynamic resultData,
    @required dynamic Function(FetchMoreOptions) fetchMore,
  }) {
    return FetchMoreOptions(
      documentNode: BookQuery.comments,
      variables: {
        'cursor': resultData['bookComments']['cursor'],
        'limit': _commentsPagination,
      },
      updateQuery: (previousResultData, fetchMoreResultData) {
        fetchMoreResultData['bookComments']['comments'] = [
          ...previousResultData['bookComments']['comments'],
          ...fetchMoreResultData['bookComments']['comments'],
        ];

        InheritedLazyScrollViewWidget.onDidUpdate(context);

        return fetchMoreResultData;
      },
    );
  }
}
