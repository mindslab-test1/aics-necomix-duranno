import 'package:duranno_app/components/common/snack_bar.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/mutation/book.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

enum BookCommentAction { Delete }

class BookComment extends StatelessWidget {
  final Map<String, dynamic> data;
  final bool canUseActions;
  final Future<QueryResult> Function() refetch;
  final DateTime _updatedAt;
  BookComment({
    Key key,
    @required this.canUseActions,
    @required this.data,
    this.refetch,
  })  : assert(data != null),
        assert(canUseActions != null),
        _updatedAt = DateTime.parse(data['updatedAt']).toLocal(),
        super(key: key);

  /* yy.mm.dd */
  String get _formattedUpdatedDate =>
      '${_updatedAt.year % 100}.${NumUtils.zeroPad(_updatedAt.month, 2)}.${NumUtils.zeroPad(_updatedAt.day, 2)}';
  /* h24:min */
  String get _formattedTime =>
      '${NumUtils.zeroPad(_updatedAt.hour, 2)}:${NumUtils.zeroPad(_updatedAt.minute, 2)}';

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    final userEmail =
        TextLabel.h6(data['user']['email'], fontWeight: FontWeight.normal);

    final content = Flexible(
      child: Text(
        data['content'],
        style: textTheme.bodyText1,
      ),
    );

    final updatedAt = Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        TextLabel.h6(
          _formattedUpdatedDate,
          color: NeocomixThemeColors.secondaryLightest,
          fontWeight: FontWeight.normal,
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8.0),
          child: Center(
            child: Container(
              color: NeocomixThemeColors.border,
              width: 1.0,
              height: 8.0,
            ),
          ),
        ),
        TextLabel.h6(
          _formattedTime,
          color: NeocomixThemeColors.secondaryLightest,
          fontWeight: FontWeight.normal,
        ),
      ],
    );

    final commentActionsButton = PopupMenuButton<BookCommentAction>(
      child: ThemeIcon(
        ThemeIconData.more,
        color: NeocomixThemeColors.secondaryLight,
        size: 24.0,
      ),
      onSelected: (value) {
        switch (value) {
          case BookCommentAction.Delete:
            {
              DeleteCommentMutation.instance.execute(
                context,
                args: {'commentId': data['id']},
                onCompleted: (data) {
                  Scaffold.of(context).showSnackBar(
                    ThemeSnackBar(
                      content: Text('삭제되었습니다.'),
                    ),
                  );

                  if (refetch != null) {
                    refetch();
                  }
                },
                onError: (error) {
                  Scaffold.of(context).showSnackBar(
                    ThemeSnackBar(
                      content: Text('오류: 댓글을 삭제하지 못했습니다.'),
                    ),
                  );
                },
              );
              break;
            }
        }
      },
      itemBuilder: (context) => <PopupMenuEntry<BookCommentAction>>[
        PopupMenuItem(
          value: BookCommentAction.Delete,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextLabel.p(
                '삭제',
                color: NeocomixThemeColors.primary,
                fontWeight: FontWeight.bold,
              ),
            ],
          ),
          height: 68.0,
        ),
      ],
      elevation: 4.0,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(4.0),
        side: BorderSide(
          width: 1.0,
          color: NeocomixThemeColors.border,
        ),
      ),
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Row(
          children: [
            userEmail,
            Spacer(),
            if (canUseActions) commentActionsButton,
          ],
        ),
        SizedBox(height: 4.0),
        content,
        SizedBox(height: 4.0),
        updatedAt
      ],
    );
  }
}
