import 'dart:async';

import 'package:duranno_app/components/book/rating_picker/rating_picker_controller.dart';
import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:flutter/material.dart';

class RatingPickerConfirmButton extends StatefulWidget {
  final RatingPickerController controller;
  final FutureOr<void> onPressed;
  RatingPickerConfirmButton({
    Key key,
    @required this.controller,
    @required this.onPressed,
  })  : assert(controller != null),
        assert(onPressed != null),
        super(key: key);

  @override
  _RatingPickerConfirmButtonState createState() =>
      _RatingPickerConfirmButtonState();
}

class _RatingPickerConfirmButtonState extends State<RatingPickerConfirmButton> {
  @override
  void initState() {
    super.initState();

    widget.controller.addListener(_refresh);
  }

  @override
  void didUpdateWidget(RatingPickerConfirmButton oldWidget) {
    if (widget.controller != oldWidget.controller) {
      oldWidget.controller.removeListener(_refresh);
      widget.controller.addListener(_refresh);
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return ThemeFlatButton.primary(
      label: '확인',
      enabled: widget.controller.rating != null,
      onPressed: widget.onPressed,
    );
  }

  void _refresh() {
    setState(() {});
  }
}
