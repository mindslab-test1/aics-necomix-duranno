import 'package:duranno_app/components/book/rating_picker/rating_picker_controller.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

final _filledStarLargeImage = SvgPicture.asset(
  'asset/image/ico-star-large-filled.svg',
  width: 36.0,
  height: 36.0,
);

final _blankStarLargeImage = SvgPicture.asset(
  'asset/image/ico-star-large-blank.svg',
  width: 36.0,
  height: 36.0,
);

class RatingPicker extends StatefulWidget {
  final RatingPickerController controller;
  RatingPicker({
    Key key,
    @required this.controller,
  })  : assert(controller != null),
        super(key: key);

  @override
  _RatingPickerState createState() => _RatingPickerState();
}

class _RatingPickerState extends State<RatingPicker> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        ...IterableUtils.join<Widget>(
          Iterable<int>.generate(widget.controller.maxRating).map(
            (index) => GestureDetector(
              child: widget.controller.rating == null ||
                      widget.controller.rating <= index
                  ? _blankStarLargeImage
                  : _filledStarLargeImage,
              onTap: () {
                setState(() {
                  widget.controller.rating = index + 1;
                });
              },
              behavior: HitTestBehavior.translucent,
            ),
          ),
          separator: SizedBox(width: 8.0),
        ),
      ],
    );
  }
}
