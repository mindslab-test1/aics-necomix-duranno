class RatingPickerController {
  final int maxRating;
  RatingPickerController({this.maxRating = 5}) : assert(maxRating != null);

  List<void Function()> _listeners = [];

  int _rating;
  int get rating => _rating;
  set rating(int value) {
    _rating = value;
    _listeners.forEach((listener) {
      listener();
    });
  }

  void addListener(void Function() listener) {
    _listeners.add(listener);
  }

  void removeListener(void Function() listener) {
    _listeners.remove(listener);
  }
}
