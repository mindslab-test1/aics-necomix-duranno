import 'package:duranno_app/components/book/book_chapter_list.dart';
import 'package:duranno_app/components/common/button/inline_button.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/query/book.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/book.dart';
import 'package:duranno_app/utils/constants.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class BookTableOfContents extends StatelessWidget {
  final String bookId;
  final Future<void> Function(
    BuildContext context, {
    String bookId,
    int sequence,
  }) maybeLinkToAudioPlayerRoute;
  BookTableOfContents({
    Key key,
    @required this.bookId,
    @required this.maybeLinkToAudioPlayerRoute,
  })  : assert(bookId != null),
        assert(maybeLinkToAudioPlayerRoute != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final textColorLight = NeocomixThemeColors.secondaryLight;

    final titleWidget = TextLabel.h4('목차', expanded: true);

    return Query(
      options: QueryOptions(
        documentNode: BookQuery.tableOfContents,
        variables: {
          'bookId': bookId,
        },
      ),
      builder: (result, {refetch, fetchMore}) {
        final hasData = result.data != null && result.data['book'] != null;

        if (!hasData) {
          return SliverList(
            delegate: SliverChildListDelegate([
              if (result.loading) wrappedProgressIndicatorDark else errorWidget,
            ]),
          );
        }

        return FutureBuilder<AudioQueue>(
          future: normalizeRawBook(result.data['book']),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return SliverList(
                delegate: SliverChildListDelegate([
                  errorWidget,
                ]),
              );
            }

            if (!snapshot.hasData) {
              return SliverList(
                delegate: SliverChildListDelegate([
                  wrappedProgressIndicatorDark,
                ]),
              );
            }

            final playAllButton = InlineButton(
              text: '전체 재생',
              textColor: NeocomixThemeColors.secondaryDark,
              borderColor: NeocomixThemeColors.secondaryLight,
              borderRadius: 4.0,
              icon: ThemeIconData.headphone,
              iconSize: 20.0,
              onPressed: () =>
                  maybeLinkToAudioPlayerRoute(context, bookId: bookId),
            );

            final playAllWidget = Row(
              children: [
                Row(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    ThemeIcon(
                      ThemeIconData.playlist,
                      color: textColorLight,
                      size: 24.0,
                    ),
                    SizedBox(width: 4.0),
                    TextLabel.p('전체 재생 시간', color: textColorLight),
                    SizedBox(width: 8.0),
                    TextLabel.p(
                      NumUtils.formatTimeInSeconds(
                          snapshot.data.totalDuration.inSeconds),
                      color: textColorLight,
                    ),
                  ],
                ),
                Spacer(),
                playAllButton,
              ],
            );

            return SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(height: 36.0),
                Padding(padding: contentPadding, child: titleWidget),
                SizedBox(height: 14.0),
                Padding(padding: contentPadding, child: playAllWidget),
                SizedBox(height: 24.0),
                if (snapshot.data.useSequence)
                  BookChapterList(
                    bookId: bookId,
                    chapters: snapshot.data.chapters,
                    maybeLinkToAudioPlayerRoute: maybeLinkToAudioPlayerRoute,
                  ),
              ]),
            );
          },
        );
      },
    );
  }
}
