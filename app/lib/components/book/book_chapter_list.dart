import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/book.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';

class BookChapterList extends StatelessWidget {
  final String bookId;
  final List<Chapter> chapters;
  final Future<void> Function(
    BuildContext context, {
    String bookId,
    int sequence,
  }) maybeLinkToAudioPlayerRoute;
  BookChapterList({
    Key key,
    @required this.bookId,
    @required this.chapters,
    @required this.maybeLinkToAudioPlayerRoute,
  })  : assert(bookId != null),
        assert(chapters != null),
        assert(maybeLinkToAudioPlayerRoute != null),
        super(key: key);

  List<dynamic> get _chaptersWithMedia =>
      chapters.where((chapter) => chapter.isSequence).toList();

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ...chapters.map<Widget>(
          (chapter) {
            final chapterTitleWidget = Flexible(
              child: TextLabel.p(
                chapter.title,
                fontWeight: FontWeight.bold,
                color: chapter.isSequence
                    ? NeocomixThemeColors.secondaryDarkest
                    : NeocomixThemeColors.secondaryLight,
                expanded: true,
              ),
            );

            final sequence = _chaptersWithMedia.indexOf(chapter);

            final durationWidget = chapter.isSequence
                ? TextLabel.h6(
                    NumUtils.formatTimeInSeconds(chapter.duration.inSeconds),
                    color: NeocomixThemeColors.secondaryLight,
                  )
                : null;

            final playChapterButton = chapter.isSequence
                ? ThemeIconButton(
                    icon: ThemeIconData.play,
                    iconSize: 20.0,
                    iconColor: NeocomixThemeColors.primaryLight,
                    onPressed: () => maybeLinkToAudioPlayerRoute(
                      context,
                      bookId: bookId,
                      sequence: sequence,
                    ),
                  )
                : SizedBox(width: 20.0);

            return Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 20.0, vertical: 4.0),
              child: Container(
                height: 40.0,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      chapterTitleWidget,
                      SizedBox(width: 12.0),
                      SizedBox(
                        width: 48.0,
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: durationWidget,
                        ),
                      ),
                      SizedBox(width: 12.0),
                      playChapterButton,
                      SizedBox(width: 8.0),
                    ],
                  ),
                ),
              ),
            );
          },
        ),
        SizedBox(height: 48.0),
      ],
    );
  }
}
