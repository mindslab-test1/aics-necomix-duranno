import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/components/book/book_rating.dart';
import 'package:duranno_app/components/book/book_tag.dart';
import 'package:duranno_app/components/common/expandable_text.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/query/book.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/constants.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

final _dividerWidget = Container(
  color: NeocomixThemeColors.background,
  height: 4.0,
);

class BookInfo extends StatelessWidget {
  final String bookId;
  BookInfo({
    Key key,
    @required this.bookId,
  })  : assert(bookId != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
        documentNode: BookQuery.info,
        variables: {
          'bookId': bookId,
        },
      ),
      builder: (result, {refetch, fetchMore}) => _buildView(context, result),
    );
  }

  Widget _buildView(BuildContext context, QueryResult result) {
    final hasData = result.data != null && result.data['book'] != null;

    if (result.loading) {
      return wrappedProgressIndicatorDark;
    }

    if (result.hasException) {
      final logger = Provider.of<EnvironmentProvider>(context).logger;
      logger.warning(result.exception.toString());
      return errorWidget;
    }

    if (!hasData) {
      return errorWidget;
    }

    final tagsWidget = Container(
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 24.0),
      child: Wrap(
        children: [
          ...result.data['book']['tags']
              .map<Widget>((tagData) => BookTag(data: tagData, large: true)),
        ],
        spacing: 8.0,
        runSpacing: 8.0,
      ),
    );

    final bookDescriptionWidget = Container(
      color: NeocomixThemeColors.white,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(height: 36.0),

          /* title */
          Align(
            alignment: Alignment.centerLeft,
            child: Padding(
              padding: contentPadding,
              child: TextLabel.h4('오디오북 소개', expanded: true),
            ),
          ),
          SizedBox(height: 16.0),

          /* description */
          if (result.data['book']['description'] != null)
            Padding(
              padding: contentPadding,
              child: ExpandableText(text: result.data['book']['description']),
            ),
          SizedBox(height: 24.0),
        ],
      ),
    );

    final authorDescriptionWidget = result.data['book']['author'] != null
        ? Container(
            color: NeocomixThemeColors.background,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 36.0),

                /* title */
                Align(
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: contentPadding,
                    child: TextLabel.h4('저자 소개', expanded: true),
                  ),
                ),
                SizedBox(height: 16.0),

                /* profile image */
                if (result.data['book']['author']['profileImageUri'] != null)
                  CachedNetworkImage(
                    imageUrl: result.data['book']['author']['profileImageUri'],
                    imageBuilder: (context, imageProvider) => Container(
                      width: 100.0,
                      height: 100.0,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100.0),
                        image: DecorationImage(
                          image: imageProvider,
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  )
                else
                  ThemeIcon(ThemeIconData.profile, size: 100.0, color: null),
                SizedBox(height: 12.0),

                /* name */
                TextLabel.span(result.data['book']['author']['name']),
                SizedBox(height: 24.0),

                /* description */
                if (result.data['book']['author']['description'] != null)
                  Padding(
                    padding: contentPadding,
                    child: ExpandableText(
                        text: result.data['book']['author']['description']),
                  ),
                if (result.data['book']['author']['description'] != null)
                  SizedBox(height: 24.0),
              ],
            ),
          )
        : null;

    final ratingWidget = Container(
      color: NeocomixThemeColors.white,
      child: Padding(
        padding: contentPadding,
        child: BookRating(bookId: bookId),
      ),
    );

    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        if (result.data['book']['tags'].isNotEmpty) tagsWidget,
        if (result.data['book']['tags'].isNotEmpty) _dividerWidget,
        bookDescriptionWidget,
        if (result.data['book']['author'] != null) authorDescriptionWidget,
        SizedBox(height: 36.0),
        ratingWidget,
        SizedBox(height: 36.0),
      ],
    );
  }
}
