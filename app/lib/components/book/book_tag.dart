import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';

class BookTag extends StatelessWidget {
  final Map<String, dynamic> data;
  final bool large;
  BookTag({
    @required this.data,
    this.large = false,
  })  : assert(data != null),
        assert(data['text'] != null && data['text'] is String),
        assert(data['color'] == null || data['color'] is String),
        assert(data['backgroundColor'] == null ||
            data['backgroundColor'] is String),
        assert(large != null);

  @override
  Widget build(BuildContext context) {
    final height = large ? 28.0 : 18.0;
    final padding = large
        ? const EdgeInsets.symmetric(horizontal: 12.0)
        : const EdgeInsets.symmetric(horizontal: 6.0);

    final borderRadius =
        large ? BorderRadius.circular(4.0) : BorderRadius.circular(2.0);

    final color = data['backgroundColor'] != null
        ? NeocomixThemeColors.white
        : ColorUtils.hexToColor(data['color'] ?? '#000000');
    final label = large
        ? TextLabel.h6(
            data['text'],
            fontWeight: FontWeight.normal,
            color: color,
          )
        : TextLabel.smallSpan(
            data['text'],
            color: color,
          );

    if (data['backgroundColor'] != null) {
      final backgroundColor = ColorUtils.hexToColor(data['backgroundColor']);
      return Container(
        height: height,
        decoration: BoxDecoration(
          color: backgroundColor,
          borderRadius: borderRadius,
        ),
        padding: padding,
        child: label,
      );
    } else {
      final color = ColorUtils.hexToColor(data['color'] ?? '#000000');
      return Container(
        height: height,
        decoration: BoxDecoration(
          borderRadius: borderRadius,
          border: Border.all(
            color: color,
            width: 0.5,
          ),
        ),
        padding: padding,
        child: label,
      );
    }
  }
}
