import 'package:duranno_app/components/book/book_simple_item.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/home/const.dart';
import 'package:duranno_app/graphql/query/book.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

const double _widgetHeight = 200.0;

class FeaturedBooksShelf extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
        documentNode: BookQuery.featured,
        variables: {
          'limit': numFeaturedCarouselItems + numFeaturedShelfItems,
        },
      ),
      builder: (result, {fetchMore, refetch}) {
        if (result.loading) {
          return Container(
            height: _widgetHeight,
            child: Center(child: wrappedProgressIndicatorDark),
          );
        }

        if (result.hasException) {
          return Container(
            height: _widgetHeight,
            child: Center(child: errorWidget),
          );
        }

        final List<dynamic> data =
            getSafeField(result.data, fields: ['featuredBooks']);

        final sublist = (data.length >= numFeaturedCarouselItems)
            ? data.sublist(numFeaturedCarouselItems)
            : [];

        if (sublist.isEmpty) {
          return Container(
            height: 60.0,
            child: Center(
              child: TextLabel.p(
                '추천 도서가 없습니다.',
                color: NeocomixThemeColors.secondaryLight,
              ),
            ),
          );
        }

        return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: Row(
              children: [
                ...IterableUtils.join<Widget>(
                  sublist.map(
                        (book) => book is Map
                            ? BookSimpleItem(
                                data: book,
                                key: ValueKey<String>(book['id']),
                              )
                            : Container(),
                      ) ??
                      [],
                  separator: SizedBox(width: 12.0),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
