import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/home/const.dart';
import 'package:duranno_app/graphql/query/book.dart';
import 'package:duranno_app/route/book_detail_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

final _ratingIcon = SvgPicture.asset(
  'asset/image/ico-star-middle-black.svg',
  width: 20.0,
  height: 20.0,
  color: NeocomixThemeColors.white,
);

final _cardColors = [
  const Color(0xFF92D1B0),
  const Color(0xFF97D1CF),
  const Color(0xFF94BDE0),
  const Color(0xFF9EA1E8),
  const Color(0xFFC5A7E8),
  const Color(0xFFD49AD6),
  const Color(0xFFDEA4C3),
  const Color(0xFFE39FA1),
];

class _FeaturedBook extends StatefulWidget {
  final Map<String, dynamic> data;
  _FeaturedBook({
    Key key,
    @required this.data,
  })  : assert(data != null),
        assert(data['id'] != null),
        assert(data['title'] != null),
        super(key: key);

  @override
  _FeaturedBookState createState() => _FeaturedBookState();
}

enum _ImageLoadState { Pending, Error, Complete }

class _FeaturedBookState extends State<_FeaturedBook> {
  Size _imageSize;
  GlobalKey _imageWidgetKey = GlobalKey();
  CachedNetworkImageProvider _imageProvider;
  _ImageLoadState _imageLoadState;
  ImageStreamListener get _imageStreamListener => ImageStreamListener(
        (image, synchronousCall) {
          setState(() {
            _imageLoadState = _ImageLoadState.Complete;
          });
          WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
            _updateDimension();
          });
        },
        onError: (exception, stackTrace) {
          setState(() {
            _imageLoadState = _ImageLoadState.Error;
          });
        },
      );

  Color get _cardColor =>
      _cardColors[widget.data['id'].hashCode % _cardColors.length];

  void _updateDimension() {
    final RenderBox imageRenderBox =
        _imageWidgetKey.currentContext?.findRenderObject();
    _imageSize = imageRenderBox?.size;

    setState(() {});
  }

  void initState() {
    super.initState();

    if (widget.data['thumbnailUri'] != null) {
      _imageLoadState = _ImageLoadState.Pending;
      _imageProvider = CachedNetworkImageProvider(widget.data['thumbnailUri']);
      _imageProvider
          .resolve(ImageConfiguration())
          .addListener(_imageStreamListener);
    } else {
      _imageLoadState = _ImageLoadState.Complete;
    }
  }

  @override
  void didUpdateWidget(_FeaturedBook oldWidget) {
    if (oldWidget.data['thumbnailUri'] != widget.data['thumbnailUri']) {
      if (widget.data['thumbnailUri'] != null) {
        setState(() {
          _imageLoadState = _ImageLoadState.Pending;
        });
        _imageProvider =
            CachedNetworkImageProvider(widget.data['thumbnailUri']);
        _imageProvider
            .resolve(ImageConfiguration())
            .addListener(_imageStreamListener);
      } else {
        setState(() {
          _imageLoadState = _ImageLoadState.Complete;
        });
      }
    }

    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;
    final textColor = NeocomixThemeColors.white;

    final thumbnailPendingWidget = Container(
      width: 116.0,
      color: NeocomixThemeColors.background,
      child: Center(child: wrappedProgressIndicatorLight),
    );

    final thumbnailErrorWidget = Container(
      width: 116.0,
      key: _imageWidgetKey,
      color: NeocomixThemeColors.background,
      child: Center(
        child: Icon(
          Icons.error,
          size: 18.0,
          color: NeocomixThemeColors.secondaryDarkest,
        ),
      ),
    );

    final thumbnailCompleteWidget = widget.data['thumbnailUri'] != null
        ? Image(
            image: _imageProvider,
            key: _imageWidgetKey,
            fit: BoxFit.fitHeight,
          )
        : thumbnailErrorWidget;

    final thumbnailWidget = Material(
      elevation: 6.0,
      child: () {
        switch (_imageLoadState) {
          case _ImageLoadState.Complete:
            return thumbnailCompleteWidget;
          case _ImageLoadState.Error:
            return thumbnailErrorWidget;
          case _ImageLoadState.Pending:
            return thumbnailPendingWidget;
        }
      }(),
    );

    final titleWidget = Text(
      widget.data['title'],
      style: textTheme.headline5.copyWith(color: textColor),
      overflow: TextOverflow.ellipsis,
      maxLines: 3,
    );

    final authorNameWidget = widget.data['author'] != null
        ? Text(
            widget.data['author']['name'],
            style: textTheme.subtitle1.copyWith(color: textColor),
            overflow: TextOverflow.ellipsis,
            maxLines: 3,
          )
        : null;

    final ratingAverageWidget = widget.data['rating']['average'] != null
        ? Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              _ratingIcon,
              SizedBox(width: 2.0),
              Text(
                (widget.data['rating']['average'] as num).toStringAsFixed(1),
                style: textTheme.headline6.copyWith(
                  fontWeight: FontWeight.normal,
                  color: textColor,
                ),
              ),
            ],
          )
        : null;

    final cardWidget = Container(
      color: _cardColor,
      padding: const EdgeInsets.symmetric(vertical: 20.0),
      child: Row(
        children: [
          SizedBox(width: (_imageSize?.width ?? 116.0) - 12.0),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                titleWidget,
                SizedBox(height: 10.0),
                if (widget.data['author'] != null) authorNameWidget,
                Spacer(),
                if (widget.data['rating']['average'] != null)
                  ratingAverageWidget,
              ],
            ),
          ),
          SizedBox(width: 16.0),
        ],
      ),
    );

    Widget carouselItem = Stack(
      children: [
        Positioned(
          left: 40.0,
          right: 0,
          top: 0,
          bottom: 0,
          child: cardWidget,
        ),
        Positioned(
          left: 16.0,
          top: 20.0,
          bottom: 20.0,
          child: thumbnailWidget,
        ),
      ],
    );

    carouselItem = GestureDetector(
      child: carouselItem,
      onTap: () {
        Navigator.of(context).pushNamed(
          RouteName.bookDetail,
          arguments: BookDetailRouteArguments(bookId: widget.data['id']),
        );
      },
    );

    return carouselItem;
  }
}

class FeaturedBooksCarousel extends StatefulWidget {
  @override
  _FeaturedBooksCarouselState createState() => _FeaturedBooksCarouselState();
}

class _FeaturedBooksCarouselState extends State<FeaturedBooksCarousel> {
  PageController _pageController;
  ValueNotifier<double> _pageValueNotifier;

  @override
  void initState() {
    super.initState();

    _pageValueNotifier = ValueNotifier<double>(0.0);
    _pageController = PageController(
      initialPage: 0,
      viewportFraction: 0.7667,
    )..addListener(
        () {
          _pageValueNotifier.value = _pageController.page;
        },
      );
  }

  @override
  void dispose() {
    _pageController.dispose();
    _pageValueNotifier.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
        documentNode: BookQuery.featured,
        variables: {
          'limit': numFeaturedCarouselItems,
        },
      ),
      builder: (result, {fetchMore, refetch}) {
        if (result.loading) {
          return Container(
            height: 210.0,
            child: Center(child: wrappedProgressIndicatorDark),
          );
        }

        if (result.hasException) {
          return Container(
            height: 210.0,
            child: Center(child: errorWidget),
          );
        }

        final List<dynamic> data =
            getSafeField(result.data, fields: ['featuredBooks']);

        if (data.isEmpty) {
          return Container(
            height: 210.0,
            child: Center(
              child: TextLabel.p(
                '추천 도서가 없습니다.',
                color: NeocomixThemeColors.secondaryLight,
              ),
            ),
          );
        }

        return Container(
          height: 210.0,
          child: PageView.builder(
            controller: _pageController,
            itemBuilder: (context, index) {
              final String bookId = getSafeField(data, fields: [index, 'id']);
              final book = getSafeField(data, fields: [index]);

              if (!(book is Map)) {
                return Center(child: wrappedProgressIndicatorDark);
              }

              return AnimatedBuilder(
                animation: _pageValueNotifier,
                builder: (context, child) {
                  final scale = lerpDouble(
                    1.0,
                    0.9,
                    (_pageValueNotifier.value - index).abs().clamp(0.0, 1.0),
                  );
                  final alignment = Alignment.lerp(
                    Alignment.center,
                    Alignment.centerRight,
                    (_pageValueNotifier.value - index).clamp(-1.0, 1.0),
                  );
                  return Transform.scale(
                    scale: scale,
                    alignment: alignment,
                    child: child,
                  );
                },
                child: Padding(
                  padding: const EdgeInsets.only(right: 10.0),
                  child: _FeaturedBook(
                    key: ValueKey<String>(bookId),
                    data: book,
                  ),
                ),
              );
            },
            itemCount: data.length,
          ),
        );
      },
    );
  }
}
