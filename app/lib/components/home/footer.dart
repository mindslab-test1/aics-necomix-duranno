import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/route/customer_service_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';

const _footerContent = """㈜네오코믹스 | 대표이사 : 권택준
주소 : 경기도 성남시 수정구 대왕판교로 815, 829, 830호
고객센터 : neocomix@neocomix.com
사업자등록증 : 462-81-01109
""";

class Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final buildLink = ({
      String label,
      void Function() onPressed,
    }) =>
        GestureDetector(
          child: TextLabel.h6(
            label,
            color: NeocomixThemeColors.secondaryLight,
          ),
          behavior: HitTestBehavior.translucent,
          onTap: onPressed,
        );

    return Container(
      color: NeocomixThemeColors.background,
      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 40.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            _footerContent,
            style: Theme.of(context).textTheme.headline6.copyWith(
                  fontWeight: FontWeight.normal,
                  color: NeocomixThemeColors.secondaryLight,
                ),
          ),
          SizedBox(height: 20.0),
          Row(
            children: [
              ...IterableUtils.join<Widget>(
                [
                  buildLink(
                    label: '제휴 문의',
                    onPressed: () {
                      Navigator.of(context).pushNamed(
                        RouteName.customerService,
                        arguments:
                            CustomerServiceRouteArguments(initialTabIndex: 2),
                      );
                    },
                  ),
                  buildLink(
                    label: '이용 약관',
                    onPressed: () {
                      Navigator.of(context).pushNamed(RouteName.termsOfService);
                    },
                  ),
                  buildLink(
                    label: '개인정보처리방침',
                    onPressed: () {
                      Navigator.of(context).pushNamed(RouteName.privacyPolicy);
                    },
                  ),
                ],
                separator: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: Container(
                    width: 1.0,
                    height: 10.0,
                    color: NeocomixThemeColors.border,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
