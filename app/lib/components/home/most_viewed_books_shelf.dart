import 'package:duranno_app/components/book/book_simple_item.dart';
import 'package:duranno_app/components/home/const.dart';
import 'package:duranno_app/graphql/query/search.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

const double _widgetHeight = 200.0;

class MostViewedBooksShelf extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Query(
      options: QueryOptions(
        documentNode: SearchQuery.books,
        variables: {
          'orderBy': 'weeklyViews',
          'limit': numMostViewedShelfItems,
        },
      ),
      builder: (result, {fetchMore, refetch}) {
        if (result.loading) {
          return Container(
            height: _widgetHeight,
            child: Center(child: wrappedProgressIndicatorDark),
          );
        }

        if (result.hasException) {
          return Container(
            height: _widgetHeight,
            child: Center(child: errorWidget),
          );
        }

        final data = (result.data['searchBooks']['items'] as List<dynamic>);

        return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 32.0),
            child: Row(
              children: [
                ...IterableUtils.join<Widget>(
                  data.map(
                    (book) => BookSimpleItem(
                      data: book,
                      key: ValueKey<String>(book['id']),
                    ),
                  ),
                  separator: SizedBox(width: 12.0),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
