import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/route/route.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

import 'dot_pagination.dart';

class CarouselDot extends StatefulWidget {
  final List<dynamic> data;
  final Duration sliderTime;
  CarouselDot({
    Key key,
    this.sliderTime = const Duration(seconds: 4),
    @required this.data,
  }) : super(key: key);

  @override
  _CarouselDotState createState() => _CarouselDotState();
}

class _CarouselDotState extends State<CarouselDot> {
  ValueNotifier<int> _currentIndexNotifier;
  ValueNotifier<int> _nextIndexAnimationNotifier;
  ValueNotifier<double> _scrollValueNotifier;

  @override
  void initState() {
    _currentIndexNotifier = ValueNotifier<int>(0);
    _nextIndexAnimationNotifier = ValueNotifier<int>(0);
    _scrollValueNotifier = ValueNotifier<double>(1);
    super.initState();
  }

  @override
  void dispose() {
    _currentIndexNotifier.dispose();
    _nextIndexAnimationNotifier.dispose();
    _scrollValueNotifier.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.data.isEmpty) {
      return Container();
    }

    return LayoutBuilder(builder: (context, constraint) {
      return Container(
        child: Stack(
          children: <Widget>[
            CarouselSlider(
                items: widget.data.map(
                  (item) {
                    final String imageUri = item['bannerImageUri'];
                    final String linkRoute = item['linkRoute'];
                    final Map<String, dynamic> linkRouteArgs =
                        item['linkRouteArgs'] != null
                            ? jsonDecode(item['linkRouteArgs'])
                            : null;

                    final imageWidget = CachedNetworkImage(
                      imageUrl: imageUri,
                      imageBuilder: (context, imageProvider) => Container(
                        height: constraint.maxWidth * 28 / 36,
                        width: constraint.maxWidth,
                        decoration: BoxDecoration(
                          image: DecorationImage(
                            image: imageProvider,
                            fit: BoxFit.fill,
                          ),
                        ),
                      ),
                    );

                    if (linkRoute != null) {
                      if (!RouteName.values.contains(linkRoute)) {
                        final logger =
                            Provider.of<EnvironmentProvider>(context).logger;
                        logger.warning('invalid route link: got $linkRoute');
                        return imageWidget;
                      }

                      return GestureDetector(
                        child: imageWidget,
                        behavior: HitTestBehavior.translucent,
                        onTap: () {
                          Navigator.of(context).pushNamed(
                            linkRoute,
                            arguments: linkRouteArgs != null
                                ? parseRouteArgs(linkRouteArgs, linkRoute)
                                : null,
                          );
                        },
                      );
                    }

                    return imageWidget;
                  },
                ).toList(),
                options: CarouselOptions(
                  initialPage: 0,
                  autoPlay: true,
                  autoPlayInterval: widget.sliderTime,
                  height: constraint.maxWidth * 28 / 36,
                  viewportFraction: 1,
                  onScrolled: (value) {
                    double tempValue;
                    if (value <= 1) {
                      if (value < 0.5) {
                        tempValue = 1 - value;
                      } else {
                        tempValue = value;
                      }
                    } else {
                      value = value - value.toInt() / 1;
                      if (value < 0.5) {
                        tempValue = 1 - value;
                      } else {
                        tempValue = value;
                      }
                    }
                    _scrollValueNotifier.value = tempValue;
                    setState(() {});
                  },
                  enableInfiniteScroll: false,
                  onPageChanged: (index, reason) {
                    setState(() {
                      _nextIndexAnimationNotifier.value = index;
                    });
                  },
                )),
            Positioned(
              left: 20,
              bottom: 26,
              child: DotPagination(
                nextIndexAnimationNotifier: _nextIndexAnimationNotifier,
                indexLength: widget.data.length,
                slideValueNotifier: _scrollValueNotifier,
                currentIndexNotifier: _currentIndexNotifier,
              ),
            ),
          ],
        ),
      );
    });
  }
}
