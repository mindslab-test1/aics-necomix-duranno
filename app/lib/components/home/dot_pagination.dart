import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/cupertino.dart';

class DotPagination extends StatefulWidget {
  final int indexLength;
  final ValueNotifier<int> currentIndexNotifier;
  final ValueNotifier<int> nextIndexAnimationNotifier;
  final ValueNotifier<double> slideValueNotifier;

  DotPagination({
    @required this.indexLength,
    @required this.nextIndexAnimationNotifier,
    @required this.slideValueNotifier,
    @required this.currentIndexNotifier,
  });

  @override
  _DotPaginationState createState() => _DotPaginationState();
}

List<Widget> _createNavigationDot(int indexLength, int current,
    List<Animation<double>> animationList, double scrolledValue) {
  return List<Widget>.generate(indexLength, (index) {
    return Container(
      width: 6 + animationList[index].value * scrolledValue,
      height: 6,
      margin: EdgeInsets.only(right: 6),
      decoration: BoxDecoration(
          borderRadius: current == index ? BorderRadius.circular(4) : null,
          shape: current == index ? BoxShape.rectangle : BoxShape.circle,
          color: current == index
              ? NeocomixThemeColors.primaryLight
              : NeocomixThemeColors.disabled),
    );
  });
}

class _DotPaginationState extends State<DotPagination>
    with TickerProviderStateMixin {
  List<AnimationController> _animationControllerList;
  List<Animation<double>> _animationList;

  void animationChangeValue() {
    _animationControllerList[widget.nextIndexAnimationNotifier.value].forward();
    _animationControllerList[widget.currentIndexNotifier.value].reverse();
    widget.currentIndexNotifier.value = widget.nextIndexAnimationNotifier.value;
  }

  void setStateMethod() {
    setState(() {});
  }

  @override
  void initState() {
    widget.nextIndexAnimationNotifier.addListener(animationChangeValue);

    //generate Controller for each pagination indicatiors
    _animationControllerList = List.generate(
      widget.indexLength,
      (index) => AnimationController(
        vsync: this,
        duration: const Duration(milliseconds: 300),
        reverseDuration: const Duration(milliseconds: 300),
      ),
    );

    //generate Tween for each pagination indicatiors
    _animationList = List.generate(
        widget.indexLength,
        (index) => Tween<double>(begin: 0, end: 22)
            .animate(_animationControllerList[index]));

    //Start FIRST indicator animation
    _animationControllerList[widget.currentIndexNotifier.value].forward();

    //ADD Listner to each Tween Animation
    for (int i = 0; i < _animationList.length; i++) {
      _animationList[i].addListener(setStateMethod);
    }
    super.initState();
  }

  @override
  void dispose() {
    //REMOVE Listner to each Tween Animation
    for (int i = 0; i < _animationList.length; i++) {
      _animationList[i].removeListener(setStateMethod);
    }
    widget.nextIndexAnimationNotifier.removeListener(animationChangeValue);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: Listenable.merge(
            [widget.nextIndexAnimationNotifier, widget.slideValueNotifier]),
        builder: (BuildContext builder, _) {
          return Row(
            children: _createNavigationDot(
                widget.indexLength,
                widget.currentIndexNotifier.value,
                _animationList,
                widget.slideValueNotifier.value),
          );
        });
  }
}
