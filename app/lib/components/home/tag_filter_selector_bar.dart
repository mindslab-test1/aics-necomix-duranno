import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';

class TagFilterSelectorBar<T> extends StatelessWidget {
  final ValueNotifier<T> valueNotifier;
  final List<T> options;
  final Map<T, String> labels;
  TagFilterSelectorBar({
    Key key,
    @required this.valueNotifier,
    @required this.options,
    @required this.labels,
  })  : assert(valueNotifier != null),
        assert(options != null),
        assert(labels != null),
        super(key: key);

  @override
  Widget build(BuildContext context) => ValueListenableBuilder(
        valueListenable: valueNotifier,
        builder: (context, value, child) => Container(
          height: 40.0,
          color: NeocomixThemeColors.background,
          child: Center(
            child: SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  SizedBox(width: 20.0),
                  ...IterableUtils.join<Widget>(
                    options.map<Widget>(
                      (option) => GestureDetector(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 12),
                          child: Center(
                            child: TextLabel.p(
                              labels[option],
                              color: option == valueNotifier.value
                                  ? NeocomixThemeColors.primary
                                  : NeocomixThemeColors.secondaryLight,
                              fontWeight: option == valueNotifier.value
                                  ? FontWeight.bold
                                  : FontWeight.w500,
                            ),
                          ),
                        ),
                        onTap: () {
                          valueNotifier.value = option;
                        },
                        behavior: HitTestBehavior.translucent,
                      ),
                    ),
                    separator: SizedBox(width: 0.0),
                  ),
                  SizedBox(width: 20.0),
                ],
              ),
            ),
          ),
        ),
      );
}
