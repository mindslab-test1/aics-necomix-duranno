import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class PostItemController {
  final bool initialExpanded;
  PostItemController({
    this.initialExpanded = false,
  }) {
    _expanded = initialExpanded;
    _expandedValueNotifier = ValueNotifier<bool>(_expanded);
  }

  bool _expanded;
  ValueNotifier<bool> _expandedValueNotifier;

  bool get expanded => _expanded;
  ValueListenable<bool> get expandedListenable => _expandedValueNotifier;

  void toggle() {
    _expanded = !_expanded;
    _expandedValueNotifier.value = _expanded;
  }

  void dispose() {
    _expandedValueNotifier.dispose();
  }
}

class PostItem extends StatefulWidget {
  final Map<String, dynamic> data;
  final PostItemController controller;
  final DateTime _updatedAt;
  PostItem({
    Key key,
    @required this.data,
    this.controller,
  })  : assert(data != null),
        assert(data['id'] != null),
        assert(data['title'] != null),
        assert(data['updatedAt'] != null),
        _updatedAt = DateTime.parse(data['updatedAt']),
        super(key: key);

  /* yy.mm.dd */
  String get formattedUpdatedDate =>
      '${_updatedAt.year}.${NumUtils.zeroPad(_updatedAt.month, 2)}.${NumUtils.zeroPad(_updatedAt.day, 2)}';

  @override
  _PostItemState createState() => _PostItemState();
}

class _PostItemState extends State<PostItem> {
  PostItemController _controller;
  PostItemController get controller => widget.controller ?? _controller;

  @override
  void initState() {
    super.initState();

    if (widget.controller == null) {
      _controller = PostItemController();
    }
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return ValueListenableBuilder<bool>(
      valueListenable: controller.expandedListenable,
      builder: (context, expanded, child) => Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          GestureDetector(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: TextLabel.p(
                    widget.data['title'],
                    fontWeight: expanded ? FontWeight.bold : FontWeight.normal,
                    color: NeocomixThemeColors.secondaryDark,
                  ),
                ),
                SizedBox(width: 8.0),
                ThemeIcon(
                  expanded ? ThemeIconData.arrow_up : ThemeIconData.arrow_down,
                  size: 16.0,
                  color: NeocomixThemeColors.secondaryLight,
                ),
              ],
            ),
            onTap: controller.toggle,
          ),
          SizedBox(height: 4.0),
          TextLabel.h6(
            widget.formattedUpdatedDate,
            color: NeocomixThemeColors.secondaryLightest,
            fontWeight: FontWeight.normal,
          ),
          if (expanded) SizedBox(height: 8.0),
          if (expanded)
            Container(
              padding: const EdgeInsets.all(12.0),
              color: NeocomixThemeColors.background,
              child: Text(
                widget.data['content'] ?? '내용이 없습니다.',
                style: textTheme.subtitle1.copyWith(
                  color: NeocomixThemeColors.secondaryDark,
                ),
              ),
            ),
        ],
      ),
    );
  }
}
