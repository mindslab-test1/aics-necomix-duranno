import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

class FaqItem extends StatefulWidget {
  final String title;
  final String content;
  FaqItem({
    Key key,
    @required this.title,
    @required this.content,
  })  : assert(title != null),
        assert(content != null),
        super(key: key);

  @override
  _FaqItemState createState() => _FaqItemState();
}

class _FaqItemState extends State<FaqItem> {
  bool _expanded;

  _FaqItemState() {
    _expanded = false;
  }

  @override
  Widget build(BuildContext context) {
    final textStyle = Theme.of(context).textTheme.subtitle1.copyWith(
          color: NeocomixThemeColors.secondaryDark,
        );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        GestureDetector(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Text(
                  widget.title,
                  style: textStyle.copyWith(
                    fontWeight: _expanded ? FontWeight.bold : null,
                  ),
                ),
              ),
              SizedBox(width: 32.0),
              Padding(
                padding: const EdgeInsets.only(top: 2),
                child: ThemeIcon(
                  _expanded ? ThemeIconData.arrow_up : ThemeIconData.arrow_down,
                  size: 16.0,
                  color: NeocomixThemeColors.secondaryLight,
                ),
              ),
            ],
          ),
          onTap: () {
            setState(() {
              _expanded = !_expanded;
            });
          },
          behavior: HitTestBehavior.translucent,
        ),
        if (_expanded) SizedBox(height: 16.0),
        if (_expanded)
          Container(
            color: NeocomixThemeColors.background,
            padding:
                const EdgeInsets.symmetric(horizontal: 12.0, vertical: 16.0),
            child: Text(widget.content, style: textStyle),
          ),
      ],
    );
  }
}
