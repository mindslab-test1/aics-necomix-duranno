import 'package:duranno_app/components/common/button/flat_button.dart';
import 'package:duranno_app/components/common/input_field.dart';
import 'package:duranno_app/components/common/select/select.dart';
import 'package:duranno_app/components/common/snack_bar.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/customer_service/feedback_form_controller.dart';
import 'package:duranno_app/graphql/mutation/feedback.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

final _createFeedbackPendingSnackBar = ThemeSnackBar(
  content: Text('문의를 등록하는 중입니다...'),
);

final _createFeedbackSuccessSnackBar = ThemeSnackBar(
  content: Text('문의를 등록했습니다.'),
);

final _createFeedbackFailedSnackBar = ThemeSnackBar(
  content: Text('오류: 문의를 등록하는 데 실패했습니다.'),
);

class FeedbackForm extends StatefulWidget {
  final String title;
  final String typeLabel;
  final FeedbackFormController controller;
  FeedbackForm({
    Key key,
    @required this.title,
    @required this.typeLabel,
    @required this.controller,
  })  : assert(title != null),
        assert(typeLabel != null),
        assert(controller != null),
        super(key: key);

  @override
  _FeedbackFormState createState() => _FeedbackFormState();
}

class _FeedbackFormState extends State<FeedbackForm> {
  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    const padding = const EdgeInsets.symmetric(horizontal: 20.0);

    final titleWidget = Text(widget.title, style: textTheme.headline4);
    final subtitleWidget =
        Text('문의를 남겨주시면 담당자가 연락드립니다.', style: textTheme.subtitle1);

    final buildLabel = (String label) => SizedBox(
          width: 60.0,
          child: Align(
            alignment: Alignment.centerLeft,
            child: TextLabel.p(
              label,
              color: NeocomixThemeColors.secondaryLight,
              fontWeight: FontWeight.w500,
            ),
          ),
        );

    final buildInput = ({
      TextEditingController controller,
      String hintText,
      int maxLine = 1,
    }) =>
        InputField(
          controller: controller,
          hintText: hintText,
          maxLine: maxLine,
          errorMessage: null,
        );

    final buildRow = ({Widget label, Widget input}) => Padding(
          padding: padding,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(padding: const EdgeInsets.only(top: 16.0), child: label),
              SizedBox(width: 20.0),
              Expanded(child: input),
            ],
          ),
        );

    final submitButton = StreamBuilder<bool>(
      stream: widget.controller.onCanSubmitChanged,
      initialData: false,
      builder: (context, snapshot) {
        return Mutation(
          options: MutationOptions(
            documentNode: FeedbackMutation.createFeedback,
            update: (cache, result) {
              if (result.hasException) {
                final environment =
                    Provider.of<EnvironmentProvider>(context, listen: false);
                environment.logger.error(result.exception, null);
                Scaffold.of(context).hideCurrentSnackBar();
                Scaffold.of(context)
                    .showSnackBar(_createFeedbackFailedSnackBar);
              } else {
                Scaffold.of(context).hideCurrentSnackBar();
                Scaffold.of(context)
                    .showSnackBar(_createFeedbackSuccessSnackBar);
                widget.controller.clear();
              }
            },
          ),
          builder: (runMutation, result) {
            return ThemeFlatButton.primary(
              label: '등록하기',
              enabled: snapshot.data && !result.loading,
              onPressed: () async {
                Scaffold.of(context)
                    .showSnackBar(_createFeedbackPendingSnackBar);
                runMutation(widget.controller.form);
              },
            );
          },
        );
      },
    );

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        SizedBox(height: 32.0),
        Padding(
          padding: padding,
          child: titleWidget,
        ),
        SizedBox(height: 4.0),
        Padding(
          padding: padding,
          child: subtitleWidget,
        ),
        SizedBox(height: 36.0),
        ...IterableUtils.join<Widget>([
          buildRow(
            label: buildLabel(widget.typeLabel),
            input: Select(
              controller: widget.controller.selectController,
              hintText: '문의 유형을 선택해주세요.',
            ),
          ),
          buildRow(
            label: buildLabel('이름'),
            input: buildInput(
              controller: widget.controller.nameController,
              hintText: '홍길동',
            ),
          ),
          buildRow(
            label: buildLabel('이메일'),
            input: buildInput(
              controller: widget.controller.emailController,
              hintText: 'Neo@neocomix.com',
            ),
          ),
          buildRow(
            label: buildLabel('제목'),
            input: buildInput(
              controller: widget.controller.titleController,
              hintText: '제목을 입력해주세요.',
            ),
          ),
          buildRow(
            label: buildLabel('내용'),
            input: buildInput(
              controller: widget.controller.contentController,
              hintText: '문의 내용을 입력해주세요.',
              maxLine: 4,
            ),
          ),
        ], separator: SizedBox(height: 8.0)),
        SizedBox(height: 40.0),
        Padding(
          padding: padding,
          child: submitButton,
        ),
        SizedBox(height: 28.0),
      ],
    );
  }
}
