import 'dart:async';

import 'package:duranno_app/components/common/select/select_controller.dart';
import 'package:flutter/material.dart';

class FeedbackFormController {
  final String type;
  final List<String> categoryOptions;

  SelectController<String> _selectController;
  TextEditingController _nameController;
  TextEditingController _emailController;
  TextEditingController _titleController;
  TextEditingController _contentController;

  SelectController<String> get selectController => _selectController;
  TextEditingController get nameController => _nameController;
  TextEditingController get emailController => _emailController;
  TextEditingController get titleController => _titleController;
  TextEditingController get contentController => _contentController;

  final StreamController<bool> _canSubmitStreamController =
      StreamController<bool>.broadcast();

  bool get canSubmit =>
      _selectController.current != null &&
      _nameController.text.isNotEmpty &&
      _emailController.text.isNotEmpty &&
      _titleController.text.isNotEmpty &&
      _contentController.text.isNotEmpty;

  Map<String, dynamic> get form => {
        'type': type,
        'category': _selectController.current,
        'name': _nameController.text,
        'email': _emailController.text,
        'title': _titleController.text,
        'content': _contentController.text,
      };

  FeedbackFormController({
    @required this.type,
    @required this.categoryOptions,
  }) {
    _selectController = SelectController<String>(
      options: categoryOptions,
      optionsLabel: Map<String, String>.fromIterable(
        categoryOptions,
        key: (e) => e,
        value: (e) => e,
      ),
      initialOption: null,
    );
    _nameController = TextEditingController();
    _emailController = TextEditingController();
    _titleController = TextEditingController();
    _contentController = TextEditingController();

    _selectController.onCurrentChanged.listen((event) {
      _canSubmitStreamController.add(canSubmit);
    });
    _nameController.addListener(() {
      _canSubmitStreamController.add(canSubmit);
    });
    _emailController.addListener(() {
      _canSubmitStreamController.add(canSubmit);
    });
    _titleController.addListener(() {
      _canSubmitStreamController.add(canSubmit);
    });
    _contentController.addListener(() {
      _canSubmitStreamController.add(canSubmit);
    });
  }

  Stream<bool> get onCanSubmitChanged => _canSubmitStreamController.stream;

  void clear() {
    _selectController.clear();
    _nameController.text = '';
    _emailController.text = '';
    _titleController.text = '';
    _contentController.text = '';
  }

  void dispose() {
    _selectController.dispose();
    _nameController.dispose();
    _emailController.dispose();
    _titleController.dispose();
    _contentController.dispose();

    _canSubmitStreamController.close();
  }
}
