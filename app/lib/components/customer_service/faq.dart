import 'dart:convert';

import 'package:duranno_app/components/customer_service/faq_item.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widget.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

class Faq extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final environment = Provider.of<EnvironmentProvider>(context);

    return FutureBuilder(
      future: http
          .get('${environment.resourceUri}/FAQ.json')
          .then((response) => jsonDecode(response.body)),
      builder: (context, snapshot) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            SizedBox(height: 36.0),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Text(
                '자주 하는 질문',
                style: Theme.of(context).textTheme.headline4,
              ),
            ),
            SizedBox(height: 24.0),
            if (snapshot.hasError) errorWidget,
            if (!snapshot.hasData && !snapshot.hasError)
              wrappedProgressIndicatorDark,
            if (snapshot.hasData)
              ...IterableUtils.join<Widget>(
                snapshot.data['FAQ'].map<Widget>(
                  (raw) => Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 32.0),
                    child: FaqItem(
                      title: raw['title'],
                      content: raw['content'],
                    ),
                  ),
                ),
                separator: SizedBox(height: 24.0),
              ),
            SizedBox(height: 24.0),
          ],
        );
      },
    );
  }
}
