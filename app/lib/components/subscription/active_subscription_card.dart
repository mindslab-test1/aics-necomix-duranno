import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

const Color _fieldTextColor = const Color(0xFFF2DBDC);
const Color _fieldTextSecondaryColor = const Color(0xFFD18184);
const String _membershipActiveSvgPath = 'asset/image/membership-active.svg';

class ActiveSubscriptionCard extends StatelessWidget {
  final Map<String, dynamic> data;
  final DateTime _subscriptionUpdatedDate;
  ActiveSubscriptionCard({
    Key key,
    @required this.data,
  })  : assert(data != null),
        assert(data['id'] != null),
        assert(data['updatedAt'] != null),
        _subscriptionUpdatedDate = DateTime.parse(data['updatedAt']),
        super(key: key);

  String get _formattedSubscriptionUpdatedDate =>
      '${_subscriptionUpdatedDate.year}.${NumUtils.zeroPad(_subscriptionUpdatedDate.month, 2)}.${NumUtils.zeroPad(_subscriptionUpdatedDate.day, 2)}';

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerLeft,
      children: [
        LayoutBuilder(
          builder: (context, constraints) => SvgPicture.asset(
            _membershipActiveSvgPath,
            width: constraints.maxWidth,
          ),
        ),
        Positioned.fill(
          child: Container(
            padding: const EdgeInsets.fromLTRB(16.0, 14.0, 16.0, 18.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                TextLabel.h5('오디오북 이용권', color: NeocomixThemeColors.white),
                Spacer(),
                ...IterableUtils.join<Widget>(
                  [
                    _buildField(context,
                        field: '구독 기간',
                        content: '$_formattedSubscriptionUpdatedDate ~'),
                  ],
                  separator: SizedBox(height: 2.0),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildField(
    BuildContext context, {
    String field,
    String content,
  }) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        TextLabel.h6(
          field,
          color: _fieldTextColor,
          fontWeight: FontWeight.normal,
        ),
        SizedBox(width: 12.0),
        Container(width: 1.0, height: 10.0, color: _fieldTextSecondaryColor),
        SizedBox(width: 12.0),
        TextLabel.h6(
          content,
          color: _fieldTextColor,
          fontWeight: FontWeight.normal,
        ),
      ],
    );
  }
}
