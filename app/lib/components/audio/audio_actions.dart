import 'dart:math';

import 'package:duranno_app/provider/audio_provider.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/audio/audio_controller.dart';
import 'package:duranno_app/utils/book.dart';
import 'package:duranno_app/utils/constants.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

class AudioActions extends StatefulWidget {
  AudioActions({
    Key key,
  }) : super(key: key);

  @override
  _AudioActionsState createState() => _AudioActionsState();
}

class _AudioActionsState extends State<AudioActions> {
  @override
  Widget build(BuildContext context) {
    final pendingWidget = SizedBox(
      width: 60.0,
      height: 60.0,
      child: Center(
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(
            NeocomixThemeColors.white,
          ),
        ),
      ),
    );

    final playButton = (IAudioController controller) => Material(
          elevation: 8.0,
          color: Colors.transparent,
          shadowColor: const Color.fromRGBO(0, 0, 0, 0.2),
          child: GestureDetector(
            child: ThemeIcon(
              ThemeIconData.play_filled,
              size: 60.0,
              color: NeocomixThemeColors.white,
            ),
            behavior: HitTestBehavior.translucent,
            onTap: controller.play,
          ),
        );

    final pauseButton = (IAudioController controller) => Material(
          elevation: 8.0,
          color: Colors.transparent,
          shadowColor: const Color.fromRGBO(0, 0, 0, 0.2),
          child: GestureDetector(
            child: ThemeIcon(
              ThemeIconData.pause,
              size: 60.0,
              color: NeocomixThemeColors.white,
            ),
            behavior: HitTestBehavior.translucent,
            onTap: controller.pause,
          ),
        );

    final audio = Provider.of<AudioProvider>(context);

    return StreamBuilder<IAudioController>(
      stream: audio.onControllerChanged,
      builder: (context, snapshot) {
        final audioController = snapshot.data;

        final playToggleButton = audioController != null
            ? StreamBuilder<AudioControllerState>(
                stream: audioController.onStateChanged,
                initialData: audioController.state,
                builder: (context, state) {
                  switch (state.data) {
                    case AudioControllerState.Pending:
                      return pendingWidget;
                    case AudioControllerState.Paused:
                    case AudioControllerState.Completed:
                    case AudioControllerState.Stopped:
                      return playButton(audioController);
                    case AudioControllerState.Playing:
                      return pauseButton(audioController);
                    default:
                      return pendingWidget;
                  }
                },
              )
            : pendingWidget;

        final seekBackwardButton = GestureDetector(
          child: Material(
            elevation: 8.0,
            color: Colors.transparent,
            shadowColor: Color.fromRGBO(0, 0, 0, 0.2),
            child: ThemeIcon(
              ThemeIconData.seek_backward,
              size: 32.0,
              color: NeocomixThemeColors.border,
            ),
          ),
          behavior: HitTestBehavior.translucent,
          onTap: () async {
            if (audioController == null) {
              return;
            }

            final seekToMillis = max(
                0,
                (await audioController.position).inMilliseconds -
                    audioRewindDuration.inMilliseconds);

            await audioController.seek(Duration(milliseconds: seekToMillis));
          },
        );

        final seekForwardButton = GestureDetector(
          child: Material(
            elevation: 8.0,
            color: Colors.transparent,
            shadowColor: Color.fromRGBO(0, 0, 0, 0.2),
            child: ThemeIcon(
              ThemeIconData.seek_forward,
              size: 32.0,
              color: NeocomixThemeColors.border,
            ),
          ),
          behavior: HitTestBehavior.translucent,
          onTap: () async {
            if (audioController == null) {
              return;
            }

            final durationMillis =
                (await audioController.duration).inMilliseconds;
            final positionMillis =
                (await audioController.position).inMilliseconds;

            final seekToMillis = min(durationMillis,
                positionMillis + audioFastForwardDuration.inMilliseconds);

            await audioController.seek(Duration(milliseconds: seekToMillis));
          },
        );

        final seekToPreviousButton = StreamBuilder<int>(
          stream: audioController?.onSequenceChanged,
          builder: (context, snapshot) {
            final canSeekToPrevious =
                snapshot.data != null && snapshot.data > 0;
            return GestureDetector(
              child: Material(
                elevation: 8.0,
                color: Colors.transparent,
                shadowColor: Color.fromRGBO(0, 0, 0, 0.2),
                child: ThemeIcon(
                  ThemeIconData.skip_previous,
                  size: 28.0,
                  color: canSeekToPrevious
                      ? NeocomixThemeColors.border
                      : NeocomixThemeColors.secondaryLight,
                ),
              ),
              behavior: HitTestBehavior.translucent,
              onTap: canSeekToPrevious ? audioController?.seekToPrevious : null,
            );
          },
        );

        final seekToNextButton = StreamBuilder<bool>(
          initialData: false,
          stream: audioController != null
              ? Rx.combineLatest2<int, AudioQueue, bool>(
                  audioController.onSequenceChanged,
                  audioController.onAudioQueueChanged,
                  (sequence, audioQueue) =>
                      sequence < audioQueue.queue.length - 1,
                )
              : null,
          builder: (context, snapshot) {
            final canSeekToNext = snapshot.data ?? false;
            return GestureDetector(
              child: Material(
                elevation: 8.0,
                color: Colors.transparent,
                shadowColor: Color.fromRGBO(0, 0, 0, 0.2),
                child: ThemeIcon(
                  ThemeIconData.skip_next,
                  size: 28.0,
                  color: canSeekToNext
                      ? NeocomixThemeColors.border
                      : NeocomixThemeColors.secondaryLight,
                ),
              ),
              behavior: HitTestBehavior.translucent,
              onTap: canSeekToNext ? audioController?.seekToNext : null,
            );
          },
        );

        return Row(
          children: [
            seekToPreviousButton,
            Spacer(),
            seekBackwardButton,
            Spacer(),
            playToggleButton,
            Spacer(),
            seekForwardButton,
            Spacer(),
            seekToNextButton,
          ],
        );
      },
    );
  }
}
