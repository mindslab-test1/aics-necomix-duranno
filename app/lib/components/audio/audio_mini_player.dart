import 'package:audio_service/audio_service.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/components/book/blurred_background_image.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/provider/audio_provider.dart';
import 'package:duranno_app/route/audio/audio_player_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/audio/audio_controller.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';
import 'package:tuple/tuple.dart';

class AudioMiniPlayer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final audio = Provider.of<AudioProvider>(context);

    return StreamBuilder<Tuple2<IAudioController, MediaItem>>(
      stream: Rx.combineLatest2(
          audio.onControllerChanged,
          audio.onMediaItemChanged,
          (controller, mediaItem) => Tuple2(controller, mediaItem)),
      initialData: Tuple2(audio.controller, audio.mediaItem),
      builder: (context, snapshot) {
        final controller = snapshot.data?.item1;
        final mediaItem = snapshot.data?.item2;

        if (controller == null || mediaItem == null) {
          return Container();
        }

        final thumbnailErrorWidget = Container(
          width: 40.0,
          height: 60.0,
          color: NeocomixThemeColors.background,
          child: Center(
            child: Icon(
              Icons.error,
              size: 18.0,
              color: NeocomixThemeColors.secondaryDark,
            ),
          ),
        );

        final thumbnailWidget = mediaItem.artUri != null
            ? CachedNetworkImage(
                imageUrl: mediaItem.artUri,
                fit: BoxFit.fitHeight,
                errorWidget: (context, url, error) => thumbnailErrorWidget,
              )
            : thumbnailErrorWidget;

        final titleWidget = TextLabel.p(
          mediaItem.album,
          fontWeight: FontWeight.bold,
          color: NeocomixThemeColors.white,
          expanded: true,
        );

        final authorNameWidget = TextLabel.h6(
          mediaItem.artist ?? '',
          fontWeight: FontWeight.normal,
          color: NeocomixThemeColors.white,
          expanded: true,
        );

        final playButton = GestureDetector(
          child: Container(
            width: 40.0,
            child: Center(
              child: ThemeIcon(
                ThemeIconData.play_filled,
                size: 24.0,
                color: NeocomixThemeColors.white,
              ),
            ),
          ),
          behavior: HitTestBehavior.translucent,
          onTap: controller?.play,
        );

        final pauseButton = GestureDetector(
          child: Container(
            width: 40.0,
            child: Center(
              child: ThemeIcon(
                ThemeIconData.pause,
                size: 24.0,
                color: NeocomixThemeColors.white,
              ),
            ),
          ),
          behavior: HitTestBehavior.translucent,
          onTap: controller?.pause,
        );

        final pendingWidget = Container(
          width: 40.0,
          child: Center(
            child: SizedBox(
              width: 24.0,
              height: 24.0,
              child: CircularProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  NeocomixThemeColors.white,
                ),
                strokeWidth: 2.0,
              ),
            ),
          ),
        );

        final playToggleButton = StreamBuilder<AudioControllerState>(
          stream: controller.onStateChanged,
          initialData: controller.state,
          builder: (context, state) {
            switch (state.data) {
              case AudioControllerState.Pending:
                return pendingWidget;
              case AudioControllerState.Paused:
              case AudioControllerState.Completed:
              case AudioControllerState.Stopped:
                return playButton;
              case AudioControllerState.Playing:
                return pauseButton;
              default:
                return pendingWidget;
            }
          },
        );

        final stopButton = GestureDetector(
          child: Container(
            width: 40.0,
            child: Center(
              child: ThemeIcon(
                ThemeIconData.close,
                size: 24.0,
                color: NeocomixThemeColors.white,
              ),
            ),
          ),
          behavior: HitTestBehavior.translucent,
          onTap: AudioService.stop,
        );

        final content = Row(
          children: [
            thumbnailWidget,
            SizedBox(width: 12.0),
            Expanded(
              child: Column(
                children: [
                  Spacer(),
                  titleWidget,
                  authorNameWidget,
                  Spacer(),
                ],
              ),
            ),
            SizedBox(width: 8.0),
            playToggleButton,
            SizedBox(width: 8.0),
            stopButton,
            SizedBox(width: 12.0),
          ],
        );

        Widget widget = Container(
          height: 64.0,
          child: Stack(
            children: [
              BlurredBackgroundImage(imageUri: mediaItem.artUri),
              Positioned.fill(child: content),
            ],
          ),
        );

        widget = GestureDetector(
          child: widget,
          onTap: () {
            final args = AudioPlayerRouteArguments(
              bookId: audio.audioQueue.bookId,
              sequence:
                  audio.audioQueue.getSequenceOfMediaItem(audio.mediaItem),
            );
            Navigator.of(context)
                .pushNamed(RouteName.audioPlayer, arguments: args);
          },
        );

        return widget;
      },
    );
  }
}
