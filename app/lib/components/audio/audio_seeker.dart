import 'dart:ui';

import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/provider/audio_provider.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/audio/audio_controller.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rxdart/rxdart.dart';

const double _progressBarKnobSize = 12.0;

class AudioSeeker extends StatefulWidget {
  AudioSeeker({
    Key key,
  }) : super(key: key);

  @override
  _AudioSeekerState createState() => _AudioSeekerState();
}

class _AudioSeekerState extends State<AudioSeeker> {
  @override
  Widget build(BuildContext context) {
    final rail = Container(
      height: 4.0,
      decoration: BoxDecoration(
        color: NeocomixThemeColors.disabled,
        borderRadius: BorderRadius.circular(1.0),
      ),
    );

    final audio = Provider.of<AudioProvider>(context);

    return StreamBuilder<IAudioController>(
      stream: audio.onControllerChanged,
      builder: (context, snapshot) {
        final audioController = snapshot.data;

        Stream<double> ratioStream = audioController != null
            ? Rx.combineLatest2<Duration, Duration, double>(
                audioController.onDurationChanged,
                audioController.onPositionChanged,
                (duration, position) =>
                    duration.inMilliseconds > 0 && position.inMilliseconds > 0
                        ? position.inMilliseconds / duration.inMilliseconds
                        : 0.0,
              ).asBroadcastStream()
            : null;

        Widget progressBarWidget = audioController != null
            ? Material(
                color: Colors.transparent,
                shadowColor: const Color.fromRGBO(0, 0, 0, 0.2),
                elevation: 8.0,
                child: SizedBox(
                  height: 12.0,
                  child: LayoutBuilder(
                    builder: (context, constraints) {
                      final track = StreamBuilder<double>(
                        stream: ratioStream,
                        builder: (context, snapshot) {
                          final ratio = snapshot.data ?? 0.0;

                          return Container(
                            height: 4.0,
                            width: constraints.maxWidth * ratio,
                            decoration: BoxDecoration(
                              color: NeocomixThemeColors.white,
                              borderRadius: BorderRadius.circular(1.0),
                            ),
                          );
                        },
                      );

                      final knob = StreamBuilder<double>(
                        stream: ratioStream,
                        builder: (context, snapshot) {
                          final ratio = snapshot.data ?? 0.0;
                          return Positioned(
                            top: 0,
                            width: _progressBarKnobSize,
                            height: _progressBarKnobSize,
                            left: lerpDouble(
                              0.0,
                              constraints.maxWidth - _progressBarKnobSize,
                              ratio,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                color: NeocomixThemeColors.white,
                                borderRadius: BorderRadius.circular(
                                    _progressBarKnobSize * 0.5),
                              ),
                            ),
                          );
                        },
                      );

                      return Stack(
                        children: [
                          Align(
                            alignment: Alignment.centerLeft,
                            child: rail,
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: track,
                          ),
                          knob,
                        ],
                      );
                    },
                  ),
                ),
              )
            : SizedBox(
                height: _progressBarKnobSize,
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: rail,
                ),
              );

        progressBarWidget = GestureDetector(
          child: Padding(
              padding: const EdgeInsets.only(top: 12, bottom: 0),
              child: progressBarWidget),
          behavior: HitTestBehavior.translucent,
          onTapUp: (TapUpDetails details) async {
            if (audioController == null) {
              return;
            }

            final RenderBox renderBox = context.findRenderObject();
            final ratio = details.localPosition.dx / renderBox.size.width;
            final seekToMillis =
                ((await audioController.duration).inMilliseconds * ratio)
                    .floor();

            await audioController.seek(Duration(milliseconds: seekToMillis));
          },
        );

        final positionWidget = audioController != null
            ? StreamBuilder<Duration>(
                stream: audioController.onPositionChanged,
                initialData: null,
                builder: (context, snapshot) {
                  final position = snapshot.data?.inSeconds;

                  return TextLabel.h6(
                    position != null
                        ? NumUtils.formatTimeInSeconds(position, round: false)
                        : '',
                    color: NeocomixThemeColors.white,
                    fontWeight: FontWeight.w500,
                  );
                },
              )
            : null;

        final durationWidget = audioController != null
            ? StreamBuilder<Duration>(
                stream: audioController.onDurationChanged,
                builder: (context, snapshot) {
                  final duration = snapshot.data?.inSeconds;

                  return TextLabel.h6(
                    duration != null
                        ? NumUtils.formatTimeInSeconds(duration)
                        : '',
                    color: NeocomixThemeColors.secondaryLightest,
                    fontWeight: FontWeight.w500,
                  );
                },
              )
            : null;

        return Column(
          children: [
            progressBarWidget,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                if (audioController != null) ...[
                  positionWidget,
                  durationWidget,
                ] else
                  SizedBox(height: 18.0),
              ],
            ),
          ],
        );
      },
    );
  }
}
