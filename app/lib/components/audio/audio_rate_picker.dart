import 'package:duranno_app/provider/audio_provider.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/audio/audio_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

const _playbackRates = [1.0, 1.25, 1.5, 1.75, 2.0];
const _iconAssetPaths = [
  'asset/image/ico-playback_rate-1x.svg',
  'asset/image/ico-playback_rate-1.25x.svg',
  'asset/image/ico-playback_rate-1.5x.svg',
  'asset/image/ico-playback_rate-1.75x.svg',
  'asset/image/ico-playback_rate-2x.svg',
];

class AudioRatePicker extends StatefulWidget {
  AudioRatePicker({
    Key key,
  }) : super(key: key);

  @override
  _AudioRatePickerState createState() => _AudioRatePickerState();
}

class _AudioRatePickerState extends State<AudioRatePicker> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    final audio = Provider.of<AudioProvider>(context);

    return StreamBuilder<IAudioController>(
      stream: audio.onControllerChanged,
      builder: (context, snapshot) {
        final audioController = snapshot.data;

        return GestureDetector(
          child: SvgPicture.asset(
            _iconAssetPaths[_currentIndex],
            color: NeocomixThemeColors.border,
            width: 52.0,
            height: 28.0,
          ),
          behavior: HitTestBehavior.translucent,
          onTap: () {
            if (audioController == null) {
              return;
            }

            final nextIndex = (_currentIndex + 1) % _playbackRates.length;
            audioController.setPlaybackRate(_playbackRates[nextIndex]);

            setState(() {
              _currentIndex = nextIndex;
            });
          },
        );
      },
    );
  }
}
