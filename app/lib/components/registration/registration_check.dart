import 'dart:async';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';

class RegistrationCheck {
  final AuthenticationProvider authenticationProvider;
  final TextEditingController name;
  final TextEditingController emailName;
  final TextEditingController password;
  final TextEditingController passwordConfirm;

  final StreamController<String> _currentStreamController =
      StreamController<String>();

  RegistrationCheck(
      {@required this.authenticationProvider,
      this.name,
      this.emailName,
      this.password,
      this.passwordConfirm})
      : assert(authenticationProvider != null);

  void dispose() {
    _currentStreamController.close();
  }

  Stream<String> get emailTextChanged => _currentStreamController.stream;

  String checkNameError() {
    if (name.text.isEmpty) {
      return '모든 칸을 입력해주세요.';
    }
    return null;
  }

  String checkEmailError() {
    if (emailName.text.isEmpty) {
      return '모든 칸을 입력해주세요.';
    }
    if (!EmailValidator.validate(emailName.text)) {
      return '올바른 이메일 형식을 입력해주세요.';
    }
    return null;
  }

  String checkPasswordError() {
    if (password.text.isEmpty) {
      return '모든 칸을 입력해주세요.';
    }
    if (passwordConfirm.text.isEmpty) {
      return null;
    }
    if (password.text != passwordConfirm.text) {
      return null;
    }
    if (password.text.length <= 5) {
      return '비밀번호는 6자리 이상 입력해주세요.';
    }
    return null;
  }

  String checkConfirmPasswordError() {
    if (passwordConfirm.text.isEmpty) {
      return '모든 칸을 입력해주세요.';
    }
    if (password.text.isEmpty) {
      return null;
    }
    if (password.text != passwordConfirm.text) {
      return '비밀번호가 일치하지 않습니다.';
    }
    if (password.text.length <= 5) {
      return null;
    }
    return null;
  }

  Future<bool> register() async {
    if (checkConfirmPasswordError() == null &&
        checkPasswordError() == null &&
        checkNameError() == null &&
        checkEmailError() == null) {
      try {
        await authenticationProvider.registerLocalUser(
            email: emailName.text, password: password.text);
      } on DurannoException catch (e) {
        if (e.code == ExceptionCode.AuthDuplicateEmail) {
          _currentStreamController.add('이미 가입된 이메일입니다.');
          return false;
        }
      }
      return true;
    }
    return false;
  }
}
