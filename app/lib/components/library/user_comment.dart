import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/route/book_detail_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

final _emptyStarWidget = SvgPicture.asset(
  'asset/image/ico-star-small-blank.svg',
  width: 16.0,
  height: 16.0,
);

final _filledStartWidget = SvgPicture.asset(
  'asset/image/ico-star-small-filled.svg',
  width: 16.0,
  height: 16.0,
);

class UserComment extends StatelessWidget {
  final Map<String, dynamic> data;
  final bool useRouteLink;
  UserComment({
    Key key,
    @required this.data,
    this.useRouteLink = true,
  })  : assert(data != null),
        assert(data['id'] != null),
        assert(data['book'] != null),
        assert(data['book']['id'] != null),
        assert(data['book']['title'] != null),
        assert(useRouteLink != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    final String bookId = data['book']['id'];
    final String bookTitle = data['book']['title'];
    final String bookThumbnailUri = data['book']['thumbnailUri'];
    final String content = data['content'];
    final int rating = data['rating'];

    final thumbnailErrorWidget = Icon(
      Icons.error,
      size: 18.0,
      color: NeocomixThemeColors.secondaryDarkest,
    );

    final bookThumbnailWidget = bookThumbnailUri != null
        ? Material(
            elevation: 4.0,
            child: CachedNetworkImage(
              imageUrl: bookThumbnailUri,
              imageBuilder: (context, imageProvider) => Container(
                child: Image(
                  image: imageProvider,
                  fit: BoxFit.cover,
                  width: 70.0,
                  height: 104.0,
                ),
              ),
              errorWidget: (context, url, error) => thumbnailErrorWidget,
            ),
          )
        : Container(
            width: 70.0,
            height: 104.0,
            color: NeocomixThemeColors.background,
            child: Center(child: thumbnailErrorWidget),
          );

    final bookTitleWidget = TextLabel.p(
      bookTitle,
      fontWeight: FontWeight.bold,
      expanded: true,
    );

    final contentWidget = content != null
        ? SizedBox(
            height: 40.0,
            child: Text(
              content,
              style: textTheme.subtitle1,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          )
        : null;

    final userComment = Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        bookThumbnailWidget,
        SizedBox(width: 12.0),
        Expanded(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              if (rating != null) _buildRatingWidget(context, rating: rating),
              SizedBox(height: 8.0),
              bookTitleWidget,
              SizedBox(height: 8.0),
              if (contentWidget != null) contentWidget,
            ],
          ),
        ),
      ],
    );

    if (useRouteLink) {
      return GestureDetector(
        child: userComment,
        behavior: HitTestBehavior.translucent,
        onTap: () {
          Navigator.of(context).pushNamed(
            RouteName.bookDetail,
            arguments: BookDetailRouteArguments(bookId: bookId),
          );
        },
      );
    } else {
      return userComment;
    }
  }

  Widget _buildRatingWidget(
    BuildContext context, {
    int rating,
    int maxRating = 5,
  }) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: [
        ...IterableUtils.join<Widget>(
          Iterable<int>.generate(maxRating, (i) => i + 1).map((index) =>
              index <= rating ? _filledStartWidget : _emptyStarWidget),
          separator: SizedBox(width: 2.0),
        ),
      ],
    );
  }
}
