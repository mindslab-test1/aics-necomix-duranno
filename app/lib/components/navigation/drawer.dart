import 'dart:async';

import 'package:duranno_app/components/common/button/inline_button.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/graphql/query/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/route/login_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:duranno_app/utils/widgets.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

final _divider = Padding(
  padding: const EdgeInsets.symmetric(horizontal: 16.0),
  child: Container(
    width: 1.0,
    height: 10.0,
    color: NeocomixThemeColors.border,
  ),
);

enum _UserType { Anonymous, Unsubscribed, Subscribed }

class NavigationDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authentication = Provider.of<AuthenticationProvider>(context);

    final userStateWidget = authentication.isEmailVerified
        ? Query(
            options: QueryOptions(documentNode: UserQuery.me),
            builder: (result, {fetchMore, refetch}) {
              if (result.loading) {
                return Center(child: wrappedProgressIndicatorDark);
              }

              if (result.hasException) {
                return Center(child: errorWidget);
              }

              final String email = result.data['me']['email'];
              final bool subscribed = result.data['me']['subscribed'];

              return _buildUserInfo(
                context,
                userType:
                    subscribed ? _UserType.Subscribed : _UserType.Unsubscribed,
                userEmail: email,
              );
            },
          )
        : _buildUserInfo(
            context,
            userType: _UserType.Anonymous,
            userEmail: null,
          );

    final customerServiceRouteLink = GestureDetector(
      child: TextLabel.p(
        '고객센터',
        color: NeocomixThemeColors.secondaryLight,
        fontWeight: FontWeight.w500,
      ),
      onTap: () {
        Navigator.of(context).pushNamed(RouteName.customerService);
      },
      behavior: HitTestBehavior.translucent,
    );

    final settingsRouteLink = GestureDetector(
      child: TextLabel.p(
        '환경설정',
        color: NeocomixThemeColors.secondaryLight,
        fontWeight: FontWeight.w500,
      ),
      onTap: () {
        Navigator.of(context).pushNamed(RouteName.settings);
      },
      behavior: HitTestBehavior.translucent,
    );

    return SizedBox(
      width: 240.0,
      child: Drawer(
        elevation: 12.0,
        child: SafeArea(
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 204.0,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: userStateWidget,
                ),
              ),
              ...IterableUtils.join<Widget>(
                [
                  _buildMainServiceLink(
                    context,
                    icon: ThemeIconData.home,
                    label: '홈',
                    onPressed: () {
                      Navigator.of(context)
                          .pushReplacementNamed(RouteName.home);
                    },
                    useArrow: false,
                  ),
                  _buildMainServiceLink(
                    context,
                    icon: ThemeIconData.bookmark,
                    label: '나의 오디오북',
                    onPressed: () {
                      Navigator.of(context).pushNamed(RouteName.library);
                    },
                  ),
                  _buildMainServiceLink(
                    context,
                    icon: ThemeIconData.user,
                    label: '내 정보',
                    onPressed: () {
                      Navigator.of(context).pushNamed(RouteName.myProfile);
                    },
                  ),
                  _buildMainServiceLink(
                    context,
                    icon: ThemeIconData.membership,
                    label: '멤버십 소개',
                    onPressed: () {
                      Navigator.of(context).pushNamed(RouteName.membershipHome);
                    },
                  ),
                ],
                separator: SizedBox(height: 24.0),
              ),
              Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  customerServiceRouteLink,
                  _divider,
                  settingsRouteLink,
                ],
              ),
              SizedBox(height: 32.0),
            ],
          ),
        ),
      ),
    );
  }

  static const _userTypeLabels = {
    _UserType.Anonymous: '게스트',
    _UserType.Unsubscribed: '무료회원',
    _UserType.Subscribed: '유료회원',
  };

  static const _userTypeDescriptions = {
    _UserType.Anonymous: '로그인 후 이용 가능합니다.',
    _UserType.Unsubscribed: '멤버십 구독 후 이용 가능합니다.',
    _UserType.Subscribed: '멤버십 구독 중 입니다.',
  };

  Widget _buildUserInfo(
    BuildContext context, {
    _UserType userType,
    String userEmail,
  }) {
    final textTheme = Theme.of(context).textTheme;
    final textStyle = textTheme.subtitle1.copyWith(
      fontWeight: FontWeight.normal,
      color: NeocomixThemeColors.secondaryLight,
    );

    final loginLinkButton = InlineButton(
      text: '로그인',
      color: NeocomixThemeColors.primaryLight,
      height: 26.0,
      onPressed: () {
        Navigator.of(context).pushNamed(
          RouteName.login,
          arguments: LoginRouteArguments(shouldPopAfterLogin: true),
        );
      },
    );

    final membershipHomeLinkButton = InlineButton(
      text: '멤버십 구독하기',
      color: NeocomixThemeColors.primaryLight,
      contentPadding: const EdgeInsets.symmetric(horizontal: 12.0),
      height: 28.0,
      icon: ThemeIconData.membership,
      onPressed: () {
        Navigator.of(context).pushNamed(RouteName.membershipHome);
      },
    );

    final subscribedUserLabel = userEmail != null
        ? Container(
            decoration: BoxDecoration(
              color: NeocomixThemeColors.background,
              borderRadius: BorderRadius.circular(2.0),
            ),
            padding: const EdgeInsets.symmetric(horizontal: 12.0),
            height: 26.0,
            child: TextLabel.h6(
              userEmail,
              color: NeocomixThemeColors.secondaryLight,
              fontWeight: FontWeight.w500,
            ),
          )
        : null;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Spacer(),
        RichText(
          text: TextSpan(
            children: [
              TextSpan(
                text: '현재 ',
                style: textStyle,
              ),
              TextSpan(
                text: _userTypeLabels[userType],
                style: textStyle.copyWith(fontWeight: FontWeight.bold),
              ),
              TextSpan(
                text: ' 입니다.',
                style: textStyle,
              ),
            ],
            style: textTheme.headline6,
          ),
        ),
        SizedBox(height: 4.0),
        Text(
          _userTypeDescriptions[userType],
          style: textStyle,
        ),
        SizedBox(height: 16.0),
        Align(
          alignment: Alignment.centerLeft,
          child: () {
            switch (userType) {
              case _UserType.Anonymous:
                return loginLinkButton;
              case _UserType.Unsubscribed:
                return membershipHomeLinkButton;
              case _UserType.Subscribed:
                return subscribedUserLabel;
            }
          }(),
        ),
        Spacer(),
      ],
    );
  }

  Widget _buildMainServiceLink(
    BuildContext context, {
    ThemeIconData icon,
    String label,
    FutureOr<void> Function() onPressed,
    bool useArrow = true,
  }) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: GestureDetector(
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ThemeIcon(icon, size: 20.0),
            SizedBox(width: 12.0),
            TextLabel.span(label),
            Spacer(),
            if (useArrow)
              ThemeIcon(
                ThemeIconData.arrow_right,
                color: NeocomixThemeColors.secondaryLightest,
                size: 24.0,
              ),
          ],
        ),
        onTap: onPressed,
        behavior: HitTestBehavior.translucent,
      ),
    );
  }
}
