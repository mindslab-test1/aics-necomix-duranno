import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/theme/neocomix/logo.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

class AppHeader extends StatefulWidget implements PreferredSizeWidget {
  final String title;
  final ThemeIconData leadingIcon;
  final bool useLeadingButton;
  final Widget leadingButtonWidget;
  final Widget actionButtonWidget;
  final bool opaquedWhenScrolled;
  final ValueListenable<double> scrollExtentListener;
  AppHeader({
    Key key,
    @required this.title,
    this.leadingIcon,
    this.useLeadingButton = true,
    this.leadingButtonWidget,
    this.actionButtonWidget,
    this.opaquedWhenScrolled = false,
    this.scrollExtentListener,
  })  : assert(useLeadingButton != null),
        assert(opaquedWhenScrolled != null),
        assert(!opaquedWhenScrolled || scrollExtentListener != null),
        super(key: key);

  Size get preferredSize => Size.fromHeight(56.0);

  @override
  _AppHeaderState createState() => _AppHeaderState();
}

class _AppHeaderState extends State<AppHeader> {
  Color _getBackgroundColor(double scrollExtent) {
    if (widget.opaquedWhenScrolled) {
      return Color.lerp(
        Colors.transparent,
        NeocomixThemeColors.white,
        scrollExtent / widget.preferredSize.height,
      );
    }

    return Colors.transparent;
  }

  Color _getPrimaryColor(double scrollExtent, {bool transparentOnTop = false}) {
    final idlePrimaryColor = widget.opaquedWhenScrolled && transparentOnTop
        ? Colors.transparent
        : Theme.of(context).brightness == Brightness.dark
            ? NeocomixThemeColors.white
            : NeocomixThemeColors.black;

    if (widget.opaquedWhenScrolled) {
      return Color.lerp(
        idlePrimaryColor,
        NeocomixThemeColors.black,
        scrollExtent / widget.preferredSize.height,
      );
    }

    return idlePrimaryColor;
  }

  @override
  Widget build(BuildContext context) {
    if (widget.opaquedWhenScrolled) {
      return ValueListenableBuilder<double>(
        valueListenable: widget.scrollExtentListener,
        builder: (context, value, child) =>
            _buildAppBar(context, scrollExtent: value),
      );
    }

    return _buildAppBar(context);
  }

  Widget _buildAppBar(BuildContext context, {double scrollExtent}) {
    return AppBar(
      automaticallyImplyLeading: false,
      backgroundColor: _getBackgroundColor(scrollExtent),
      shadowColor: Colors.transparent,
      brightness: Brightness.light,
      titleSpacing: 8.0,
      title: Row(
        children: [
          _buildLeadingButton(context, scrollExtent: scrollExtent),
          Expanded(child: _buildTitle(context, scrollExtent: scrollExtent)),
          _buildActionButton(context, scrollExtent: scrollExtent),
        ],
      ),
    );
  }

  Widget _buildLeadingButton(BuildContext context, {double scrollExtent}) {
    final isBottomMostRoute = ModalRoute.of(context).isFirst;

    Widget leadingButton;

    leadingButton = isBottomMostRoute
        ? ThemeIconButton(
            icon: widget.leadingIcon ?? ThemeIconData.menu,
            iconColor: _getPrimaryColor(scrollExtent),
            onPressed: () => Scaffold.of(context).openDrawer(),
          )
        : ThemeIconButton(
            icon: widget.leadingIcon ?? ThemeIconData.back,
            iconColor: _getPrimaryColor(scrollExtent),
            onPressed: () => Navigator.of(context).pop(),
          );

    leadingButton = widget.useLeadingButton
        ? leadingButton
        : SizedBox(
            width: 48.0,
            height: 48.0,
          );

    leadingButton = widget.leadingButtonWidget ?? leadingButton;

    return leadingButton;
  }

  Widget _buildTitle(BuildContext context, {double scrollExtent}) {
    final isHomeRoute = ModalRoute.of(context).settings.name == RouteName.home;

    return isHomeRoute
        ? GestureDetector(
            child: NeocomixLogo(width: 100.0),
            behavior: HitTestBehavior.translucent,
            onTap: () {
              Navigator.of(context).pushReplacementNamed(RouteName.home);
            },
          )
        : widget.title != null
            ? Center(
                child: TextLabel.h5(
                  widget.title,
                  color: _getPrimaryColor(scrollExtent, transparentOnTop: true),
                  expanded: true,
                  textAlign: TextAlign.center,
                ),
              )
            : Container();
  }

  Widget _buildActionButton(BuildContext context, {double scrollExtent}) {
    final isHomeRoute = ModalRoute.of(context).settings.name == RouteName.home;
    return widget.actionButtonWidget ??
        (isHomeRoute
            ? ThemeIconButton(
                icon: ThemeIconData.search,
                iconColor: _getPrimaryColor(scrollExtent),
                iconSize: 20.0,
                buttonSize: 48.0,
                onPressed: () {
                  Navigator.of(context).pushNamed(RouteName.search);
                },
              )
            : SizedBox(width: 48.0, height: 48.0));
  }
}
