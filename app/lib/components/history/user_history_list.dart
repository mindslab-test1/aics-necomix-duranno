import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/components/history/history_card.dart';
import 'package:duranno_app/graphql/query/user.dart';
import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/pagination/paginated_sliver_list.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

const double _widgetHeight = 80.0;
const double _listPadding = 32.0;

class UserHistoryList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final authentication = Provider.of<AuthenticationProvider>(context);

    final usePaginatedList = !authentication.isAnonymous;

    final emptyWidget = LayoutBuilder(builder: (context, constraints) {
      final width = (constraints.maxWidth.isInfinite
              ? MediaQuery.of(context).size.width
              : constraints.maxWidth) -
          (usePaginatedList ? 2 * _listPadding : 0.0);

      return Container(
        width: width,
        height: _widgetHeight,
        child: Center(
          child: TextLabel.p(
            '최근 재생 기록이 없습니다.',
            color: NeocomixThemeColors.secondaryLight,
          ),
        ),
      );
    });

    if (!usePaginatedList) {
      return emptyWidget;
    }

    return Container(
      height: _widgetHeight,
      child: PaginatedSliverList<dynamic, dynamic, String>(
        queryOptions: QueryOptions(
          documentNode: MyHistoryQuery.instance.documentNode,
        ),
        itemsSelector: MyHistoryQuery.instance.itemsSelector,
        cursorSelector: MyHistoryQuery.instance.cursorSelector,
        mergeFetchMoreResult: MyHistoryQuery.instance.mergeFetchMoreResult,
        itemBuilder: ({itemData, key, refetch}) =>
            HistoryCard(data: itemData, key: key),
        itemKeySelector: (itemData) => itemData['id'],
        scrollDirection: Axis.horizontal,
        marginTop: _listPadding,
        contentMarginBottom: _listPadding,
        marginBetweenItems: 8.0,
        emptyLabelWidget: emptyWidget,
      ),
    );
  }
}
