import 'package:cached_network_image/cached_network_image.dart';
import 'package:duranno_app/components/common/text/text_label.dart';
import 'package:duranno_app/route/audio/audio_player_route.dart';
import 'package:duranno_app/route/route.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';

class HistoryCard extends StatelessWidget {
  final Map<String, dynamic> data;
  final bool useRouteLink;
  HistoryCard({
    Key key,
    @required this.data,
    this.useRouteLink = true,
  })  : assert(data != null),
        assert(data['book'] != null),
        assert(data['book']['id'] != null),
        assert(data['book']['title'] != null),
        assert(useRouteLink != null),
        super(key: key);

  int get _sequence => data['sequence'];
  String get _bookId => data['book']['id'];
  String get _bookTitle => data['book']['title'];
  String get _bookThumbnailUri => data['book']['thumbnailUri'];
  bool get _hasAuthor => data['book']['author'] != null;
  String get _authorName => _hasAuthor ? data['book']['author']['name'] : null;

  @override
  Widget build(BuildContext context) {
    final thumbnailWidget = _bookThumbnailUri != null
        ? Material(
            elevation: 1.0,
            child: SizedBox(
              height: 60.0,
              child: CachedNetworkImage(
                imageUrl: _bookThumbnailUri,
                fit: BoxFit.fitHeight,
                progressIndicatorBuilder: (context, url, progress) => SizedBox(
                  width: 40.0,
                  child: Center(
                    child: SizedBox(
                      width: 16.0,
                      height: 16.0,
                      child: CircularProgressIndicator(
                        valueColor: AlwaysStoppedAnimation<Color>(
                            NeocomixThemeColors.border),
                        strokeWidth: 2.0,
                      ),
                    ),
                  ),
                ),
              ),
            ),
          )
        : Container(
            width: 40.0,
            height: 60.0,
            color: NeocomixThemeColors.border,
          );

    final titleWidget = TextLabel.p(
      _bookTitle,
      fontWeight: FontWeight.w500,
      expanded: true,
    );

    final authorNameWidget = TextLabel.h6(
      _authorName,
      fontWeight: FontWeight.normal,
      color: NeocomixThemeColors.secondaryLight,
      expanded: true,
    );

    final bookCard = Container(
      decoration: BoxDecoration(
        color: NeocomixThemeColors.white,
        border: Border.all(
          color: NeocomixThemeColors.border,
          width: 1.0,
        ),
        borderRadius: BorderRadius.circular(4.0),
      ),
      padding: const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          thumbnailWidget,
          SizedBox(width: 12.0),
          SizedBox(
            width: 111.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 2.0),
                titleWidget,
                SizedBox(height: 4.0),
                authorNameWidget,
              ],
            ),
          ),
          SizedBox(width: 8.0),
          ThemeIcon(
            ThemeIconData.play,
            color: NeocomixThemeColors.primaryLight,
            size: 32.0,
          ),
        ],
      ),
    );

    if (useRouteLink) {
      return GestureDetector(
        child: bookCard,
        behavior: HitTestBehavior.translucent,
        onTap: () {
          final routeArgs = AudioPlayerRouteArguments(
            bookId: _bookId,
            sequence: _sequence,
          );

          Navigator.of(context)
              .pushNamed(RouteName.audioPlayer, arguments: routeArgs);
        },
      );
    }

    return bookCard;
  }
}
