import 'package:duranno_app/provider/authentication_provider.dart';
import 'package:duranno_app/provider/environment_provider.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:duranno_app/utils/logger.dart';
import 'package:flutter/material.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';

class GraphQLApp extends StatefulWidget {
  final Widget child;
  GraphQLApp({Key key, @required this.child})
      : assert(child != null),
        super(key: key);

  @override
  _GraphQLAppState createState() => _GraphQLAppState();
}

class _GraphQLAppState extends State<GraphQLApp> {
  AuthenticationProvider _authentication;
  Logger _logger;
  String _userId;

  NormalizedInMemoryCache _cache;
  HttpLink _httpLink;
  ValueNotifier<GraphQLClient> _gqlClient = ValueNotifier(null);

  @override
  void initState() {
    super.initState();

    _cache =
        NormalizedInMemoryCache(dataIdFromObject: typenameDataIdFromObject);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();

    if (_authentication != null) {
      _authentication.removeListener(_onAuthStateChange);
    }
    _authentication =
        Provider.of<AuthenticationProvider>(context, listen: false);
    try {
      _userId = _authentication.user.uid;
    } on DurannoException catch (e) {
      if (e.code == ExceptionCode.AuthUnauthorized ||
          e.code == ExceptionCode.AuthEmailNotVerified) {
        _userId = null;
      } else {
        throw e;
      }
    }
    _authentication.addListener(_onAuthStateChange);

    final environment =
        Provider.of<EnvironmentProvider>(context, listen: false);

    _logger = environment.logger;

    _httpLink = HttpLink(uri: environment.apiUri + '/graphql', headers: {
      if (environment.appStage == AppStage.Development) 'x-debug-user-id': '0'
    });
    _reset(
      hasAuth: _authentication.isSigned,
      emailVerified: _authentication.isEmailVerified,
    );
  }

  @override
  void dispose() {
    _authentication.removeListener(_onAuthStateChange);

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GraphQLProvider(
      client: _gqlClient,
      child: widget.child,
    );
  }

  void _onAuthStateChange() {
    try {
      final user = _authentication.user;
      if (user.uid != _userId) {
        _reset(hasAuth: true, emailVerified: true);
        _userId = user.uid;
      }
    } on DurannoException catch (e) {
      if (e.code == ExceptionCode.AuthUnauthorized) {
        _reset(hasAuth: false, emailVerified: false);
        _userId = null;
      } else if (e.code == ExceptionCode.AuthEmailNotVerified) {
        _reset(hasAuth: true, emailVerified: false);
        _userId = null;
      } else {
        throw e;
      }
    }
  }

  void _reset({
    @required bool hasAuth,
    @required bool emailVerified,
  }) {
    _cache.reset();

    if (hasAuth) {
      _gqlClient.value = GraphQLClient(
        cache: _cache,
        link: AuthLink(
          getToken: () async => 'Bearer ${await _authentication.idToken}',
        ).concat(_httpLink),
      );

      _logger.debug('Auth Token updated.');
    } else {
      _gqlClient.value = GraphQLClient(
        cache: _cache,
        link: _httpLink,
      );

      _logger.debug('Auth Token cleared.');
    }
  }
}
