import 'package:duranno_app/model/model.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:meta/meta.dart';

class BookModel extends Model {
  @override
  String get typename => 'Book';

  BookModel({
    @required String id,
    Cache cache,
  }) : super(id: id, cache: cache);

  set _isInMyPlaylist(bool value) {
    data = data
      ..addAll({
        'isInMyPlaylist': value,
      });
  }

  void addToPlaylist() {
    if (data == null) {
      return;
    }

    _isInMyPlaylist = true;
  }

  void removeFromPlaylist() {
    if (data == null) {
      return;
    }

    _isInMyPlaylist = false;
  }
}
