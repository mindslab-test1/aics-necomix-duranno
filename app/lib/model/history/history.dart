import 'package:duranno_app/model/history/user_history.dart';
import 'package:duranno_app/model/model.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:meta/meta.dart';

class HistoryModel extends Model {
  @override
  String get typename => 'History';

  HistoryModel({
    @required String id,
    Cache cache,
  }) : super(id: id, cache: cache);

  List<UserHistoryModel> get _items => List<dynamic>.from(data['list']['items'])
      .map((item) => UserHistoryModel(id: item['id'])..from(item))
      .toList();

  set _items(Iterable<UserHistoryModel> value) {
    data = data
      ..addAll({
        'list': Map<String, dynamic>.from(data['list'])
          ..addAll({
            'items': value.map((el) => el.data).toList(),
          }),
      });
  }

  void update(UserHistoryModel item) {
    assert(item != null);
    if (data == null) {
      return;
    }

    _items = _items
      ..remove(item)
      ..insertAll(0, [item]);
  }
}
