import 'package:duranno_app/model/model.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:meta/meta.dart';

class UserHistoryModel extends Model {
  @override
  String get typename => 'UserHistory';

  UserHistoryModel({
    @required String id,
    Cache cache,
  }) : super(id: id, cache: cache);
}
