import 'package:equatable/equatable.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:meta/meta.dart';

class _MutableData {
  dynamic origin;
  dynamic mutated;

  Map<String, dynamic> get copy =>
      hasData ? Map<String, dynamic>.from(mutated) : null;

  bool get hasData => mutated != null;

  void init(dynamic value) {
    origin = value;
    mutated = value;
  }

  void commit() {
    origin = mutated;
  }
}

abstract class Model extends Equatable {
  String get typename;

  final String id;
  final Cache cache;
  final _MutableData _data;
  Model({
    @required this.id,
    this.cache,
  })  : assert(id != null),
        _data = _MutableData() {
    if (cache != null) {
      _data.init(cache.read(cacheKey));
    }
  }

  @override
  List<Object> get props => [typename, id];

  @protected
  String get cacheKey => '$typename/$id';

  @protected
  Map<String, dynamic> get data => _data.copy;

  @protected
  set data(Map<String, dynamic> value) {
    _data.mutated = value;
  }

  void from(dynamic data) {
    assert(data != null);

    _data.mutated = data;
  }

  void commit() {
    if (cache != null && _data.hasData) {
      cache.write(cacheKey, _data.mutated);
      _data.commit();
    }
  }

  @override
  String toString() => cacheKey;
}
