import 'package:duranno_app/model/model.dart';
import 'package:duranno_app/model/playlist/playlist_item.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:meta/meta.dart';

class PlaylistModel extends Model {
  @override
  String get typename => 'Playlist';

  PlaylistModel({
    @required String id,
    Cache cache,
  }) : super(id: id, cache: cache);

  int get _count => data['count'];

  set _count(int value) {
    data = data..addAll({'count': value});
  }

  String get _cursor => data['list']['cursor'];

  set _cursor(String value) {
    data = data
      ..addAll({
        'list': Map<String, dynamic>.from(data['list'])
          ..addAll({
            'cursor': value,
          }),
      });
  }

  List<PlaylistItemModel> get _items =>
      List<dynamic>.from(data['list']['items'])
          .map((item) => PlaylistItemModel(id: item['id'])..from(item))
          .toList();

  set _items(Iterable<PlaylistItemModel> value) {
    data = data
      ..addAll({
        'list': Map<String, dynamic>.from(data['list'])
          ..addAll({
            'items': value.map((el) => el.data).toList(),
          })
      });
  }

  void add(PlaylistItemModel item) {
    assert(item != null);
    if (data == null) {
      return;
    }

    if (_items.any((_item) => _item.id == item.id)) {
      return;
    }

    _count = _count + 1;
    _items = _items..insertAll(0, [item]);
  }

  void delete(String itemId) {
    assert(itemId != null);
    if (data == null) {
      return;
    }

    final item = _items.firstWhere((item) => item.id == itemId);
    final itemIndex = _items.indexOf(item);

    if (_cursor == itemId) {
      _cursor = itemIndex > 0 ? _items[itemIndex - 1].id : null;
    }

    _count = _count - 1;
    _items = _items..removeWhere((item) => item.id == itemId);
  }
}
