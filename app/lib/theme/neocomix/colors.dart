import 'package:flutter/material.dart';

class NeocomixThemeColors {
  static const primary = const Color(0xFFA82025);
  static const primaryLight = const Color(0xFFBE4A4F);

  static const secondaryDarkest = const Color(0xFF000000);
  static const secondaryDark = const Color(0xFF333333);
  static const secondaryLight = const Color(0xFF808080);
  static const secondaryLightest = const Color(0xFFCCCCCC);

  static const border = const Color(0xFFE0E0E0);
  static const background = const Color(0xFFF5F5F5);
  static const disabled = const Color(0xFFCCCCCC);
  static const error = const Color(0xFFBE4A4F);

  static const black = const Color(0xFF000000);
  static const white = const Color(0xFFFFFFFF);
}
