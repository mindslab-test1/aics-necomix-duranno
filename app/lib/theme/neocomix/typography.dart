import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final _commonTextStyle = GoogleFonts.notoSans();
final typography = TextTheme(
  headline1: _commonTextStyle.copyWith(
    fontWeight: FontWeight.bold,
    color: NeocomixThemeColors.secondaryDarkest,
    fontSize: 28.0,
    height: 40.0 / 28.0,
  ),
  headline2: _commonTextStyle.copyWith(
    fontWeight: FontWeight.bold,
    color: NeocomixThemeColors.secondaryDarkest,
    fontSize: 24.0,
    height: 36.0 / 24.0,
  ),
  headline3: _commonTextStyle.copyWith(
    fontWeight: FontWeight.bold,
    color: NeocomixThemeColors.secondaryDarkest,
    fontSize: 20.0,
    height: 30.0 / 20.0,
  ),
  headline4: _commonTextStyle.copyWith(
    fontWeight: FontWeight.bold,
    color: NeocomixThemeColors.secondaryDark,
    fontSize: 18.0,
    height: 27.0 / 18.0,
  ),
  headline5: _commonTextStyle.copyWith(
    fontWeight: FontWeight.bold,
    color: NeocomixThemeColors.secondaryDark,
    fontSize: 16.0,
    height: 24.0 / 16.0,
  ),
  headline6: _commonTextStyle.copyWith(
    fontWeight: FontWeight.bold,
    color: NeocomixThemeColors.secondaryDark,
    fontSize: 12.0,
    height: 18.0 / 12.0,
  ),
  bodyText1: _commonTextStyle.copyWith(
    fontWeight: FontWeight.normal,
    color: NeocomixThemeColors.secondaryDarkest,
    fontSize: 16.0,
    height: 24.0 / 16.0,
  ),
  bodyText2: _commonTextStyle.copyWith(
    fontWeight: FontWeight.normal,
    color: NeocomixThemeColors.secondaryDarkest,
    fontSize: 10.0,
    height: 15.0 / 10.0,
  ),
  button: _commonTextStyle.copyWith(
    fontWeight: FontWeight.bold,
    color: NeocomixThemeColors.white,
    fontSize: 14.0,
    height: 20.0 / 14.0,
  ),
  subtitle1: _commonTextStyle.copyWith(
    fontWeight: FontWeight.normal,
    color: NeocomixThemeColors.secondaryDarkest,
    fontSize: 14.0,
    height: 20.0 / 14.0,
  ),
);
