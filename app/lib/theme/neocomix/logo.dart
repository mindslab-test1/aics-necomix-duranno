import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class NeocomixLogo extends StatelessWidget {
  final double width;
  final double height;
  NeocomixLogo({
    this.width,
    this.height,
  });

  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      'asset/image/logo-neocomix.svg',
      semanticsLabel: 'logo-neocomix',
      width: width,
      height: height,
    );
  }
}
