import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

enum ThemeIconData {
  home,
  search,
  back,
  menu,
  user,
  like,
  share,
  comment,
  bell,
  megaphone,
  edit,
  delete,
  bookmark,
  book,
  openbook,
  headphone,
  soundwaves,
  camera,
  image,
  checked,
  refresh,
  tick,
  add,
  settings,
  phone,
  mail,
  more,
  more_vert,
  play,
  arrow_right,
  check,
  uncheck,
  arrow_up,
  arrow_down,
  playlist,
  profile,
  play_filled,
  pause,
  seek_backward,
  seek_forward,
  membership,
  close,
  skip_previous,
  skip_next,
}

class ThemeIcon extends StatelessWidget {
  final ThemeIconData icon;
  final double size;
  final Color color;
  ThemeIcon(
    this.icon, {
    Key key,
    this.size = 24.0,
    this.color = const Color(0xFF000000),
  })  : assert(icon != null),
        super(key: key);

  @override
  Widget build(BuildContext context) => SvgPicture.asset(
        _iconAssetPath,
        semanticsLabel: _iconDataStr,
        width: size,
        height: size,
        color: color,
      );

  static const String _iconAssetDirectory = 'asset/icon/';

  String get _iconDataStr => icon.toString().split('.').last;
  String get _iconAssetPath => _iconAssetDirectory + 'ico-$_iconDataStr.svg';
}

class ThemeIconButton extends StatelessWidget {
  final ThemeIconData icon;
  final IconData materialIcon;

  /// [iconSize] is the size of the icon inside ThemeIconButton.
  /// The actual size of [ThemeIconButton] will be [2 * iconSize], if [buttonSize] is not given.
  final double iconSize;

  final double buttonSize;
  final bool enabled;
  final Color buttonColor;
  final Color splashColor;
  final Color iconColor;
  final void Function() onPressed;
  ThemeIconButton({
    Key key,
    this.icon,
    this.materialIcon,
    this.iconSize = 24.0,
    this.buttonSize,
    this.enabled = true,
    this.buttonColor = Colors.transparent,
    this.splashColor = const Color.fromRGBO(0, 0, 0, 0.5),
    this.iconColor = const Color(0xFF000000),
    @required this.onPressed,
  })  : assert(icon != null || materialIcon != null),
        assert(enabled != null),
        assert(onPressed != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final iconWidget = icon != null
        ? ThemeIcon(
            icon,
            size: iconSize,
            color: iconColor,
          )
        : Icon(
            materialIcon,
            size: iconSize,
            color: iconColor,
          );

    if (!enabled) {
      return SizedBox(
        width: buttonSize ?? 2 * iconSize,
        height: buttonSize ?? 2 * iconSize,
        child: Center(
          child: iconWidget,
        ),
      );
    }

    return ClipOval(
      child: Material(
        color: buttonColor,
        child: SizedBox(
          width: buttonSize ?? 2 * iconSize,
          height: buttonSize ?? 2 * iconSize,
          child: InkWell(
            splashColor: splashColor,
            child: Center(
              child: iconWidget,
            ),
            onTap: onPressed,
          ),
        ),
      ),
    );
  }
}
