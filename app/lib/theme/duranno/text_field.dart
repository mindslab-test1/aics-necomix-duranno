import 'package:duranno_app/theme/duranno/colors.dart';
import 'package:duranno_app/theme/duranno/typography.dart';
import 'package:flutter/material.dart';

final inputDecorationTheme = InputDecorationTheme(
  labelStyle: typography.headline4,
  hintStyle: typography.headline4.copyWith(
    color: DurannoThemeColors.secondaryLight,
  ),
  floatingLabelBehavior: FloatingLabelBehavior.always,
  contentPadding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 19.2),
  border: OutlineInputBorder(
    borderSide: BorderSide(
      color: DurannoThemeColors.secondaryLight,
      width: 1.0,
    ),
    borderRadius: BorderRadius.circular(8.0),
  ),
  enabledBorder: OutlineInputBorder(
    borderSide: BorderSide(
      color: DurannoThemeColors.secondaryLight,
      width: 1.0,
    ),
    borderRadius: BorderRadius.circular(8.0),
  ),
  focusedBorder: OutlineInputBorder(
    borderSide: BorderSide(
      color: DurannoThemeColors.primaryGreen,
      width: 1.0,
    ),
    borderRadius: BorderRadius.circular(8.0),
  ),
  isCollapsed: true,
  isDense: false,
);

class DurannoThemeTextField extends StatefulWidget {
  final String label;
  final String hint;
  final TextEditingController controller;
  final FocusNode focusNode;
  final TextInputType keyboardType;
  final bool obscureText;
  final TextInputAction textInputAction;
  final TextCapitalization textCapitalization;
  final bool readOnly;
  final bool autofocus;
  final Color backgroundColor;
  final void Function(String) onChanged;
  final void Function() onEditingComplete;
  final void Function() onTap;
  final void Function(String) onSubmitted;
  DurannoThemeTextField({
    Key key,
    @required this.label,
    this.hint,
    this.controller,
    this.focusNode,
    this.keyboardType,
    this.obscureText = false,
    this.textInputAction,
    this.textCapitalization = TextCapitalization.none,
    this.readOnly = false,
    this.autofocus = false,
    this.backgroundColor,
    this.onChanged,
    this.onEditingComplete,
    this.onTap,
    this.onSubmitted,
  })  : assert(label != null),
        super(key: key);

  @override
  _DurannoThemeTextFieldState createState() => _DurannoThemeTextFieldState();
}

class _DurannoThemeTextFieldState extends State<DurannoThemeTextField> {
  FocusNode _focusNode;
  FocusNode get focusNode => widget.focusNode ?? _focusNode;

  @override
  void initState() {
    super.initState();

    _focusNode = FocusNode();
    _focusNode.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _focusNode.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final baseTextStyle = theme.textTheme.headline4;

    return Stack(
      children: <Widget>[
        Container(
          padding: EdgeInsets.only(
            top: 0.5 * baseTextStyle.fontSize * baseTextStyle.height,
          ),
          child: TextField(
            controller: widget.controller,
            focusNode: focusNode,
            decoration: InputDecoration(
              hintText: widget.hint,
              floatingLabelBehavior:
                  theme.inputDecorationTheme.floatingLabelBehavior,
              filled: true,
              fillColor:
                  widget.backgroundColor ?? theme.scaffoldBackgroundColor,
            ),
            keyboardType: widget.keyboardType,
            textInputAction: widget.textInputAction,
            textCapitalization: widget.textCapitalization,
            style: baseTextStyle.copyWith(fontWeight: FontWeight.bold),
            textAlign: TextAlign.start,
            textAlignVertical: TextAlignVertical.center,
            readOnly: widget.readOnly,
            showCursor: true,
            obscureText: widget.obscureText,
            autofocus: widget.autofocus,
            maxLines: 1,
            expands: false,
            onChanged: widget.onChanged,
            onEditingComplete: widget.onEditingComplete,
            onTap: widget.onTap,
            onSubmitted: widget.onSubmitted,
          ),
        ),
        Positioned(
          left: 16.0,
          child: Container(
            decoration: BoxDecoration(
              color: widget.backgroundColor ?? theme.scaffoldBackgroundColor,
            ),
            padding: const EdgeInsets.symmetric(horizontal: 5.0),
            child: Text(
              widget.label,
              style: baseTextStyle.copyWith(
                color: focusNode.hasFocus
                    ? DurannoThemeColors.primaryGreen
                    : DurannoThemeColors.secondaryLight,
              ),
            ),
          ),
        )
      ],
    );
  }
}
