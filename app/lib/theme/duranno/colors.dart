import 'package:flutter/material.dart';

class DurannoThemeColors {
  static const primaryTeal = const Color(0xFF006782);
  static const primaryGreen = const Color(0xFFA1BD00);
  static const primary = primaryTeal;

  static const secondaryDarkest = const Color(0xFF2B2B2B);
  static const secondaryDark = const Color(0xFF7C8C8D);
  static const secondaryLight = const Color(0xFFBCC3C7);
  static const secondaryLightest = const Color(0xFFEBF0F1);
  static const secondary = secondaryDarkest;

  static const black = const Color(0xFF000000);
  static const white = const Color(0xFFFFFFFF);
  static const google = const Color(0xFFFFFFFF);
  static const googleButtonText = const Color(0xFF6E6E6E);
  static const googleButtonBorder = const Color(0xFFD3D3D3);
  static const kakao = const Color(0xFFFAE300);
  static const naver = const Color(0xFF06BE34);
  static const facebook = const Color(0xFF1877F2);
  static const apple = const Color(0xFF000000);
  static const appleButtonText = const Color(0xFFFFFFFF);
}
