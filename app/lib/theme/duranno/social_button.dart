import 'dart:async';

import 'package:duranno_app/theme/duranno/colors.dart';
import 'package:duranno_app/theme/duranno/typography.dart';
import 'package:duranno_app/theme/neocomix/colors.dart';
import 'package:duranno_app/utils/error.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

enum DurannoSocialIconData {
  google,
  facebook,
  kakao,
  naver,
  apple,
}

class DurannoSocialIcon {
  static Widget google = _create(DurannoSocialIconData.google);
  static Widget facebook = _create(DurannoSocialIconData.facebook);
  static Widget kakao = _create(DurannoSocialIconData.kakao);
  static Widget naver = _create(DurannoSocialIconData.naver);
  static Widget apple = _create(DurannoSocialIconData.apple);

  static Widget _create(DurannoSocialIconData icon) => SvgPicture.asset(
        _getIconAssetPath(icon),
        semanticsLabel: _getIconDataStr(icon),
        width: _iconSize,
        height: _iconSize,
      );

  static const String _iconAssetDirectory = 'asset/icon/';
  static const double _iconSize = 20.0;

  static String _getIconDataStr(DurannoSocialIconData icon) =>
      icon.toString().split('.').last;
  static String _getIconAssetPath(DurannoSocialIconData icon) =>
      _iconAssetDirectory + 'ico-${_getIconDataStr(icon)}.svg';
}

enum DurannoSocialIconButtonType {
  google,
  facebook,
  kakao,
  naver,
}

class DurannoSocialIconButton extends StatelessWidget {
  final DurannoSocialIconButtonType type;
  final FutureOr<void> Function() onPressed;
  DurannoSocialIconButton({
    @required this.type,
    @required this.onPressed,
  }) : assert(type != null);

  factory DurannoSocialIconButton.google(
          {@required FutureOr<void> Function() onPressed}) =>
      DurannoSocialIconButton(
        type: DurannoSocialIconButtonType.google,
        onPressed: onPressed,
      );

  factory DurannoSocialIconButton.facebook(
          {@required FutureOr<void> Function() onPressed}) =>
      DurannoSocialIconButton(
        type: DurannoSocialIconButtonType.facebook,
        onPressed: onPressed,
      );

  factory DurannoSocialIconButton.kakao(
          {@required FutureOr<void> Function() onPressed}) =>
      DurannoSocialIconButton(
        type: DurannoSocialIconButtonType.kakao,
        onPressed: onPressed,
      );

  factory DurannoSocialIconButton.naver(
          {@required FutureOr<void> Function() onPressed}) =>
      DurannoSocialIconButton(
        type: DurannoSocialIconButtonType.naver,
        onPressed: onPressed,
      );

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case DurannoSocialIconButtonType.google:
        {
          return GestureDetector(
            child: googleButton,
            onTap: onPressed,
          );
        }
      case DurannoSocialIconButtonType.facebook:
        {
          return GestureDetector(
            child: facebookButton,
            onTap: onPressed,
          );
        }
      case DurannoSocialIconButtonType.kakao:
        {
          return GestureDetector(
            child: kakaoButton,
            onTap: onPressed,
          );
        }
      case DurannoSocialIconButtonType.naver:
        {
          return GestureDetector(
            child: naverButton,
            onTap: onPressed,
          );
        }
      default:
        throw DurannoError(
          code: ErrorCode.Logic,
        );
    }
  }

  static Widget googleButton = _createButton(
    DurannoSocialIcon.google,
    DurannoThemeColors.google,
    useBorder: true,
  );
  static Widget facebookButton =
      _createButton(DurannoSocialIcon.facebook, DurannoThemeColors.facebook);
  static Widget kakaoButton =
      _createButton(DurannoSocialIcon.kakao, DurannoThemeColors.kakao);
  static Widget naverButton =
      _createButton(DurannoSocialIcon.naver, DurannoThemeColors.naver);

  static Widget _createButton(Widget iconWidget, Color backgroundColor,
          {bool useBorder = false}) =>
      Container(
        width: _buttonSize,
        height: _buttonSize,
        decoration: BoxDecoration(
          color: backgroundColor,
          border: useBorder
              ? Border.all(
                  color: DurannoThemeColors.secondaryLightest,
                  width: 1.0,
                )
              : null,
          borderRadius: BorderRadius.circular(0.5 * _buttonSize),
        ),
        child: Center(child: iconWidget),
      );

  static const double _buttonSize = 56.0;
}

enum DurannoSocialLoginButtonType {
  google,
  apple,
}

class DurannoSocialLoginButton extends StatelessWidget {
  final DurannoSocialLoginButtonType type;
  final FutureOr<void> Function() onPressed;
  DurannoSocialLoginButton({
    @required this.type,
    @required this.onPressed,
  }) : assert(type != null);

  /// Creates a google login button.
  ///
  /// Typical usage will be:
  /// ```
  /// DurannoSocialLoginButton.google(
  ///   onPressed: Provider.of<AuthenticationProvider>(context).signInWithGoogle,
  /// )
  /// ```
  factory DurannoSocialLoginButton.google(
          {@required FutureOr<void> Function() onPressed}) =>
      DurannoSocialLoginButton(
        type: DurannoSocialLoginButtonType.google,
        onPressed: onPressed,
      );

  /// Creates an apple login button.
  ///
  /// Typical usage will be:
  /// ```
  /// DurannoSocialLoginButton.apple(
  ///   onPressed: Provider.of<AuthenticationProvider>(context).signInWithApple,
  /// )
  /// ```
  factory DurannoSocialLoginButton.apple(
          {@required FutureOr<void> Function() onPressed}) =>
      DurannoSocialLoginButton(
        type: DurannoSocialLoginButtonType.apple,
        onPressed: onPressed,
      );

  @override
  Widget build(BuildContext context) {
    switch (type) {
      case DurannoSocialLoginButtonType.google:
        {
          return GestureDetector(
            child: googleButton,
            onTap: onPressed,
            behavior: HitTestBehavior.translucent,
          );
        }
      case DurannoSocialLoginButtonType.apple:
        {
          return GestureDetector(
            child: appleButton,
            onTap: onPressed,
            behavior: HitTestBehavior.translucent,
          );
        }
      default:
        throw DurannoError(
          code: ErrorCode.Logic,
        );
    }
  }

  static Widget googleButton = Container(
    decoration: BoxDecoration(
      color: NeocomixThemeColors.white,
      border: Border.all(
        color: NeocomixThemeColors.border,
        width: 1.0,
      ),
      borderRadius: BorderRadius.circular(2.0),
    ),
    padding: const EdgeInsets.only(left: 16, right: 20, top: 10, bottom: 10),
    child: Row(
      children: <Widget>[
        SizedBox(width: 20.0, height: 20.0, child: DurannoSocialIcon.google),
        Expanded(
          child: Center(
            child: Text(
              'Google 로그인',
              style: typography.button.copyWith(
                color: NeocomixThemeColors.secondaryLight,
              ),
            ),
          ),
        ),
      ],
    ),
  );

  static Widget appleButton = Container(
    decoration: BoxDecoration(
      color: DurannoThemeColors.apple,
      borderRadius: BorderRadius.circular(2.0),
    ),
    padding: const EdgeInsets.only(left: 16, right: 20, top: 10, bottom: 10),
    child: Row(children: <Widget>[
      SizedBox(width: 20.0, height: 20.0, child: DurannoSocialIcon.apple),
      Expanded(
        child: Center(
          child: Text(
            'Apple 로그인',
            style: typography.button.copyWith(
              color: NeocomixThemeColors.white,
            ),
          ),
        ),
      ),
    ]),
  );
}
