import 'package:duranno_app/theme/duranno/colors.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

final _commonTextStyle = GoogleFonts.notoSans();

final typography = TextTheme(
  headline1: _commonTextStyle.copyWith(
    fontWeight: FontWeight.bold,
    color: DurannoThemeColors.secondary,
    fontSize: 48.0,
    height: 1.2,
    letterSpacing: -2.16,
  ),
  headline2: _commonTextStyle.copyWith(
    fontWeight: FontWeight.bold,
    color: DurannoThemeColors.secondary,
    fontSize: 36.0,
    height: 1.2,
    letterSpacing: -1.62,
  ),
  headline3: _commonTextStyle.copyWith(
    fontWeight: FontWeight.normal,
    color: DurannoThemeColors.secondary,
    fontSize: 24.0,
    height: 1.5,
    letterSpacing: -1.08,
  ),
  headline4: _commonTextStyle.copyWith(
    fontWeight: FontWeight.normal,
    color: DurannoThemeColors.secondary,
    fontSize: 16.0,
    height: 1.6,
  ),
  headline5: _commonTextStyle.copyWith(
    fontWeight: FontWeight.normal,
    color: DurannoThemeColors.secondary,
    fontSize: 16.0,
    height: 1.6,
  ),
  headline6: _commonTextStyle.copyWith(
    fontWeight: FontWeight.normal,
    color: DurannoThemeColors.secondary,
    fontSize: 16.0,
    height: 1.6,
  ),
  bodyText1: _commonTextStyle.copyWith(
    fontWeight: FontWeight.normal,
    color: DurannoThemeColors.secondary,
    fontSize: 16.0,
    height: 1.6,
    letterSpacing: -0.4,
  ),
  bodyText2: _commonTextStyle.copyWith(
    fontWeight: FontWeight.normal,
    color: DurannoThemeColors.secondary,
    fontSize: 16.0,
    height: 1.6,
    letterSpacing: -0.4,
  ),
  button: _commonTextStyle.copyWith(
    fontWeight: FontWeight.w500,
    fontSize: 12.0,
    height: 18.0 / 12.0,
  ),
);
