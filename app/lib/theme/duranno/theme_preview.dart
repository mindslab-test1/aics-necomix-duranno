import 'package:duranno_app/theme/duranno/button.dart';
import 'package:duranno_app/theme/duranno/colors.dart';
import 'package:duranno_app/theme/icons.dart';
import 'package:duranno_app/theme/duranno/social_button.dart';
import 'package:duranno_app/theme/duranno/text_field.dart';
import 'package:duranno_app/utils/misc.dart';
import 'package:flutter/material.dart';

class DurannoThemePreview extends StatelessWidget {
  DurannoThemePreview({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ...IterableUtils.join<Widget>(
            [
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ...IterableUtils.join<Widget>(
                    [
                      Text(
                        '타이포그래피',
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      Text(
                        '오디오북\nDURANNO',
                        style: Theme.of(context).textTheme.headline1,
                      ),
                      Text(
                        '오디오북\nDURANNO',
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      Text(
                        '오디오북\nDURANNO',
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      Text(
                        '오디오북\nDURANNO',
                        style: Theme.of(context).textTheme.headline4,
                      ),
                      Flexible(
                        child: Text(
                          '기쁘며, 몸이 그들의 고동을 그들은 풍부하게 것이다. 천고에 거선의 것이다. 심장은 타오르고 속에 얼마나 대한 있다. 품었기 보라, 가진 않는 있을 할지라도 군영과 말이다. 얼마나 피어나기 우리는 철환하였는가 봄바람이다.',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                      Flexible(
                        child: Text(
                          'Lorem ipsum dolor sit amet, nisl vidisse fabulas ad usu, at usu suas noster impetus, an vim illum falli. Eu putent regione usu, consul soleat commodo mea at. His iudico pertinacia te.',
                          style: Theme.of(context).textTheme.bodyText1,
                        ),
                      ),
                    ],
                    separator: _itemSeparator,
                  ),
                ],
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ...IterableUtils.join<Widget>(
                    [
                      Text(
                        '아이콘',
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      ...ThemeIconData.values.map((icon) => Wrap(
                            children: <Widget>[
                              ThemeIcon(icon),
                              ThemeIcon(icon,
                                  color: DurannoThemeColors.primary),
                              ThemeIcon(icon,
                                  color: DurannoThemeColors.primaryGreen),
                              ThemeIcon(icon,
                                  color: DurannoThemeColors.secondary),
                              ThemeIcon(icon,
                                  color: DurannoThemeColors.secondaryLight),
                              ThemeIcon(icon, size: 36.0),
                              ThemeIcon(icon, size: 48.0),
                            ],
                            spacing: 8.0,
                            crossAxisAlignment: WrapCrossAlignment.end,
                          )),
                    ],
                    separator: _itemSeparator,
                  ),
                ],
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ...IterableUtils.join<Widget>(
                    [
                      Text(
                        '버튼',
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      Wrap(
                        spacing: 16.0,
                        children: <Widget>[
                          DurannoSocialIconButton.kakao(onPressed: () {}),
                          DurannoSocialIconButton.google(onPressed: () {}),
                          DurannoSocialIconButton.naver(onPressed: () {}),
                          DurannoSocialIconButton.facebook(onPressed: () {}),
                        ],
                      ),
                      DurannoSocialLoginButton.google(onPressed: () {}),
                      DurannoSocialLoginButton.apple(onPressed: () {}),
                      DurannoThemeButton(
                        type: DurannoThemeButtonType.primary,
                        label: '가입하기',
                        onPressed: () {},
                      ),
                      DurannoThemeButton(
                        type: DurannoThemeButtonType.secondary,
                        label: '가입하기',
                        onPressed: () {},
                      ),
                      DurannoThemeButton(
                        type: DurannoThemeButtonType.tertiary,
                        label: '가입하기',
                        onPressed: () {},
                      ),
                      DurannoThemeButton(
                        type: DurannoThemeButtonType.link,
                        label: '가입하기',
                        onPressed: () {},
                      ),
                    ],
                    separator: _itemSeparator,
                  ),
                ],
              ),
              Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  ...IterableUtils.join<Widget>(
                    [
                      Text(
                        '텍스트 필드',
                        style: Theme.of(context).textTheme.headline3,
                      ),
                      DurannoThemeTextField(
                        label: '이름',
                        hint: '이름을 입력하세요.',
                      ),
                      DurannoThemeTextField(
                        label: '비밀번호',
                        hint: '비밀번호를 입력하세요.',
                        obscureText: true,
                      ),
                    ],
                    separator: _itemSeparator,
                  ),
                ],
              ),
            ],
            separator: _categorySeparator,
          ),
        ],
      );

  static Widget _categorySeparator = SizedBox(height: 32.0);
  static Widget _itemSeparator = SizedBox(height: 16.0);
}
