import 'package:duranno_app/theme/duranno/colors.dart';
import 'package:flutter/material.dart';

enum DurannoThemeButtonType {
  primary,
  secondary,
  tertiary,
  link,
}

class DurannoThemeButton extends StatefulWidget {
  final DurannoThemeButtonType type;
  final String label;
  final void Function() onPressed;
  DurannoThemeButton({
    Key key,
    @required this.type,
    @required this.label,
    @required this.onPressed,
  })  : assert(type != null),
        assert(label != null),
        assert(onPressed != null),
        super(key: key);

  @override
  _DurannoThemeButtonState createState() => _DurannoThemeButtonState();
}

class _DurannoThemeButtonState extends State<DurannoThemeButton> {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: _shouldExpand ? 1 : 0,
          child: Padding(
            padding: EdgeInsets.only(
              left: _shadowSizes[widget.type] ?? 0,
              right: _shadowSizes[widget.type] ?? 0,
              bottom: _shadowSizes[widget.type] ?? 0,
            ),
            child: GestureDetector(
              onTap: widget.onPressed,
              behavior: HitTestBehavior.translucent,
              child: Container(
                decoration: BoxDecoration(
                  color: _colors[widget.type] ?? DurannoThemeColors.white,
                  border: _borders[widget.type],
                  borderRadius: _borderRadii[widget.type],
                  boxShadow: [
                    BoxShadow(
                      color: _shadowColors[widget.type] ?? Colors.transparent,
                      blurRadius: _shadowSizes[widget.type] ?? 0,
                      offset: Offset(0, _shadowSizes[widget.type] ?? 0),
                    ),
                  ],
                ),
                padding: _paddings[widget.type],
                child: Center(
                  child: Text(
                    widget.label,
                    style: Theme.of(context).textTheme.button.copyWith(
                          color: _textColors[widget.type],
                        ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }

  bool get _shouldExpand => widget.type != DurannoThemeButtonType.link;

  static const Map<DurannoThemeButtonType, Color> _colors = {
    DurannoThemeButtonType.primary: DurannoThemeColors.primaryTeal,
    DurannoThemeButtonType.secondary: DurannoThemeColors.white,
    DurannoThemeButtonType.tertiary: DurannoThemeColors.white,
  };

  static const Map<DurannoThemeButtonType, Color> _textColors = {
    DurannoThemeButtonType.primary: DurannoThemeColors.white,
    DurannoThemeButtonType.secondary: DurannoThemeColors.primaryTeal,
    DurannoThemeButtonType.tertiary: DurannoThemeColors.primaryGreen,
    DurannoThemeButtonType.link: DurannoThemeColors.primaryTeal,
  };

  static final Map<DurannoThemeButtonType, BorderRadius> _borderRadii = {
    DurannoThemeButtonType.primary: BorderRadius.circular(8.0),
    DurannoThemeButtonType.secondary: BorderRadius.circular(8.0),
    DurannoThemeButtonType.tertiary: BorderRadius.circular(8.0),
  };

  static final Map<DurannoThemeButtonType, Border> _borders = {
    DurannoThemeButtonType.tertiary:
        Border.all(color: DurannoThemeColors.primaryGreen, width: 1.0),
    DurannoThemeButtonType.link: Border(
      bottom: BorderSide(
        color: DurannoThemeColors.primaryTeal,
        width: 1.0,
      ),
    )
  };

  static final Map<DurannoThemeButtonType, double> _shadowSizes = {
    DurannoThemeButtonType.secondary: 16.0,
  };

  static final Map<DurannoThemeButtonType, Color> _shadowColors = {
    DurannoThemeButtonType.secondary: const Color.fromRGBO(0, 0, 0, 0.16),
  };

  static final Map<DurannoThemeButtonType, EdgeInsets> _paddings = {
    DurannoThemeButtonType.primary: EdgeInsets.symmetric(vertical: 12.0),
    DurannoThemeButtonType.secondary: EdgeInsets.symmetric(vertical: 12.0),
    DurannoThemeButtonType.tertiary: EdgeInsets.symmetric(vertical: 12.0),
  };
}
