# aics-necomix-duranno

## Configure development environment (using docker-compose)

1. Install `docker` and `docker-compose`.

- Install docker: https://docs.docker.com/get-docker/
- Install docker-compose: https://docs.docker.com/compose/install/

2. Configure a new Firebase project.

- Enter the [Firebase Console](https://console.firebase.google.com), and create a new project.

- Add a web app to the project. If you do so, then the configuration like the follows will be given:

```
{
    apiKey: 'AIzaSyBy5kcwmtXl5HM2u7euQgd7EOQt2ue2Kvk',
    authDomain: 'duranno-audiobook-dev.firebaseapp.com',
    databaseURL: 'https://duranno-audiobook-dev.firebaseio.com',
    projectId: 'duranno-audiobook-dev',
    storageBucket: 'duranno-audiobook-dev.appspot.com',
    messagingSenderId: '402830798110',
    appId: '1:402830798110:web:1c308a2df7c9013e30ad0f',
    measurementId: 'G-13X9ECEW0E',
}
```

- Copy this configuration to **`admin-app/src/utils/const.ts`.**

```
...

export const getFirebaseConfig = (): { [key: string]: string } => {
  const env = getEnv('stage');
  if (!env) {
    throw Error('invalid environment variable "STAGE"');
  }

  if (env === 'development') {
    /* Copy here */
    return {
      apiKey: 'AIzaSyBy5kcwmtXl5HM2u7euQgd7EOQt2ue2Kvk',
      authDomain: 'duranno-audiobook-dev.firebaseapp.com',
      databaseURL: 'https://duranno-audiobook-dev.firebaseio.com',
      projectId: 'duranno-audiobook-dev',
      storageBucket: 'duranno-audiobook-dev.appspot.com',
      messagingSenderId: '402830798110',
      appId: '1:402830798110:web:1c308a2df7c9013e30ad0f',
      measurementId: 'G-13X9ECEW0E',
    };
  }

...
```

- Add **Authentication** to project. Add "Anonymous", "Email/Password", and "Google" at the "Sign-in method" tab.

- [Add Admin SDK](https://firebase.google.com/docs/admin/setup) to the project.

  - Save the json configuration of the service account to **`server/.credentials/firebase-admin-dev.json`**. (Do not share this configuration on public workspace.)

- Finally, write the name of the Firebase project at `docker-compose.yml`.

```
...

 server:
    build: ./server
    entrypoint: bash docker-entrypoint.sh
    environment:
      ...
      LOG_LEVEL: debug
      DB_USERNAME: system
      DB_PASSWORD: oracle
      ORACLEDB_CONNECTION_STRING: db:1521
      FIREBASE_PROJECT_NAME: duranno-audiobook-dev # <- Write project name here
      GOOGLE_APPLICATION_CREDENTIALS: /usr/src/app/.credentials/firebase-admin-dev.json
      ...
...
```

3. Configure AWS.

- Create a S3 bucket.
- Enter the "Permissions" tab, and set the "CORS" field as follows:

```
[
    {
        "AllowedHeaders": [
            "*"
        ],
        "AllowedMethods": [
            "PUT"
        ],
        "AllowedOrigins": [
            "http://localhost:7008"
        ],
        "ExposeHeaders": []
    },
    {
        "AllowedHeaders": [
            "*"
        ],
        "AllowedMethods": [
            "GET"
        ],
        "AllowedOrigins": [
            "*"
        ],
        "ExposeHeaders": []
    }
]
```

- Create an IAM user. Grant a policy about AWS S3 to this user. Fill the ARN of S3 bucket you just made at `<bucket-arn>` (e.g. `"arn:aws:s3:::dev.resource.neocomix-audiobook.mindslab.ai/*"`).

```
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": [
                "s3:*Object",
                "s3:PutObjectAcl"
            ],
            "Resource": [
                <bucket-arn>
            ],
            "Effect": "Allow"
        }
    ]
}
```

- Fill the bucket and IAM user information at `docker-compose.yml` and `.env`. (You should create `.env` file yourself, since it is not staged to git)

`docker-compose.yml`

```
...

 server:
    build: ./server
    entrypoint: bash docker-entrypoint.sh
    environment:
        ...
        # mutiple users can act as admin, by separating the uids with comma
        ADMIN_UIDS: <admin-uid>
        # set resouce bucket to own buckets, or request an aws role to use these buckets
        RESOURCE_BUCKET: dev.resource.neocomix-audiobook.mindslab.ai # <- Write bucket name here
        MEDIA_BUCKET: dev.resource.neocomix-audiobook.mindslab.ai # <- Write bucket name here
        # secret environment values: write them at .env
        JWT_SECRET_KEY: ${JWT_SECRET_KEY}
        AWS_ACCESS_KEY_ID: ${AWS_ACCESS_KEY_ID}
        ...
...
```

`.env`

```
AWS_ACCESS_KEY_ID=***************
AWS_SECRET_ACCESS_KEY=*****************************
```

4. Fill `JWT_SECRET_KEY` environment variable at `.env` file.

- It can be a random string, but we typically use `openssl`.

```
$ openssl rand -base64 24
```

- Write the resulting string to `.env`.

```
JWT_SECRET_KEY=<random-string>     # <- Write here.
AWS_ACCESS_KEY_ID=***************
AWS_SECRET_ACCESS_KEY=*****************************
```

5. At the project root directory, create docker containers of database, server, and admin web app.

```
$ docker-compose build
$ docker-compose up -d
```

6. Check if the containers are active.

```
$ docker ps
7eea9f897c80        aics-necomix-duranno_admin-app           "/docker-entrypoint.…"   4 minutes ago       Up 4 minutes        0.0.0.0:7008->80/tcp                       aics-necomix-duranno_admin-app_1
e8b247f0999f        aics-necomix-duranno_server              "bash docker-entrypo…"   4 minutes ago       Up 4 minutes        0.0.0.0:7007->7007/tcp                     aics-necomix-duranno_server_1
b7336dc7d51e        oracleinanutshell/oracle-xe-11g:latest   "/bin/sh -c '/usr/sb…"   4 minutes ago       Up 4 minutes        22/tcp, 8080/tcp, 0.0.0.0:1521->1521/tcp   aics-necomix-duranno_db_1
```

```
# Check server connection
$ curl localhost:7007
{"msg":"duranno-server@0.1.0-local server is alive! :)"}
```

7. Connect to admin web app. (http://localhost:7008)

- If you sign in using a google account, you will see the message "관리자로 등록되지 않은 계정입니다". Move to the Firebase console.
- At the "User" tab of authentication, you will see the google account you used to sign in. Copy the id of the account.
- Fill the id to `ADMIN_UID` field of `docker-compose.yml`.

```
...
    server:
    build: ./server
    entrypoint: bash docker-entrypoint.sh
    environment:
        ...
        # set this as uid of firebase user you want to make admin
        # mutiple users can act as admin, by separating the uids with comma
        ADMIN_UIDS: <admin-uid> # <- Write here.
        ...
...
```

- Restart the service by `docker-compose up -d`. Refresh the admin web page, then the page will be properly shown.

- Feel free to add some books.

8. [Start front-end app development.](https://pms.maum.ai/bitbucket/projects/AICS/repos/aics-necomix-duranno/browse/app)
