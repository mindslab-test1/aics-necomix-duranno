import React from 'react';
import styled from 'styled-components';
import { flexbox } from '../../styles/utils';

interface TableRowProps {
  items: React.ReactNode[];
  flexItems?: number[];
  width?: number | string;
  isPrimary?: boolean;
  className?: string;
}

interface TableCellProps {
  isPrimary?: boolean;
  flex?: number;
}

const StyledCell = styled.div<TableCellProps>`
  padding: 8px 12px;
  max-height: 200px;
  overflow-x: auto;
  overflow-y: auto;
  ${flexbox({ direction: 'row', justify: 'flex-start', align: 'flex-start' })}
  ${(props) =>
    props.isPrimary &&
    `
    background-color: #e0e0e0;
    font-weight: bold;
  `}
  ${(props) => props.flex && `flex: ${props.flex};`}

  p {
    white-space: pre-line;
  }
`;

const StyledContainer = styled.div<Partial<TableRowProps>>`
  ${flexbox({
    direction: 'row',
    align: 'flex-stretch',
  })} > ${StyledCell} + ${StyledCell} {
    border-left: 1px solid #ffffff;
  }
`;

const TableRow: React.FunctionComponent<TableRowProps> = (
  props: TableRowProps,
) => {
  return (
    <StyledContainer className={props.className} style={{ width: props.width }}>
      {props.items.map((item, idx) => (
        <StyledCell
          key={idx}
          isPrimary={props.isPrimary}
          flex={props.flexItems && props.flexItems[idx]}
        >
          {item}
        </StyledCell>
      ))}
    </StyledContainer>
  );
};

export default TableRow;
