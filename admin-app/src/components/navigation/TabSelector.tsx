import React from 'react';
import styled from 'styled-components';
import { flexbox } from '../../styles/utils';
import { Tab, TabValues } from '../routes/DashboardRoute';

interface TabSelectorProps {
  current: Tab;
  setCurrent: (value: Tab) => void;
}

interface TabSelectorItemProps {
  active: boolean;
}

const StyledTabItemContainer = styled.div<TabSelectorItemProps>`
  width: 100%;
  padding: 16px;
  border-bottom: 1px solid #3f51b5;
  background-color: #283593;
  cursor: pointer;
  &:hover {
    background-color: #3f51b5;
  }
  color: #ffffff;
  ${flexbox({ direction: 'row', justify: 'flex-start' })}

  ${(props) =>
    props.active &&
    `
    color: #283593;
    background-color: #ffffff;
    cursor: initial;
    font-weight: bold;
    &:hover {
      background-color: #ffffff;
    }
  `}
`;

const StyledTabContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 256px;
  height: 100%;
  background-color: #283593;
  border-right: 1px solid #283593;
  ${flexbox({
    direction: 'column',
    justify: 'flex-start',
    align: 'flex-start',
  })}
`;

const TabSelector: React.FunctionComponent<TabSelectorProps> = (
  props: TabSelectorProps,
) => {
  return (
    <StyledTabContainer>
      {TabValues.map((tab) => (
        <StyledTabItemContainer
          key={tab.toString()}
          active={tab === props.current}
          onClick={() => props.setCurrent(tab)}
        >
          {tab.toString()}
        </StyledTabItemContainer>
      ))}
    </StyledTabContainer>
  );
};

export default TabSelector;
