import React from 'react';
import styled from 'styled-components';
import firebase from 'firebase';
import useAuthObserver from '../../hooks/authObserver';
import { flexbox } from '../../styles/utils';

const StyledHeaderContainer = styled.div`
  position: relative;
  width: 100%;
  height: 64px;
  padding: 0 32px;
  border-bottom: 1px solid #cccccc;
  ${flexbox({ direction: 'row' })}
`;

const StyledAuthActionContainer = styled.div`
  position: absolute;
  right: 32px;
  ${flexbox({ direction: 'row' })}

  > * + * {
    margin-left: 16px;
  }
`;

const Header: React.FunctionComponent = () => {
  const { user, isSigned } = useAuthObserver();

  const onSignOut = () => {
    if (isSigned) {
      firebase.auth().signOut();
    } else {
      console.warn('User is not signed, but onSigned is called.');
    }
  };

  return (
    <StyledHeaderContainer>
      <h2>Neocomix Audiobook Admin</h2>
      <StyledAuthActionContainer>
        {isSigned && <span>{user?.email}</span>}
        {isSigned && <button onClick={onSignOut}>로그아웃</button>}
      </StyledAuthActionContainer>
    </StyledHeaderContainer>
  );
};

export default Header;
