import React from 'react';
import classnames from 'classnames';
import styled from 'styled-components';
import { flexbox } from '../../styles/utils';
import useModal from '../../hooks/modal';
import EditBookTagModal from '../modal/EditBookTagModal';

interface BookTagProps {
  className?: string;
  data: {
    id: string;
    text: string;
    color?: string;
    backgroundColor?: string;
  };
}

export interface BookTagData {
  text: string;
  color?: string;
  backgroundColor?: string;
}

export const StyledTag = styled.div<{ data: BookTagData }>`
  display: inline-block;
  border-radius: 4px;
  padding: 2px 6px;
  cursor: pointer;
  ${(props) => props.data.color && `color: ${props.data.color};`}
  ${(props) =>
    props.data.backgroundColor &&
    `
    background-color: ${props.data.backgroundColor};
    color: #ffffff;  
  `}
  ${(props) =>
    !props.data.backgroundColor &&
    `border: 1px solid ${props.data.color || '#000000'};`}
  ${flexbox({ direction: 'row' })}
`;

const BookTag: React.FunctionComponent<BookTagProps> = (
  props: BookTagProps,
) => {
  const { Modal, open } = useModal();
  return (
    <React.Fragment>
      <StyledTag
        className={classnames(props.className)}
        data={props.data}
        onClick={open}
      >
        <span>{props.data.text}</span>
      </StyledTag>
      <Modal>
        <EditBookTagModal data={props.data} />
      </Modal>
    </React.Fragment>
  );
};

export default BookTag;
