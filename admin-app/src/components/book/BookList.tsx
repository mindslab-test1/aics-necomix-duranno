import { useQuery } from '@apollo/client';
import classnames from 'classnames';
import React from 'react';
import styled from 'styled-components';
import { SEARCH_BOOKS } from '../../graphql/query';
import { flexbox } from '../../styles/utils';
import { formatDuration } from '../../utils/misc';
import TableRow from '../common/TableRow';
import Author from './Author';
import BookTableOfContents from './BookTableOfContents';
import BookTag from './BookTag';

interface BookListProps {
  keyword?: string;
  tag?: string;
}

const StyledContainer = styled.div`
  > * + * {
    margin-top: 16px;
  }

  .book-list-item {
    border-bottom: 1px solid #cccccc;
  }

  .book-index span {
    display: block;
  }

  .book-tag-list {
    ${flexbox({ direction: 'column', align: 'flex-start' })}
  }

  .book-tag + .book-tag {
    margin-top: 4px;
  }

  .fetch-more {
    ${flexbox({ direction: 'row', justify: 'flex-start' })}
    > * + * {
      margin-left: 8px;
    }
  }
`;

const COLUMN_LABELS = [
  '',
  '',
  '제목',
  '책 설명',
  '저자',
  '총 조회수',
  '주간 조회수',
  '댓글 수',
  '별점',
  '태그',
  '오디오북 총 길이',
  '목차',
];

const TABLE_WIDTH = 2400;
const COLUMN_FLEX = [1, 1, 1, 2, 2, 1, 1, 1, 1, 1, 1, 3];

const PAGINATION_LIMIT = 10;

const BookList: React.FunctionComponent<BookListProps> = (
  props: BookListProps,
) => {
  const { loading, error, data, fetchMore } = useQuery(SEARCH_BOOKS, {
    variables: {
      keyword: props.keyword || '',
      tag: props.tag || '',
      limit: PAGINATION_LIMIT,
    },
  });

  const [fetchMorePending, setFetchMorePending] = React.useState<boolean>(
    false,
  );

  if (loading) return <span>Loading ...</span>;
  if (error) {
    console.warn(error);
    return <span>Error :(</span>;
  }

  const {
    searchBookCount: count,
    searchBooks: { items, cursor },
  } = data;

  const onFetchMore = () => {
    setFetchMorePending(true);
    fetchMore({
      query: SEARCH_BOOKS,
      variables: {
        keyword: '',
        tag: '',
        cursor,
        limit: PAGINATION_LIMIT,
      },
    }).then(() => setFetchMorePending(false));
  };

  return (
    <StyledContainer>
      <div className={classnames('book-list')}>
        <TableRow
          items={COLUMN_LABELS}
          isPrimary
          flexItems={COLUMN_FLEX}
          width={TABLE_WIDTH}
        />
        {items.map((item: any, index: number) => {
          const {
            author,
            description,
            duration,
            id,
            mediaUri,
            metadataUri,
            numComments,
            numViews,
            rating,
            tags,
            thumbnailUri,
            title,
            weeklyViews,
          } = item;
          const { average: ratingAverage } = rating;

          const rowItems = [
            <div className={classnames('book-index')} key="index">
              <span key="index">{index + 1}</span>
              <span key="id" className={classnames('light')}>
                id: {id}
              </span>
            </div>,
            thumbnailUri ? (
              <img src={thumbnailUri} alt="thumbnail" width="100%" />
            ) : (
              <div />
            ),
            <span key="title">{title}</span>,
            <p key="description">{description}</p>,
            <Author key="author" data={author} />,
            <span key="numViews">{numViews}</span>,
            <span key="weeklyViews">{weeklyViews}</span>,
            <span key="numComments">{numComments}</span>,
            <span key="ratingAverage">
              {ratingAverage === null
                ? '-'
                : (ratingAverage as number).toFixed(1)}
            </span>,
            <div className={classnames('book-tag-list')} key="tags">
              {tags.map((tag: any) => (
                <BookTag
                  className={classnames('book-tag')}
                  data={tag}
                  key={tag.id}
                />
              ))}
            </div>,
            <span key="duration">{formatDuration(duration)}</span>,
            <BookTableOfContents
              key="metadata"
              mediaUri={mediaUri}
              metadataUri={metadataUri}
            />,
          ];

          return (
            <TableRow
              key={id}
              items={rowItems}
              flexItems={COLUMN_FLEX}
              className={classnames('book-list-item')}
              width={TABLE_WIDTH}
            />
          );
        })}
      </div>
      {cursor && items.length >= PAGINATION_LIMIT && (
        <div className={classnames('fetch-more')}>
          <button onClick={onFetchMore}>
            {PAGINATION_LIMIT}개 더 불러오기
          </button>
          {fetchMorePending && <span>로드 중...</span>}
        </div>
      )}
      <div>
        <span>
          <b>총 {count}개</b> (현재 {items.length}개 로드)
        </span>
      </div>
    </StyledContainer>
  );
};

export default BookList;
