import React from 'react';
import classnames from 'classnames';
import styled from 'styled-components';

interface AuthorProps {
  data?: {
    id: string;
    name: string;
    description?: string;
    profileImageUri?: string;
  };
}

const StyledContainer = styled.div`
  .author-id {
    color: #888888;
  }

  > * + * {
    margin-top: 8px;
  }

  h5 {
    margin-top: 24px;
  }

  p {
    display: block;
  }
`;

const Author: React.FunctionComponent<AuthorProps> = (props: AuthorProps) => {
  if (!props.data) return <span>-</span>;

  const { name, description, profileImageUri, id } = props.data;

  return (
    <StyledContainer>
      <span className={classnames('author-id')}>{id}</span>
      <h5>이름</h5>
      <span>{name}</span>
      {description && (
        <React.Fragment>
          <h5>저자 설명</h5>
          <p>{description}</p>
        </React.Fragment>
      )}
      {profileImageUri && (
        <React.Fragment>
          <h5>프로필 이미지</h5>
          <img src={profileImageUri} alt="profile" width="100%" />
        </React.Fragment>
      )}
    </StyledContainer>
  );
};

export default Author;
