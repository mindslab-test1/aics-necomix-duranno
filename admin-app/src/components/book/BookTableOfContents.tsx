import React from 'react';
import classnames from 'classnames';
import styled from 'styled-components';
import { flexbox } from '../../styles/utils';

interface BookTableOfContentsProps {
  mediaUri?: string;
  metadataUri: string;
}

interface BookChapterProps {
  hasMedia?: boolean;
}

const StyledChapterContainer = styled.div<BookChapterProps>`
  ${(props) => !props.hasMedia && `color: #cccccc;`}
  ${flexbox({ direction: 'column', align: 'flex-start' })}
  .chapter-title {
    font-weight: 600;
  }
`;

const StyledContainer = styled.div`
  ${StyledChapterContainer} + ${StyledChapterContainer} {
    margin-top: 8px;
  }
`;

const BookTableOfContents: React.FunctionComponent<BookTableOfContentsProps> = (
  props: BookTableOfContentsProps,
) => {
  const [metadata, setMetadata] = React.useState<any>();

  React.useEffect(() => {
    if (props.metadataUri) {
      fetch(props.metadataUri)
        .then((response) => response.json())
        .then((json) => setMetadata(json));
    }
  }, [props.metadataUri]);

  if (!props.metadataUri) {
    if (props.mediaUri) {
      return <audio controls src={props.mediaUri} />;
    } else {
      return <span>오디오 파일이 존재하지 않습니다.</span>;
    }
  }
  if (!metadata) return <span>Loading ...</span>;

  const { chapters } = metadata;

  return (
    <StyledContainer>
      {chapters.map((chapter: Record<string, string>, idx: number) => {
        const { label, mediaUri } = chapter;
        return (
          <StyledChapterContainer key={idx} hasMedia={!!mediaUri}>
            <span className={classnames('chapter-title')}>{label}</span>
            {mediaUri && <audio controls src={mediaUri} />}
          </StyledChapterContainer>
        );
      })}
    </StyledContainer>
  );
};

export default BookTableOfContents;
