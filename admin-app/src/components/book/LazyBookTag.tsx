import { useQuery } from '@apollo/client';
import React from 'react';
import { TAG } from '../../graphql/query';
import { StyledTag } from './BookTag';

interface LazyBookTagProps {
  text: string;
}

const LazyBookTag: React.FunctionComponent<LazyBookTagProps> = (props) => {
  const { loading, error, data } = useQuery(TAG, {
    variables: { text: props.text },
  });

  if (loading) return <span>Loading ...</span>;
  if (error) {
    console.warn(error);
    return <span>Error :(</span>;
  }

  const { tag } = data;

  if (!tag) {
    return (
      <StyledTag data={{ text: props.text, color: '#000000' }}>
        {props.text} *
      </StyledTag>
    );
  }

  return <StyledTag data={tag}>{tag.text}</StyledTag>;
};

export default LazyBookTag;
