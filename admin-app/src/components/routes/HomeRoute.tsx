import { useQuery } from '@apollo/client';
import React from 'react';
import { Redirect } from 'react-router-dom';
import styled from 'styled-components';
import { ME } from '../../graphql/query';
import { flexbox } from '../../styles/utils';

const StyledContainer = styled.div`
  width: 100%;
  height: 100%;
  ${flexbox({ direction: 'column' })}
`;

const HomeRoute: React.FunctionComponent = () => {
  const { loading, error, data } = useQuery(ME);

  if (loading) return <span>Loading ...</span>;
  if (error) {
    console.warn(error);
    return <span>Error :(</span>;
  }

  const {
    me: { admin },
  } = data;

  if (admin) {
    return <Redirect to="/dashboard" />;
  }

  return (
    <StyledContainer>
      <h3>관리자로 등록되지 않은 계정입니다.</h3>
    </StyledContainer>
  );
};

export default HomeRoute;
