import React from 'react';
import classnames from 'classnames';
import styled from 'styled-components';
import TabSelector from '../navigation/TabSelector';
import BookListTab from '../tab/BookListTab';

export enum Tab {
  BookList = '책 목록',
}

export const TabValues = [Tab.BookList];

const Container = styled.div`
  position: relative;
  width: 100%;
  height: 100%;

  .tab-content {
    margin-left: 256px;
    padding: 40px 32px;
    width: calc(100% - 256px);
    height: 100%;
    overflow-x: hidden;
    overflow-y: auto;
  }
`;

const DashboardRoute: React.FunctionComponent = () => {
  const [tab, setTab] = React.useState<Tab>(Tab.BookList);

  return (
    <Container>
      <TabSelector current={tab} setCurrent={setTab} />
      <div className={classnames('tab-content')}>
        {tab === Tab.BookList && <BookListTab />}
      </div>
    </Container>
  );
};

export default DashboardRoute;
