import React from 'react';
import firebase from 'firebase';
import styled from 'styled-components';
import { flexbox } from '../../styles/utils';
import { StyledFirebaseAuth } from 'react-firebaseui';

const uiConfig = {
  signInFlow: 'popup',
  signInSuccessUrl: '/',
  signInOptions: [
    firebase.auth.GoogleAuthProvider.PROVIDER_ID,
    firebase.auth.EmailAuthProvider.PROVIDER_ID,
  ],
};

const StyledContainer = styled.div`
  width: 100%;
  height: 100%;
  ${flexbox({ direction: 'column' })}
`;

const LoginRoute: React.FunctionComponent = () => {
  return (
    <StyledContainer>
      <h4>네오코믹스 오디오북 관리자 페이지</h4>
      <span>로그인 하세요.</span>
      <StyledFirebaseAuth uiConfig={uiConfig} firebaseAuth={firebase.auth()} />
    </StyledContainer>
  );
};

export default LoginRoute;
