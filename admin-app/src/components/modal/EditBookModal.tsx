import { useApolloClient, useMutation, useQuery } from '@apollo/client';
import React from 'react';
import styled from 'styled-components';
import {
  CREATE_AUTHOR,
  CREATE_BOOK,
  CREATE_TAG,
  EDIT_BOOK,
} from '../../graphql/mutation';
import { AUTHORS, TAGS, UPLOAD_BOOK_AUDIO_URI } from '../../graphql/query';
import { flexbox } from '../../styles/utils';
import { formatDuration } from '../../utils/misc';
import LazyBookTag from '../book/LazyBookTag';

enum AuthorLinkMethod {
  Create = 'create',
  Select = 'select',
  None = 'none',
}

enum BookTableOfContentsType {
  WithChapters = 'with-chapters',
  AsWhole = 'as-whole',
}

type AuthorLinkMethodStrings = keyof typeof AuthorLinkMethod;

type BookTableOfContentsTypeStrings = keyof typeof BookTableOfContentsType;

interface EditBookModalProps {
  data?: {
    id: string;
    title: string;
    description?: string;
    author?: {
      name: string;
      description?: string;
    };
  };
  close(): void;
}

interface BookInput {
  title?: string;
  description?: string;
  authorId?: string;
  authorName?: string;
  authorDescription?: string;
  authorThumbnail?: File;
  tagTexts?: string[];
  audioFile?: File;
  duration?: number;
  durationPending?: boolean;
  chapters?: {
    label?: string;
    audioFile?: File;
    duration?: number;
    durationPending?: boolean;
  }[];
  chapterTotalDuration?: number;
  thumbnail?: File;
}

const StyledModalContent = styled.div`
  ${flexbox({ direction: 'column', align: 'flex-start' })}
  .secondary {
    color: #888888;
  }
  form {
    width: 100%;
    ${flexbox({ direction: 'column', align: 'flex-start' })}
    > * + * {
      margin-top: 8px;
    }
    > label {
      margin-top: 16px;
    }
    > label + label {
      margin-top: 4px;
    }
    > h3 {
      margin-top: 40px;
    }
    > label.radio {
      align-self: stretch;
      ${flexbox({ direction: 'row', justify: 'flex-start' })}
      input {
        width: 36px;
      }
    }
    select {
      min-width: 128px;
    }
    input,
    textarea {
      width: 100%;
    }
    textarea {
      resize: none;
      height: 200px;
    }
    .author,
    .book-toc {
      margin-left: 16px;
      ${flexbox({ direction: 'column', align: 'flex-start' })}
      background-color: #f5f5f5;
      padding: 8px;
      width: 100%;
      > * + * {
        margin-top: 16px;
      }
    }
    .add-tag,
    .tag-item {
      ${flexbox({ direction: 'row', justify: 'flex-start' })}
      > * + * {
        margin-left: 16px;
      }
      width: 100%;
      input {
        max-width: 200px;
      }
    }
    .chapter-item {
      ${flexbox({ direction: 'column', align: 'flex-start' })}
      > * + * {
        margin-top: 16px;
      }
      width: 100%;
      input {
        max-width: 200px;
      }
      border: 1px solid #888888;
      padding: 16px;
    }
    p {
      white-space: pre-line;
    }
  }
  > * + * {
    margin-top: 16px;
  }
  .submit {
    margin-top: 40px;
    ${flexbox({ direction: 'row', justify: 'flex-start' })}
    > * + * {
      margin-left: 16px;
    }
  }
`;

const EditBookModal: React.FunctionComponent<EditBookModalProps> = (props) => {
  const isEditing = !!props.data;

  const client = useApolloClient();

  const [bookData, setBookData] = React.useState<BookInput>(
    props.data
      ? {
          title: props.data.title,
          description: props.data.description,
          authorName: props.data.author?.name,
        }
      : {},
  );
  const [
    authorLinkMethodString,
    setAuthorLinkMethod,
  ] = React.useState<AuthorLinkMethodStrings | null>(null);
  const [
    bookTocTypeString,
    setBookTocType,
  ] = React.useState<BookTableOfContentsTypeStrings | null>(null);

  const [
    createAuthor,
    { loading: createAuthorLoading, error: createAuthorError },
  ] = useMutation(CREATE_AUTHOR);
  const [isCheckingTags, setIsCheckingTags] = React.useState<boolean>(false);
  const [
    createTag,
    { loading: createTagLoading, error: createTagError },
  ] = useMutation(CREATE_TAG);
  const [
    createBook,
    { loading: createBookLoading, error: createBookError },
  ] = useMutation(CREATE_BOOK);
  const [
    editBook,
    { loading: editBookLoading, error: editBookError },
  ] = useMutation(EDIT_BOOK);

  const { data: authorsData } = useQuery(AUTHORS);
  const { authorList } = authorsData || {};

  const authorLinkMethod = authorLinkMethodString
    ? AuthorLinkMethod[authorLinkMethodString]
    : null;

  const bookTocType = bookTocTypeString
    ? BookTableOfContentsType[bookTocTypeString]
    : null;

  const [mediaUploadPending, setMediaUploadPending] = React.useState<boolean>(
    false,
  );
  const [mediaUploadProgress, setMediaUploadProgress] = React.useState<number>(
    0,
  );
  const [numMedia, setNumMedia] = React.useState<number>(0);

  const onChangeInput = (
    e: React.ChangeEvent<
      HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
    >,
  ) => {
    setBookData((book) => ({ ...book, [e.target.name]: e.target.value }));
  };

  const onChangeAuthorLinkMethod = (e: React.ChangeEvent<HTMLInputElement>) => {
    setAuthorLinkMethod(e.target.value as AuthorLinkMethodStrings);
  };

  const onChangeBookTocMethod = (e: React.ChangeEvent<HTMLInputElement>) => {
    setBookTocType(e.target.value as BookTableOfContentsTypeStrings);
  };

  const onChangeAuthorThumbnail = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.item(0);
    if (file) {
      setBookData((book) => ({ ...book, authorThumbnail: file }));
    }
  };

  const onChangeBookThumbnail = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.item(0);
    if (file) {
      setBookData((book) => ({ ...book, thumbnail: file }));
    }
  };

  const onChangeWholeAudioFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.item(0);
    if (file) {
      setBookData((book) => ({ ...book, audioFile: file }));

      const reader = new FileReader();
      reader.onload = (e) => {
        const data = e.target?.result;
        if (!data) {
          return;
        }

        const audioContext = new window.AudioContext();
        audioContext.decodeAudioData(data as ArrayBuffer, (buffer) => {
          setBookData((book) => ({
            ...book,
            duration: buffer.duration,
            durationPending: false,
          }));
        });
      };
      reader.onerror = (e) => {
        console.warn(e);
        setBookData((book) => ({
          ...book,
          duration: undefined,
          durationPending: false,
        }));
      };

      reader.readAsArrayBuffer(file);
      setBookData((book) => ({ ...book, durationPending: true }));
    }
  };

  const onChangeChapterAudioFile = (index: number) => (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    const file = e.target.files?.item(0);
    const setChapterData = (field: string, value: any) =>
      setBookData((book) => ({
        ...book,
        chapters: book.chapters?.map((chapter, idx) => {
          if (idx !== index) return chapter;

          return { ...chapter, [field]: value };
        }),
      }));

    if (file) {
      setChapterData('audioFile', file);

      const reader = new FileReader();
      reader.onload = (e) => {
        const data = e.target?.result;
        if (!data) {
          return;
        }

        const audioContext = new window.AudioContext();
        audioContext.decodeAudioData(data as ArrayBuffer, (buffer) => {
          setChapterData('duration', buffer.duration);
          setChapterData('durationPending', false);
        });
      };
      reader.onerror = (e) => {
        console.warn(e);
        setChapterData('durationPending', false);
      };

      reader.readAsArrayBuffer(file);
      setChapterData('durationPending', true);
    }
  };

  const onChangeChapterInput = (index: number) => (
    e: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setBookData((book) => ({
      ...book,
      chapters: book.chapters?.map((chapter, idx) => {
        if (idx !== index) return chapter;

        return { ...chapter, [e.target.name]: e.target.value };
      }),
    }));
  };

  const [tagInput, setTagInput] = React.useState<string>('');

  const addToTags = (text: string) =>
    setBookData((book) => ({
      ...book,
      tagTexts: [
        ...(book.tagTexts?.includes(text) ? [] : [text]),
        ...(book.tagTexts || []),
      ],
    }));

  const removeFromTags = (text: string) =>
    setBookData((book) => ({
      ...book,
      tagTexts: book.tagTexts?.filter((item) => item !== text),
    }));

  const onAddTag = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    if (tagInput !== '') {
      addToTags(tagInput);
      setTagInput('');
    }
  };

  const onAddChapter = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    setBookData((book) => ({
      ...book,
      chapters: [...(book.chapters || []), {}],
    }));
  };

  const onRemoveChapter = (index: number) => (
    e: React.MouseEvent<HTMLButtonElement>,
  ) => {
    e.preventDefault();

    setBookData((book) => ({
      ...book,
      chapters: book.chapters?.filter((_, idx) => index !== idx),
    }));
  };

  React.useEffect(() => {
    const totalDuration = bookData.chapters?.reduce(
      (acc, chapter) => acc + (chapter.duration || 0),
      0,
    );
    setBookData((book) => ({ ...book, chapterTotalDuration: totalDuration }));
  }, [bookData.chapters]);

  const isCreateAuthorDataValid = !!bookData.authorName;

  const [submitPending, setSubmitPending] = React.useState<boolean>(false);
  const onSubmit = async (e: React.MouseEvent<HTMLButtonElement>) => {
    e.preventDefault();

    setSubmitPending(true);

    try {
      const variables: { [key: string]: any } = {};

      variables.title = bookData.title;
      variables.description = bookData.description;

      if (authorLinkMethod === AuthorLinkMethod.Create) {
        if (isCreateAuthorDataValid) {
          try {
            const {
              data: {
                createAuthor: { id: authorId },
              },
              errors,
            } = await createAuthor({
              variables: {
                name: bookData.authorName,
                profileImage: bookData.authorThumbnail,
                description: bookData.authorDescription,
              },
            });
            if (errors) {
              throw Error(`create author failed: ${errors}`);
            }
            variables.authorId = authorId;
          } catch (e) {
            console.warn(e);
          }
        }
      } else if (authorLinkMethod === AuthorLinkMethod.Select) {
        variables.authorId = bookData.authorId;
      } else if (authorLinkMethod === AuthorLinkMethod.None) {
        variables.authorId = null;
      }

      if (bookData.tagTexts) {
        setIsCheckingTags(true);
        const {
          data: { tags },
          errors,
        } = await client.query({
          query: TAGS,
          variables: { texts: bookData.tagTexts },
        });
        setIsCheckingTags(false);
        if (errors) {
          throw Error(`check tags failed: ${errors}`);
        }
        const tagIds = await Promise.all(
          tags.map(async (tag: any, idx: number) => {
            if (tag) {
              return tag.id;
            }

            const newTagText = bookData.tagTexts
              ? bookData.tagTexts[idx]
              : null;

            if (!newTagText) {
              return;
            }

            const {
              data: {
                createTag: { id: newTagId },
              },
              errors,
            } = await createTag({
              variables: { text: newTagText },
            });

            if (errors) {
              throw Error(`create tags failed: ${errors}`);
            }

            return newTagId;
          }),
        );

        variables.tags = tagIds;
      }

      variables.thumbnail = bookData.thumbnail;

      if (bookTocType == BookTableOfContentsType.AsWhole) {
        variables.duration = bookData.duration;
      } else if (bookTocType == BookTableOfContentsType.WithChapters) {
        variables.duration = bookData.chapterTotalDuration;
      }

      const {
        data: { createBook: book },
      } = await createBook({
        variables,
      });

      const editBookVariables: { [key: string]: any } = {
        bookId: book.id,
        title: book.title,
        description: book.description,
        authorId: book.author?.id,
        tags: book.tags?.map((tag: any) => tag.id),
        duration: book.duration,
        thumbnailUri: book.thumbnailUri,
      };

      setMediaUploadPending(true);
      setMediaUploadProgress(0);

      if (bookTocType == BookTableOfContentsType.AsWhole) {
        setNumMedia(1);

        const {
          data: { uploadBookAudioUri },
          errors,
        } = await client.query({
          query: UPLOAD_BOOK_AUDIO_URI,
          variables: {
            bookId: book.id,
          },
        });

        if (errors) {
          throw Error(`request upload uri failed: ${errors}`);
        }

        await fetch(uploadBookAudioUri, {
          method: 'PUT',
          headers: {
            'Content-Type': 'multipart/form-data',
          },
          body: bookData.audioFile,
        }).catch((err) => console.log(err));

        editBookVariables.mediaUri = `${process.env.REACT_APP_MEDIA_BUCKET}/${book.id}.mp3`;
      } else if (bookTocType == BookTableOfContentsType.WithChapters) {
        if (!bookData.chapters) {
          throw Error(`No chapters provided`);
        }

        const chaptersWithMedia =
          bookData.chapters?.filter((item) => !!item.duration) || [];
        setNumMedia(chaptersWithMedia.length);

        const getSequence = (chapter: any): number =>
          chaptersWithMedia.indexOf(chapter) || 0;

        const uploadUris = await Promise.all(
          chaptersWithMedia.map((chapter) =>
            client
              .query({
                query: UPLOAD_BOOK_AUDIO_URI,
                variables: {
                  bookId: book.id,
                  sequence: getSequence(chapter),
                },
              })
              .then((result) => result.data.uploadBookAudioUri as string),
          ),
        );

        await Promise.all(
          uploadUris.map(async (uri, index) => {
            await fetch(uri, {
              method: 'PUT',
              headers: {
                'Content-Type': 'multipart/form-data',
              },
              body: chaptersWithMedia[index].audioFile,
            });
            setMediaUploadProgress((p) => p + 1);
          }),
        );

        editBookVariables.metadata = {
          chapters: bookData.chapters?.map((chapter) => ({
            label: chapter.label,
            mediaUri: chapter.duration
              ? `${process.env.REACT_APP_MEDIA_BUCKET}/${book.id}-${getSequence(
                  chapter,
                )}.mp3`
              : undefined,
            duration: chapter.duration,
          })),
        };
      }

      const { errors } = await editBook({
        variables: editBookVariables,
      });

      if (errors) {
        console.warn(errors);
        throw Error(`edit book failed`);
      }

      setMediaUploadPending(false);

      setSubmitPending(false);
      props.close();
    } catch (e) {
      console.warn(e);
      setMediaUploadPending(false);
      setSubmitPending(false);
    }
  };

  const isDataValid =
    bookData.title &&
    ((authorLinkMethod === AuthorLinkMethod.Create &&
      isCreateAuthorDataValid) ||
      authorLinkMethod == AuthorLinkMethod.Select ||
      authorLinkMethod == AuthorLinkMethod.None) &&
    ((bookTocType == BookTableOfContentsType.AsWhole && bookData.audioFile) ||
      (bookTocType == BookTableOfContentsType.WithChapters &&
        bookData.chapters?.every((chapter) => chapter.label))) &&
    !bookData.durationPending &&
    !bookData.chapters?.some((chapter) => chapter.durationPending);

  return (
    <StyledModalContent>
      <h2>{isEditing ? '책 수정' : '새 책 만들기'}</h2>
      <form>
        {isEditing && (
          <React.Fragment>
            <label className="seconadry">ID</label>
            <span className="secondary">{props.data?.id}</span>
          </React.Fragment>
        )}
        <label htmlFor="title">
          <b>제목</b> *
        </label>
        <input
          name="title"
          value={bookData.title || ''}
          onChange={onChangeInput}
        />
        <label htmlFor="description">
          <b>책 설명</b>
        </label>
        <textarea
          name="description"
          value={bookData.description || ''}
          onChange={onChangeInput}
        />
        <h3>저자</h3>
        <label className="radio">
          <input
            type="radio"
            name="authorLinkMethod"
            value="Create"
            onChange={onChangeAuthorLinkMethod}
          />
          <span>새 저자 만들기</span>
        </label>
        <label className="radio">
          <input
            type="radio"
            name="authorLinkMethod"
            value="Select"
            onChange={onChangeAuthorLinkMethod}
          />
          <span>기존 저자 연결하기</span>
        </label>
        <label className="radio">
          <input
            type="radio"
            name="authorLinkMethod"
            value="None"
            onChange={onChangeAuthorLinkMethod}
          />
          <span>저자 없음</span>
        </label>
        <div
          className="author"
          style={{
            display:
              authorLinkMethod === AuthorLinkMethod.Create ? 'flex' : 'none',
          }}
        >
          <label htmlFor="authorName">
            <b>저자명</b> *
          </label>
          <input
            name="authorName"
            value={bookData.authorName || ''}
            onChange={onChangeInput}
          />
          <label htmlFor="authorDescription">
            <b>저자 설명</b>
          </label>
          <textarea
            name="authorDescription"
            value={bookData.authorDescription || ''}
            onChange={onChangeInput}
          />
          <label htmlFor="authorProfileImage">
            <b>저자 썸네일</b>
          </label>
          <input
            type="file"
            name="authorProfileImage"
            accept="image/*"
            onChange={onChangeAuthorThumbnail}
          />
        </div>
        <div
          className="author"
          style={{
            display:
              authorLinkMethod === AuthorLinkMethod.Select ? 'flex' : 'none',
          }}
        >
          <label htmlFor="authorId">
            <b>저자 선택</b>
          </label>
          <select name="authorId" onChange={onChangeInput}>
            <option key={null} value="">
              (저자 선택)
            </option>
            {authorList &&
              authorList.map((author: any) => (
                <option key={author.id} value={author.id}>
                  {author.name}
                </option>
              ))}
          </select>
          {(() => {
            const currentSelectedAuthor = (authorList as Array<any>)?.find(
              (author) => author.id === bookData.authorId,
            );

            if (!currentSelectedAuthor) {
              return null;
            }

            return (
              <React.Fragment>
                <label className="secondary">
                  <b>ID</b>
                </label>
                <span className="secondary">{currentSelectedAuthor.id}</span>
                <label>
                  <b>저자명</b>
                </label>
                <span>{currentSelectedAuthor.name}</span>
                {currentSelectedAuthor.description && (
                  <React.Fragment>
                    <label>
                      <b>저자 설명</b>
                    </label>
                    <p>{currentSelectedAuthor.description}</p>
                  </React.Fragment>
                )}
                {currentSelectedAuthor.profileImageUri && (
                  <React.Fragment>
                    <label>
                      <b>저자 썸네일</b>
                    </label>
                    <img
                      src={currentSelectedAuthor.profileImageUri}
                      width="200"
                      alt="author-thumbnail"
                    />
                  </React.Fragment>
                )}
              </React.Fragment>
            );
          })()}
        </div>
        <h3>태그</h3>
        <div className="add-tag">
          <input
            value={tagInput}
            onChange={(e) => setTagInput(e.target.value)}
          />
          <button onClick={onAddTag}>+</button>
        </div>
        {bookData.tagTexts?.map((text) => (
          <div key={text} className="tag-item">
            <LazyBookTag text={text} />
            <button onClick={() => removeFromTags(text)}>-</button>
          </div>
        ))}
        <h3>목차</h3>
        <label className="radio">
          <input
            type="radio"
            name="bookTocType"
            value="AsWhole"
            onChange={onChangeBookTocMethod}
          />
          <span>챕터 없음 (전체 오디오 파일 업로드)</span>
        </label>
        <label className="radio">
          <input
            type="radio"
            name="bookTocType"
            value="WithChapters"
            onChange={onChangeBookTocMethod}
          />
          <span>챕터별 오디오 파일 업로드</span>
        </label>
        <div
          className="book-toc"
          style={{
            display:
              bookTocType === BookTableOfContentsType.AsWhole ? 'flex' : 'none',
          }}
        >
          <label>
            <b>전체 오디오 파일</b> *
          </label>
          <input
            type="file"
            accept="audio/mpeg"
            onChange={onChangeWholeAudioFile}
          />
          <label>
            <b>오디오북 총 길이</b>
          </label>
          <span>
            {bookData.durationPending
              ? 'Loading ...'
              : bookData.duration
              ? formatDuration(bookData.duration)
              : '-'}
          </span>
        </div>
        <div
          className="book-toc"
          style={{
            display:
              bookTocType === BookTableOfContentsType.WithChapters
                ? 'flex'
                : 'none',
          }}
        >
          <button onClick={onAddChapter}>챕터 추가</button>
          {bookData.chapters &&
            bookData.chapters.map((_, index) => {
              if (!bookData.chapters || !bookData.chapters[index]) {
                return;
              }

              const chapter = bookData.chapters[index];

              return (
                <div className="chapter-item" key={index}>
                  <label htmlFor="label">
                    <b>챕터 제목</b> *
                  </label>
                  <input
                    name="label"
                    value={chapter.label || ''}
                    onChange={onChangeChapterInput(index)}
                  />
                  <label htmlFor="audioFile">
                    <b>파일</b>
                  </label>
                  <input
                    type="file"
                    name="audioFile"
                    accept="audio/mpeg"
                    onChange={onChangeChapterAudioFile(index)}
                  />
                  <label>
                    <b>길이</b>
                  </label>
                  <span>
                    {chapter.durationPending
                      ? 'Loading ...'
                      : chapter.duration
                      ? formatDuration(chapter.duration)
                      : '-'}
                  </span>
                  <button onClick={onRemoveChapter(index)}>챕터 삭제</button>
                </div>
              );
            })}
          <label>
            <b>오디오북 총 길이</b>
          </label>
          <span>
            {bookData.chapterTotalDuration
              ? formatDuration(bookData.chapterTotalDuration)
              : '-'}
          </span>
        </div>
        <label>
          <b>썸네일</b>
        </label>
        <input type="file" onChange={onChangeBookThumbnail} accept="image/*" />
        <div className="submit">
          <button onClick={onSubmit} disabled={submitPending || !isDataValid}>
            확인
          </button>
          {createAuthorLoading && <span>저자 생성 중 ...</span>}
          {createAuthorError && <span>에러: {createAuthorError.message}</span>}
          {isCheckingTags && <span>태그 검사 중 ...</span>}
          {createTagLoading && <span>태그 생성 중 ...</span>}
          {createTagError && <span>에러: {createTagError.message}</span>}
          {createBookLoading && <span>책 생성 중...</span>}
          {createBookError && <span>에러: {createBookError.message}</span>}
          {editBookLoading && <span>미디어 파일 적용 중...</span>}
          {editBookError && <span>에러: {editBookError.message}</span>}
          {mediaUploadPending && (
            <span>
              오디오 파일 업로드 중 ...
              {`(${mediaUploadProgress}/${numMedia})`}
            </span>
          )}
        </div>
      </form>
    </StyledModalContent>
  );
};

export default EditBookModal;
