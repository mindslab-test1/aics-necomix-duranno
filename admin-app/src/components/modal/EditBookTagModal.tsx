import React from 'react';
import styled from 'styled-components';
import { StyledTag, BookTagData } from '../book/BookTag';
import { flexbox } from '../../styles/utils';
import { useMutation } from '@apollo/client';
import { EDIT_TAG } from '../../graphql/mutation';

interface EditBookTagModalProps {
  data: {
    id: string;
    text: string;
    color?: string;
    backgroundColor?: string;
  };
}

const StyledModalContent = styled.div`
  ${flexbox({ direction: 'column', align: 'flex-start' })}
  .secondary {
    color: #888888;
  }
  form {
    ${flexbox({ direction: 'column', align: 'flex-start' })}
    * + * {
      margin-top: 8px;
    }
    label {
      margin-top: 16px;
    }
  }
  > * + * {
    margin-top: 16px;
  }
`;

const hexcodeRegex = /^#[0-9A-Fa-f]{6}$/i;

const EditBookTagModal: React.FunctionComponent<EditBookTagModalProps> = (
  props,
) => {
  const { id, text, color, backgroundColor } = props.data;
  const [editTag, { error, loading }] = useMutation(EDIT_TAG);

  const [tagData, setTagData] = React.useState<BookTagData>({
    text,
    color,
    backgroundColor,
  });

  const onChangeInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setTagData((tag) => ({ ...tag, [e.target.name]: e.target.value }));
  };

  const isDataValid =
    tagData.text &&
    (!!tagData.color?.match(hexcodeRegex) ||
      !!tagData.backgroundColor?.match(hexcodeRegex));

  const onSubmit = async () => {
    if (!isDataValid) {
      return;
    }

    await editTag({
      variables: {
        tagId: id,
        text: tagData.text,
        color: tagData.color,
        backgroundColor: tagData.backgroundColor,
      },
    });
  };

  return (
    <StyledModalContent>
      <h2>태그 수정</h2>
      <form>
        <label className="secondary">ID</label>
        <span className="secondary">{id}</span>
        <label htmlFor="text">
          <b>태그</b> *
        </label>
        <input name="text" value={tagData.text} onChange={onChangeInput} />
        <label htmlFor="color">
          <b>텍스트 색</b> (e.g. #000000, 기본: #000000)
        </label>
        <input
          name="color"
          value={tagData.color || ''}
          onChange={onChangeInput}
          placeholder="(없음)"
        />
        <label htmlFor="backgroundColor">
          <b>배경색</b> (e.g. #000000, 기본: 없음)
        </label>
        <input
          name="backgroundColor"
          value={tagData.backgroundColor || ''}
          onChange={onChangeInput}
          placeholder="(없음)"
        />
      </form>
      <h4>미리보기</h4>
      {isDataValid ? (
        <StyledTag data={tagData}>
          <span>{tagData.text}</span>
        </StyledTag>
      ) : (
        <span>태그를 올바르게 설정해주세요.</span>
      )}
      <button disabled={!isDataValid || loading} onClick={onSubmit}>
        확인
      </button>
      {loading && <span>Loading ...</span>}
      {error && <span>{error}</span>}
    </StyledModalContent>
  );
};

export default EditBookTagModal;
