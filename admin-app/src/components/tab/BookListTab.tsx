import React from 'react';
import classnames from 'classnames';
import styled from 'styled-components';
import BookList from '../book/BookList';
import { flexbox } from '../../styles/utils';
import useModal from '../../hooks/modal';
import EditBookModal from '../modal/EditBookModal';

const StyledContainer = styled.div`
  overflow-x: auto;

  form {
    > * + * {
      margin-left: 16px;
    }
    label {
      margin-left: 32px;
      font-weight: bold;
    }
    input,
    select {
      min-width: 100px;
    }
  }

  > * + * {
    margin-top: 16px;
  }

  .book-tab-header {
    ${flexbox({ direction: 'row', justify: 'flex-start' })}
    > * + * {
      margin-left: 20px;
    }
  }
`;

const BookListTab: React.FunctionComponent = () => {
  const {
    Modal,
    open: openCreateBookModal,
    close: closeCreateBookModal,
  } = useModal();

  return (
    <React.Fragment>
      <StyledContainer>
        <div className={classnames('book-tab-header')}>
          <h3>책 목록</h3>
          <button onClick={openCreateBookModal}>+ 새 책</button>
        </div>
        <BookList />
      </StyledContainer>
      <Modal>
        <EditBookModal close={closeCreateBookModal} />
      </Modal>
    </React.Fragment>
  );
};

export default BookListTab;
