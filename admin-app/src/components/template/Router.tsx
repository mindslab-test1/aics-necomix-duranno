import { ApolloProvider } from '@apollo/client';
import React, { lazy, Suspense } from 'react';
import {
  Redirect,
  Route,
  RouteComponentProps,
  Switch,
  withRouter,
} from 'react-router-dom';
import useAuthObserver from '../../hooks/authObserver';
import useGraphQLClient from '../../hooks/gqlClient';

const HomeRoute = lazy(() => import('../routes/HomeRoute'));
const LoginRoute = lazy(() => import('../routes/LoginRoute'));
const DashboardRoute = lazy(() => import('../routes/DashboardRoute'));

interface RouterProps extends RouteComponentProps {
  /* pass */
}

const Router: React.FunctionComponent<RouterProps> = (props: RouterProps) => {
  const { isSigned } = useAuthObserver();
  const client = useGraphQLClient();

  React.useEffect(() => {
    if (!isSigned && props.location.pathname !== '/login') {
      props.history.push('/login');
    } else if (isSigned && props.location.pathname === '/login') {
      props.history.push('/');
    }
  }, [isSigned, props.location]);

  return (
    <Suspense fallback={<div>...</div>}>
      <Switch>
        <Route
          path="/"
          exact
          render={() =>
            client ? (
              <ApolloProvider client={client}>
                <HomeRoute />
              </ApolloProvider>
            ) : (
              <HomeRoute />
            )
          }
        />
        <Route path="/login" render={() => <LoginRoute />} />
        <Route
          path="/dashboard"
          render={() =>
            client ? (
              <ApolloProvider client={client}>
                <DashboardRoute />
              </ApolloProvider>
            ) : (
              <DashboardRoute />
            )
          }
        />
        <Redirect to="/" />
      </Switch>
    </Suspense>
  );
};

export default withRouter(Router);
