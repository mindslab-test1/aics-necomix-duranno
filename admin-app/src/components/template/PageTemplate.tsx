import React from 'react';
import classnames from 'classnames';
import styled from 'styled-components';
import Header from '../navigation/Header';

interface PageTemplateProps {
  children?: React.ReactNode;
}

const StyledPageTemplate = styled.div`
  width: 100%;
  height: 100%;
  .template-header {
    position: absolute;
    width: 100%;
    height: 64px;
    top: 0;
    left: 0;
  }
  .template-content {
    position: absolute;
    top: 64px;
    left: 0;
    width: 100%;
    height: calc(100% - 64px);
    overflow-x: hidden;
    overflow-y: auto;
  }
`;

const PageTemplate: React.FunctionComponent<PageTemplateProps> = (props) => {
  return (
    <StyledPageTemplate>
      <div className={classnames('template-header')}>
        <Header />
      </div>
      <div className={classnames('template-content')}>{props.children}</div>
    </StyledPageTemplate>
  );
};

export default PageTemplate;
