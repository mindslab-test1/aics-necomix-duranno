export function padInteger(value: number, digits: number): string {
  return Math.floor(value).toString().padStart(digits, '0');
}

export function formatDuration(durationInSeconds: number): string {
  const hours = Math.floor(durationInSeconds / 3600.0);
  const minutes = Math.floor(durationInSeconds / 60.0) % 60;
  const seconds = Math.floor(durationInSeconds) % 60;

  if (hours) {
    return `${hours}:${padInteger(minutes, 2)}:${padInteger(seconds, 2)}`;
  }
  return `${minutes}:${padInteger(seconds, 2)}`;
}
