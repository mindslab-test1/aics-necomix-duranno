import { getEnv } from './env';

export const getFirebaseConfig = (): { [key: string]: string } => {
  const env = getEnv('stage');
  if (!env) {
    throw Error('invalid environment variable "STAGE"');
  }

  if (env === 'development') {
    return {
      apiKey: 'AIzaSyBy5kcwmtXl5HM2u7euQgd7EOQt2ue2Kvk',
      authDomain: 'duranno-audiobook-dev.firebaseapp.com',
      databaseURL: 'https://duranno-audiobook-dev.firebaseio.com',
      projectId: 'duranno-audiobook-dev',
      storageBucket: 'duranno-audiobook-dev.appspot.com',
      messagingSenderId: '402830798110',
      appId: '1:402830798110:web:1c308a2df7c9013e30ad0f',
      measurementId: 'G-13X9ECEW0E',
    };
  }

  if (env === 'beta') {
    return {
      apiKey: 'AIzaSyCt2doFeeM9nPByEnh97UB9CoUhQgYTRiY',
      authDomain: 'duranno-audiobook-beta.firebaseapp.com',
      databaseURL: 'https://duranno-audiobook-beta.firebaseio.com',
      projectId: 'duranno-audiobook-beta',
      storageBucket: 'duranno-audiobook-beta.appspot.com',
      messagingSenderId: '558221147887',
      appId: '1:558221147887:web:a7c8e82da42254310f651a',
      measurementId: 'G-DSKMCS6SFX',
    };
  }

  if (env == 'production') {
    return {
      apiKey: 'AIzaSyC_PBY06NNSmnFQ87Qelv2FNoO0Vl_t4BY',
      authDomain: 'duranno-audiobook-release.firebaseapp.com',
      databaseURL: 'https://duranno-audiobook-release.firebaseio.com',
      projectId: 'duranno-audiobook-release',
      storageBucket: 'duranno-audiobook-release.appspot.com',
      messagingSenderId: '203324520214',
      appId: '1:203324520214:web:ebce809fef8c9d8e0bc237',
      measurementId: 'G-7M6L5LK7WN',
    };
  }

  throw Error('invalid environment variable "STAGE"');
};
