export function getEnv(key: string): string | undefined {
  if (!key.includes('REACT_APP')) {
    key = `REACT_APP_${key.toUpperCase()}`;
  } else {
    key = key.toUpperCase();
  }

  return process.env[key];
}
