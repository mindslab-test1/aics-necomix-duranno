import { gql } from '@apollo/client';

export const ME = gql`
  query Me {
    me {
      __typename
      id
      email
      emailVerified
      admin
    }
  }
`;

export const SEARCH_BOOKS = gql`
  query SearchBooks($keyword: String, $tag: String, $cursor: ID, $limit: Int) {
    searchBooks(
      input: { keyword: $keyword, tag: $tag }
      cursor: $cursor
      limit: $limit
    ) {
      items {
        __typename
        id
        title
        description
        author {
          __typename
          id
          name
          profileImageUri
          description
        }
        numViews
        weeklyViews
        numComments
        rating {
          average
          numSubmissions
          items {
            value
            numSubmissions
          }
        }
        tags {
          __typename
          id
          text
          color
          backgroundColor
        }
        duration
        metadataUri
        mediaUri
        thumbnailUri
      }
      cursor
    }
    searchBookCount(input: { keyword: $keyword, tag: $tag })
  }
`;

export const AUTHORS = gql`
  query Authors {
    authorList {
      __typename
      id
      name
      profileImageUri
      description
    }
  }
`;

export const TAG = gql`
  query Tag($text: String!) {
    tag(text: $text) {
      __typename
      id
      text
      color
      backgroundColor
    }
  }
`;

export const TAGS = gql`
  query Tags($texts: [String]!) {
    tags(texts: $texts) {
      __typename
      id
      text
      color
      backgroundColor
    }
  }
`;

export const UPLOAD_BOOK_AUDIO_URI = gql`
  query UploadBookAudioUri($bookId: ID!, $sequence: Int) {
    uploadBookAudioUri(bookId: $bookId, sequence: $sequence)
  }
`;
