import { gql } from '@apollo/client';

export const CREATE_TAG = gql`
  mutation CreateTag($text: String!, $color: String, $backgroundColor: String) {
    createTag(
      input: { text: $text, color: $color, backgroundColor: $backgroundColor }
    ) {
      __typename
      id
      text
      color
      backgroundColor
    }
  }
`;

export const EDIT_TAG = gql`
  mutation EditTag(
    $tagId: ID!
    $text: String!
    $color: String
    $backgroundColor: String
  ) {
    editTag(
      tagId: $tagId
      input: { text: $text, color: $color, backgroundColor: $backgroundColor }
    ) {
      __typename
      id
      text
      color
      backgroundColor
    }
  }
`;

export const CREATE_AUTHOR = gql`
  mutation CreateAuthor(
    $name: String!
    $profileImageUri: String
    $profileImage: Upload
    $description: String
  ) {
    createAuthor(
      input: {
        name: $name
        profileImageUri: $profileImageUri
        profileImage: $profileImage
        description: $description
      }
    ) {
      __typename
      id
      name
      profileImageUri
      description
    }
  }
`;

export const CREATE_BOOK = gql`
  mutation CreateBook(
    $title: String!
    $description: String
    $authorId: ID
    $tags: [ID]
    $duration: Float
    $metadataUri: String
    $metadata: JSON
    $mediaUri: String
    $thumbnailUri: String
    $thumbnail: Upload
  ) {
    createBook(
      input: {
        title: $title
        description: $description
        authorId: $authorId
        tags: $tags
        duration: $duration
        metadataUri: $metadataUri
        metadata: $metadata
        mediaUri: $mediaUri
        thumbnailUri: $thumbnailUri
        thumbnail: $thumbnail
      }
    ) {
      __typename
      id
      title
      description
      author {
        __typename
        id
        name
        profileImageUri
        description
      }
      tags {
        __typename
        id
        text
        color
        backgroundColor
      }
      duration
      metadataUri
      mediaUri
      thumbnailUri
    }
  }
`;

export const EDIT_BOOK = gql`
  mutation EditBook(
    $bookId: ID!
    $title: String!
    $description: String
    $authorId: ID
    $tags: [ID]
    $duration: Float
    $metadataUri: String
    $metadata: JSON
    $mediaUri: String
    $thumbnailUri: String
    $thumbnail: Upload
  ) {
    editBook(
      bookId: $bookId
      input: {
        title: $title
        description: $description
        authorId: $authorId
        tags: $tags
        duration: $duration
        metadataUri: $metadataUri
        metadata: $metadata
        mediaUri: $mediaUri
        thumbnailUri: $thumbnailUri
        thumbnail: $thumbnail
      }
    ) {
      __typename
      id
      title
      description
      author {
        __typename
        id
        name
        profileImageUri
        description
      }
      tags {
        __typename
        id
        text
        color
        backgroundColor
      }
      duration
      metadataUri
      mediaUri
      thumbnailUri
    }
  }
`;
