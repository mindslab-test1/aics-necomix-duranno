import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import Modal from 'react-modal';
import firebase from 'firebase';
import PageTemplate from './components/template/PageTemplate';
import Router from './components/template/Router';

import GlobalStyle from './styles/global';
import { getFirebaseConfig } from './utils/const';

Modal.setAppElement('#root');

const App: React.FunctionComponent<never> = () => {
  const [ready, setReady] = React.useState<boolean>(false);

  React.useEffect(() => {
    firebase.initializeApp(getFirebaseConfig());
    console.log('Initialized firebase app.');
    setReady(true);
  }, []);

  if (!ready) {
    return <div>Initializing firebase app ...</div>;
  }

  return (
    <React.Fragment>
      <GlobalStyle />
      <BrowserRouter>
        <PageTemplate>
          <Router />
        </PageTemplate>
      </BrowserRouter>
    </React.Fragment>
  );
};

export default App;
