interface FlexboxOptions {
  direction: string;
  justify?: string;
  align?: string;
}

export const flexbox = (options: FlexboxOptions): string => `
display: flex;
flex-direction: ${options.direction};
justify-content: ${options.justify || 'center'};
align-items: ${options.align || 'center'};
`;
