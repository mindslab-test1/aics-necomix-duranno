import React from 'react';
import ReactModal from 'react-modal';
import styled from 'styled-components';
import { flexbox } from '../styles/utils';

const StyledCloseButton = styled.button`
  position: absolute;
  right: 24px;
  top: 24px;
  width: 24px;
  height: 24px;
  border: none;
  background-color: #ffffff;
  cursor: pointer;
  &:hover {
    background-color: #e0e0e0;
  }
  ${flexbox({ direction: 'row' })}
`;

const useModal = (): {
  Modal: React.FunctionComponent<{ children?: React.ReactNode }>;
  isOpen: boolean;
  open(): void;
  close(): void;
} => {
  const [isModalOpen, setIsModalOpen] = React.useState<boolean>(false);

  const open = () => setIsModalOpen(true);
  const close = () => setIsModalOpen(false);

  const Modal = (props: { children?: React.ReactNode }) => (
    <ReactModal
      isOpen={isModalOpen}
      style={{
        content: {
          top: '50%',
          left: '50%',
          width: '800px',
          maxHeight: '640px',
          transform: 'translate(-50%, -50%)',
          overflowY: 'auto',
        },
      }}
    >
      {props.children}
      <StyledCloseButton onClick={close}>
        <i className="material-icons md-24">clear</i>
      </StyledCloseButton>
    </ReactModal>
  );

  return { Modal, isOpen: isModalOpen, open, close };
};

export default useModal;
