import {
  ApolloClient,
  InMemoryCache,
  NormalizedCacheObject,
} from '@apollo/client';
import { createUploadLink } from 'apollo-upload-client';
import React from 'react';
import { getEnv } from '../utils/env';
import useAuthObserver from './authObserver';

function offsetFromCursor(items: any, cursor: any, readField: any) {
  // Search from the back of the list because the cursor we're
  // looking for is typically the ID of the last item.
  for (let i = items.length - 1; i >= 0; --i) {
    const item = items[i];
    // Using readField works for both non-normalized objects
    // (returning item.id) and normalized references (returning
    // the id field from the referenced entity object), so it's
    // a good idea to use readField when you're not sure what
    // kind of elements you're dealing with.
    if (readField('id', item) === cursor) {
      // Add one because the cursor identifies the item just
      // before the first item in the page we care about.
      return i + 1;
    }
  }
  // Report that the cursor could not be found.
  return -1;
}

export default function useGraphQLClient():
  | ApolloClient<NormalizedCacheObject>
  | undefined {
  const [client, setClient] = React.useState<
    ApolloClient<NormalizedCacheObject>
  >();
  const { user, isSigned } = useAuthObserver();

  React.useEffect(() => {
    const uri = getEnv('api_uri') + '/graphql';
    const update = async () => {
      const cache = new InMemoryCache({
        typePolicies: {
          Query: {
            fields: {
              searchBooks: {
                keyArgs: false,
                merge(existing, incoming, { args, readField }) {
                  const { items: existingItems = [] } = existing || {};
                  const { items: incomingItems, cursor } = incoming;

                  const mergedItems = existingItems ? [...existingItems] : [];
                  let offset = args?.cursor
                    ? offsetFromCursor(mergedItems, args.cursor, readField)
                    : 0;
                  // If we couldn't find the cursor, default to appending to
                  // the end of the list, so we don't lose any data.
                  if (offset < 0) offset = mergedItems.length;
                  // Now that we have a reliable offset, the rest of this logic
                  // is the same as in offsetLimitPagination.
                  for (let i = 0; i < incomingItems.length; ++i) {
                    mergedItems[offset + i] = incomingItems[i];
                  }

                  return {
                    items: mergedItems,
                    cursor,
                  };
                },
              },
            },
          },
        },
      });
      setClient(
        new ApolloClient({
          cache,
          link: createUploadLink({
            uri,
            headers: {
              Authorization: isSigned
                ? `Bearer ${await user?.getIdToken()}`
                : '',
            },
          }),
        }),
      );
    };
    update();
  }, [user]);

  return client;
}
