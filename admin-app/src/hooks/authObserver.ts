import React from 'react';
import firebase from 'firebase';

export default function useAuthObserver(): {
  user: firebase.User | null;
  isSigned: boolean;
} {
  const [user, setUser] = React.useState<firebase.User | null>(null);

  React.useEffect(() => {
    const unregister = firebase.auth().onAuthStateChanged(setUser);
    return unregister;
  }, []);

  return {
    user,
    isSigned: !!user,
  };
}
