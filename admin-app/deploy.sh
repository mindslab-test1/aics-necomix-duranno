CREDENTIALS_FILE=./aws-credentials.csv
IAM_USER=neocomix-duranno-devops
STAGE=beta

while getopts "s:" opt; do
  case $opt in
    s)
      STAGE=$OPTARG
      ;;
  esac
done

if [ "$STAGE" != "beta" ] && [ "$STAGE" != "production" ]; then
    printf "illegal app stage: %s\n" "$STAGE" >&2
    exit 1
fi

export AWS_ACCESS_KEY_ID=$(cat $CREDENTIALS_FILE | grep $IAM_USER | cut -f3 -d',')
export AWS_SECRET_ACCESS_KEY=$(cat $CREDENTIALS_FILE | grep $IAM_USER | cut -f4 -d',')

S3_BUCKET_NAME=s3://$STAGE.admin.neocomix-audiobook.mindslab-ai

aws s3 cp --recursive --region ap-northeast-2 build/ $S3_BUCKET_NAME
