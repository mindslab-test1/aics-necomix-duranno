CREDENTIALS_FILE=./aws-credentials.csv
IAM_USER=neocomix-duranno-devops
CONFIG_FILE=./serverless.yml

while getopts "c:" opt; do
  case $opt in
    c)
      CONFIG_FILE=$OPTARG
      ;;
  esac
done

export AWS_ACCESS_KEY_ID=$(cat $CREDENTIALS_FILE | grep $IAM_USER | cut -f3 -d',')
export AWS_SECRET_ACCESS_KEY=$(cat $CREDENTIALS_FILE | grep $IAM_USER | cut -f4 -d',')

npx serverless remove --config $CONFIG_FILE
