# duranno-server

Serverless, koa.js, typescript, AWS Cloudformation, AWS RDS 등을 사용하는 백엔드 서비스 템플릿

## 설정

### 개발자 환경 설정

1. **Node.js와 Node.js 패키지를 설치합니다.**

- [Node.js](https://nodejs.org/en/download/)를 설치합니다.
  - 필요한 경우 node version manager ([nvm](https://github.com/nvm-sh/nvm))를 이용하세요.

```
# Node.js 최신 버전(12.x)을 사용하고 있는지 확인하세요.
$ node --version
v12.xx.xx
```

- Node.js 패키지 관리를 위해 [yarn](https://classic.yarnpkg.com/en/docs/install)을 설치하세요.

```
# yarn이 설치되었는지 확인하세요.
$ yarn --version
1.22.4  # or over
```

- 필요한 Node.js 패키지들을 설치하세요.

```
$ yarn
```

2. **로컬 기기에 Oracle Database를 설치하거나, in-house Oracle Database에 접속할 수 있는 사용자를 구하세요.**

프로젝트 담당자에게 문의하여 in-house 데이터베이스에 접근할 수 있는 사용자를 생성합니다. 로컬 API 서버와 테스트용 데이터베이스 연결을 위해 별개의 사용자를 사용해도 좋습니다. 개발 중에는 로컬 기기에 설치된 데이터베이스를 이용하는 것을 권장합니다. (in-house 데이터베이스 연결은 최초 연결 시까지 수 초가 걸릴 수 있습니다.)

데이터베이스 연결 정보를 `.env.local`, `.env.test` 파일에 입력하세요.

```
DB_USERNAME=<username>
DB_PASSWORD=<password>
ORACLEDB_CONNECTION_STRING=<oracledb-connection-string>
```

3. **Oracle Client 라이브러리를 프로젝트에 연동하세요.**

이 프로젝트에서는 `typeorm`과 `oracledb` 패키지를 사용합니다. 그러나, `oracledb` 패키지는 단순히 로컬에 설치된 oracle client 라이브러리를 실행시킨 후 결과를 Node.js 단으로 중계하는 역할만 담당하기 때문에, 개발 기기에 oracle client 라이브러리를 실치한 후 이 프로젝트에 연결해야 합니다.

이를 위해, Oracle Instant Client를 설치하고, `libclntsh.dylib` 라이브러리를 `node_modules/oracledb/build/Release`로 symbolic link를 생성합니다.

```
ln -s <instant-client-directory>/libclntsh.dylib node_modules/oracledb/build/Release/
```

자세한 정보는 [https://github.com/oracle/node-oracledb/blob/master/INSTALL.md]에서 확인하세요.

4. **로컬 서버를 실행하고 작동하는지 확인하세요.**

```
$ yarn start:local
```

### IDE 설정

1. Linter를 사용하세요.

- VS Code를 사용하고 있다면 (추천), **"ESLint"** 패키지를 설치하세요. `.vscode/settings.json`에 정의된 linter 설정이 적용됩니다.

## 개발

### 로컬 환경에서 개발하기

로컬 서버를 실행하세요.

```
$ yarn start:local
```

로컬 서버는 `APP_STAGE=local` 환경에서 실행됩니다. `./src` 이하에 존재하는 파일을 변경하면, 서버가 재시작됩니다. `.env.local` 파일을 변경하여 서버 포트 등 설정을 변경할 수 있습니다.

## 테스트

테스트 환경은 [`mocha`](https://mochajs.org/)와 [`chai`](https://www.chaijs.com/)를 사용합니다.

테스트 커맨드는 `src/` 아래의 모든 `*.test.ts` 파일을 실행합니다.

### 로컬 테스트

먼저 로컬 서버를 실행하세요.

```
$ yarn start:local
```

다른 터미널을 켜고, 테스트 스크립트를 실행하세요.

```
$ yarn test:local
```

## 프로젝트 배포

DevOps 스크립트에 `AWS_ACCESS_KEY_ID`와 `AWS_SECRET_ACCESS_KEY` 환경 변수를 설정하세요. `serverless` 프레임워크를 이용하기 위한 IAM 유저의 인증 정보를 설정해야 합니다. 해당 IAM 유저는 `AdministratorAccess`를 가지고 있는 것이 편리합니다.

### 비밀 키와 환경 변수 설정하기

[AWS SSM](https://ap-northeast-2.console.aws.amazon.com/systems-manager/home)로 이동하세요. 비밀 키나 환경 변수를 저장하기 위해 AWS SSM을 사용합니다. [Parameter Store](https://ap-northeast-2.console.aws.amazon.com/systems-manager/parameters)에서 parameter들을 설정하세요.

AWS SSM에 저장한 키들은 안전하게 저장해야 합니다. 이후 DB migration 등 작업을 수행할 때 해당 키들을 활용하세요.

### 커맨드

- 베타 배포: `$ yarn deploy:beta`
- 베타 배포 내리기: `$ yarn remove:beta`
- 릴리즈 배포: `$ yarn deploy:release`
- 릴리즈 배포 내리기: `$ yarn remove:release`
