# duranno-server

Boilerplate for backend using serverless, koa.js, typescript, AWS CloudFormation, AWS RDS, etc

## Setup

### Environment setup (for developers)

1. **Setup Node.js and node packages.**

- Install [Node.js](https://nodejs.org/en/download/).
  - Use node version manager ([nvm](https://github.com/nvm-sh/nvm)) if necessary.

```
# Ensure that you are using the latest version of node (12.x).
$ node --version
v12.xx.xx
```

- Install [yarn](https://classic.yarnpkg.com/en/docs/install), a package manager for node

```
# Check if yarn is successfully installed.
$ yarn --version
1.22.4  # or over
```

- Install node.js package dependencies.

```
$ yarn
```

2. **Setup Oracle server or acquire connection to in-house oracle server.**

Consult to project manager to create an account for in-house oracle database server. You may use different user for local api server and test process. In development environment, using **local** database is strongly recommended. (Connection to remote database server requires few seconds.)

Write your account credentials to environment configuration file `.env.local` and `.env.test`.

```
DB_USERNAME=<username>
DB_PASSWORD=<password>
ORACLEDB_CONNECTION_STRING=<oracledb-connection-string>
```

3. **Link Oracle Client Libraries to project.**

In this project, we use `typeorm` and `oracledb` npm packages. However, `oracledb` requires local libraries to work with. (It merely executes client libaries and forward the results into Node.js environment.)

In order to do this, install Oracle Instant Client for your OS, then link `libclntsh.dylib` client library to `node_modules/oracledb/build/Release`.

```
ln -s <instant-client-directory>/libclntsh.dylib node_modules/oracledb/build/Release/
```

Check [https://github.com/oracle/node-oracledb/blob/master/INSTALL.md] for more details.

4. **Check if local server is working.**

```
$ yarn start:local
```

### IDE Setup

1. Use Linter.

- If you are using VS Code, install **"ESLint"** package. Then, you are good to go! VS Code will automatically format your code, according to `.vscode/settings.json`.

## Development

### Local development

To run server locally, use:

```
$ yarn start:local
```

The server is executed with environment `APP_STAGE=local`. Since `--respawn` option is enabled for `ts-node-dev`, any changes within `./src` will restart server automatically. `.env.local` file contains configuration for local development. Feel free to adjust some variables (e.g. dev server port, etc)

## Testing

The test environment uses [`mocha`](https://mochajs.org/) and [`chai`](https://www.chaijs.com/).

Test command executes every `*.test.ts` files in `src` directory.

### Local testing

To test your server locally, run the server:

```
$ yarn start:local
```

Then, at another terminal execute:

```
$ yarn test:local
```

## Deployment (for project managers)

On devops script, configure `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` environment variables from your AWS IAM user. The devops IAM user should have `AdministratorAccess` granted to deploy resources.

### Storing parameters and secret keys

Navigate to [AWS SSM](https://ap-northeast-2.console.aws.amazon.com/systems-manager/home) (Systems Manager). We will use SSM to store secret parameters (database master password, jwt signing key, etc). Set your parameters at [Parameter Store](https://ap-northeast-2.console.aws.amazon.com/systems-manager/parameters).

You may set any arbitrary string to those parameters. However, you should securely store those parameters and do not distribute on public. (For example, you may need database master password to handle DB migration, etc)

### Commands

- Beta stage deployment: `$ yarn deploy:beta`
- Beta stgae unmount: `$ yarn remove:beta`
- Release stage deployment: `$ yarn deploy:release`
- Release stgae unmount: `$ yarn remove:release`
