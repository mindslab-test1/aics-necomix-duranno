/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable no-console */
import admin from 'firebase-admin';
import firebase from 'firebase';
import fs from 'fs';

console.log(
  `Generating test id token for stage: ${process.env.APP_STAGE || ''}`
);

process.env.GOOGLE_APPLICATION_CREDENTIALS = `.credentials/firebase-admin-${
  process.env.APP_STAGE || ''
}.json`;
process.env.FIREBASE_PROJECT_NAME = `duranno-audiobook-${
  process.env.APP_STAGE || ''
}`;

admin.initializeApp({
  credential: admin.credential.applicationDefault(),
  databaseURL: `https://${process.env.FIREBASE_PROJECT_NAME}.firebaseio.com`,
});

const firebaseConfig = JSON.parse(
  fs
    .readFileSync(
      `.firebase/firebase-config-${process.env.APP_STAGE || ''}.json`
    )
    .toString()
);

firebase.initializeApp(firebaseConfig);

const uid = 'test';
admin
  .auth()
  .createCustomToken(uid)
  .then((token) => firebase.auth().signInWithCustomToken(token))
  .then((credential) => credential.user?.getIdToken())
  .then((idToken) => console.log(idToken))
  .catch((err) => console.error('Error creating custom token:', err));
