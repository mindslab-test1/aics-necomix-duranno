import {
  GraphQLRequestContext,
  GraphQLRequestContextDidEncounterErrors,
  GraphQLRequestContextExecutionDidStart,
  GraphQLRequestContextValidationDidStart,
  GraphQLRequestContextWillSendResponse,
} from 'apollo-server-types';
import { LogLevel } from '../types';
import { GraphqlContext } from '../types/graphql';

export const parseLogLevel = (level: string): LogLevel => {
  if (level.toLowerCase().includes('error')) {
    return LogLevel.Error;
  }
  if (level.toLowerCase().includes('warn')) {
    return LogLevel.Warning;
  }
  if (level.toLowerCase().includes('info')) {
    return LogLevel.Info;
  }
  if (level.toLowerCase().includes('debug')) {
    return LogLevel.Debug;
  }
  if (level.toLowerCase().includes('verbose')) {
    return LogLevel.Verbose;
  }

  throw Error(`Invalid log level: got ${level}`);
};

const getSeverity = (level: LogLevel): number => {
  switch (level) {
    case LogLevel.Verbose:
      return 100;
    case LogLevel.Debug:
      return 200;
    case LogLevel.Info:
      return 300;
    case LogLevel.Warning:
      return 400;
    case LogLevel.Error:
      return 500;
    default:
      return 0;
  }
};

export class Logger {
  private severity: number;

  constructor(level: LogLevel) {
    this.severity = getSeverity(level);
  }

  public log(level: LogLevel, message: string | Error): void {
    if (this.severity <= getSeverity(level)) {
      // eslint-disable-next-line no-console
      console.log(
        `[${new Date().toISOString()}] [${level.toString()}]`,
        message
      );
      if (message instanceof Error) {
        // eslint-disable-next-line no-console
        console.log(message.stack);
      }
    }
  }

  public error = (message: string | Error): void =>
    this.log(LogLevel.Error, message);

  public warning = (message: string | Error): void =>
    this.log(LogLevel.Warning, message);

  public info = (message: string | Error): void =>
    this.log(LogLevel.Info, message);

  public debug = (message: string | Error): void =>
    this.log(LogLevel.Debug, message);

  public verbose = (message: string | Error): void =>
    this.log(LogLevel.Verbose, message);
}

const filterGraphqlContext = (
  context: GraphqlContext
): Partial<GraphqlContext> => {
  return {
    requestId: context.requestId,
    user: context.user
      ? {
          id: context.user.id,
          emailVerified: context.user.emailVerified,
        }
      : null,
    admin: context.admin,
  };
};

export const gqlLogger = (() => {
  function validationDidStart(
    requestContext: GraphQLRequestContextValidationDidStart<GraphqlContext>
  ) {
    const { context: ctx, document } = requestContext;

    ctx.logger.info(
      `Validation: ${JSON.stringify({
        requestId: ctx.requestId,
        document,
      })}`
    );
  }

  function executionDidStart(
    requestContext: GraphQLRequestContextExecutionDidStart<GraphqlContext>
  ) {
    const { context: ctx, operationName } = requestContext;

    ctx.logger.info(
      `Execution: ${JSON.stringify({
        requestId: ctx.requestId,
        operationName,
      })}`
    );
  }

  function didEncounterErrors(
    requestContext: GraphQLRequestContextDidEncounterErrors<GraphqlContext>
  ) {
    const { context: ctx, errors } = requestContext;

    ctx.logger.info(
      `Error: ${JSON.stringify({
        requestId: ctx.requestId,
        errors: errors.map((err) => err.message),
      })}`
    );
  }

  function willSendResponse(
    requestContext: GraphQLRequestContextWillSendResponse<GraphqlContext>
  ) {
    const {
      context: ctx,
      response: { errors },
    } = requestContext;

    ctx.logger.info(
      `Response: ${JSON.stringify({
        requestId: ctx.requestId,
        success: !errors,
        timestamp: ctx.requestMetadata.timestamp,
        duration: Date.now() - ctx.requestMetadata.timestamp,
      })}`
    );
  }

  function requestDidStart(
    requestContext: GraphQLRequestContext<GraphqlContext>
  ) {
    const { context: ctx } = requestContext;

    ctx.logger.info(`Request: ${JSON.stringify(filterGraphqlContext(ctx))}`);

    return {
      validationDidStart,
      executionDidStart,
      didEncounterErrors,
      willSendResponse,
    };
  }

  function onContextCreationFailed(
    context: Partial<GraphqlContext>,
    error: Error,
    logger: Logger
  ) {
    logger.info(`Request: ${JSON.stringify(context)}`);
    logger.info(`GraphQL context creation failed: ${error.message}`);
  }

  return {
    plugin: {
      requestDidStart,
    },
    onContextCreationFailed,
  };
})();
