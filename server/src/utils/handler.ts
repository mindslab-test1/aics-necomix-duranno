import { ParameterizedContext } from 'koa';
import { ContextState, Handler, HandlerOptions } from '../types/koa';

const checkPrivileged = (ctx: ParameterizedContext<ContextState>): boolean => {
  if (!ctx.state.user) {
    ctx.status = 401;
    return false;
  }

  return true;
};

const handler = (func: Handler, option: HandlerOptions = {}): Handler => {
  return async (ctx) => {
    if (option.privileged && !checkPrivileged(ctx)) {
      return;
    }

    await func(ctx);
  };
};

export default handler;
