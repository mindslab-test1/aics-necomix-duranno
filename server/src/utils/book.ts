type Chapter = {
  label: string;
  mediaUri?: string;
  duration?: number;
};

export type BookMetadata = {
  chapters: Chapter[];
};

export function attachMediaUris(
  metadata: BookMetadata,
  mediaUris: string[]
): Record<string, unknown> {
  const { chapters } = metadata;

  const getSequence = (chapter: Chapter): number =>
    chapters.filter((item) => !!item.duration).indexOf(chapter);

  const attachedChapters = chapters.map((chapter) => {
    if (!chapter.duration) {
      return chapter;
    }

    return { ...chapter, mediaUri: mediaUris[getSequence(chapter)] };
  });

  return { ...metadata, chapters: attachedChapters };
}

export function getMediaUri(
  bookId: string,
  sequence?: number,
  options?: { ext?: string; excludeExt?: boolean }
) {
  const extStr = options?.excludeExt ? '' : options?.ext || '.mp3';

  if (sequence === null || sequence === undefined) {
    return `/${bookId}${extStr}`;
  }

  return `/${bookId}-${sequence}${extStr}`;
}
