export const getBuildStr = (): string => {
  const { APP_NAME, APP_STAGE, APP_VERSION, TEST_ENV } = process.env;
  if (APP_STAGE !== 'test') {
    return `${APP_NAME || 'undefined'}@${APP_VERSION || 'undefined'}-${
      APP_STAGE || 'undefined'
    }`;
  }
  return `${APP_NAME || 'undefined'}@${APP_VERSION || 'undefined'}-${
    TEST_ENV || 'undefined'
  }`;
};

type HashOptions = {
  unsigned?: boolean;
};

export const hashString = (value: string, options?: HashOptions): number => {
  let hash = Array.from(value).reduce(
    (s, c) => Math.imul(31, s) + c.charCodeAt(0),
    0
  );

  if (options?.unsigned) {
    hash += 2147483647;
  }

  return hash;
};
