import jwt, { SignOptions } from 'jsonwebtoken';
import { getSsmVariable } from '../config';

export enum TokenSubject {
  Media = 'MEDIA_TOKEN',
}

async function getSecretKey(): Promise<string> {
  const { APP_STAGE, JWT_SECRET_KEY } = process.env;
  const secretKey =
    APP_STAGE === 'local'
      ? JWT_SECRET_KEY
      : await getSsmVariable('JWT_SECRET_KEY');

  if (!secretKey) {
    throw Error(`JWT_SECRET_KEY missing`);
  }

  return secretKey;
}

export async function generateToken<T extends Record<string, unknown>>(
  data: Partial<T>,
  options?: SignOptions
): Promise<string> {
  const secretKey = await getSecretKey();
  return new Promise((resolve, reject) => {
    jwt.sign(data, secretKey, { ...options }, (err, token) => {
      if (err || !token) {
        reject(err);
      } else {
        resolve(token);
      }
    });
  });
}

export async function verifyToken<T extends Record<string, unknown>>(
  token: string
): Promise<T> {
  const secretKey = await getSecretKey();
  return new Promise((resolve, reject) => {
    jwt.verify(token, secretKey, (err, decoded) => {
      if (err) {
        reject(err);
      } else {
        resolve(decoded as T);
      }
    });
  });
}
