import { ICsvFileWriter, IEmailSender, IFileUploader } from '../types/service';
import { Logger } from '../utils/logger';
import { AWSS3Uploader } from './aws/s3';
import AWSSESEmailSender from './aws/ses';
import CsvWriter from './csv/csvWriter';

type FileUploaderParams = {
  logger: Logger;
};

export const getResourceFileUploader = (() => {
  let instance: IFileUploader | undefined;

  return (params: FileUploaderParams): IFileUploader => {
    if (!instance) {
      const { RESOURCE_BUCKET } = process.env;

      if (!RESOURCE_BUCKET) {
        throw Error('"RESOURCE_BUCKET" env variable not set');
      }

      instance = new AWSS3Uploader(
        {
          destinationBucketName: process.env.RESOURCE_BUCKET || '',
          publicRead: true,
        },
        params.logger
      );
    }

    return instance;
  };
})();

export const getMediaFileUploader = (() => {
  let instance: IFileUploader | undefined;

  return (params: FileUploaderParams): IFileUploader => {
    if (!instance) {
      const { MEDIA_BUCKET } = process.env;

      if (!MEDIA_BUCKET) {
        throw Error('"MEDIA_BUCKET" env variable not set');
      }

      instance = new AWSS3Uploader(
        {
          destinationBucketName: process.env.MEDIA_BUCKET || '',
          publicRead: false,
        },
        params.logger
      );
    }

    return instance;
  };
})();

type EmailSenderParams = {
  logger: Logger;
};

export const getEmailSender = (() => {
  let instance: IEmailSender | undefined;

  return (params: EmailSenderParams): IEmailSender => {
    if (!instance) {
      instance = new AWSSESEmailSender({}, params.logger);
    }

    return instance;
  };
})();

type CsvFileWriterParams = {
  logger: Logger;
  filename: string;
};

export const getCsvFileWriter = (() => {
  let instance: ICsvFileWriter | undefined;

  return (params: CsvFileWriterParams): ICsvFileWriter => {
    if (!instance) {
      instance = new CsvWriter(
        {
          filename: params.filename,
          localDirectory: '.tmp',
        },
        params.logger
      );
    }

    return instance;
  };
})();
