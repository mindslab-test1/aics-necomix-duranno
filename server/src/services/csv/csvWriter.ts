import { createObjectCsvWriter } from 'csv-writer';
import path from 'path';
import { ICsvFileWriter } from '../../types/service';
import { Logger } from '../../utils/logger';

export type CsvWriterConfig = {
  filename?: string;
  localDirectory?: string;
};

const idGenerator = (() => {
  let counter = 0;

  return {
    generate() {
      const value = counter;
      counter += 1;
      return value;
    },
  };
})();

export default class CsvWriter implements ICsvFileWriter {
  private config: CsvWriterConfig;

  private filename: string;

  private logger: Logger;

  constructor(config: CsvWriterConfig, logger: Logger) {
    this.logger = logger;
    this.config = config;

    if (config.filename) {
      this.filename = config.filename;
      if (!this.filename.endsWith('.csv')) {
        this.filename += '.csv';
      }
    } else {
      this.filename = CsvWriter.generateDefaultFilename();
    }
  }

  getFilePath(): string {
    const directory =
      process.env.APP_STAGE === 'local'
        ? this.config.localDirectory || '.'
        : '/tmp';

    return path.resolve(directory, this.filename);
  }

  async write(
    headers: { id: string; title: string }[],
    records: { [key: string]: string }[]
  ): Promise<void> {
    const filePath = this.getFilePath();
    const writer = createObjectCsvWriter({
      path: filePath,
      header: headers,
    });

    try {
      await writer.writeRecords(records);
      this.log(`file successfully written to ${filePath}`);
    } catch (e) {
      this.error(e);
    }
  }

  private static generateDefaultFilename(): string {
    const name = `file-${idGenerator.generate()}.csv`;
    return name;
  }

  private log(message: string): void {
    this.logger.info(`[CSV Writer] ${message}`);
  }

  private error(error: Error): void {
    this.logger.error(`[CSV Writer] an error occurred:`);
    this.logger.error(error);
  }
}
