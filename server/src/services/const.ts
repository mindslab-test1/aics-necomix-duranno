export const IMAGE_DIR = 'images';
export const ETC_DIR = 'etc';

export const AUTHOR_DIR = 'author';

export const AUTHOR_THUMBNAIL_DIR = [IMAGE_DIR, AUTHOR_DIR, 'thumbnail'].join(
  '/'
);

export const BOOK_DIR = 'book';

export const BOOK_THUMBNAIL_DIR = [IMAGE_DIR, BOOK_DIR, 'thumbnail'].join('/');

export const BOOK_METADATA_DIR = [ETC_DIR, BOOK_DIR, 'metadata'].join('/');

export const BANNER_DIR = [ETC_DIR, 'banner'].join('/');
