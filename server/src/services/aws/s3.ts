import AWS from 'aws-sdk';
import path from 'path';
import { Readable } from 'stream';
import { GraphqlFile, GraphqlFileResponse } from '../../types/graphql';
import {
  FileCreateOptions,
  FileUploadOptions,
  IFileUploader,
} from '../../types/service';
import { Logger } from '../../utils/logger';
import { AWS_DEFAULT_REGION, AWS_SIGNED_URI_EXPIRATION } from './const';

export type S3UploadConfig = {
  destinationBucketName: string;
  region?: string;
  publicRead: boolean;
};

type PutObjectOptions = {
  mimetype?: string;
  encoding?: string;
  publicRead?: boolean;
};

const MIME_TO_EXT: { [key: string]: string } = {
  'image/jpeg': 'jpg',
  'image/png': 'png',
  'audio/mpeg': 'mp3',
  'audio/mp3': 'mp3',
};

function generateCreateFilePath(options: FileCreateOptions): string {
  return `${([options.directory, options.name].filter(
    (e) => !!e
  ) as string[]).join('/')}.${options.ext}`;
}

function generateUploadFilePath(
  options: FileUploadOptions,
  mimetype: string
): string {
  if (!Object.keys(MIME_TO_EXT).includes(mimetype)) {
    throw Error(`mime type not supported: ${mimetype}`);
  }

  const ext = MIME_TO_EXT[mimetype];

  return `${([options.directory, options.name || 'file'].filter(
    (e) => !!e
  ) as string[]).join('/')}.${ext}`;
}

export class AWSS3Uploader implements IFileUploader {
  private s3: AWS.S3;

  private logger: Logger;

  public config: S3UploadConfig;

  constructor(config: S3UploadConfig, logger: Logger) {
    AWS.config = new AWS.Config();
    AWS.config.update({
      region: config.region || AWS_DEFAULT_REGION,
      signatureVersion: 'v4',
    });

    this.s3 = new AWS.S3();
    this.config = config;

    this.logger = logger;
  }

  async singleFileUploadResolver(
    file: Promise<GraphqlFile>,
    options?: FileUploadOptions
  ): Promise<GraphqlFileResponse> {
    const fileToUpload = await file;
    const { filename, mimetype, encoding } = fileToUpload;

    this.log(
      `Uploading file: ${JSON.stringify(
        {
          filename,
          mimetype,
          encoding,
        },
        null,
        2
      )} using options ${JSON.stringify(options, null, 2)}`
    );

    const stream = fileToUpload.createReadStream();

    const filePath = generateUploadFilePath(
      options || { name: path.basename(filename, path.extname(filename)) },
      mimetype
    );

    const { Location } = await this.upload(filePath, stream, {
      publicRead: options?.publicRead,
      mimetype,
      encoding,
    });

    return { filename, mimetype, encoding, uri: Location };
  }

  async createFileResolver(
    content: string | Buffer | Uint8Array,
    options: FileCreateOptions
  ): Promise<GraphqlFileResponse> {
    this.log(`Creating file using options ${JSON.stringify(options, null, 2)}`);

    const filePath = generateCreateFilePath(options);
    const stream = Readable.from([content]);

    const { Location } = await this.upload(filePath, stream, options);

    return {
      filename: filePath,
      mimetype: options.mimetype || 'unknown',
      encoding: options.encoding || 'unknown',
      uri: Location,
    };
  }

  async generateSignedUploadUri(options: FileCreateOptions): Promise<string> {
    this.log(
      `Generating file upload uri using options ${JSON.stringify(
        options,
        null,
        2
      )}`
    );

    const params = this.generatePutObjectParams(options);

    return this.s3.getSignedUrlPromise('putObject', {
      ...params,
      Key: generateCreateFilePath(options),
      Expires: AWS_SIGNED_URI_EXPIRATION,
    });
  }

  private upload(key: string, stream: Readable, options?: PutObjectOptions) {
    return this.s3
      .upload({
        Key: key,
        Body: stream,
        ...this.generatePutObjectParams(options),
      })
      .promise();
  }

  private generatePutObjectParams(options?: PutObjectOptions) {
    const { mimetype, encoding, publicRead = this.config.publicRead } =
      options || {};

    const contentType = mimetype
      ? `${mimetype}${
          encoding === 'utf-8' || encoding === 'utf8' ? '; charset=utf-8' : ''
        }`
      : undefined;

    const params = {
      Bucket: this.config.destinationBucketName,
      ACL: publicRead ? 'public-read' : undefined,
      ContentType: contentType,
      ContentEncoding: encoding,
    };

    this.log(`Using PutObject parameters: ${JSON.stringify(params, null, 2)}`);

    return params;
  }

  private log(message: string): void {
    this.logger.info(`[AWS S3 Uploader] ${message}`);
  }
}
