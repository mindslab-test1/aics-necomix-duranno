import AWS from 'aws-sdk';
import nodemailer from 'nodemailer';
import Mail from 'nodemailer/lib/mailer';
import { IEmailSender, SendEmailOptions } from '../../types/service';
import { Logger } from '../../utils/logger';
import { AWS_DEFAULT_REGION } from './const';

type SESConfig = {
  region?: string;
};

export default class AWSSESEmailSender implements IEmailSender {
  private transport: Mail;

  private logger: Logger;

  private config: SESConfig;

  constructor(config: SESConfig, logger: Logger) {
    this.config = config;
    this.logger = logger;

    const awsConfig = new AWS.Config();
    awsConfig.update({
      region: config.region || AWS_DEFAULT_REGION,
      signatureVersion: 'v4',
    });

    this.transport = nodemailer.createTransport({
      SES: new AWS.SES({
        ...awsConfig,
        apiVersion: '2010-12-01',
      }),
    });
  }

  async send(options: SendEmailOptions): Promise<void> {
    try {
      const result = await new Promise((resolve, reject) => {
        this.transport.sendMail(options, (err, info) => {
          if (err) {
            reject(err);
          } else {
            resolve(info);
          }
        });
      });

      this.log(result as string);
    } catch (e) {
      this.logError(e);
    }
  }

  private log(message: string): void {
    this.logger.info(`[AWS SES Email Sender] ${message}`);
  }

  private logError(error: Error): void {
    this.logger.error(`[AWS SES Email Sender] an error occurred:`);
    this.logger.error(error);
  }
}
