import 'oracledb';
import {
  Connection,
  ConnectionManager,
  ConnectionOptions,
  createConnection,
  getConnection,
  getConnectionManager,
} from 'typeorm';
import { getSsmVariable } from '../config';
import entities from '../entities';

export default class Database {
  private connectionManager: ConnectionManager;

  private DEFAULT_CONNECTION_NAME = 'default';

  private TEST_CONNECTION_NAME = 'test';

  constructor() {
    this.connectionManager = getConnectionManager();
  }

  private getConnectionName(): string {
    const { APP_STAGE } = process.env;

    return APP_STAGE === 'test'
      ? this.TEST_CONNECTION_NAME
      : this.DEFAULT_CONNECTION_NAME;
  }

  public async getConnection(): Promise<Connection> {
    const {
      APP_STAGE,
      ORACLEDB_CONNECTION_STRING,
      DB_USERNAME,
      DB_PASSWORD,
    } = process.env;

    const connectionName = this.getConnectionName();

    if (this.connectionManager.has(connectionName)) {
      const connection = getConnection(connectionName);
      if (!connection.isConnected) {
        await connection.connect();
      }

      return connection;
    }

    const connectString =
      APP_STAGE === 'local'
        ? ORACLEDB_CONNECTION_STRING
        : await getSsmVariable('ORACLEDB_CONNECTION_STRING');

    const username =
      APP_STAGE === 'local' ? DB_USERNAME : await getSsmVariable('DB_USERNAME');

    const password =
      APP_STAGE === 'local' ? DB_PASSWORD : await getSsmVariable('DB_PASSWORD');

    const connectionOptions: ConnectionOptions = {
      name: connectionName,
      type: `oracle`,
      entities,
      subscribers: [],
      logging: APP_STAGE === 'local' || APP_STAGE === 'test',
      synchronize: APP_STAGE === 'local',
      connectString,
      username,
      password,
    };

    return createConnection(connectionOptions);
  }

  public async closeConnection(): Promise<void> {
    await this.getConnection().then((connection) => connection.close());
  }
}
