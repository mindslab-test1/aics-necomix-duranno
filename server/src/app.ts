import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import cors from '@koa/cors';
import auth from './middlewares/auth';
import db from './middlewares/db';
import error from './middlewares/error';
import logger from './middlewares/logger';
import apolloServer, { graphqlUploadMiddleware } from './apolloServer';
import routes from './routes';

const app = new Koa();

app.use(logger());
app.use(cors());
app.use(error);
app.use(db());
app.use(auth());
app.use(bodyParser());
app.use(routes.routes()).use(routes.allowedMethods());
app.use(graphqlUploadMiddleware());
app.use(apolloServer().getMiddleware());

export default app;
