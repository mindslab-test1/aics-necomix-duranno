import '../utils/extension';

import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import Book from './book';
import User from './user';

@Entity('ratings')
@Index(['fkBookId', 'value'])
@Unique(['fkBookId', 'fkUserId'])
export default class Rating {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column('uuid', { name: 'fk_user_id' })
  fkUserId!: string;

  @Column('uuid', { name: 'fk_book_id' })
  fkBookId!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column('float')
  value!: number;

  @ManyToOne(() => User, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_user_id' })
  readonly user!: User;

  @ManyToOne(() => Book, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_book_id' })
  readonly book!: Book;
}
