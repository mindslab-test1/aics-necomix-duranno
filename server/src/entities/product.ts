import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('products')
export default class Product {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column({ length: 255, type: 'varchar', unique: true })
  level!: string;

  @Column({ length: 255, type: 'varchar', nullable: true })
  title?: string;

  @Column('integer')
  limit!: number;

  @Column('varchar2', { length: 4000, nullable: true })
  metadata?: string;
}
