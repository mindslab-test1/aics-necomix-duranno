import '../utils/extension';

import DataLoader from 'dataloader';
import {
  Column,
  Connection,
  Entity,
  getRepository,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('authors')
export default class Author {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column({ length: 255, type: 'varchar' })
  name!: string;

  @Column({
    nullable: true,
    length: 255,
    type: 'varchar',
    name: 'profile_image_uri',
  })
  profileImageUri?: string;

  @Column('varchar2', { length: 4000, nullable: true })
  description?: string;
}

export const createAuthorLoader = (getConnection: () => Promise<Connection>) =>
  new DataLoader<string, Author>(async (authorIds) => {
    await getConnection();
    const normalized = await getRepository(Author)
      .findByIds(authorIds as string[])
      .then((authors) =>
        Array.normalize<Author>(authors, (author) => author.id)
      );
    return authorIds.map((authorId) => normalized[authorId]);
  });
