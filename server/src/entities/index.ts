import { Connection } from 'typeorm';
import Author, { createAuthorLoader } from './author';
import Book, { createBookLoader } from './book';
import BookStatistics, { createBookStatsLoader } from './bookStats';
import Comment from './comment';
import Event from './event';
import Feedback from './feedback';
import HomeBanner from './homeBanner';
import PlaylistItem, { createUserPlaylistLoader } from './playlistItem';
import Post from './post';
import Product from './product';
import Rating from './rating';
import Subscription from './subscription';
import Tag, { createBookTagsLoader, createTagsLoader } from './tag';
import User, { createUserLoader, createUserSubscriptionsLoader } from './user';
import UserHistory from './userHistory';
import UserProfile from './userProfile';

const entities = [
  Author,
  Book,
  BookStatistics,
  Comment,
  Event,
  Feedback,
  HomeBanner,
  PlaylistItem,
  Post,
  Product,
  Rating,
  Subscription,
  Tag,
  User,
  UserHistory,
  UserProfile,
];

export const createLoaders = (getConnection: () => Promise<Connection>) => ({
  author: createAuthorLoader(getConnection),
  book: createBookLoader(getConnection),
  bookStats: createBookStatsLoader(getConnection),
  bookTags: createBookTagsLoader(getConnection),
  tag: createTagsLoader(getConnection),
  user: createUserLoader(getConnection),
  userPlaylist: createUserPlaylistLoader(getConnection),
  userSubscriptions: createUserSubscriptionsLoader(getConnection),
});

export type Loaders = ReturnType<typeof createLoaders>;

export default entities;
