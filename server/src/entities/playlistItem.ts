import '../utils/extension';

import DataLoader from 'dataloader';
import {
  Column,
  Connection,
  CreateDateColumn,
  Entity,
  getRepository,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
} from 'typeorm';
import Book from './book';
import User from './user';

@Entity('playlist_items')
@Index(['fkUserId', 'createdAt'])
@Unique(['fkUserId', 'fkBookId'])
export default class PlaylistItem {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column('uuid', { name: 'fk_user_id' })
  fkUserId!: string;

  @Column('uuid', { name: 'fk_book_id' })
  fkBookId!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @ManyToOne(() => User, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_user_id' })
  readonly user!: User;

  @ManyToOne(() => Book, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_book_id' })
  readonly book!: Book;
}

export const createUserPlaylistLoader = (
  getConnection: () => Promise<Connection>
) =>
  new DataLoader<string, PlaylistItem[]>(async (userIds) => {
    await getConnection();
    const normalized = await getRepository(PlaylistItem)
      .createQueryBuilder('playlist_item')
      .where(`"playlist_item"."fk_user_id" IN (:...userIds)`, { userIds })
      .leftJoinAndSelect('playlist_item.user', 'user')
      .leftJoinAndSelect('playlist_item.book', 'book')
      .orderBy('"playlist_item"."fk_user_id"', 'ASC')
      .orderBy('"playlist_item"."created_at"', 'DESC')
      .getMany()
      .then((items) =>
        Array.groupBy<PlaylistItem>(items, (item) => item.fkUserId)
      );

    return userIds.map((userId) => normalized[userId] || []);
  });
