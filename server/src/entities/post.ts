import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import User from './user';

export enum PostCategory {
  Announcement = 'announcement',
}

@Entity('posts')
export default class Post {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  @Index()
  readonly updatedAt!: Date;

  @Column('uuid', { name: 'fk_user_id' })
  fkUserId!: string;

  @Column({ length: 255, type: 'varchar' })
  category!: string;

  @Column({ length: 255, type: 'varchar' })
  title!: string;

  @Column('varchar2', { length: 4000, nullable: true })
  content?: string;

  @Column('char', { length: 1, default: 'N' })
  deleted!: string;

  get isDeleted(): boolean {
    return this.deleted === 'Y';
  }

  set isDeleted(value: boolean) {
    this.deleted = value ? 'Y' : 'N';
  }

  @ManyToOne(() => User, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_user_id' })
  readonly user!: User;
}
