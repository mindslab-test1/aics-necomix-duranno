import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('feedbacks')
export default class Feedback {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column('uuid', { name: 'fk_user_id', nullable: true })
  fkUserId?: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @Index()
  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column({ length: 255, type: 'varchar' })
  type!: string;

  @Column({ length: 255, type: 'varchar' })
  category!: string;

  @Column({ length: 255, type: 'varchar' })
  name!: string;

  @Column({ length: 255, type: 'varchar' })
  email!: string;

  @Column({ length: 255, type: 'varchar' })
  title!: string;

  @Column('varchar2', { length: 4000, nullable: true })
  content?: string;

  public toLocalizedObject(): {
    type: string;
    category: string;
    name: string;
    email: string;
    title: string;
    content: string;
    createdAt: string;
  } {
    const typeString = (() => {
      switch (this.type) {
        case 'individual':
          return '1:1 문의';
        case 'cooperation':
          return '제휴 문의';
        default:
          return '(알 수 없음)';
      }
    })();

    return {
      type: typeString,
      category: this.category,
      name: this.name,
      email: this.email,
      title: this.title,
      content: this.content || '(내용 없음)',
      createdAt: this.createdAt.toLocaleString(),
    };
  }
}

export const FEEDBACK_CSV_COLUMNS: { id: string; title: string }[] = [
  { id: 'type', title: '종류' },
  { id: 'category', title: '문의 유형' },
  { id: 'name', title: '이름' },
  { id: 'email', title: '이메일' },
  { id: 'title', title: '제목' },
  { id: 'content', title: '내용' },
  { id: 'createdAt', title: '작성 시간' },
];
