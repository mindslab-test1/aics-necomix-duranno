import '../utils/extension';

import DataLoader from 'dataloader';
import {
  Column,
  Connection,
  CreateDateColumn,
  Entity,
  getManager,
  getRepository,
  JoinColumn,
  OneToOne,
  PrimaryColumn,
  UpdateDateColumn,
} from 'typeorm';
import Subscription from './subscription';
import UserProfile from './userProfile';

@Entity('users')
export default class User {
  @PrimaryColumn({ type: 'varchar2', length: 255 })
  readonly id!: string;

  @Column('varchar2', { length: 255, nullable: true })
  email?: string;

  get obfuscatedEmail(): string | undefined {
    if (!this.email) {
      return this.email;
    }

    const delimeter = '@';
    return this.email
      .split(delimeter)
      .map((str, idx) => (idx > 0 ? '*'.repeat(str.length) : str))
      .join(delimeter);
  }

  @Column('uuid', { nullable: true, name: 'fk_profile_id' })
  fkProfileId?: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @OneToOne(() => UserProfile, {
    eager: true,
    cascade: true,
    nullable: true,
  })
  @JoinColumn({ name: 'fk_profile_id' })
  readonly profile?: UserProfile;
}

export const createUserLoader = (getConnection: () => Promise<Connection>) =>
  new DataLoader<string, User>(async (userIds) => {
    await getConnection();
    const normalized = await getRepository(User)
      .createQueryBuilder('user')
      .leftJoinAndSelect('user.profile', 'profile')
      .whereInIds(userIds)
      .getMany()
      .then((users) => Array.normalize<User>(users, (user) => user.id));
    return userIds.map((id) => normalized[id]);
  });

export const createUserSubscriptionsLoader = (
  getConnection: () => Promise<Connection>
) =>
  new DataLoader<string, Subscription[]>(async (userIds) => {
    await getConnection();
    const normalized = await getManager()
      .createQueryBuilder(Subscription, 'subscription')
      .leftJoinAndSelect('subscription.product', 'product')
      .where('"subscription"."fk_user_id" IN (:...userIds)', { userIds })
      .orderBy('"subscription"."fk_user_id"', 'ASC')
      .orderBy('"subscription"."updated_at"', 'DESC')
      .getMany()
      .then((items) =>
        Array.groupBy<Subscription>(items, (item) => item.fkUserId)
      );

    return userIds.map((userId) => normalized[userId] || []);
  });
