import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('user_profiles')
export default class UserProfile {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column('char', { length: 1, default: 'N', name: 'promotion_email' })
  allowedPromotionEmail!: string;

  get isAllowedPromotionEmail(): boolean {
    return this.allowedPromotionEmail === 'Y';
  }

  set isAllowedPromotionEmail(value: boolean) {
    this.allowedPromotionEmail = value ? 'Y' : 'N';
  }
}
