import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

export enum EventCategories {
  bookDetails = 'BOOK_DETAILS',
}

export enum EventNames {
  viewBook = 'VIEW_BOOK',
}

@Entity('events')
@Index(['category', 'name', 'updatedAt'])
@Index(['category', 'name', 'fkBookId', 'updatedAt'])
export default class Event {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column('uuid', { name: 'fk_user_id', nullable: true })
  fkUserId?: string;

  @Column('uuid', { name: 'fk_book_id', nullable: true })
  fkBookId?: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column({ length: 255, type: 'varchar' })
  category!: string;

  @Column({ length: 255, type: 'varchar' })
  name!: string;

  @Column('varchar2', { length: 4000, nullable: true })
  value?: string;
}
