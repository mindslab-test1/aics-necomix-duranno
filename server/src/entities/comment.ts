import '../utils/extension';

import {
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import Book from './book';
import User from './user';

@Entity('comments')
export default class Comment {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column('uuid', { name: 'fk_user_id' })
  fkUserId!: string;

  @Column('uuid', { name: 'fk_book_id' })
  fkBookId!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column('varchar2', { length: 4000, nullable: true })
  content?: string;

  @Column('char', { length: 1, default: 'N' })
  deleted!: string;

  @ManyToOne(() => User, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_user_id' })
  readonly user!: User;

  @ManyToOne(() => Book, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_book_id' })
  readonly book!: Book;
}
