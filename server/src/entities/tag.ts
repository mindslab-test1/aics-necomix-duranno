import '../utils/extension';

import DataLoader from 'dataloader';
import {
  Column,
  Connection,
  Entity,
  getManager,
  getRepository,
  PrimaryGeneratedColumn,
} from 'typeorm';
import Book from './book';

@Entity('tags')
export default class Tag {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column({ type: 'varchar2', length: 255, unique: true })
  text!: string;

  @Column({ type: 'varchar2', length: 255, nullable: true })
  color?: string;

  @Column({
    name: 'background_color',
    type: 'varchar2',
    length: 255,
    nullable: true,
  })
  backgroundColor?: string;
}

export const createTagsLoader = (getConnection: () => Promise<Connection>) =>
  new DataLoader<string, Tag>(async (tagTexts) => {
    await getConnection();
    const normalized = await getRepository(Tag)
      .createQueryBuilder()
      .where('"text" IN (:...tagTexts)', { tagTexts })
      .getMany()
      .then((tags) => Array.normalize<Tag>(tags, (tag) => tag.text));
    return tagTexts.map((text) => normalized[text]);
  });

export const createBookTagsLoader = (
  getConnection: () => Promise<Connection>
) =>
  new DataLoader<string, Tag[]>(async (bookIds) => {
    await getConnection();
    const normalized = await getManager()
      .createQueryBuilder(Book, 'book')
      .leftJoinAndSelect('book.tags', 'tag')
      .whereInIds(bookIds)
      .orderBy({ '"tag"."text"': 'ASC' })
      .getMany()
      .then((books) => Array.normalize<Book>(books, (book) => book.id));
    return bookIds.map((bookId) => normalized[bookId]?.tags || []);
  });
