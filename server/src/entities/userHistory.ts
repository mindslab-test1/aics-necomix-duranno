import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  Unique,
  UpdateDateColumn,
} from 'typeorm';
import Book from './book';

@Entity('user_histories')
@Index(['fkUserId', 'updatedAt'])
@Unique(['fkUserId', 'fkBookId'])
export default class UserHistory {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column('uuid', { name: 'fk_user_id' })
  fkUserId!: string;

  @Column('uuid', { name: 'fk_book_id' })
  fkBookId!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column('integer', { nullable: true })
  sequence?: number;

  @Column('varchar', { nullable: true, length: 255, name: 'chapter_title' })
  chapterTitle?: string;

  @Column('varchar', { length: 255, name: 'media_uri' })
  mediaUri!: string;

  @Column('integer')
  position!: number;

  @ManyToOne(() => Book, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_book_id' })
  readonly book!: Book;
}
