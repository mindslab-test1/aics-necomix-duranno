import {
  Column,
  CreateDateColumn,
  Entity,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity('home_banners')
export default class HomeBanner {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column({ length: 255, type: 'varchar', name: 'banner_image_uri' })
  bannerImageUri!: string;

  @Column({ length: 255, type: 'varchar', nullable: true, name: 'link_route' })
  linkRoute!: string | null;

  @Column('varchar2', { length: 4000, nullable: true, name: 'link_route_args' })
  linkRouteArgs!: string | null;

  @Column('char', { length: 1, default: 'N' })
  deleted!: string;

  get isDeleted(): boolean {
    return this.deleted === 'Y';
  }

  set isDeleted(value: boolean) {
    this.deleted = value ? 'Y' : 'N';
  }

  @Column({ type: 'timestamp', name: 'expires_at', nullable: true })
  expiresAt!: Date | null;

  get isExpired(): boolean {
    if (!this.expiresAt) {
      return false;
    }

    return this.expiresAt.getTime() < Date.now();
  }
}
