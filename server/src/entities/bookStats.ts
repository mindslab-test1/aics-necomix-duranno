import DataLoader from 'dataloader';
import {
  Column,
  Connection,
  CreateDateColumn,
  Entity,
  getManager,
  Index,
  JoinColumn,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import Book from './book';

@Entity('book_statistics')
@Index(['weeklyViews', 'fkBookId'])
export default class BookStatistic {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column('uuid', { name: 'fk_book_id' })
  fkBookId!: string;

  @Column('integer', { name: 'num_views', default: 0 })
  numViews!: number;

  @Column('integer', { name: 'weekly_views', default: 0 })
  weeklyViews!: number;

  @OneToOne(() => Book, (book) => book.stats, { cascade: true })
  @JoinColumn({ name: 'fk_book_id' })
  readonly book!: Book;
}

export const createBookStatsLoader = (
  getConnection: () => Promise<Connection>
) =>
  new DataLoader<string, BookStatistic>(async (bookIds) => {
    await getConnection();
    const normalized = await getManager()
      .createQueryBuilder(BookStatistic, 'book_statistic')
      .where('"book_statistic"."fk_book_id" IN (:...bookIds)', { bookIds })
      .orderBy('"book_statistic"."fk_book_id"', 'ASC')
      .getMany()
      .then((items) =>
        Array.normalize<BookStatistic>(items, (item) => item.fkBookId)
      );

    return bookIds.map((bookId) => normalized[bookId]);
  });
