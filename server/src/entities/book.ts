import '../utils/extension';

import DataLoader from 'dataloader';
import {
  Column,
  Connection,
  CreateDateColumn,
  Entity,
  getRepository,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import Author from './author';
import Comment from './comment';
import Rating from './rating';
import Tag from './tag';
import BookStatistic from './bookStats';

@Entity('books')
export default class Book {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column('uuid', { name: 'fk_author_id', nullable: true })
  fkAuthorId!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column({ length: 255, type: 'varchar' })
  title!: string;

  @Column('varchar2', { length: 4000, nullable: true })
  description?: string;

  @Column('float', { nullable: true })
  duration?: number;

  @Column({
    nullable: true,
    length: 255,
    type: 'varchar',
    name: 'metadata_uri',
  })
  metadataUri?: string;

  @Column({ nullable: true, length: 255, type: 'varchar', name: 'media_uri' })
  mediaUri?: string;

  @Column({
    nullable: true,
    length: 255,
    type: 'varchar',
    name: 'thumbnail_uri',
  })
  thumbnailUri?: string;

  @Column('char', { length: 1, default: 'N' })
  deleted!: string;

  get isDeleted(): boolean {
    return this.deleted === 'Y';
  }

  set isDeleted(value: boolean) {
    this.deleted = value ? 'Y' : 'N';
  }

  @ManyToOne(() => Author, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_author_id' })
  readonly author?: Author;

  @OneToMany(() => Rating, (rating) => rating.book)
  ratings!: Rating[];

  @OneToMany(() => Comment, (comment) => comment.book)
  comments!: Comment[];

  @ManyToMany(() => Tag)
  @JoinTable({
    name: 'book_tags',
    joinColumn: {
      name: 'fk_book_id',
    },
    inverseJoinColumn: {
      name: 'fk_tag_id',
    },
  })
  tags!: Tag[];

  @OneToOne(() => BookStatistic, (stats) => stats.book, { nullable: true })
  stats?: BookStatistic;

  async syncTags(tagIds: string[]) {
    this.tags = await getRepository(Tag).findByIds(tagIds);
    await getRepository(Book).save(this);
  }
}

export const createBookLoader = (getConnection: () => Promise<Connection>) =>
  new DataLoader<string, Book>(async (bookIds) => {
    await getConnection();
    const normalized = await getRepository(Book)
      .findByIds(bookIds as string[])
      .then((books) => Array.normalize<Book>(books, (book) => book.id));
    return bookIds.map((bookId) => normalized[bookId]);
  });
