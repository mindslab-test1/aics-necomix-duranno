import {
  Column,
  CreateDateColumn,
  Entity,
  Index,
  JoinColumn,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import Product from './product';
import User from './user';

@Entity('subscriptions')
@Index(['fkUserId', 'updatedAt'])
export default class Subscription {
  @PrimaryGeneratedColumn('uuid')
  readonly id!: string;

  @Column('uuid', { name: 'fk_user_id' })
  fkUserId!: string;

  @CreateDateColumn({ type: 'timestamp', name: 'created_at' })
  readonly createdAt!: Date;

  @UpdateDateColumn({ type: 'timestamp', name: 'updated_at' })
  readonly updatedAt!: Date;

  @Column('char', { length: 1, default: 'N' })
  promotionEnabled!: string;

  @Column('char', { length: 1, default: 'N' })
  cancelled!: string;

  @Column({ length: 255, type: 'varchar', nullable: true })
  provider?: string;

  @Column('uuid', { name: 'fk_product_id' })
  fkProductId!: string;

  @Column('integer', { nullable: true })
  limit?: number;

  @ManyToOne(() => User, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_user_id' })
  readonly user!: User;

  @ManyToOne(() => Product, { cascade: true, eager: true })
  @JoinColumn({ name: 'fk_product_id' })
  readonly product!: Product;

  isActive(): boolean {
    return this.cancelled === 'N';
  }

  isPromotionEnabled(): boolean {
    return this.promotionEnabled === 'Y';
  }
}
