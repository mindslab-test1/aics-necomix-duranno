import '../utils/extension';

import Database from '../db';
import { TestContext, TestMiddleware, TestHandlerOptions } from '../types/test';

const handler = <T = never>(
  func: TestMiddleware<T>,
  options?: TestHandlerOptions<T>
): Mocha.Func => {
  return (done) => {
    const database = new Database();
    database
      .getConnection()
      .then((connection) => {
        const initialContext: TestContext<T> = { connection };
        return initialContext;
      })
      .then((context) =>
        Promise.chain<TestContext<T>>(options?.before || [], context)
      )
      .then(func)
      .then((context) =>
        Promise.chain<TestContext<T>>(options?.after || [], context)
      )
      .then(() => done())
      .catch((err) => done(err));
  };
};

export default handler;
