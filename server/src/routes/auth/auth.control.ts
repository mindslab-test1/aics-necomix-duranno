/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import handler from '../../utils/handler';

export const check = handler(
  (ctx) => {
    ctx.status = 200;
    ctx.body = {
      user_id: ctx.state.user?.id,
    };
  },
  { privileged: true }
);
