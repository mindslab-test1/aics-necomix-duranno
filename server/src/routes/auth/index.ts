import Router from '@koa/router';
import * as authControl from './auth.control';

const auth = new Router();

auth.get('/check', authControl.check);

export default auth;
