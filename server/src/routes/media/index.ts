import Router from '@koa/router';
import * as mediaControl from './media.control';

const media = new Router();

media.get('/token', mediaControl.token);

export default media;
