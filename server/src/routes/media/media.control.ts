import { getManager, getRepository } from 'typeorm';
import Book from '../../entities/book';
import BookStatistic from '../../entities/bookStats';
import Event, { EventCategories, EventNames } from '../../entities/event';
import Subscription from '../../entities/subscription';
import { MediaTokenPayload } from '../../types/token';
import { getMediaUri } from '../../utils/book';
import handler from '../../utils/handler';
import { generateToken, TokenSubject } from '../../utils/token';

type GetTokenQuery = {
  book_id: string;
  sequence?: string;
};

export const token = handler(
  async (ctx) => {
    const { book_id: bookId, sequence } = ctx.query as GetTokenQuery;

    if (!bookId) {
      ctx.status = 400;
      ctx.body = {
        msg: `missing query parameter: book_id`,
      };
      return;
    }

    const { user } = ctx.state;

    if (!user || !user.emailVerified) {
      ctx.status = 401;
      return;
    }

    await ctx.state.getConnection();

    const subscriptions = await getManager()
      .createQueryBuilder(Subscription, 'subscription')
      .select()
      .leftJoinAndSelect('subscription.product', 'product')
      .where({ fkUserId: user.id })
      .getMany();

    const activeSubscription = subscriptions.find((subscription) =>
      subscription.isActive()
    );

    if (!activeSubscription) {
      ctx.status = 403;
      ctx.body = {
        error: 'user is not subscribed',
      };
      return;
    }

    const book = await getManager()
      .createQueryBuilder(Book, 'book')
      .select()
      .where({ id: bookId })
      .getMany()
      .then((books) => books[0]);

    if (!book) {
      ctx.status = 400;
      ctx.body = {
        error: 'invalid bookId',
      };
      return;
    }

    if (book.isDeleted) {
      ctx.status = 400;
      ctx.body = {
        error: 'book is deleted',
      };
      return;
    }

    const userViewHistory = await getManager()
      .createQueryBuilder(Event, 'event')
      .where(
        `
      "event"."fk_user_id" = '${user.id}'
        AND "event"."name" = '${EventNames.viewBook}'
        AND "event"."updated_at" >= (CURRENT_TIMESTAMP - INTERVAL '1' MONTH)
    `
      )
      .getMany();

    const limit = activeSubscription.limit || activeSubscription.product.limit;

    const monthlyCount = new Set<string>(
      userViewHistory.map((item) => item.fkBookId as string).filter((e) => !!e)
    ).size;

    const resourceUri = getMediaUri(
      bookId,
      sequence ? parseInt(sequence, 10) : undefined
    );

    const tokenPayload = {
      resourceUri,
      userId: user.id,
      subscriptionId: activeSubscription.id,
      productId: activeSubscription.fkProductId,
      subscriptionProvider: activeSubscription.provider,
      limit,
      remaining: 0,
    };

    const tokenOptions = {
      subject: TokenSubject.Media,
      expiresIn: '3h',
    };

    const viewBook = async () => {
      const bookStats = await getRepository(BookStatistic)
        .find({ where: { fkBookId: bookId } })
        .then((rows) => rows[0]);
      bookStats.numViews += 1;
      await getRepository(BookStatistic).save(bookStats);

      const event = new Event();
      event.fkBookId = bookId;
      event.fkUserId = user.id;
      event.category = EventCategories.bookDetails.toString();
      event.name = EventNames.viewBook.toString();
      await getRepository(Event).save(event);
    };

    if (userViewHistory.some((item) => item.fkBookId === bookId)) {
      /* user already viewed this book this month, pass */
      await viewBook();
      ctx.status = 200;
      ctx.body = {
        token: await generateToken<MediaTokenPayload>(
          {
            ...tokenPayload,
            remaining: limit - monthlyCount,
          },
          tokenOptions
        ),
      };
      return;
    }

    /* otherwise, check if user did not exceed monthly limit */
    if (monthlyCount < limit) {
      await viewBook();
      ctx.status = 200;
      ctx.body = {
        token: await generateToken<MediaTokenPayload>(
          {
            ...tokenPayload,
            remaining: limit - monthlyCount - 1,
          },
          tokenOptions
        ),
      };
      return;
    }

    ctx.status = 403;
    ctx.body = {
      error: 'exceeded monthly limit',
    };
  },
  { privileged: true }
);
