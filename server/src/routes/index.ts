import Router from '@koa/router';
import { ParameterizedContext } from 'koa';
import { ContextState } from '../types/koa';
import { getBuildStr } from '../utils/misc';
import auth from './auth';
import media from './media';
import user from './user';

const routes = new Router();

routes.get('/', (ctx: ParameterizedContext<ContextState>) => {
  ctx.status = 200;
  ctx.body = {
    msg: `${getBuildStr()} server is alive! :)`,
  };
});

routes.use('/auth', auth.routes());
routes.use('/media', media.routes());
routes.use('/user', user.routes());

export default routes;
