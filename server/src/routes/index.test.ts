import { assert, request } from 'chai';
import handler from '../test';
import testConfig from '../test/config';
import { getBuildStr } from '../utils/misc';

testConfig();

describe(`Running tests for ${getBuildStr()} ...`, () => {
  it(
    'Server is alive and responding',
    handler(async (context) => {
      const response = await request(process.env.HOST).get('/');

      assert.equal(response.status, 200);

      return context;
    })
  );
});
