import { getRepository } from 'typeorm';
import UserHistory from '../../entities/userHistory';
import handler from '../../utils/handler';

type PutUserHistoryQuery = {
  bookId: string;
  sequence?: number;
  chapterTitle?: string;
  mediaUri: string;
  position: number;
};

export const putUserHistory = handler(
  async (ctx) => {
    const { bookId, sequence, chapterTitle, mediaUri, position } = ctx.request
      .body as PutUserHistoryQuery;

    if (!bookId || !mediaUri || !position) {
      ctx.status = 400;
      ctx.body = {
        msg: 'missing request body parameters',
      };
      return;
    }

    const { user } = ctx.state;

    if (!user || !user.emailVerified) {
      ctx.status = 401;
      return;
    }

    await ctx.state.getConnection();

    const repo = getRepository(UserHistory);

    const userHistory =
      (await repo
        .find({
          where: { fkUserId: user.id, fkBookId: bookId },
        })
        .then((result) => result[0])) || new UserHistory();

    userHistory.fkUserId = user.id;
    userHistory.fkBookId = bookId;
    userHistory.sequence = sequence;
    userHistory.chapterTitle = chapterTitle;
    userHistory.mediaUri = mediaUri;
    userHistory.position = position;

    await repo.save(userHistory);

    const returning = await repo
      .findByIds([userHistory.id])
      .then((rows) => rows[0]);

    ctx.status = 200;
    ctx.body = {
      history: returning,
    };
  },
  { privileged: true }
);
