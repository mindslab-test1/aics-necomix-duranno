import Router from '@koa/router';
import * as userControl from './user.control';

const user = new Router();

user.put('/history', userControl.putUserHistory);

export default user;
