import { GraphqlFile, GraphqlFileResponse } from './graphql';

export type FileCreateOptions = {
  directory?: string;
  name: string;
  ext: string;
  mimetype?: string;
  encoding?: BufferEncoding;
  publicRead?: boolean;
};

export type FileUploadOptions = {
  directory?: string;
  name?: string;
  publicRead?: boolean;
};

export interface IFileUploader {
  singleFileUploadResolver: (
    file: Promise<GraphqlFile>,
    options?: FileUploadOptions
  ) => Promise<GraphqlFileResponse>;
  createFileResolver: (
    content: string | Buffer | Uint8Array,
    options: FileCreateOptions
  ) => Promise<GraphqlFileResponse>;
  generateSignedUploadUri: (options: FileCreateOptions) => Promise<string>;
}

export interface ICsvFileWriter {
  getFilePath(): string;
  write(
    headers: { id: string; title: string }[],
    records: { [key: string]: string }[]
  ): Promise<void>;
}

type EmailAddress = { name: string; address: string };

export type SendEmailOptions = {
  from: string | EmailAddress;
  to: string | EmailAddress | Array<string | EmailAddress>;
  cc?: string | EmailAddress | Array<string | EmailAddress>;
  bcc?: string | EmailAddress | Array<string | EmailAddress>;
  subject: string;
  text: string;
  attachments: {
    filename: string;
    path: string;
  }[];
};

export interface IEmailSender {
  send(options: SendEmailOptions): Promise<void>;
}
