import { Connection } from 'typeorm';

export interface TestContext<T> {
  connection: Connection;
  data?: T;
}

export type TestMiddleware<T = never> = (
  context: TestContext<T>
) => Promise<TestContext<T>>;

export interface TestHandlerOptions<T> {
  before?: TestMiddleware<T>[];
  after?: TestMiddleware<T>[];
}
