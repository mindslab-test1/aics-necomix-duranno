export type TokenPayload = {
  iat: number;
  exp: number;
  sub: string;
  iss: string;
};

export type MediaTokenPayload = TokenPayload & {
  resourceUri: string;
  userId: string;
  subscriptionId: string;
  productId: string;
  subscriptionProvider: string;
  limit: number;
  remaining: number;
};
