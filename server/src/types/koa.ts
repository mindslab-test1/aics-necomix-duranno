import { ParameterizedContext } from 'koa';
import { Connection } from 'typeorm';
import { Logger } from '../utils/logger';

export interface ContextState {
  getConnection(): Promise<Connection>;
  logger: Logger;
  user?: {
    id: string;
    email?: string;
    emailVerified?: boolean;
  };
  admin?: boolean;
}

export type Handler = (
  ctx: ParameterizedContext<ContextState>
) => Promise<void> | void;

export interface HandlerOptions {
  privileged?: boolean;
}
