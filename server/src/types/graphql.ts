import { Readable } from 'stream';
import { Connection } from 'typeorm';
import { Loaders } from '../entities';
import { Logger } from '../utils/logger';

export type GraphqlContext = {
  requestId: string;
  requestMetadata: {
    timestamp: number;
  };
  user: {
    id: string;
    email?: string;
    emailVerified?: boolean;
  } | null;
  admin?: boolean;
  loaders: Loaders;
  getConnection(): Promise<Connection>;
  logger: Logger;
};

export type GraphqlFile = {
  createReadStream(): Readable;
  filename: string;
  mimetype: string;
  encoding: string;
};

export type GraphqlFileResponse = {
  filename: string;
  mimetype: string;
  encoding: string;
  uri: string;
};
