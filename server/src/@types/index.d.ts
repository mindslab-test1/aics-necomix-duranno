export declare global {
  interface PromiseConstructor {
    chain<T>(
      promises: ((value: T) => Promise<T>)[],
      initialValue: T
    ): Promise<T>;
  }

  interface ArrayConstructor {
    normalize<T>(
      array: T[],
      selector: (item: T) => string | number
    ): { [key: string]: T };

    groupBy<T>(
      array: T[],
      identifier: (item: T) => string | number
    ): { [key: string]: T[] };
  }
}
