import {
  ApolloError,
  AuthenticationError,
  gql,
  IResolvers,
} from 'apollo-server-koa';
import { getRepository } from 'typeorm';
import HomeBanner from '../entities/homeBanner';
import { getResourceFileUploader } from '../services';
import { BANNER_DIR } from '../services/const';
import { GraphqlContext, GraphqlFile } from '../types/graphql';

export const typeDef = gql`
  type HomeBanner {
    id: ID!
    createdAt: Date!
    updatedAt: Date!
    expiresAt: Date
    bannerImageUri: String!
    linkRoute: String
    linkRouteArgs: JSON
    isActive: Boolean!
    isDeleted: Boolean!
    isExpired: Boolean!
  }

  type HomeBannerQueries {
    all: [HomeBanner]!
  }

  type HomeBannerOps {
    create(input: HomeBannerInput!): HomeBanner
    update(input: HomeBannerInput!): HomeBanner
    remove: Boolean!
  }

  input HomeBannerInput {
    bannerImage: Upload
    bannerImageUri: String
    linkRoute: String
    linkRouteArgs: JSON
    expiresAt: Date
  }

  extend type Query {
    HomeBanner: HomeBannerQueries
  }

  extend type Mutation {
    # admin
    HomeBanner(id: ID): HomeBannerOps
  }
`;

type HomeBannerInput = {
  bannerImage?: Promise<GraphqlFile>;
  bannerImageUri?: string;
  linkRoute?: string;
  linkRouteArgs?: unknown;
  expiresAt?: Date;
};

export const resolvers: IResolvers = {
  HomeBanner: {
    isActive: (parent: HomeBanner) => !parent.isDeleted && !parent.isExpired,
    isDeleted: (parent: HomeBanner) => parent.isDeleted,
    isExpired: (parent: HomeBanner) => parent.isExpired,
  },
  Query: {
    HomeBanner: async (
      parent: unknown,
      args: unknown,
      { getConnection }: GraphqlContext
    ) => {
      await getConnection();

      const all = async () => {
        const banners = await getRepository(HomeBanner)
          .createQueryBuilder('home_banner')
          .where(
            `
  "home_banner"."deleted" = 'N' 
    AND ("home_banner"."expires_at" IS NULL OR "home_banner"."expires_at" > CURRENT_TIMESTAMP)
          `
          )
          .getMany();

        return banners;
      };

      return { all };
    },
  },
  Mutation: {
    HomeBanner: async (
      parent: unknown,
      { id }: { id?: string },
      { admin, user, logger, getConnection }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();
      const repo = getRepository(HomeBanner);

      const banner = id
        ? await repo.findByIds([id]).then((rows) => rows[0])
        : undefined;

      const log = ({ name, args }: { name: string; args: unknown }) => {
        logger.info(
          `User ${
            user.id
          } issued HomeBanner.${name} mutation with arguments ${JSON.stringify(
            args,
            null,
            2
          )}`
        );
      };

      const verifyInput = (input: HomeBannerInput) => {
        if (input.bannerImage && input.bannerImageUri) {
          throw new ApolloError(
            'Cannot provide both bannerImage and bannerImageUri',
            'BAD_REQUEST'
          );
        }
        if (!input.bannerImage && !input.bannerImageUri) {
          throw new ApolloError(
            'Should provide one of bannerImage or bannerImageUri',
            'BAD_REQUEST'
          );
        }
      };

      const create = async ({ input }: { input: HomeBannerInput }) => {
        verifyInput(input);

        log({ name: 'create', args: { id, input } });

        const newBanner = new HomeBanner();

        if (input.bannerImageUri) {
          newBanner.bannerImageUri = input.bannerImageUri;
        }

        newBanner.expiresAt = input.expiresAt || null;
        newBanner.linkRoute = input.linkRoute || null;
        newBanner.linkRouteArgs = input.linkRouteArgs
          ? JSON.stringify(input.linkRouteArgs)
          : null;

        await repo.save(newBanner);

        if (input.bannerImage) {
          const { uri } = await getResourceFileUploader({
            logger,
          }).singleFileUploadResolver(input.bannerImage, {
            directory: BANNER_DIR,
            name: newBanner.id,
          });

          newBanner.bannerImageUri = uri;
          await repo.save(newBanner);
        }

        return newBanner;
      };

      const update = async ({ input }: { input: HomeBannerInput }) => {
        verifyInput(input);

        log({ name: 'update', args: { id, input } });

        if (!banner) {
          return null;
        }

        if (input.bannerImageUri) {
          banner.bannerImageUri = input.bannerImageUri;
        }

        banner.expiresAt = input.expiresAt || null;
        banner.linkRoute = input.linkRoute || null;
        banner.linkRouteArgs = input.linkRouteArgs
          ? JSON.stringify(input.linkRouteArgs)
          : null;

        if (input.bannerImage) {
          const { uri } = await getResourceFileUploader({
            logger,
          }).singleFileUploadResolver(input.bannerImage, {
            directory: BANNER_DIR,
            name: banner.id,
          });

          banner.bannerImageUri = uri;
        }

        await repo.save(banner);

        return banner;
      };

      const remove = async () => {
        log({ name: 'remove', args: { id } });

        if (!banner) {
          return false;
        }

        banner.isDeleted = true;
        await getRepository(HomeBanner).save(banner);

        return true;
      };

      return {
        create,
        update,
        remove,
      };
    },
  },
};
