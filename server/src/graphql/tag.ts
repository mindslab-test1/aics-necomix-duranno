import { AuthenticationError, gql, IResolvers } from 'apollo-server-koa';
import { getRepository } from 'typeorm';
import Tag from '../entities/tag';
import { GraphqlContext } from '../types/graphql';

export const typeDef = gql`
  type Tag {
    id: ID!
    text: String
    color: String
    backgroundColor: String
  }

  input TagInput {
    text: String!
    color: String
    backgroundColor: String
  }

  extend type Query {
    # admin
    tag(text: String!): Tag
    tags(texts: [String]!): [Tag]!
  }

  extend type Mutation {
    # admin
    createTag(input: TagInput!): Tag
    editTag(tagId: ID!, input: TagInput!): Tag
    deleteTag(tagId: ID!): Boolean!
  }
`;

type TagInput = {
  text: string;
  color?: string;
  backgroundColor?: string;
};

type TagQueryArgs = {
  text: string;
};

type TagsQueryArgs = {
  texts: string[];
};

type CreateTagMutationArgs = {
  input: TagInput;
};

type EditTagMutationArgs = {
  tagId: string;
  input: TagInput;
};

type DeleteTagMutationArgs = {
  tagId: string;
};

export const resolvers: IResolvers = {
  Query: {
    tag: async (
      parent: unknown,
      { text }: TagQueryArgs,
      { admin, user, loaders, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      logger.info(
        `User ${user.id} issued tag query with arguments ${JSON.stringify({
          text,
        })}`
      );

      return loaders.tag.load(text);
    },
    tags: async (
      parent: unknown,
      { texts }: TagsQueryArgs,
      { admin, user, loaders, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      logger.info(
        `User ${user.id} issued tags query with arguments ${JSON.stringify({
          texts,
        })}`
      );

      return loaders.tag.loadMany(texts);
    },
  },
  Mutation: {
    createTag: async (
      parent: unknown,
      { input }: CreateTagMutationArgs,
      { admin, user, loaders, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      logger.info(
        `User ${
          user.id
        } issued createTag mutation with arguments ${JSON.stringify({ input })}`
      );

      if (await loaders.tag.load(input.text)) {
        return null;
      }

      const tag = new Tag();
      tag.text = input.text;
      tag.color = input.color;
      tag.backgroundColor = input.backgroundColor;

      await getRepository(Tag).save(tag);

      return tag;
    },
    editTag: async (
      parent: unknown,
      { tagId, input }: EditTagMutationArgs,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      logger.info(
        `User ${
          user.id
        } issued editTag mutation with arguments ${JSON.stringify({
          tagId,
          input,
        })}`
      );

      const repo = getRepository(Tag);

      const tag = await repo.findByIds([tagId]).then((tags) => tags[0]);

      if (!tag) {
        return null;
      }

      tag.text = input.text;
      tag.color = input.color;
      tag.backgroundColor = input.backgroundColor;

      await getRepository(Tag).save(tag);

      return tag;
    },
    deleteTag: async (
      parent: unknown,
      { tagId }: DeleteTagMutationArgs,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      logger.info(
        `User ${
          user.id
        } issued deleteTag mutation with arguments ${JSON.stringify({ tagId })}`
      );

      const repo = getRepository(Tag);

      const tag = await repo.findByIds([tagId]).then((tags) => tags[0]);

      if (!tag) {
        return false;
      }

      await repo.remove(tag);

      return true;
    },
  },
};
