import {
  ApolloError,
  AuthenticationError,
  gql,
  IResolvers,
} from 'apollo-server-koa';
import { getRepository } from 'typeorm';
import Comment from '../entities/comment';
import Rating from '../entities/rating';
import { GraphqlContext } from '../types/graphql';

export const typeDef = gql`
  type Comment {
    id: ID!
    user: User
    book: Book
    createdAt: Date
    updatedAt: Date
    content: String
    rating: Float
  }

  type PaginatedComments {
    id: ID!
    comments: [Comment]!
    cursor: ID
  }

  extend type Mutation {
    writeComment(bookId: ID!, content: String!): Comment
    editComment(commentId: ID!, content: String!): Comment
    deleteComment(commentId: ID!): Boolean!
  }
`;

type WriteCommentMutationArgs = {
  bookId: string;
  content: string;
};

type EditCommentMutationArgs = {
  commentId: string;
  content: string;
};

type DeleteCommentMutationArgs = {
  commentId: string;
};

export const resolvers: IResolvers = {
  Comment: {
    book: (parent: Comment) => parent.book,
    user: (parent: Comment) => parent.user,
    rating: async (
      parent: Comment,
      args: unknown,
      { getConnection }: GraphqlContext
    ) => {
      await getConnection();
      return getRepository(Rating)
        .find({
          where: { fkUserId: parent.fkUserId, fkBookId: parent.fkBookId },
        })
        .then((ratings) => ratings[0]?.value);
    },
  },
  Mutation: {
    writeComment: async (
      parent: unknown,
      { bookId, content }: WriteCommentMutationArgs,
      { user, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      if (content.length > 1000) {
        throw new ApolloError('Content too long');
      }
      const isSubscribed = await loaders.userSubscriptions
        .load(user.id)
        .then((subscriptions) =>
          subscriptions.some((subscription) => subscription.isActive())
        );
      if (!isSubscribed) {
        throw new AuthenticationError('User is not subscribed');
      }

      const repo = getRepository(Comment);

      const comment = new Comment();
      comment.fkUserId = user.id;
      comment.fkBookId = bookId;
      comment.content = content;

      await repo.save(comment);

      return repo.findByIds([comment.id]).then((rows) => rows[0]);
    },
    editComment: async (
      parent: unknown,
      { commentId, content }: EditCommentMutationArgs,
      { user, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      if (content.length > 1000) {
        throw new ApolloError('Content too long');
      }

      const isSubscribed = await loaders.userSubscriptions
        .load(user.id)
        .then((subscriptions) =>
          subscriptions.some((subscription) => subscription.isActive())
        );
      if (!isSubscribed) {
        throw new AuthenticationError('User is not subscribed');
      }

      const repo = getRepository(Comment);
      const comment = await repo.findByIds([commentId]).then((rows) => rows[0]);

      if (!comment) {
        return null;
      }

      if (comment.fkUserId !== user.id) {
        throw new AuthenticationError('This is not your comment');
      }

      comment.content = content;

      await repo.save(comment);

      return comment;
    },
    deleteComment: async (
      parent: unknown,
      { commentId }: DeleteCommentMutationArgs,
      { user, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      const isSubscribed = await loaders.userSubscriptions
        .load(user.id)
        .then((subscriptions) =>
          subscriptions.some((subscription) => subscription.isActive())
        );
      if (!isSubscribed) {
        throw new AuthenticationError('User is not subscribed');
      }

      const repo = getRepository(Comment);
      const comment = await repo.findByIds([commentId]).then((rows) => rows[0]);

      if (!comment) {
        return false;
      }

      if (comment.fkUserId !== user.id) {
        throw new AuthenticationError('This is not your comment');
      }

      comment.deleted = 'Y';

      await repo.save(comment);

      return true;
    },
  },
};
