import {
  ApolloError,
  AuthenticationError,
  gql,
  IResolvers,
} from 'apollo-server-koa';
import { getManager, getRepository } from 'typeorm';
import Book from '../entities/book';
import BookStatistic from '../entities/bookStats';
import Comment from '../entities/comment';
import Rating from '../entities/rating';
import UserHistory from '../entities/userHistory';
import { getResourceFileUploader } from '../services';
import { BOOK_METADATA_DIR, BOOK_THUMBNAIL_DIR } from '../services/const';
import { GraphqlContext, GraphqlFile } from '../types/graphql';
import { hashString } from '../utils/misc';

export const typeDef = gql`
  type BookRatingItem {
    value: Float!
    numSubmissions: Int!
  }

  type BookRating {
    average: Float
    me: Float
    numSubmissions: Int
    items: [BookRatingItem]
  }

  type Book {
    id: ID!
    title: String
    description: String
    author: Author
    numViews: Int
    weeklyViews: Int
    numComments: Int
    rating: BookRating
    tags: [Tag]
    duration: Float
    metadataUri: String
    mediaUri: JSON
    thumbnailUri: String
    isDeleted: Boolean
    isInMyPlaylist: Boolean
    myHistory: UserHistory
  }

  input BookInput {
    title: String!
    description: String
    authorId: ID
    tags: [ID] = []
    duration: Float
    metadataUri: String
    metadata: JSON
    mediaUri: String
    thumbnailUri: String
    thumbnail: Upload
  }

  input BookSearchInput {
    tag: String
    keyword: String
    keywordScope: [String]
    orderBy: String
  }

  type PaginatedBooks {
    items: [Book]!
    cursor: ID
  }

  extend type Query {
    book(bookId: String!): Book
    bookComments(bookId: String!, cursor: ID, limit: Int): PaginatedComments
    searchBooks(
      input: BookSearchInput
      cursor: ID
      limit: Int
      showDeleted: Boolean
    ): PaginatedBooks!
    searchBookCount(input: BookSearchInput, showDeleted: Boolean): Int!
    featuredBooks(limit: Int): [Book]!
  }

  extend type Mutation {
    rateBook(bookId: ID!, value: Int!): Boolean!
    deleteRating(bookId: ID!): Boolean!
    # admin
    createBook(input: BookInput!): Book!
    editBook(bookId: ID!, input: BookInput!): Book
    deleteBook(bookId: ID!, soft: Boolean): Boolean!
  }
`;

type BookRatingItem = {
  value: number;
  numSubmissions: number;
};

type BookRating = {
  average: number;
  me: number;
  numSubmissions: number;
  items: BookRatingItem[];
};

type BookInput = {
  title: string;
  description?: string;
  authorId: string;
  tags: string[];
  duration?: number;
  metadataUri?: string;
  metadata?: unknown;
  mediaUri?: string;
  thumbnailUri?: string;
  thumbnail?: Promise<GraphqlFile>;
};

const ALLOWED_ORDER_BY = ['weeklyViews', 'updatedAt', 'keyword'];
const ALLOWED_KEYWORD_SCOPE = ['title', 'authorName'];
const JWS_THRESHOLD = 60;
type BookSearchInput = {
  tag?: string;
  keyword?: string;
  keywordScope?: string[];
  orderBy?: string;
};

type BookQueryArgs = {
  bookId: string;
};

type BookCommentsQueryArgs = {
  bookId: string;
  cursor?: string;
  limit?: number;
};

type FeaturedBooksQueryArgs = {
  limit?: number;
};

type SearchBooksQueryArgs = {
  input?: BookSearchInput;
  cursor?: string;
  limit?: number;
  showDeleted?: boolean;
};

type SearchBookCountQueryArgs = {
  input?: BookSearchInput;
  showDeleted?: boolean;
};

type RateBookMutationArgs = {
  bookId: string;
  value: number;
};

type DeleteRatingMutationArgs = {
  bookId: string;
};

type CreateBookMutationArgs = {
  input: BookInput;
};

type EditBookMutationArgs = {
  bookId: string;
  input: BookInput;
};

type DeleteBookMutationArgs = {
  bookId: string;
  soft?: boolean;
};

function validateBookInput(input: BookInput): void {
  if (input.thumbnail && input.thumbnailUri) {
    throw new ApolloError(
      'Cannot provide both thumbnail and thumbnailUri',
      'BAD_REQUEST'
    );
  }

  if (input.metadata && input.metadataUri) {
    throw new ApolloError(
      'Cannot provide both metadata and metadataUri',
      'BAD_REQUEST'
    );
  }

  if (input.duration !== undefined && input.duration < 0) {
    throw new ApolloError('duration is negative', 'BAD_REQUEST');
  }
}

export const resolvers: IResolvers = {
  Book: {
    isDeleted: (parent: Book) => parent.isDeleted,
    author: (parent: Book, args: unknown, { loaders }: GraphqlContext) => {
      if (!parent.fkAuthorId) {
        return null;
      }
      return parent.author || loaders.author.load(parent.fkAuthorId);
    },
    numViews: async (
      parent: Book,
      args: unknown,
      { loaders }: GraphqlContext
    ) => {
      if (parent.stats) {
        return parent.stats.numViews;
      }

      const stat = await loaders.bookStats.load(parent.id);

      if (stat) {
        return stat.numViews;
      }

      return 0;
    },
    weeklyViews: async (
      parent: Book,
      args: unknown,
      { loaders }: GraphqlContext
    ) => {
      if (parent.stats) {
        return parent.stats.weeklyViews;
      }

      const stat = await loaders.bookStats.load(parent.id);

      if (stat) {
        return stat.weeklyViews;
      }

      return 0;
    },
    numComments: async (
      parent: Book,
      args: unknown,
      { getConnection }: GraphqlContext
    ) => {
      await getConnection();

      type QueryResult = {
        numComments: number;
      };

      const result = (await getRepository(Comment)
        .createQueryBuilder()
        .select('COUNT("id") AS "numComments"')
        .where({ fkBookId: parent.id })
        .execute()) as QueryResult[];

      return result[0]?.numComments;
    },
    rating: async (
      parent: Book,
      args: unknown,
      { getConnection, user }: GraphqlContext
    ) => {
      await getConnection();
      const repo = getRepository(Rating);

      const items: BookRatingItem[] = (await repo
        .createQueryBuilder()
        .select('"value", COUNT("id") AS "numSubmissions"')
        .where({ fkBookId: parent.id })
        .groupBy('"value"')
        .execute()) as BookRatingItem[];

      const me =
        user && user.emailVerified
          ? (await repo
              .find({ where: { fkBookId: parent.id, fkUserId: user.id } })
              .then((ratings) => ratings[0]?.value)) || null
          : null;

      const { average, numSubmissions } = await repo
        .createQueryBuilder()
        .select('COUNT("id") AS "numSubmissions", AVG("value") AS "average"')
        .where({ fkBookId: parent.id })
        .execute()
        .then((rows: Partial<BookRating>[]) => rows[0]);

      return {
        average,
        me,
        items,
        numSubmissions,
      };
    },
    tags: (parent: Book, args: unknown, { loaders }: GraphqlContext) => {
      return loaders.bookTags.load(parent.id);
    },
    isInMyPlaylist: (
      parent: Book,
      args: unknown,
      { user, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        return null;
      }

      return loaders.userPlaylist
        .load(user.id)
        .then((playlistItems) =>
          playlistItems.some((item) => item.fkBookId === parent.id)
        );
    },
    myHistory: async (
      parent: Book,
      args: unknown,
      { user, getConnection }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        return null;
      }

      await getConnection();

      return getRepository(UserHistory)
        .find({ where: { fkUserId: user.id, fkBookId: parent.id } })
        .then((rows) => rows[0]);
    },
  },
  Query: {
    book: async (
      parent: unknown,
      { bookId }: BookQueryArgs,
      { loaders }: GraphqlContext
    ) => loaders.book.load(bookId),
    bookComments: async (
      parent: unknown,
      { bookId, cursor, limit = 20 }: BookCommentsQueryArgs,
      { loaders }: GraphqlContext
    ) => {
      if (limit > 64) {
        throw new ApolloError('Max limit is 64', 'BAD_REQUEST');
      }

      const book = await loaders.book.load(bookId);
      if (!book) {
        return null;
      }

      const commentAtCursor = cursor
        ? await getRepository(Comment)
            .findByIds([cursor])
            .then((comments) => comments[0])
        : null;

      type CommentsSubQueryRow = {
        comment_id: string;
        comment_fk_user_id: string;
        comment_fk_book_id: string;
        comment_created_at: Date;
        comment_updated_at: Date;
        comment_content?: string;
        comment_deleted: string;
      };

      const commentsSubQb = getManager()
        .createQueryBuilder(Comment, 'comment')
        .leftJoinAndSelect('comment.user', 'user')
        .where(
          `"comment"."fk_book_id" = :bookId ${
            commentAtCursor
              ? `AND "comment"."updated_at" < :cursorUpdatedAt AND "comment"."id" <> :cursorId`
              : ``
          }`,
          {
            bookId,
            ...(commentAtCursor
              ? {
                  cursorUpdatedAt: commentAtCursor.updatedAt,
                  cursorId: commentAtCursor.id,
                }
              : {}),
          }
        )
        .andWhere(`"comment"."deleted" = 'N'`)
        .orderBy('"comment"."updated_at"', 'DESC');

      const comments = await getManager()
        .createQueryBuilder()
        .select('"comment".*')
        .from(`(${commentsSubQb.getQuery()})`, 'comment')
        .where('rownum <= :limit', { limit })
        .setParameters(commentsSubQb.getParameters())
        .getRawMany()
        .then((raws) =>
          Promise.all(
            raws.map(async (raw: CommentsSubQueryRow) => ({
              id: raw.comment_id,
              fkUserId: raw.comment_fk_user_id,
              fkBookId: raw.comment_fk_book_id,
              createdAt: raw.comment_created_at,
              updatedAt: raw.comment_updated_at,
              content: raw.comment_content,
              deleted: raw.comment_deleted,
              user: await loaders.user.load(raw.comment_fk_user_id),
            }))
          )
        );

      return {
        id: bookId,
        comments,
        cursor: comments.length > 0 ? comments[comments.length - 1].id : null,
      };
    },
    searchBooks: async (
      parent: unknown,
      { input = {}, cursor, limit = 20, showDeleted }: SearchBooksQueryArgs,
      { admin, getConnection, loaders }: GraphqlContext
    ) => {
      const { tag, keyword, keywordScope = [...ALLOWED_KEYWORD_SCOPE] } = input;
      let { orderBy = 'updatedAt' } = input;

      if (keyword) {
        orderBy = 'keyword';
      }

      if (limit > 64) {
        throw new ApolloError('Max limit is 64', 'BAD_REQUEST');
      }

      if (!ALLOWED_ORDER_BY.includes(orderBy)) {
        throw new ApolloError('invalid orderBy parameter', 'BAD_REQUEST');
      }

      if (
        !keywordScope ||
        keywordScope.some((item) => !ALLOWED_KEYWORD_SCOPE.includes(item))
      ) {
        throw new ApolloError('invalid keywordScope parameter', 'BAD_REQUEST');
      }

      if (showDeleted && !admin) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      const scope2column: { [key: string]: string } = {
        title: `"book"."title"`,
        authorName: `"author"."name"`,
      };
      const sortOrder2column: { [key: string]: string } = {
        weeklyViews: `"weekly_views"`,
        updatedAt: `"updated_at"`,
        keyword: `"jws"`,
      };

      const manager = getManager();

      type QueryRow = {
        id: string;
        updated_at?: Date;
        jws?: number;
        weekly_views?: number | null;
      };

      const cursorRecord = cursor
        ? ((await manager.query(
            `
  SELECT "book"."id" AS "id", "book"."updated_at" AS "updated_at", ${
    keyword
      ? `GREATEST(
      ${keywordScope
        .map(
          (scope) => `
    UTL_MATCH.jaro_winkler_similarity(UPPER(${
      scope2column[scope] || ''
    }), UPPER(:${keywordScope.indexOf(scope) + 1}))`
        )
        .join(`, `)}
      
    ) AS "jws", `
      : ``
  }"stat"."weekly_views" AS "weekly_views"
    FROM "books" "book"
      LEFT JOIN "authors" "author" ON "book"."fk_author_id" = "author"."id"
      LEFT JOIN "book_statistics" "stat" ON "stat"."fk_book_id" = "book"."id"
    WHERE "book"."id" = :${keywordScope.length + 1}
        `,
            [
              ...(keyword
                ? Array<string>(keywordScope.length).fill(keyword)
                : []),
              cursor,
            ]
          )) as QueryRow[])[0]
        : null;

      const params = [
        ...(keyword ? Array<string>(keywordScope.length).fill(keyword) : []),
        ...(tag ? [tag] : []),
        ...(cursor ? [cursor] : []),
        ...(cursor && keyword
          ? [cursorRecord?.jws, cursorRecord?.jws, cursor]
          : []),
        ...(cursor && orderBy === 'updatedAt'
          ? [cursorRecord?.updated_at, cursorRecord?.updated_at, cursor]
          : []),
        ...(cursor && orderBy === 'weeklyViews'
          ? [cursorRecord?.weekly_views, cursorRecord?.weekly_views, cursor]
          : []),
        limit,
      ];

      const numKeywords = keyword ? keywordScope.length : 0;
      const numTag = tag ? 1 : 0;
      const numCursorId = cursor ? 1 : 0;
      const numCursorComparator = cursor ? 3 : 0;

      const tagIndex = numKeywords + 1;
      const cursorIdIndex = numKeywords + numTag + 1;
      const cursorComparatorIndex = numKeywords + numTag + numCursorId + 1;
      const limitIndex =
        numKeywords + numTag + numCursorId + numCursorComparator + 1;

      const result = (await manager.query(
        `
  SELECT "q2".* FROM (
    SELECT "q1".* FROM (
      SELECT ${[
        `DISTINCT "book"."id" AS "id"`,
        `"book"."updated_at" AS "updated_at"`,
        ...(keyword
          ? [
              `
        GREATEST(
          ${keywordScope
            .map(
              (scope) => `
          UTL_MATCH.jaro_winkler_similarity(UPPER(${
            scope2column[scope] || ''
          }), UPPER(:${keywordScope.indexOf(scope) + 1}))`
            )
            .join(`, `)}
          
        ) AS "jws"`,
            ]
          : []),
        `"stat"."weekly_views" AS "weekly_views"`,
      ].join(', ')}
        FROM "books" "book"
          LEFT JOIN "authors" "author" ON "book"."fk_author_id" = "author"."id"
          LEFT JOIN "book_statistics" "stat" ON "stat"."fk_book_id" = "book"."id"
          LEFT JOIN "book_tags" "book_tag" ON "book_tag"."fk_book_id" = "book"."id"
          LEFT JOIN "tags" "tag" ON "book_tag"."fk_tag_id" = "tag"."id"
        ${!showDeleted || tag ? `WHERE` : ``} ${[
          ...(showDeleted ? [] : [`"book"."deleted" = 'N'`]),
          ...(tag ? [`"tag"."text" = (:${tagIndex})`] : []),
        ].join(` AND `)}
      ) "q1"
      ${keyword || cursor ? `WHERE` : ``} ${[
          ...(keyword ? [`"q1"."jws" >= ${JWS_THRESHOLD}`] : []),
          ...(cursor ? [`"q1"."id" <> :${cursorIdIndex}`] : []),
          ...(cursor
            ? [
                `("q1".${
                  sortOrder2column[orderBy]
                } < :${cursorComparatorIndex} OR ( "q1".${
                  sortOrder2column[orderBy]
                } = :${cursorComparatorIndex + 1} AND "q1"."id" > :${
                  cursorComparatorIndex + 2
                } ))`,
              ]
            : []),
        ].join(' AND ')}
      ORDER BY "q1".${sortOrder2column[orderBy]} DESC, "q1"."id"
    ) "q2"
      WHERE rownum <= :${limitIndex}
        `,
        params
      )) as QueryRow[];

      const bookIds = result.map((row) => row.id);
      const books = await loaders.book.loadMany(bookIds);

      return {
        items: books,
        cursor: bookIds.length > 0 ? bookIds[bookIds.length - 1] : null,
      };
    },
    searchBookCount: async (
      parent: unknown,
      { input = {}, showDeleted }: SearchBookCountQueryArgs,
      { admin, getConnection }: GraphqlContext
    ) => {
      const { tag, keyword, keywordScope = [...ALLOWED_KEYWORD_SCOPE] } = input;

      if (
        !keywordScope ||
        keywordScope.some((item) => !ALLOWED_KEYWORD_SCOPE.includes(item))
      ) {
        throw new ApolloError('invalid keywordScope parameter', 'BAD_REQUEST');
      }

      if (showDeleted && !admin) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      const scope2column: { [key: string]: string } = {
        title: `"book"."title"`,
        authorName: `"author"."name"`,
      };

      type QueryRow = {
        count: number;
      };

      const params = [
        ...(keyword ? Array<string>(keywordScope.length).fill(keyword) : []),
        ...(tag ? [tag] : []),
      ];

      const numKeywords = keyword ? keywordScope.length : 0;

      const tagIndex = numKeywords + 1;

      const result = ((await getManager().query(
        `
  SELECT COUNT("q1"."id") AS "count" FROM (
    SELECT ${[
      `DISTINCT "book"."id" AS "id"`,
      ...(keyword
        ? [
            `
      GREATEST(
        ${keywordScope
          .map(
            (scope) => `
        UTL_MATCH.jaro_winkler_similarity(UPPER(${
          scope2column[scope] || ''
        }), UPPER(:${keywordScope.indexOf(scope) + 1}))`
          )
          .join(`, `)}
        
      ) AS "jws"`,
          ]
        : []),
    ].join(', ')}
      FROM "books" "book"
        LEFT JOIN "authors" "author" ON "book"."fk_author_id" = "author"."id"
        LEFT JOIN "book_statistics" "stat" ON "stat"."fk_book_id" = "book"."id"
        LEFT JOIN "book_tags" "book_tag" ON "book_tag"."fk_book_id" = "book"."id"
        LEFT JOIN "tags" "tag" ON "book_tag"."fk_tag_id" = "tag"."id"
      ${!showDeleted || tag ? `WHERE` : ``} ${[
          ...(showDeleted ? [] : [`"book"."deleted" = 'N'`]),
          ...(tag ? [`"tag"."text" = (:${tagIndex})`] : []),
        ].join(` AND `)}
  ) "q1"
    ${keyword ? `WHERE "q1"."jws" >= ${JWS_THRESHOLD}` : ``}
        `,
        params
      )) as QueryRow[])[0];

      return result.count;
    },
    featuredBooks: async (
      parent: unknown,
      { limit = 20 }: FeaturedBooksQueryArgs,
      { user, loaders, getConnection }: GraphqlContext
    ) => {
      if (limit > 64) {
        throw new ApolloError('Max limit is 64', 'BAD_REQUEST');
      }

      await getConnection();

      const userIdHash = user
        ? hashString(user.id, { unsigned: true })
        : Date.now();

      type QueryRow = {
        id: string;
      };

      const result = (await getManager().query(
        `
  SELECT "q2".* FROM (
  SELECT "q1".* FROM (
  SELECT "book"."id" AS "id", ora_hash("book"."id", 64, ${userIdHash}) AS "hash"
    FROM "books" "book"
    WHERE "book"."deleted" = 'N'
  ) "q1"
    ORDER BY "q1"."hash", "q1"."id"
  ) "q2"
    WHERE rownum <= :1
        `,
        [limit]
      )) as QueryRow[];

      const bookIds = result.map((row) => row.id);
      const books = await loaders.book.loadMany(bookIds);

      return books;
    },
  },
  Mutation: {
    rateBook: async (
      parent: unknown,
      { bookId, value }: RateBookMutationArgs,
      { user, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      if (!Number.isInteger(value) || value < 1 || value > 5) {
        throw new ApolloError(`Invalid argument "value": got ${value}`);
      }

      const isSubscribed = await loaders.userSubscriptions
        .load(user.id)
        .then((subscriptions) =>
          subscriptions.some((subscription) => subscription.isActive())
        );
      if (!isSubscribed) {
        throw new AuthenticationError('User is not subscribed');
      }

      const rating =
        (await getRepository(Rating)
          .find({
            where: { fkBookId: bookId, fkUserId: user.id },
          })
          .then((rows) => rows[0])) || new Rating();

      rating.fkUserId = user.id;
      rating.fkBookId = bookId;
      rating.value = value;

      await getRepository(Rating).save(rating);

      return true;
    },
    deleteRating: async (
      parent: unknown,
      { bookId }: DeleteRatingMutationArgs,
      { user, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      const isSubscribed = await loaders.userSubscriptions
        .load(user.id)
        .then((subscriptions) =>
          subscriptions.some((subscription) => subscription.isActive())
        );
      if (!isSubscribed) {
        throw new AuthenticationError('User is not subscribed');
      }

      const rating = await getRepository(Rating)
        .find({ where: { fkBookId: bookId, fkUserId: user.id } })
        .then((rows) => rows[0]);

      if (!rating) {
        return false;
      }

      await getRepository(Rating).delete(rating.id);

      return true;
    },
    createBook: async (
      parent: unknown,
      { input }: CreateBookMutationArgs,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      validateBookInput(input);

      await getConnection();

      logger.info(
        `User ${
          user.id
        } issued createBook mutation with arguments ${JSON.stringify({
          input,
        })}`
      );

      const repo = getRepository(Book);

      const book = new Book();
      book.title = input.title;
      book.description = input.description;
      book.fkAuthorId = input.authorId;
      book.duration = input.duration;
      book.metadataUri = input.metadataUri;
      book.mediaUri = input.mediaUri;
      book.thumbnailUri = input.thumbnailUri;

      await repo.save(book);

      if (input.thumbnail) {
        const { uri } = await getResourceFileUploader({
          logger,
        }).singleFileUploadResolver(input.thumbnail, {
          directory: BOOK_THUMBNAIL_DIR,
          name: book.id,
        });

        book.thumbnailUri = uri;
      }

      if (input.metadata) {
        const { uri: metadataUri } = await getResourceFileUploader({
          logger,
        }).createFileResolver(JSON.stringify(input.metadata), {
          directory: BOOK_METADATA_DIR,
          name: book.id,
          ext: 'json',
          mimetype: 'application/json',
          encoding: 'utf-8',
        });

        book.metadataUri = metadataUri;
      }

      await repo.save(book);

      await book.syncTags(input.tags);

      const bookStats = new BookStatistic();
      bookStats.fkBookId = book.id;

      await getRepository(BookStatistic).save(bookStats);

      return book;
    },
    editBook: async (
      parent: unknown,
      { bookId, input }: EditBookMutationArgs,
      { admin, user, loaders, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      validateBookInput(input);

      logger.info(
        `User ${
          user.id
        } issued editBook mutation with arguments ${JSON.stringify({
          bookId,
          input,
        })}`
      );

      const book = await loaders.book.load(bookId);

      if (!book) {
        return null;
      }

      book.title = input.title;
      book.description = input.description;
      book.fkAuthorId = input.authorId;
      book.duration = input.duration;
      book.metadataUri = input.metadataUri;
      book.mediaUri = input.mediaUri;
      book.thumbnailUri = input.thumbnailUri;

      if (input.thumbnail) {
        const { uri } = await getResourceFileUploader({
          logger,
        }).singleFileUploadResolver(input.thumbnail, {
          directory: BOOK_THUMBNAIL_DIR,
          name: book.id,
        });

        book.thumbnailUri = uri;
      }

      if (input.metadata) {
        const { uri: metadataUri } = await getResourceFileUploader({
          logger,
        }).createFileResolver(JSON.stringify(input.metadata), {
          directory: BOOK_METADATA_DIR,
          name: book.id,
          ext: 'json',
          mimetype: 'application/json',
          encoding: 'utf-8',
        });

        book.metadataUri = metadataUri;
      }

      await getRepository(Book).save(book);

      await book.syncTags(input.tags);

      return book;
    },
    deleteBook: async (
      parent: unknown,
      { bookId, soft = true }: DeleteBookMutationArgs,
      { admin, user, loaders, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      logger.info(
        `User ${
          user.id
        } issued deleteBook mutation with arguments ${JSON.stringify({
          bookId,
          soft,
        })}`
      );

      const book = await loaders.book.load(bookId);

      if (!book) {
        return false;
      }

      if (soft) {
        book.isDeleted = true;
        await getRepository(Book).save(book);
      } else {
        await getRepository(Book).remove(book);
      }

      return true;
    },
  },
};
