import {
  ApolloError,
  AuthenticationError,
  gql,
  IResolvers,
} from 'apollo-server-koa';
import { getManager, getRepository } from 'typeorm';
import Post, { PostCategory } from '../entities/post';
import { GraphqlContext } from '../types/graphql';

export const typeDef = gql`
  type Post {
    id: ID!
    createdAt: Date
    updatedAt: Date
    user: User
    category: String
    title: String
    content: String
  }

  type PaginatedPosts {
    items: [Post]!
    cursor: ID
  }

  input PostInput {
    title: String!
    content: String
  }

  extend type Query {
    announcements(limit: Int, cursor: ID): PaginatedPosts!
  }

  extend type Mutation {
    # admin
    createAnnouncement(input: PostInput!): Post!
    editPost(postId: ID!, input: PostInput!): Post
    deletePost(postId: ID!): Boolean!
  }
`;

type PostInput = {
  title: string;
  content?: string;
};

type AnnouncementsQueryArgs = {
  limit?: number;
  cursor?: string;
};

type CreateAnnouncementMutationArgs = {
  input: PostInput;
};

type EditPostMutationArgs = {
  postId: string;
  input: PostInput;
};

type DeletePostMutationArgs = {
  postId: string;
};

export const resolvers: IResolvers = {
  Query: {
    announcements: async (
      parent: unknown,
      { limit = 20, cursor }: AnnouncementsQueryArgs,
      { getConnection, loaders }: GraphqlContext
    ) => {
      if (limit > 64) {
        throw new ApolloError('Max limit is 64', 'BAD_REQUEST');
      }

      await getConnection();

      const postAtCursor = cursor
        ? await getRepository(Post)
            .findByIds([cursor])
            .then((posts) => posts[0])
        : null;

      type AnnouncementsSubQueryRow = {
        post_id: string;
        post_created_at: Date;
        post_updated_at: Date;
        post_fk_user_id: string;
        post_category: string;
        post_title: string;
        post_content?: string;
        post_deleted: string;
      };

      const announcementsSubQb = getManager()
        .createQueryBuilder(Post, 'post')
        .leftJoinAndSelect('post.user', 'user')
        .where(
          `"post"."category" = '${PostCategory.Announcement}' ${
            postAtCursor
              ? `AND "post"."updated_at" < :cursorUpdatedAt AND "post"."id" <> :cursorId`
              : ``
          }`,
          {
            ...(postAtCursor
              ? {
                  cursorUpdatedAt: postAtCursor.updatedAt,
                  cursorId: postAtCursor.id,
                }
              : {}),
          }
        )
        .andWhere(`"post"."deleted" = 'N'`)
        .orderBy(`"post"."updated_at"`, 'DESC');

      const announcements = await getManager()
        .createQueryBuilder()
        .select('"post".*')
        .from(`(${announcementsSubQb.getQuery()})`, 'post')
        .where('rownum <= :limit', { limit })
        .setParameters(announcementsSubQb.getParameters())
        .getRawMany()
        .then((raws) =>
          Promise.all(
            raws.map(async (raw: AnnouncementsSubQueryRow) => ({
              id: raw.post_id,
              createdAt: raw.post_created_at,
              updatedAt: raw.post_updated_at,
              fkUserId: raw.post_fk_user_id,
              category: raw.post_category,
              title: raw.post_title,
              content: raw.post_content,
              deleted: raw.post_deleted,
              user: await loaders.user.load(raw.post_fk_user_id),
            }))
          )
        );

      return {
        items: announcements,
        cursor:
          announcements.length > 0
            ? announcements[announcements.length - 1].id
            : null,
      };
    },
  },
  Mutation: {
    createAnnouncement: async (
      parent: unknown,
      { input }: CreateAnnouncementMutationArgs,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      if (!admin) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      logger.info(
        `User ${
          user.id
        } issued createAnnouncement mutation with arguments ${JSON.stringify({
          input,
        })}`
      );

      const post = new Post();
      post.fkUserId = user.id;
      post.category = PostCategory.Announcement;
      post.title = input.title;
      post.content = input.content;

      await getRepository(Post).save(post);

      return post;
    },
    editPost: async (
      parent: unknown,
      { postId, input }: EditPostMutationArgs,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      if (!admin) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      logger.info(
        `User ${
          user.id
        } issued editPost mutation with arguments ${JSON.stringify({
          postId,
          input,
        })}`
      );

      const post = await getRepository(Post)
        .findByIds([postId])
        .then((rows) => rows[0]);

      if (!post) {
        return null;
      }

      post.title = input.title;
      post.content = input.content;

      await getRepository(Post).save(post);

      return post;
    },
    deletePost: async (
      parent: unknown,
      { postId }: DeletePostMutationArgs,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      if (!admin) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      logger.info(
        `User ${
          user.id
        } issued deletePost mutation with arguments ${JSON.stringify({
          postId,
        })}`
      );

      const post = await getRepository(Post)
        .findByIds([postId])
        .then((rows) => rows[0]);

      if (!post) {
        return false;
      }

      await getRepository(Post).remove(post);

      return true;
    },
  },
};
