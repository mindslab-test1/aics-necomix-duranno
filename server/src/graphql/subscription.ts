import { AuthenticationError, gql, IResolvers } from 'apollo-server-koa';
import { getRepository } from 'typeorm';
import Product from '../entities/product';
import Subscription from '../entities/subscription';
import { GraphqlContext } from '../types/graphql';

export const typeDef = gql`
  type ProductSubscription {
    id: ID!
    createdAt: Date
    updatedAt: Date
    promotionEnabled: Boolean
    cancelled: Boolean
    provider: String
    productId: ID
    level: String
    title: String
    limit: Int
    metadata: String
  }

  input ProductInput {
    level: String!
    title: String
    limit: Int!
    metadata: String
  }

  extend type Mutation {
    # admin
    createOrEditProduct(input: ProductInput!): ID
    deleteProduct(productID: ID!): Boolean!
  }
`;

type ProductInput = {
  level: string;
  title?: string;
  limit: number;
  metadata?: string;
};

type CreateOrEditProductMutationArgs = {
  input: ProductInput;
};

type DeleteProductMurationArgs = {
  productId: string;
};

export const resolvers: IResolvers = {
  ProductSubscription: {
    promotionEnabled: (parent: Subscription) => parent.isPromotionEnabled(),
    cancelled: (parent: Subscription) => !parent.isActive(),
    productId: (parent: Subscription) => parent.fkProductId,
    level: (parent: Subscription) => parent.product.level,
    title: (parent: Subscription) => parent.product.title,
    limit: (parent: Subscription) => parent.limit || parent.product.limit,
    metadata: (parent: Subscription) => parent.product.metadata,
  },
  Mutation: {
    createOrEditProduct: async (
      parent: unknown,
      { input }: CreateOrEditProductMutationArgs,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      logger.info(
        `User ${
          user.id
        } issued createOrEditProduct mutation with arguments ${JSON.stringify({
          input,
        })}`
      );

      const product =
        (await getRepository(Product)
          .find({ where: { level: input.level } })
          .then((rows) => rows[0])) || new Product();

      product.level = input.level;
      product.title = input.title;
      product.limit = input.limit;
      product.metadata = input.metadata;

      await getRepository(Product).save(product);

      return product.id;
    },
    deleteProduct: async (
      parent: unknown,
      { productId }: DeleteProductMurationArgs,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      logger.info(
        `User ${
          user.id
        } issued deleteProduct mutation with arguments ${JSON.stringify({
          productId,
        })}`
      );

      const product = await getRepository(Product)
        .findByIds([productId])
        .then((rows) => rows[0]);

      if (!product) {
        return false;
      }

      await getRepository(Product).remove(product);

      return true;
    },
  },
};
