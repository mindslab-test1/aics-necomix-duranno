import { AuthenticationError, gql, IResolvers } from 'apollo-server-koa';
import { getRepository } from 'typeorm';
import Feedback from '../entities/feedback';
import { GraphqlContext } from '../types/graphql';

export const typeDef = gql`
  type Feedback {
    id: ID!
    type: String
    category: String
    name: String
    email: String
    title: String
    content: String
  }

  input FeedbackInput {
    type: String!
    category: String!
    name: String!
    email: String!
    title: String!
    content: String
  }

  extend type Mutation {
    createFeedback(input: FeedbackInput!): Feedback!
  }
`;

type FeedbackInput = {
  type: string;
  category: string;
  name: string;
  email: string;
  title: string;
  content?: string;
};

type CreateFeedbackMutationArgs = {
  input: FeedbackInput;
};

export const resolvers: IResolvers = {
  Mutation: {
    createFeedback: async (
      parent: unknown,
      { input }: CreateFeedbackMutationArgs,
      { user, getConnection }: GraphqlContext
    ) => {
      if (!user) {
        throw new AuthenticationError('User is not signed');
      }

      await getConnection();

      const feedback = new Feedback();
      if (user.emailVerified) {
        feedback.fkUserId = user.id;
      }
      feedback.type = input.type;
      feedback.category = input.category;
      feedback.name = input.name;
      feedback.email = input.email;
      feedback.title = input.title;
      feedback.content = input.content;

      await getRepository(Feedback).save(feedback);

      return feedback;
    },
  },
};
