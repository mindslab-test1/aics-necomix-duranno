import {
  ApolloError,
  AuthenticationError,
  gql,
  IResolvers,
} from 'apollo-server-koa';
import { getMediaFileUploader } from '../services';
import { GraphqlContext } from '../types/graphql';
import { getMediaUri } from '../utils/book';

export const typeDef = gql`
  extend type Query {
    # admin
    uploadBookAudioUri(bookId: ID!, sequence: Int): String
  }
`;

type UploadMediaUriQueryArgs = {
  bookId: string;
  sequence?: number;
};

export const resolvers: IResolvers = {
  Query: {
    uploadBookAudioUri: async (
      parent: unknown,
      { bookId, sequence }: UploadMediaUriQueryArgs,
      { admin, user, loaders, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      const book = await loaders.book.load(bookId);

      if (!book) {
        throw new ApolloError('invalid bookId', 'BAD_REQUEST');
      }

      if (!(sequence === null || sequence === undefined) && sequence < 0) {
        throw new ApolloError('invalid sequence', 'BAD_REQUEST');
      }

      logger.info(
        `User ${
          user.id
        } issued uploadMediaUri mutation with arguments ${JSON.stringify({
          bookId,
          sequence,
        })}`
      );

      const enablePublicRead = process.env.APP_STAGE === 'local';

      const filename = getMediaUri(bookId, sequence, {
        excludeExt: true,
      }).replace('/', '');

      const uri = await getMediaFileUploader({
        logger,
      }).generateSignedUploadUri({
        name: filename,
        ext: 'mp3',
        mimetype: 'audio/mpeg',
        publicRead: enablePublicRead,
      });

      return uri;
    },
  },
};
