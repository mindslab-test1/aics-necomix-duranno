import {
  ApolloError,
  AuthenticationError,
  gql,
  IResolvers,
} from 'apollo-server-koa';
import { getManager, getRepository } from 'typeorm';
import BookStatistic from '../entities/bookStats';
import Comment from '../entities/comment';
import Event, { EventCategories, EventNames } from '../entities/event';
import Product from '../entities/product';
import Subscription from '../entities/subscription';
import User from '../entities/user';
import UserProfile from '../entities/userProfile';
import { GraphqlContext } from '../types/graphql';
import { MediaTokenPayload } from '../types/token';
import { getMediaUri } from '../utils/book';
import { generateToken, TokenSubject } from '../utils/token';

export const typeDef = gql`
  type UserProfile {
    createdAt: Date
    updatedAt: Date
    isAllowedPromotionEmail: Boolean!
  }

  type User {
    id: String!
    email: String
    emailVerified: Boolean
    registered: Boolean
    admin: Boolean
    profile: UserProfile
    subscribed: Boolean
    activeSubscription: ProductSubscription
    subscriptions: [ProductSubscription]!
  }

  type UserCommentQueries {
    count: Int!
  }

  type SubscriptionToken {
    token: String
    error: String
  }

  input RegisterInput {
    isAllowedPromotionEmail: Boolean
  }

  input MockSubscriptionInput {
    productId: ID
    productLevel: String
    promotionEnabled: Boolean
  }

  extend type Query {
    me: User
    MyComments: UserCommentQueries!
    myComments(cursor: ID, limit: Int): PaginatedComments! # TODO: move myComments into MyComments (more organized GraphQL schema)
    subscriptionToken(bookId: ID!, sequence: Int): SubscriptionToken!
  }

  extend type Mutation {
    register(input: RegisterInput!): Boolean!
    mockSubscribe(input: MockSubscriptionInput!): ProductSubscription
  }
`;

type MyCommentsQueryArgs = {
  cursor?: string;
  limit?: number;
};

type SubscriptionTokenQueryArgs = {
  bookId: string;
  sequence?: number;
};

type RegisterInput = {
  isAllowedPromotionEmail?: boolean;
};

type MockSubscriptionInput = {
  productId?: string;
  productLevel?: string;
  promotionEnabled?: boolean;
};

type RegisterMutationArgs = {
  input: RegisterInput;
};

type MockSubscribeMutationArgs = {
  input: MockSubscriptionInput;
};

export const resolvers: IResolvers = {
  User: {
    email: (parent: User, args: unknown, { user }: GraphqlContext) =>
      user && user.id === parent.id ? parent.email : parent.obfuscatedEmail,
    registered: async (
      parent: User,
      args: unknown,
      { loaders }: GraphqlContext
    ) => !!(await loaders.user.load(parent.id)),
    admin: (parent: User, args: unknown, { admin }: GraphqlContext) =>
      admin || false,
    profile: async (
      parent: User,
      args: unknown,
      { loaders }: GraphqlContext
    ) => {
      return (await loaders.user.load(parent.id)).profile;
    },
    subscribed: async (
      parent: User,
      args: unknown,
      { loaders }: GraphqlContext
    ) => {
      const subscriptions = await loaders.userSubscriptions.load(parent.id);

      return subscriptions.some((subscription) => subscription.isActive());
    },
    activeSubscription: async (
      parent: User,
      args: unknown,
      { loaders }: GraphqlContext
    ) => {
      const subscriptions = await loaders.userSubscriptions.load(parent.id);

      return (
        subscriptions.find((subscription) => subscription.isActive()) || null
      );
    },
    subscriptions: (
      parent: User,
      args: unknown,
      { loaders }: GraphqlContext
    ) => {
      return loaders.userSubscriptions.load(parent.id);
    },
  },
  Query: {
    me: (_parent, _args, { user }: GraphqlContext) => user,
    MyComments: (
      parent: unknown,
      args: unknown,
      { user, getConnection }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      const count = async () => {
        await getConnection();

        return getRepository(Comment)
          .createQueryBuilder('comment')
          .leftJoinAndSelect('comment.book', 'book')
          .where(`"book"."deleted" = 'N'`)
          .andWhere(`"comment"."fk_user_id" = :userId`, { userId: user.id })
          .andWhere(`"comment"."deleted" = 'N'`)
          .getCount();
      };

      return { count };
    },
    myComments: async (
      parent: unknown,
      { cursor, limit = 20 }: MyCommentsQueryArgs,
      { user, getConnection, loaders }: GraphqlContext
    ) => {
      if (limit > 64) {
        throw new ApolloError('Max limit is 64', 'BAD_REQUEST');
      }

      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      await getConnection();

      const commentAtCursor = cursor
        ? await getRepository(Comment)
            .findByIds([cursor])
            .then((comments) => comments[0])
        : null;

      type CommentsSubQueryRow = {
        comment_id: string;
        comment_fk_user_id: string;
        comment_fk_book_id: string;
        comment_created_at: Date;
        comment_updated_at: Date;
        comment_content?: string;
        comment_deleted: string;
      };

      const commentsSubQb = getManager()
        .createQueryBuilder(Comment, 'comment')
        .leftJoinAndSelect('comment.book', 'book')
        .where(
          `"book"."deleted" = 'N' AND "comment"."fk_user_id" = :userId ${
            commentAtCursor
              ? `AND "comment"."updated_at" < :cursorUpdatedAt AND "comment"."id" <> :cursorId`
              : ``
          }`,
          {
            userId: user.id,
            ...(commentAtCursor
              ? {
                  cursorUpdatedAt: commentAtCursor.updatedAt,
                  cursorId: commentAtCursor.id,
                }
              : {}),
          }
        )
        .andWhere(`"comment"."deleted" = 'N'`)
        .orderBy(`"comment"."updated_at"`, 'DESC');

      const comments = await getManager()
        .createQueryBuilder()
        .select('"comment".*')
        .from(`(${commentsSubQb.getQuery()})`, 'comment')
        .where('rownum <= :limit', { limit })
        .setParameters(commentsSubQb.getParameters())
        .getRawMany()
        .then((raws) =>
          Promise.all(
            raws.map(async (raw: CommentsSubQueryRow) => ({
              id: raw.comment_id,
              fkUserId: raw.comment_fk_user_id,
              fkBookId: raw.comment_fk_book_id,
              createdAt: raw.comment_created_at,
              updatedAt: raw.comment_updated_at,
              content: raw.comment_content,
              deleted: raw.comment_deleted,
              book: await loaders.book.load(raw.comment_fk_book_id),
            }))
          )
        );

      return {
        id: user.id,
        comments,
        cursor: comments.length > 0 ? comments[comments.length - 1].id : null,
      };
    },
    subscriptionToken: async (
      parent: unknown,
      { bookId, sequence }: SubscriptionTokenQueryArgs,
      { user, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      const subscriptions = await loaders.userSubscriptions.load(user.id);
      const activeSubscription = subscriptions.find((subscription) =>
        subscription.isActive()
      );

      if (!activeSubscription) {
        return {
          error: 'user is not subscribed',
        };
      }

      const book = await loaders.book.load(bookId);

      if (!book) {
        return {
          error: 'invalid bookId',
        };
      }

      if (book.isDeleted) {
        return {
          error: 'book is deleted',
        };
      }

      const userViewHistory = await getRepository(Event)
        .createQueryBuilder('event')
        .where(
          `
      "event"."fk_user_id" = '${user.id}'
        AND "event"."name" = '${EventNames.viewBook}'
        AND "event"."updated_at" >= (CURRENT_TIMESTAMP - INTERVAL '1' MONTH)
    `
        )
        .getMany();

      const limit =
        activeSubscription.limit || activeSubscription.product.limit;

      const monthlyCount = new Set<string>(
        userViewHistory
          .map((item) => item.fkBookId as string)
          .filter((e) => !!e)
      ).size;

      const resourceUri = getMediaUri(bookId, sequence);

      const tokenPayload = {
        resourceUri,
        userId: user.id,
        subscriptionId: activeSubscription.id,
        productId: activeSubscription.fkProductId,
        subscriptionProvider: activeSubscription.provider,
        limit,
        remaining: 0,
      };

      const tokenOptions = {
        subject: TokenSubject.Media,
        expiresIn: '3h',
      };

      const viewBook = async () => {
        const bookStats = await getRepository(BookStatistic)
          .find({ where: { fkBookId: bookId } })
          .then((rows) => rows[0]);
        bookStats.numViews += 1;
        await getRepository(BookStatistic).save(bookStats);

        const event = new Event();
        event.fkBookId = bookId;
        event.fkUserId = user.id;
        event.category = EventCategories.bookDetails.toString();
        event.name = EventNames.viewBook.toString();
        await getRepository(Event).save(event);
      };

      if (userViewHistory.some((item) => item.fkBookId === bookId)) {
        /* user already viewed this book this month, pass */
        await viewBook();
        return {
          token: await generateToken<MediaTokenPayload>(
            {
              ...tokenPayload,
              remaining: limit - monthlyCount,
            },
            tokenOptions
          ),
        };
      }

      /* otherwise, check if user did not exceed monthly limit */
      if (monthlyCount < limit) {
        await viewBook();
        return {
          token: await generateToken<MediaTokenPayload>(
            {
              ...tokenPayload,
              remaining: limit - monthlyCount - 1,
            },
            tokenOptions
          ),
        };
      }

      return {
        error: 'exceeded monthly limit',
      };
    },
  },
  Mutation: {
    register: async (
      parent: unknown,
      { input }: RegisterMutationArgs,
      { user, getConnection, loaders }: GraphqlContext
    ) => {
      if (!user) {
        throw new AuthenticationError('User is not signed');
      }

      await getConnection();

      if (await loaders.user.load(user.id)) {
        return false;
      }

      const { isAllowedPromotionEmail = false } = input;

      const profile = new UserProfile();
      profile.isAllowedPromotionEmail = isAllowedPromotionEmail;
      await getRepository(UserProfile).save(profile);

      const repo = getRepository(User);
      await repo.insert({
        id: user.id,
        email: user.email,
        fkProfileId: profile.id,
      });

      return true;
    },
    mockSubscribe: async (
      parent: unknown,
      { input }: MockSubscribeMutationArgs,
      { user, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      if (!input.productId && !input.productLevel) {
        throw new ApolloError(
          'productId or productLevel is not given',
          'BAD_REQUEST'
        );
      }

      const subscriptions = await loaders.userSubscriptions.load(user.id);
      const subscribed = subscriptions.some((subscription) =>
        subscription.isActive()
      );

      if (subscribed) {
        return null;
      }

      const product = await getRepository(Product)
        .find({
          where: {
            ...(input.productId && { id: input.productId }),
            ...(input.productLevel && { level: input.productLevel }),
          },
        })
        .then((rows) => rows[0]);

      if (!product) {
        return null;
      }

      const subscription = new Subscription();
      subscription.fkUserId = user.id;
      if (input.promotionEnabled !== undefined) {
        subscription.promotionEnabled = input.promotionEnabled ? 'Y' : 'N';
      }
      subscription.provider = 'MOCK';
      subscription.fkProductId = product.id;

      await getRepository(Subscription).save(subscription);

      return subscription;
    },
  },
};
