import {
  ApolloError,
  AuthenticationError,
  gql,
  IResolvers,
} from 'apollo-server-koa';
import { getManager, getRepository } from 'typeorm';
import UserHistory from '../entities/userHistory';
import { GraphqlContext } from '../types/graphql';

export const typeDef = gql`
  type UserHistory {
    id: ID!
    book: Book!
    createdAt: Date!
    updatedAt: Date!
    sequence: Int
    chapterTitle: String
    mediaUri: String!
    position: Int!
  }

  type UserHistories {
    items: [UserHistory]!
    cursor: ID
  }

  type History {
    id: ID!
    list(limit: Int, cursor: ID): UserHistories!
  }

  type UserHistoryOps {
    update(bookId: ID!, input: UserHistoryInput!): UserHistory
    remove(bookId: ID!): ID
  }

  input UserHistoryInput {
    sequence: Int
    chapterTitle: String
    mediaUri: String!
    position: Int!
  }

  extend type Query {
    History: History!
  }

  extend type Mutation {
    History: UserHistoryOps!
  }
`;

export const resolvers: IResolvers = {
  Query: {
    History: (
      parent: unknown,
      args: unknown,
      { user, getConnection, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      const id = () => user.id;

      const list = async ({
        cursor,
        limit = 20,
      }: {
        cursor?: string;
        limit?: number;
      }) => {
        if (limit > 64) {
          throw new ApolloError('Max limit is 64', 'BAD_REQUEST');
        }
        if (!user || !user.emailVerified) {
          throw new AuthenticationError('User is not signed');
        }

        await getConnection();

        const userHistoryAtCursor = cursor
          ? await getRepository(UserHistory)
              .findByIds([cursor])
              .then((items) => items[0])
          : null;

        type UserHistoriesSubQueryRow = {
          user_history_id: string;
          user_history_fk_user_id: string;
          user_history_fk_book_id: string;
          user_history_created_at: Date;
          user_history_updated_at: Date;
          user_history_sequence?: number;
          user_history_chapter_title?: string;
          user_history_media_uri: string;
          user_history_position: number;
        };

        const userHistoriesSubQb = getManager()
          .createQueryBuilder(UserHistory, 'user_history')
          .leftJoinAndSelect('user_history.book', 'book')
          .where(
            `"book"."deleted" = 'N' AND "user_history"."fk_user_id" = :userId ${
              userHistoryAtCursor
                ? `AND "user_history"."updated_at" < :cursorUpdatedAt AND "user_history"."id" <> :cursorId`
                : ``
            }`,
            {
              userId: user.id,
              ...(userHistoryAtCursor
                ? {
                    cursorUpdatedAt: userHistoryAtCursor.updatedAt,
                    cursorId: userHistoryAtCursor.id,
                  }
                : {}),
            }
          )
          .orderBy('"user_history"."updated_at"', 'DESC');

        const userHistories = await getManager()
          .createQueryBuilder()
          .select('"user_history".*')
          .from(`(${userHistoriesSubQb.getQuery()})`, 'user_history')
          .where('rownum <= :limit', { limit })
          .setParameters(userHistoriesSubQb.getParameters())
          .getRawMany()
          .then((raws) =>
            Promise.all(
              raws.map(async (raw: UserHistoriesSubQueryRow) => ({
                id: raw.user_history_id,
                fkUserId: raw.user_history_fk_user_id,
                fkBookId: raw.user_history_fk_book_id,
                createdAt: raw.user_history_created_at,
                updatedAt: raw.user_history_updated_at,
                sequence: raw.user_history_sequence,
                chapterTitle: raw.user_history_chapter_title,
                mediaUri: raw.user_history_media_uri,
                position: raw.user_history_position,
                book: await loaders.book.load(raw.user_history_fk_book_id),
              }))
            )
          );

        return {
          items: userHistories,
          cursor:
            userHistories.length > 0
              ? userHistories[userHistories.length - 1].id
              : null,
        };
      };

      return { id, list };
    },
  },
  Mutation: {
    History: (
      parent: unknown,
      args: unknown,
      { user, getConnection }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      const update = async ({
        bookId,
        input,
      }: {
        bookId: string;
        input: {
          sequence?: number;
          chapterTitle?: string;
          mediaUri: string;
          position: number;
        };
      }) => {
        await getConnection();

        const repo = getRepository(UserHistory);

        const userHistory =
          (await repo
            .find({ where: { fkUserId: user.id, fkBookId: bookId } })
            .then((result) => result[0])) || new UserHistory();

        userHistory.fkUserId = user.id;
        userHistory.fkBookId = bookId;
        userHistory.sequence = input.sequence;
        userHistory.chapterTitle = input.chapterTitle;
        userHistory.mediaUri = input.mediaUri;
        userHistory.position = input.position;

        await repo.save(userHistory);

        return repo.findByIds([userHistory.id]).then((rows) => rows[0]);
      };

      const remove = async ({ bookId }: { bookId: string }) => {
        await getConnection();

        const repo = getRepository(UserHistory);

        const userHistory = await repo
          .find({ where: { fkUserId: user.id, fkBookId: bookId } })
          .then((rows) => rows[0]);

        if (!userHistory) {
          return null;
        }

        const { id } = userHistory;

        await repo.createQueryBuilder().delete().where({ id }).execute();

        return id;
      };

      return { update, remove };
    },
  },
};
