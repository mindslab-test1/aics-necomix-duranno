import {
  ApolloError,
  AuthenticationError,
  gql,
  IResolvers,
} from 'apollo-server-koa';
import { getManager, getRepository } from 'typeorm';
import PlaylistItem from '../entities/playlistItem';
import { GraphqlContext } from '../types/graphql';

export const typeDef = gql`
  type PlaylistItem {
    id: ID!
    book: Book!
    createdAt: Date!
  }

  type PlaylistItems {
    items: [PlaylistItem]!
    cursor: ID
  }

  type Playlist {
    id: ID!
    count: Int!
    list(cursor: ID, limit: Int): PlaylistItems!
  }

  type PlaylistOps {
    add(bookId: ID!): PlaylistItem
    remove(bookId: ID!): ID
  }

  extend type Query {
    Playlist: Playlist!
  }

  extend type Mutation {
    Playlist: PlaylistOps!
  }
`;

export const resolvers: IResolvers = {
  Query: {
    Playlist: (
      parent: unknown,
      args: unknown,
      { user, getConnection, loaders }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      const id = () => user.id;

      const count = async () => {
        await getConnection();

        return getRepository(PlaylistItem)
          .createQueryBuilder('playlist_item')
          .leftJoinAndSelect('playlist_item.book', 'book')
          .where(`"book"."deleted" = 'N'`)
          .andWhere(`"playlist_item"."fk_user_id" = :userId`, {
            userId: user.id,
          })
          .getCount();
      };

      const list = async ({
        cursor,
        limit = 20,
      }: {
        cursor?: string;
        limit?: number;
      }) => {
        if (limit > 64) {
          throw new ApolloError('Max limit is 64', 'BAD_REQUEST');
        }
        if (!user || !user.emailVerified) {
          throw new AuthenticationError('User is not signed');
        }

        await getConnection();

        const playlistItemAtCursor = cursor
          ? await getRepository(PlaylistItem)
              .findByIds([cursor])
              .then((items) => items[0])
          : null;

        type PlaylistItemsSubQueryRow = {
          playlist_item_id: string;
          playlist_item_fk_user_id: string;
          playlist_item_fk_book_id: string;
          playlist_item_created_at: Date;
        };

        const playlistItemsSubQb = getManager()
          .createQueryBuilder(PlaylistItem, 'playlist_item')
          .leftJoinAndSelect('playlist_item.book', 'book')
          .where(
            `"book"."deleted" = 'N' AND "playlist_item"."fk_user_id" = :userId ${
              playlistItemAtCursor
                ? `AND "playlist_item"."created_at" < :cursorCreatedAt AND "playlist_item"."id" <> :cursorId`
                : ``
            }`,
            {
              userId: user.id,
              ...(playlistItemAtCursor
                ? {
                    cursorCreatedAt: playlistItemAtCursor.createdAt,
                    cursorId: playlistItemAtCursor.id,
                  }
                : {}),
            }
          )
          .orderBy('"playlist_item"."created_at"', 'DESC');

        const playlistItems = await getManager()
          .createQueryBuilder()
          .select('"playlist_item".*')
          .from(`(${playlistItemsSubQb.getQuery()})`, 'playlist_item')
          .where('rownum <= :limit', { limit })
          .setParameters(playlistItemsSubQb.getParameters())
          .getRawMany()
          .then((raws) =>
            Promise.all(
              raws.map(async (raw: PlaylistItemsSubQueryRow) => ({
                id: raw.playlist_item_id,
                fkUserId: raw.playlist_item_fk_user_id,
                fkBookId: raw.playlist_item_fk_book_id,
                createdAt: raw.playlist_item_created_at,
                book: await loaders.book.load(raw.playlist_item_fk_book_id),
              }))
            )
          );

        return {
          items: playlistItems,
          cursor:
            playlistItems.length > 0
              ? playlistItems[playlistItems.length - 1].id
              : null,
        };
      };

      return { id, count, list };
    },
  },
  Mutation: {
    Playlist: (
      parent: unknown,
      args: unknown,
      { user, loaders, getConnection }: GraphqlContext
    ) => {
      if (!user || !user.emailVerified) {
        throw new AuthenticationError('User is not signed');
      }

      const add = async ({ bookId }: { bookId: string }) => {
        const playlist = await loaders.userPlaylist.load(user.id);
        if (playlist.some((item) => item.fkBookId === bookId)) {
          return null;
        }

        const repo = getRepository(PlaylistItem);

        const { id } = await repo
          .createQueryBuilder()
          .insert()
          .values({ fkBookId: bookId, fkUserId: user.id })
          .returning(['id'])
          .execute()
          .then((result) => result.raw as { id: string });

        return repo.findByIds([id]).then((rows) => rows[0]);
      };

      const remove = async ({ bookId }: { bookId: string }) => {
        await getConnection();

        const repo = getRepository(PlaylistItem);

        const item = await repo
          .find({ where: { fkBookId: bookId, fkUserId: user.id } })
          .then((rows) => rows[0]);

        if (!item) {
          return null;
        }

        const { id } = item;

        await repo.createQueryBuilder().delete().where({ id }).execute();

        return id;
      };

      return { add, remove };
    },
  },
};
