import { gql, makeExecutableSchema, IResolvers } from 'apollo-server-koa';
import { mergeResolvers } from '@graphql-tools/merge';
import { GraphQLScalarType, Kind } from 'graphql';
import { GraphQLUpload } from 'graphql-upload';
import GraphQLJSON from 'graphql-type-json';
import * as author from './author';
import * as book from './book';
import * as comment from './comment';
import * as feedback from './feedback';
import * as homeBanner from './homeBanner';
import * as media from './media';
import * as playlist from './playlist';
import * as post from './post';
import * as subscription from './subscription';
import * as tag from './tag';
import * as user from './user';
import * as userHistory from './userHistory';

const typeDef = gql`
  scalar Date
  scalar JSON
  scalar Upload

  type Query {
    _version: String
  }

  type Mutation {
    _placeholder: String
  }
`;

const resolvers: IResolvers = {
  JSON: GraphQLJSON,
  Upload: GraphQLUpload,
  Date: new GraphQLScalarType({
    name: 'Date',
    parseValue: (value) => new Date(Date.parse(value)),
    serialize: (value: Date) => value.toISOString(),
    parseLiteral: (ast) => {
      if (ast.kind === Kind.STRING) {
        return new Date(Date.parse(ast.value));
      }
      return null;
    },
  }),
  Query: {
    _version: () => '0.1.1',
  },
  Mutation: {},
};

const schema = makeExecutableSchema({
  typeDefs: [
    typeDef,
    author.typeDef,
    book.typeDef,
    comment.typeDef,
    feedback.typeDef,
    homeBanner.typeDef,
    media.typeDef,
    playlist.typeDef,
    post.typeDef,
    subscription.typeDef,
    tag.typeDef,
    user.typeDef,
    userHistory.typeDef,
  ],
  resolvers: mergeResolvers([
    resolvers,
    author.resolvers,
    book.resolvers,
    comment.resolvers,
    feedback.resolvers,
    homeBanner.resolvers,
    media.resolvers,
    playlist.resolvers,
    post.resolvers,
    subscription.resolvers,
    tag.resolvers,
    user.resolvers,
    userHistory.resolvers,
  ]),
});

export default schema;
