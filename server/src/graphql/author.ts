import {
  ApolloError,
  AuthenticationError,
  gql,
  IResolvers,
} from 'apollo-server-koa';
import { getRepository } from 'typeorm';
import Author from '../entities/author';
import { getResourceFileUploader } from '../services';
import { AUTHOR_THUMBNAIL_DIR } from '../services/const';
import { GraphqlContext, GraphqlFile } from '../types/graphql';

export const typeDef = gql`
  type Author {
    id: ID!
    name: String
    profileImageUri: String
    description: String
  }

  input AuthorInput {
    name: String!
    profileImageUri: String
    profileImage: Upload
    description: String
  }

  extend type Query {
    author(authorId: String!): Author
    # admin
    authorList: [Author]!
  }

  extend type Mutation {
    # admin
    createAuthor(input: AuthorInput!): Author!
    editAuthor(authorId: ID!, input: AuthorInput!): Author
    deleteAuthor(authorId: ID!): Boolean!
  }
`;

type AuthorInput = {
  name: string;
  profileImageUri?: string;
  profileImage?: Promise<GraphqlFile>;
  description?: string;
};

type AuthorQueryArgs = {
  authorId: string;
};

type CreateAuthorMutationArgs = {
  input: AuthorInput;
};

type EditAuthorMutationArgs = {
  authorId: string;
  input: AuthorInput;
};

type DeleteAuthorMutationArgs = {
  authorId: string;
};

export const resolvers: IResolvers = {
  Query: {
    author: (
      parent: unknown,
      { authorId }: AuthorQueryArgs,
      { loaders }: GraphqlContext
    ) => {
      return loaders.author.load(authorId);
    },
    authorList: async (
      parent: unknown,
      args: unknown,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      await getConnection();

      logger.info(`User ${user.id} issued authorList query`);

      return getRepository(Author).find();
    },
  },
  Mutation: {
    createAuthor: async (
      parent: unknown,
      { input }: CreateAuthorMutationArgs,
      { admin, user, getConnection, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      if (input.profileImage && input.profileImageUri) {
        throw new ApolloError(
          'Cannot provide both profileImage and profileImageUri',
          'BAD_REQUEST'
        );
      }

      await getConnection();

      logger.info(
        `User ${
          user.id
        } issued createAuthor mutation with arguments ${JSON.stringify({
          input,
        })}`
      );

      const repo = getRepository(Author);

      const author = new Author();
      author.name = input.name;
      author.profileImageUri = input.profileImageUri;
      author.description = input.description;

      await repo.save(author);

      if (input.profileImage) {
        const { uri } = await getResourceFileUploader({
          logger,
        }).singleFileUploadResolver(input.profileImage, {
          directory: AUTHOR_THUMBNAIL_DIR,
          name: author.id,
        });

        author.profileImageUri = uri;
      }

      await repo.save(author);

      return author;
    },
    editAuthor: async (
      parent: unknown,
      { authorId, input }: EditAuthorMutationArgs,
      { admin, user, loaders, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      if (input.profileImage && input.profileImageUri) {
        throw new ApolloError(
          'Cannot provide both profileImage and profileImageUri',
          'BAD_REQUEST'
        );
      }

      logger.info(
        `User ${
          user.id
        } issued editAuthor mutation with arguments ${JSON.stringify({
          authorId,
          input,
        })}`
      );

      const repo = getRepository(Author);

      const author = await loaders.author.load(authorId);

      if (!author) {
        return null;
      }

      author.name = input.name;
      author.profileImageUri = input.profileImageUri;
      author.description = input.description;

      if (input.profileImage) {
        const { uri } = await getResourceFileUploader({
          logger,
        }).singleFileUploadResolver(input.profileImage, {
          directory: AUTHOR_THUMBNAIL_DIR,
          name: author.id,
        });

        author.profileImageUri = uri;
      }

      await repo.save(author);

      return author;
    },
    deleteAuthor: async (
      parent: unknown,
      { authorId }: DeleteAuthorMutationArgs,
      { admin, user, loaders, logger }: GraphqlContext
    ) => {
      if (!admin || !user) {
        throw new AuthenticationError('Requires admin privilege');
      }

      logger.info(
        `User ${
          user.id
        } issued deleteAuthor mutation with arguments ${JSON.stringify({
          authorId,
        })}`
      );

      const repo = getRepository(Author);

      const author = await loaders.author.load(authorId);

      if (!author) {
        return false;
      }

      await repo.remove(author);

      return true;
    },
  },
};
