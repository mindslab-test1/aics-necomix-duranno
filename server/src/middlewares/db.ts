import { Middleware } from 'koa';
import { Connection } from 'typeorm';
import Database from '../db';
import { ContextState } from '../types/koa';

const dbMiddleware = (): Middleware<ContextState> => async (ctx, next) => {
  const { APP_STAGE } = process.env;
  const isLambdaEnvironment =
    APP_STAGE === 'beta' || APP_STAGE === 'production';

  const database = new Database();

  let connection: Connection | null = null;
  ctx.state.getConnection = async () => {
    if (!connection) {
      connection = await database.getConnection();
    }
    return connection;
  };

  await next();

  if (isLambdaEnvironment && connection) {
    await database.closeConnection();
  }
};

export default dbMiddleware;
