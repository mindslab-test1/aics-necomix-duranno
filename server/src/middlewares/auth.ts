import { Middleware } from 'koa';
import admin from 'firebase-admin';
import { ContextState } from '../types/koa';
import { getSsmVariable } from '../config';

const authorizationHeaderRegex = /Bearer ([a-zA-Z0-9\-_.]+)/;

const authMiddleware = (): Middleware<ContextState> => {
  let firebaseAdminInitialized = false;

  return async (ctx, next) => {
    const { FIREBASE_PROJECT_NAME, APP_STAGE, ADMIN_UIDS } = process.env;

    if (!firebaseAdminInitialized) {
      firebaseAdminInitialized = true;

      if (FIREBASE_PROJECT_NAME) {
        if (APP_STAGE !== 'local') {
          process.env.GOOGLE_APPLICATION_CREDENTIALS = `${
            process.env.LAMBDA_TASK_ROOT || ''
          }/${process.env.GOOGLE_APPLICATION_CREDENTIALS || ''}`;
        }

        admin.initializeApp({
          credential: admin.credential.applicationDefault(),
          databaseURL: `https://${FIREBASE_PROJECT_NAME}.firebaseio.com`,
        });
        ctx.state.logger.debug('initialized firebase admin project.');
      } else {
        ctx.state.logger.warning(
          'cannot initialize firebase admin project: FIREBASE_PROJECT_NAME environment variable is not set.'
        );
      }
    }

    if (APP_STAGE === 'local') {
      const debugUserId = ctx.get('x-debug-user-id');
      const debugUserEmail = ctx.get('x-debug-user-email');
      if (debugUserId) {
        ctx.state.user = {
          id: debugUserId,
          email: debugUserEmail,
          emailVerified: true,
        };
      }

      const debugAdmin = ctx.get('x-debug-admin');
      if (debugAdmin) {
        ctx.state.admin = true;
      }
    }

    const idToken = (authorizationHeaderRegex.exec(ctx.get('Authorization')) ||
      [])[1];

    if (idToken) {
      try {
        const {
          uid: id,
          email,
          email_verified: emailVerified,
        } = await admin.auth().verifyIdToken(idToken);

        ctx.state.user = { id, email, emailVerified };

        const adminUidsRaw =
          APP_STAGE === 'local'
            ? ADMIN_UIDS
            : await getSsmVariable('ADMIN_UIDS');

        const adminUids = (adminUidsRaw || '').split(',');

        if (adminUids.some((adminUid) => adminUid === id)) {
          ctx.state.admin = true;
        }
      } catch (e) {
        // pass
      }
    }

    await next();
  };
};

export default authMiddleware;
