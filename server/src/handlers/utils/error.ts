export class UnauthorizedError extends Error {
  static type = 'UNAUTHORIZED';
}
