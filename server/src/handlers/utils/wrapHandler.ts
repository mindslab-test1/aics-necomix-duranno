/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Handler } from 'aws-lambda';
import { Connection } from 'typeorm';
import { getSsmVariable } from '../../config';
import Database from '../../db';
import { Logger, parseLogLevel } from '../../utils/logger';
import { Context, LambdaHandler } from '../types';
import { UnauthorizedError } from './error';

const wrapHandler = (handler: LambdaHandler): Handler => (event: any) => {
  let connection: Connection | null = null;

  const database = new Database();

  const { LOG_LEVEL } = process.env;
  const level = parseLogLevel(LOG_LEVEL || 'debug');

  const context: Context = {
    getConnection: async () => {
      if (!connection) {
        connection = await database.getConnection();
      }
      return connection;
    },
    logger: new Logger(level),
    status: 501,
    body: null,
  };

  const customInvokationAuthorizer = () =>
    new Promise<void>((resolve, reject) => {
      /* authorize requests for custom invokation */
      if (event['detail-type'] !== 'Scheduled Event') {
        getSsmVariable('CUSTOM_INVOKATION_KEY')
          .then((customInvokationKey) => {
            if (event.headers['x-api-key'] !== customInvokationKey) {
              reject(new UnauthorizedError('api key mismatch'));
            } else {
              resolve();
            }
          })
          .catch((err) => {
            reject(Error(err));
          });
      } else {
        resolve();
      }
    });

  const promise = async () => {
    await handler(context);
  };

  return new Promise((resolve, reject) => {
    customInvokationAuthorizer()
      .then(promise)
      .then(() => {
        resolve({
          statusCode: context.status,
          body:
            typeof context.body === 'string'
              ? context.body
              : JSON.stringify(context.body, null, 2),
        });
      })
      .catch((err) => {
        if (err instanceof UnauthorizedError) {
          resolve({
            statusCode: 401,
            body: err.message,
          });
          return;
        }

        context.logger.error(
          typeof err === 'string' ? err : JSON.stringify(err, null, 2)
        );
        reject(Error(err));
      });
  });
};

export default wrapHandler;
