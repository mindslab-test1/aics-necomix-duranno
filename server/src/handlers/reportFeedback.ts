import axios from 'axios';
import { getRepository } from 'typeorm';
import { getSsmVariable } from '../config';
import Feedback, { FEEDBACK_CSV_COLUMNS } from '../entities/feedback';
import { getCsvFileWriter, getEmailSender } from '../services';
import wrapHandler from './utils/wrapHandler';

export const handler = wrapHandler(async (ctx) => {
  ctx.logger.info('Reporting user feedback');

  await ctx.getConnection();

  const feedbacks = await getRepository(Feedback)
    .createQueryBuilder('feedback')
    .where(`"feedback"."updated_at" >= (CURRENT_TIMESTAMP - INTERVAL '1' DAY)`)
    .orderBy('"feedback"."updated_at"', 'ASC')
    .getMany();

  /* Send feedback via slack */
  const webhookUri = await getSsmVariable('SLACK_WEBHOOK_URI');

  if (!webhookUri) {
    throw Error('SLACK_WEBHOOK_URI not configured');
  }

  await axios.post(webhookUri, {
    text: JSON.stringify(feedbacks, null, 2),
  });

  /* Send feedback to email */
  const csvWriter = getCsvFileWriter({
    logger: ctx.logger,
    filename: 'feedback.csv',
  });

  await csvWriter.write(
    FEEDBACK_CSV_COLUMNS,
    feedbacks.map((feedback) => feedback.toLocalizedObject())
  );

  const today = new Date();
  const formattedDate = `${today.getFullYear()}-${(today.getMonth() + 1)
    .toString()
    .padStart(2, '0')}-${today.getDate().toString().padStart(2, '0')}`; // yyyy-mm-dd

  const sender = process.env.FEEDBACK_EMAIL_SENDER || '';
  const receipients = process.env.FEEDBACK_EMAIL_RECEIPIENTS?.split(',') || [];

  if (sender && receipients) {
    await getEmailSender({ logger: ctx.logger }).send({
      from: sender,
      to: receipients,
      subject: `[네오 오디오] ${today.toLocaleDateString()} 고객문의${
        process.env.APP_STAGE === 'beta' ? ' (BETA)' : ''
      }`,
      text: '',
      attachments: [
        {
          filename: `daily-feedback-${formattedDate}.csv`,
          path: csvWriter.getFilePath(),
        },
      ],
    });
  } else {
    if (!sender) {
      ctx.logger.warning(
        'FEEDBACK_EMAIL_SENDER environment variable not set. Cannot send feedback report via email.'
      );
    }

    if (!receipients) {
      ctx.logger.warning(
        'FEEDBACK_EMAIL_RECEIPIENTS environment variable not set. Cannot send feedback report via email.'
      );
    }
  }

  ctx.status = 200;
  ctx.body = { feedbacks };
});
