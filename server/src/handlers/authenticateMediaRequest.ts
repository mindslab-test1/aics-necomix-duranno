/* eslint-disable @typescript-eslint/restrict-template-expressions */
/* eslint-disable @typescript-eslint/no-unsafe-return */
/* eslint-disable @typescript-eslint/no-unsafe-assignment */
/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Handler } from 'aws-lambda';
import { TokenExpiredError } from 'jsonwebtoken';
import { parse } from 'querystring';
import { MediaTokenPayload } from '../types/token';
import { TokenSubject, verifyToken } from '../utils/token';

const APP_NAME_HEADER = 'x-app-name';
const APP_STAGE_HEADER = 'x-app-stage';
const MEDIA_AUTHORIZATION_HEADER = 'x-media-authorization';

const APP_NAME_PARAM = 'app_name';
const APP_STAGE_PARAM = 'app_stage';
const MEDIA_AUTHORIZATION_PARAM = 'token';

const unauthorizedReponse = {
  status: 401,
  statusDescription: 'Unauthorized',
  body: 'Unauthorized',
};

const forbiddenResponse = {
  status: 403,
  statusDescription: 'Forbidden',
  body: 'Forbidden',
};

const goneResponse = {
  status: 410,
  statusDescription: 'Gone',
  body: 'Token expired',
};

function getHeader(request: any, key: string): string | undefined {
  const { headers } = request;
  const headerValue = headers[key];

  if (!headerValue) {
    return undefined;
  }

  return headerValue[0]?.value;
}

export const handler: Handler = (event, context, callback) => {
  const { request } = event.Records[0].cf;
  const { uri, querystring } = request;
  const queryParams = parse(querystring);

  process.env.LOG_LEVEL = 'info';

  process.env.APP_NAME =
    (queryParams[APP_NAME_PARAM] as string) ||
    getHeader(request, APP_NAME_HEADER);

  process.env.APP_STAGE =
    (queryParams[APP_STAGE_PARAM] as string) ||
    getHeader(request, APP_STAGE_HEADER);

  const token: string | undefined =
    (queryParams[MEDIA_AUTHORIZATION_PARAM] as string) ||
    getHeader(request, MEDIA_AUTHORIZATION_HEADER);

  if (!token) {
    callback(null, {
      ...unauthorizedReponse,
      body: 'subscription token missing',
    });
    return;
  }

  verifyToken<MediaTokenPayload>(token)
    .then((decoded) => {
      const { resourceUri, sub } = decoded;

      if (sub !== TokenSubject.Media) {
        callback(null, {
          ...unauthorizedReponse,
          body: 'invalid subscription token: wrong subject',
        });
        return;
      }

      if (uri !== resourceUri) {
        callback(null, {
          ...forbiddenResponse,
          body: `the token is signed for resource ${resourceUri}, but the user requested for ${uri}.`,
        });
        return;
      }

      callback(null, request);
    })
    .catch((err) => {
      if (err instanceof TokenExpiredError) {
        callback(null, goneResponse);

        return;
      }

      callback(null, {
        ...unauthorizedReponse,
        body: `invalid subscription token: verification failed, err=${err.message}`,
      });
    });
};
