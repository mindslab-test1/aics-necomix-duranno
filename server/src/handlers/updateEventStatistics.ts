import { getManager, getRepository } from 'typeorm';
import BookStatistic from '../entities/bookStats';
import { EventNames } from '../entities/event';
import wrapHandler from './utils/wrapHandler';

export const handler = wrapHandler(async (ctx) => {
  ctx.logger.info('Updating event stats');

  await ctx.getConnection();

  type QueryResult = {
    fkBookId: string;
    weeklyViews: number;
  };

  const rows = (await getManager()
    .createQueryBuilder()
    .select(
      `"event"."fk_book_id" AS "fkBookId", COUNT("event"."id") AS "weeklyViews"`
    )
    .from('events', 'event')
    .where(`"event"."name" = '${EventNames.viewBook}'`)
    .andWhere(`"event"."updated_at" >= (CURRENT_TIMESTAMP - INTERVAL '7' DAY)`)
    .groupBy('"event"."fk_book_id"')
    .execute()) as QueryResult[];

  const repo = getRepository(BookStatistic);
  const stats = await repo.find();

  const updatedStats = await Promise.all(
    rows.map(async (row) => {
      const stat = stats.find((item) => item.fkBookId === row.fkBookId);

      if (stat) {
        stat.weeklyViews = row.weeklyViews;
        await repo.save(stat);

        return stat;
      }

      const newStat = new BookStatistic();
      newStat.fkBookId = row.fkBookId;
      newStat.weeklyViews = row.weeklyViews;
      await repo.save(newStat);

      return newStat;
    })
  );

  ctx.status = 200;
  ctx.body = { stats: updatedStats };
});
