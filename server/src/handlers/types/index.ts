import { Connection } from 'typeorm';
import { Logger } from '../../utils/logger';

export interface Context {
  getConnection(): Promise<Connection>;
  logger: Logger;
  status: number;
  body: unknown;
}

export type LambdaHandler = (context: Context) => void | Promise<void>;
