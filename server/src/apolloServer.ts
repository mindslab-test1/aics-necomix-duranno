import { ApolloServer, AuthenticationError } from 'apollo-server-koa';
import { graphqlUploadKoa } from 'graphql-upload';
import { ParameterizedContext } from 'koa';
import { v4 as uuidv4 } from 'uuid';
import { createLoaders } from './entities';
import schema from './graphql/schema';
import { GraphqlContext } from './types/graphql';
import { ContextState } from './types/koa';
import { gqlLogger } from './utils/logger';

export const graphqlUploadMiddleware = () =>
  graphqlUploadKoa({
    maxFileSize: 10 * 1024 * 1024 /* 10 MB */,
    maxFiles: 20,
  });

const apollo = (): ApolloServer => {
  const { APP_STAGE } = process.env;

  return new ApolloServer({
    schema,
    context: ({
      ctx,
    }: {
      ctx: ParameterizedContext<ContextState>;
    }): GraphqlContext => {
      const requestId: string = (uuidv4 as () => string)();
      const requestMetadata = {
        timestamp: Date.now(),
      };

      if (!ctx.state.user && !ctx.state.admin) {
        const err = new AuthenticationError('User is not signed');
        gqlLogger.onContextCreationFailed({ requestId }, err, ctx.state.logger);

        throw err;
      }

      return {
        requestId,
        requestMetadata,
        user: ctx.state.user || null,
        admin: ctx.state.admin,
        loaders: createLoaders(() => ctx.state.getConnection()),
        getConnection: () => ctx.state.getConnection(),
        logger: ctx.state.logger,
      };
    },
    playground: APP_STAGE === 'local',
    uploads: false,
    plugins: [gqlLogger.plugin],
  });
};

export default apollo;
